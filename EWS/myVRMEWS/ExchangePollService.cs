﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace myVRMEWS
{
    public partial class myVRMEWSService : ServiceBase
    {
        System.Timers.Timer pollConferences = new System.Timers.Timer();

        public myVRMEWSService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            double pollConfs = 30000;
            try
            {

                pollConferences.Elapsed += new System.Timers.ElapsedEventHandler(pollConferences_Elapsed);
                pollConferences.Interval = pollConfs;
                pollConferences.AutoReset = false;
                pollConferences.Start();

            }
            catch (Exception)
            {
                
            }
            
        }

        protected override void OnStop()
        {
            pollConferences.Enabled = false;
            pollConferences.AutoReset = false;
            pollConferences.Stop();
        }

        private void pollConferences_Elapsed(object sender, EventArgs e)
        {
            double pollConfs = 30000;
            try
            {
                pollConferences.Stop();

                ExcahngePollNms.ExcahngePoll exchangePollService = new ExcahngePollNms.ExcahngePoll();
                exchangePollService.start();

            }
            catch (Exception)
            {
            }

            pollConferences.Interval = pollConfs;
            pollConferences.AutoReset = false;
            pollConferences.Start();
            
        }
    }
}
