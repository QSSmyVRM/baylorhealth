<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_SuperAdministrator.SuperAdministrator" %>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%--Edited for FF--%><%--ZD 101837--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script runat="server"></script>
<script type="text/javascript" language="javascript" src='script/lib.js'></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script><%--FB 2659--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>

<script language="JavaScript">
<!--
    if (navigator.userAgent.indexOf('Trident') > -1) {
        document.onkeydown = function(evt) {
            fnOnKeyDown(evt);
        };
    }
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //FB Case 807 starts here
    function SavePassword()// FB 1896
    {

        if (document.getElementById("hdnMailServer").value != "") {

            document.getElementById("txtMSPassword1").value = document.getElementById("hdnMailServer").value;
            document.getElementById("txtMSPassword2").value = document.getElementById("hdnMailServer").value;
        }
        if (document.getElementById("hdnLDAPPassword").value != "") {
            document.getElementById("txtLDAPAccountPassword1").value = document.getElementById("hdnLDAPPassword").value;
            document.getElementById("txtLDAPAccountPassword2").value = document.getElementById("hdnLDAPPassword").value;
        }
        //FB 2363
        if (document.getElementById("hdnExternalPassword").value != "") {
            document.getElementById("txtP1Password").value = document.getElementById("hdnExternalPassword").value;
            document.getElementById("txtP2Password").value = document.getElementById("hdnExternalPassword").value;
        }
        // FB 2501 EM7 Starts
        if (document.getElementById("hdnEM7Password").value != "") {
            document.getElementById("txtEM7Password").value = document.getElementById("hdnEM7Password").value;
            document.getElementById("txtEM7ConformPassword").value = document.getElementById("hdnEM7Password").value;
        }
        // FB 2501 EM7 Ends
    }
	//FB 2659
    function PopupWindow() {
        var popwin = window.showModalDialog("DefaultLicense.aspx", document, "resizable: yes; scrollbar: no; help: yes; status:yes; dialogHeight:580px; dialogWidth:510px");
    }


    function PreservePassword()// FB 1896
    {
        document.getElementById("hdnMailServer").value = document.getElementById("txtMSPassword1").value
        document.getElementById("hdnLDAPPassword").value = document.getElementById("txtLDAPAccountPassword2").value;
        document.getElementById("hdnExternalPassword").value = document.getElementById("txtP2Password").value; //FB 2363
        document.getElementById("hdnEM7Password").value = document.getElementById("txtEM7Password").value; //FB 2501 EM7
        //ZD 100152 Starts
        document.getElementById("hdnGoogleClientID").value = document.getElementById("txtGoogleClientID").value;
        document.getElementById("hdnGoogleSecretID").value = document.getElementById("txtGoogleSecretID").value;
        document.getElementById("hdnGoogleApiKey").value = document.getElementById("txtGoogleAPIKey").value;
        //ZD 100152 Ends
        
    }
    //FB Case 807 ends here
    function TestLDAPServerConnection() {
        if (document.getElementById("txtLDAPAccountPassword1").value == "") {
            alert(SuperLDAPConn);
            document.getElementById("txtLDAPAccountPassword1").focus();
            return false;
        }

        if (document.getElementById("txtLDAPAccountLogin").value == "") {
            alert(SuperLDAPConn);
            document.getElementById("txtLDAPAccountLogin").focus();
            return false;
        }

        if (document.getElementById("txtLDAPServerPort").value == "") {
            alert(SuperLDAPConn);
            document.getElementById("txtLDAPServerPort").focus();
            return false;
        }
        if (document.getElementById("txtLDAPServerAddress").value == "") {
            alert(ReqAccEmail);
            document.getElementById("txtLDAPServerAddress").focus();
            return false;
        }
        if (document.getElementById("lstLDAPConnectionTimeout").value == "") {
            alert(ValidTimeout);
            document.getElementById("lstLDAPConnectionTimeout").focus();
            return false;
        }
        //	alert(document.frmMainsuperadministrator.LDAPConnectionTimeout.value);
        url = "dispatcher/admindispatcher.asp?cmd=TestLDAPConnection" + "&sadd=" + escape(document.getElementById("txtLDAPServerAddress").value) + "&port=" + document.getElementById("txtLDAPServerPort").value + "&lg=" + escape(document.getElementById("txtLDAPAccountLogin").value) + "&pd=" + escape(document.getElementById("txtLDAPAccountPassword1").value) + "&cto=" + document.getElementById("lstLDAPConnectionTimeout").value;

        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }


    function TestMailServerConnection() {
        if (document.getElementById("txtMailServerLogin").value == "") {
            alert(MailserverLogin);
            document.getElementById("txtMailServerLogin").focus();
            return false;
        }

        if (document.getElementById("txtMSPassword1").value == "") {
            alert(MailserverPassword);
            document.getElementById("txtMSPassword1").focus();
            return false;
        }

        if (document.getElementById("txtServerAddress").value == "") {
            alert(MailserverAddress);
            document.getElementById("txtServerAddress").focus();
            return false;
        }
        url = "dispatcher/admindispatcher.asp?cmd=TestMailConnection" + "&sa=" + escape(document.getElementById("txtServerAddress").value) + "&lg=" + escape(document.getElementById("txtMailServerLogin").value) + "&sp=" + document.getElementById("txtServerPort").value + "&se=" + escape(document.getElementById("txtSystemEmail").value) + "&dn=" + escape(document.getElementById("txtDisplayName").value) + "&pd=" + escape(document.getElementById("txtMSPassword1").value);
        //alert(url);
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }


    function getYourOwnEmailList(i) {
        url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }

    function frmMainsuperadministrator_Validator() {
        if (Trim(document.getElementById("txtSystemEmail").value) == "") {
            alert(EN_81);
            document.getElementById("txtSystemEmail").focus();
            return (false);
        }

        if (!checkemail(document.getElementById("txtSystemEmail").value)) {
            alert(EN_81);
            document.getElementById("txtSystemEmail").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtwebsiteURL").value) == "") {
            alert(WebsiteURL);
            document.getElementById("txtwebsiteURL").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtServerAddress").value) == "") {
            alert(EN_82);
            document.getElementById("txtServerAddress").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtServerPort").value) == "") {
            alert(EN_83);
            document.getElementById("txtServerPort").focus();
            return (false);
        }

        if (isPositiveInt(document.getElementById("txtServerPort").value, "Port No") != 1) {
            document.getElementById("txtServerPort").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtLDAPServerPort").value) != "") {
            if (isPositiveInt(document.getElementById("txtLDAPServerPort").value, "LDAP Port No") != 1) {
                document.getElementById("txtLDAPServerPort").focus();
                return (false);
            }

            pn = parseInt(document.getElementById("txtLDAPServerPort").value, 10);
            if (pn != 389) {
                var isChangeLDAPPortNo = confirm(LDAPPort)
                if (isChangeLDAPPortNo == false) {
                    document.getElementById("txtLDAPServerPort").focus()
                    return (false);
                }
            }
        }
        return (true);
    }
    //Code addded for organisation
    function OpenOrg() {
        //window.location.replace("ManageOrganization.aspx"); //FB 2565
        DataLoading(1); // ZD 100176 
        window.location = 'ManageOrganization.aspx';
        return false; //ZD 100420
    }

    //Code added for displaying User login FB 1969
    function Openuser() {
        //window.location.replace("UserHistoryReport.aspx"); //FB 2565
        DataLoading(1); // ZD 100176  
        window.location='UserHistoryReport.aspx';
		return false; //ZD 100420
    }
    //ZD 100902
    function OpenReservation() {
        DataLoading(1);
        window.location = 'ReservationHistory.aspx';
        return false; 
    }

    /*Commented for FB 1849
    function fnChangeOrganization()
    {
    var btnchng = document.getElementById("BtnChangeOrganization");
    var drporg = document.getElementById("DrpOrganization");
    var cnfrm = confirm("Hereafter all the transactions performed in system will be for the selected organization.Do you wish to continue");
            
    if(cnfrm)
    return true;
    else
    return false;
   
    }*/
 
//-->
    //FB 2594 Starts
    function SetButtonStatus(sender, target) {

        if (sender.value.length > 0)

            document.getElementById(target).disabled = false;

        else

            document.getElementById(target).disabled = true;

    }
    //    function PollWhygo() {
    //        if ((document.getElementById("txtWhyGoURL").value == "") || (document.getElementById("txtWhygoUsr").value == "") || document.getElementById("trWhygoPassword").value == "") ){
    //            alert("Please enter the Whygo Integration Settings.");
    //            document.getElementById("txtWhyGoURL").focus();
    //            return false;
    //        } 
    //    }
    //FB 2594 Ends
    //FB 3054 Starts
    function SMTPTC() {
        document.getElementById("btnTestMailConnection").disabled = "true";
        document.getElementById("btnTestMailConnection").className = "btndisable";
    }
    function LDAPTC() {
        document.getElementById("btnTestLDAPConnection").disabled = "true";
        document.getElementById("btnTestLDAPConnection").className = "btndisable";
    }
    function PasswordChange(par,par1) {
        if (par == 1) {

            document.getElementById("hdnSMTPPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnSMTP1Visit").value = true;
            else
                document.getElementById("hdnSMTP2Visit").value = true;
        }
        else if (par == 2) {

            document.getElementById("hdnLDAPPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnLDAP1Visit").value = true;
            else
                document.getElementById("hdnLDAP2Visit").value = true;
        }
        else if (par == 3) {

            document.getElementById("hdnEM7PW").value = true;
            if (par1 == 1)
                document.getElementById("hdnEM71Visit").value = true;
            else
                document.getElementById("hdnEM72Visit").value = true;
        }
        else if (par == 4) {

            document.getElementById("hdnExternalPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnExt1Visit").value = true;
            else
                document.getElementById("hdnExt2Visit").value = true;
        }
        else if (par == 5) {

            document.getElementById("hdnWhygoPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnWhygo1Visit").value = true;
            else
                document.getElementById("hdnWhygo2Visit").value = true;
        }
        //ZD 100152 Starts
        else if (par == 6) {

            document.getElementById("hdnGoogleClientID1").value = true;
            if (par1 == 1)
                document.getElementById("hdnGoogleClientIDVisit1").value = true;

        }
        else if (par == 7) {

            document.getElementById("hdnGoogleSecretID1").value = true;
            if (par1 == 1)
                document.getElementById("hdnGoogleSecretIDVisit1").value = true;

        }
        else if (par == 8) {

            document.getElementById("hdnGoogleApiKey1").value = true;
            if (par1 == 1)
                document.getElementById("hdnGoogleApiKeyVisit1").value = true;

        }
        //ZD 100152 Ends
    }
    function fnTextFocus(xid, par, par2) {

        var obj = document.getElementById(xid);
        if (par2 == 1) {
            // ZD 100263 Starts
            var obj1 = document.getElementById("txtMSPassword1");
            var obj2 = document.getElementById("txtMSPassword2");
            if (obj1.value == "" && obj2.value == "") {
                document.getElementById("txtMSPassword1").style.backgroundImage = "";
                document.getElementById("txtMSPassword2").style.backgroundImage = "";
                document.getElementById("txtMSPassword1").value = "";
                document.getElementById("txtMSPassword2").value = "";
            }
            return false;
            // ZD 100263 Ends
        
            if (par == 1) {
                if (document.getElementById("hdnSMTP2Visit") != null) {
                    if (document.getElementById("hdnSMTP2Visit").value == "false") {
                        document.getElementById("txtMSPassword1").value = "";
                        document.getElementById("txtMSPassword2").value = "";
                    } else {
                        document.getElementById("txtMSPassword1").value = "";
                    }
                }
                else {
                    document.getElementById("txtMSPassword1").value = "";
                    document.getElementById("txtMSPassword2").value = "";
                }
            } else {
                if (document.getElementById("hdnSMTP1Visit") != null) {
                    if (document.getElementById("hdnSMTP1Visit").value == "false") {
                        document.getElementById("txtMSPassword1").value = "";
                        document.getElementById("txtMSPassword2").value = "";
                    } else {
                        document.getElementById("txtMSPassword2").value = "";
                    }
                }
                else {
                    document.getElementById("txtMSPassword1").value = "";
                    document.getElementById("txtMSPassword2").value = "";
                }
            }
        }
        else if (par2 == 2) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtLDAPAccountPassword1");
        var obj2 = document.getElementById("txtLDAPAccountPassword2");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtLDAPAccountPassword1").style.backgroundImage = "";
            document.getElementById("txtLDAPAccountPassword2").style.backgroundImage = "";
            document.getElementById("txtLDAPAccountPassword1").value = "";
            document.getElementById("txtLDAPAccountPassword2").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnLDAP2Visit") != null) {
                    if (document.getElementById("hdnLDAP2Visit").value == "false") {
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                    } else {
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                    }
                }
                else {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                    document.getElementById("txtLDAPAccountPassword2").value = "";
                }
            } else {
                if (document.getElementById("hdnLDAP1Visit") != null) {
                    if (document.getElementById("hdnLDAP1Visit").value == "false") {
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                        document.getElementById("txtLDAPAccountPassword2").value = "";
                    } else {
                        document.getElementById("txtLDAPAccountPassword2").value = "";
                    }
                }
                else {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                    document.getElementById("txtLDAPAccountPassword2").value = "";
                }
            }

        }
        else if (par2 == 3) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtEM7Password");
        var obj2 = document.getElementById("txtEM7ConformPassword");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtEM7Password").style.backgroundImage = "";
            document.getElementById("txtEM7ConformPassword").style.backgroundImage = "";
            document.getElementById("txtEM7Password").value = "";
            document.getElementById("txtEM7ConformPassword").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnEM72Visit") != null) {
                    if (document.getElementById("hdnEM72Visit").value == "false") {
                        document.getElementById("txtEM7Password").value = "";
                        document.getElementById("txtEM7ConformPassword").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    } else {
                        document.getElementById("txtEM7Password").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    }
                }
                else {
                    document.getElementById("txtEM7Password").value = "";
                    document.getElementById("txtEM7ConformPassword").value = "";
                    document.getElementById("hdnEM7Password").value = "";
                }
            } else {
                if (document.getElementById("hdnEM71Visit") != null) {
                    if (document.getElementById("hdnEM71Visit").value == "false") {
                        document.getElementById("txtEM7Password").value = "";
                        document.getElementById("txtEM7ConformPassword").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    } else {
                        document.getElementById("txtEM7ConformPassword").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    }
                }
                else {
                    document.getElementById("txtEM7Password").value = "";
                    document.getElementById("txtEM7ConformPassword").value = "";
                    document.getElementById("hdnEM7Password").value = "";
                }
            }
        }
        else if (par2 == 4) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtP1Password");
        var obj2 = document.getElementById("txtP2Password");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtP1Password").style.backgroundImage = "";
            document.getElementById("txtP2Password").style.backgroundImage = "";
            document.getElementById("txtP1Password").value = "";
            document.getElementById("txtP2Password").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnExt2Visit") != null) {
                    if (document.getElementById("hdnExt2Visit").value == "false") {
                        document.getElementById("txtP1Password").value = "";
                        document.getElementById("txtP2Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    } else {
                        document.getElementById("txtP1Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    }
                }
                else {
                    document.getElementById("txtP1Password").value = "";
                    document.getElementById("txtP2Password").value = "";
                    document.getElementById("hdnExternalPassword").value = "";
                }
            } else {
                if (document.getElementById("hdnExt1Visit") != null) {
                    if (document.getElementById("hdnExt1Visit").value == "false") {
                        document.getElementById("txtP1Password").value = "";
                        document.getElementById("txtP2Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    } else {
                        document.getElementById("txtP2Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    }
                }
                else {
                    document.getElementById("txtP1Password").value = "";
                    document.getElementById("txtP2Password").value = "";
                    document.getElementById("hdnExternalPassword").value = "";
                }
            }
        } else if (par2 == 5) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtWhygoPwd");
        var obj2 = document.getElementById("txtWhygoPwd2");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtWhygoPwd").style.backgroundImage = "";
            document.getElementById("txtWhygoPwd2").style.backgroundImage = "";
            document.getElementById("txtWhygoPwd").value = "";
            document.getElementById("txtWhygoPwd2").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnWhygo2Visit") != null) {
                    if (document.getElementById("hdnWhygo2Visit").value == "false") {
                        document.getElementById("txtWhygoPwd").value = "";
                        document.getElementById("txtWhygoPwd2").value = "";
                    } else {
                        document.getElementById("txtWhygoPwd").value = "";
                    }
                }
                else {
                    document.getElementById("txtWhygoPwd").value = "";
                    document.getElementById("txtWhygoPwd2").value = "";
                }
            } else {
                if (document.getElementById("hdnWhygo1Visit") != null) {
                    if (document.getElementById("hdnWhygo1Visit").value == "false") {
                        document.getElementById("txtWhygoPwd").value = "";
                        document.getElementById("txtWhygoPwd2").value = "";
                    } else {
                        document.getElementById("txtWhygoPwd2").value = "";
                    }
                }
                else {
                    document.getElementById("txtWhygoPwd").value = "";
                    document.getElementById("txtWhygoPwd2").value = "";
                }
            }
        }
        //ZD 100152 Starts
        else if (par2 == 6) {
            var obj1 = document.getElementById("txtGoogleClientID");
            if (obj1.value == "") {
                document.getElementById("txtGoogleClientID").style.backgroundImage = "";
                document.getElementById("txtGoogleClientID").value = "";

            }
            return false;
        }
        else if (par2 == 7) {
            var obj1 = document.getElementById("txtGoogleSecretID");
            if (obj1.value == "") {
                document.getElementById("txtGoogleSecretID").style.backgroundImage = "";
                document.getElementById("txtGoogleSecretID").value = "";

            }
            return false;
        }
        else if (par2 == 8) {
            var obj1 = document.getElementById("hdnGoogleApiKey");
            if (obj1.value == "") {
                document.getElementById("txtGoogleAPIKey").style.backgroundImage = "";
                document.getElementById("txtGoogleAPIKey").value = "";

            }
            return false;
        }
        //ZD 100152 Ends

        if (document.getElementById("cmpValPassword1") != null) {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
    }
    //FB 3054 Ends

    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block';//ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none';//ZD 100678
    }
    //ZD 100176 End

    //ZD 100526
    function fnCheckConfID() {
        var txtStartConfID = document.getElementById("txtStartConfID");
        var hdnConfIDStartValue = document.getElementById("hdnConfIDStartValue");
        var cmpValConfStartVal = document.getElementById('cmpValConfStartVal');
        cmpValConfStartVal.innerHTML = "<asp:Literal Text='<%$ Resources:WebResources, NumericValuesonly3%>' runat='server'></asp:Literal>";
        
        if(txtStartConfID.value != '')
        {
            if (txtStartConfID.value.search(/[\d]+/) == -1)
            {   
                cmpValConfStartVal.style.display = 'block';
                cmpValConfStartVal.innerHTML = "<asp:Literal Text='<%$ Resources:WebResources, NumericValuesonly3%>' runat='server'></asp:Literal>";
                txtStartConfID.focus();
                return false;
            }
        }

        if (hdnConfIDStartValue.value != "") {
            if (txtStartConfID.value == "") {
                cmpValConfStartVal.style.display = 'block';
                cmpValConfStartVal.innerHTML = "<br>Please enter Conferece ID.";
                txtStartConfID.focus();
                return false;
            }
            else if (eval(txtStartConfID.value) < eval(hdnConfIDStartValue.value)) {
                cmpValConfStartVal.style.display = 'block';
                cmpValConfStartVal.innerHTML = "<br>Conference ID should be greater than the actual Conference ID.";
                txtStartConfID.focus();
                return false;
            }
        }
        else {
            if (txtStartConfID.value == "0") {
                cmpValConfStartVal.style.display = 'block';
                cmpValConfStartVal.innerHTML = "<br>Conference ID must be greater than 0.";
                txtStartConfID.focus();
                return false;
            }
        }

        return true;
    }
    <%--ZD 101443 Starts--%>
    function fnTenantOption() {
        if (document.getElementById("chkDomain").checked == true) {
            document.getElementById("drpTenantOption").style.display = "";
            document.getElementById("imgTenantOption").style.display = "";
            if(document.getElementById("drpTenantOption").value =="-1")
                ValidatorEnable(document.getElementById("reqTenantOption"), true);
            else
                ValidatorEnable(document.getElementById("reqTenantOption"), false);
        }
        else {
            document.getElementById("drpTenantOption").style.display = "none";
            document.getElementById("imgTenantOption").style.display = "none";
            document.getElementById("drpTenantOption").value = "-1"
            ValidatorEnable(document.getElementById("reqTenantOption"), false);
            if(document.getElementById("chkSSOMode") != null) document.getElementById("chkSSOMode").disabled = false;
        }
    }
    function fnTenantOptionchange()
    {
      if(document.getElementById("drpTenantOption").value =="2"){
          document.getElementById("chkSSOMode").checked =true;
          document.getElementById("chkSSOMode").disabled= true;
          }
      else{
          document.getElementById("chkSSOMode").checked =false;
          document.getElementById("chkSSOMode").disabled= false;
          }
      if (document.getElementById("chkDomain").checked == true &&  document.getElementById("drpTenantOption").value =="-1")
          ValidatorEnable(document.getElementById("reqTenantOption"), true);
      else
          ValidatorEnable(document.getElementById("reqTenantOption"), false);
    }
    <%--ZD 101443 End--%>
</script>

<script type="text/javascript" src="script/approverdetails.js"> <%-- ZD 102723 --%>

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<%--FB 1969 Start--%>
<head>
        <%--ZD 100902--%>
    <style type="text/css">
        #btnuser
        {
            width: 100px;
        }
        #btnResetvation
        {
            width: 100px;
        }        
    </style>
    <%--FB 2363 End--%>
    <title>Super administartor</title>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1982--%>
</head>
<%--FB 1969 End--%>
<body>
    <form name="frmMainsuperadministrator" id="frmMainsuperadministrator" method="Post"
    action="SuperAdministrator.aspx" autocomplete="off" language="JavaScript" runat="server"><%--ZD 101190--%>
    <!-- -->
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <center>
        <input type="hidden" id="helpPage" value="92">
        <input type="hidden" id="hdnMailServer" runat="server" />
        <input type="hidden" id="hdnLDAPPassword" runat="server" />
        <input type="hidden" id="hdnExternalPassword" runat="server" />
        <input type="hidden" id="hdnESMailUsrSent" runat="server" /> <%--FB 2363--%>
        <input type="hidden" id="hdnEM7Password" runat="server" /><%-- FB 2501 EM7--%>
        <%-- ZD 100152 Starts--%>
        <input type="hidden" id="hdnGoogleClientID" runat="server" />
        <input type="hidden" id="hdnGoogleClientID1" runat="server" /> 
        <input type="hidden" id="hdnGoogleClientIDVisit1" value="false" runat="server" /> 
        <input type="hidden" id="hdnGoogleSecretID" runat="server" />
        <input type="hidden" id="hdnGoogleSecretID1" runat="server" /> 
        <input type="hidden" id="hdnGoogleSecretIDVisit1" value="false" runat="server" /> 
        <input type="hidden" id="hdnGoogleApiKey" runat="server" />
        <input type="hidden" id="hdnGoogleApiKey1" runat="server" /> 
        <input type="hidden" id="hdnGoogleApiKeyVisit1" value="false" runat="server" /> 
        <%-- ZD 100152 Ends--%>        
        <%--FB 3054 Starts--%>
        <input type="hidden" id="hdnSMTPPW" runat="server" /> 
        <input type="hidden" id="hdnLDAPPW" runat="server" /> 
        <input type="hidden" id="hdnEM7PW" runat="server" /> 
        <input type="hidden" id="hdnExternalPW" runat="server" /> 
        <input type="hidden" id="hdnWhygoPW" runat="server" /> 
        <input type="hidden" id="hdnSMTP1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnSMTP2Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnLDAP1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnLDAP2Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnEM71Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnEM72Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnExt1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnExt2Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnWhygo1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnWhygo2Visit" value="false" runat="server" /> 
        <%--FB 3054 Ends--%>
        <input type="hidden" id="hdnConfIDStartValue" runat="server" /> <%--ZD 100526--%>
        <h3>
            <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_SiteSettings%>" runat="server"></asp:Literal></h3>
        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
    </center>
    <div id="dataLoadingDIV" align="center" style="display:none">
        <img border='0' src='image/wait1.gif'  alt='Loading..' /><%--ZD 100678--%>
    </div> <%--ZD 100176--%>
    <table width="100%" border="0">
        <tr valign="top" id="trSwt" runat="server">
            <td colspan="5" align="left" valign="top" style="display: none">
                <a id="ChgOrg" runat="server" href="#" class="blueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ChgOrg%>" runat="server"></asp:Literal></a>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_License%>" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="6%">
            </td>
            <%--Window Dressing--%>
            <td align="left" class="blackblodtext" height="21" style="font-weight: bold" width="18%" valign="top"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_myVRMLicenseK%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" valign="top" width="30%">
                <asp:TextBox ID="txtLicenseKey" runat="server" Columns="8" Rows="4" CssClass="altText"
                    TextMode="MultiLine" Width="213px" Height="74px"></asp:TextBox>
            </td>
            <%--Window Dressing--%>
            <td align="left" valign="top" style="font-weight: bold;width:23%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LicenseDetails%>" runat="server"></asp:Literal></td>
            <%--Window Dressing--%>
            <td class="altblackblodttext" width="23%" colspan="2" align="left"> <%--ZD 100393--%>
                <%--FB 2685 Start--%>
                <table width="100%" border="0">
                    <tr>
                        <%--<td align="left" nowrap valign="top">
                            <span class="blackblodtext"></span>
                        </td>--%>
                        <td style="height: 21px;" valign="top" width="100%">
                        <asp:TextBox ID="lblLicenseDetails" TextMode="MultiLine" ReadOnly="true" Width="300" Height="160" runat="server" CssClass ="altText" 
                              Columns="8" Rows="10"></asp:TextBox>
                            <%--<asp:Label ID="lblLicenseDetails" Style="vertical-align: baseline;" runat="server"
                                Width="95%"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <%--FB 2685 End--%>
                <table width="100%" cellspacing="3" cellpadding="3" border="0">
                    <tr>
                        <td width="30%" valign="top" nowrap>
                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LicenseStatus%>" runat="server"></asp:Literal></span><asp:Label ID="ActStatus" runat="server"></asp:Label>
                            <asp:Label ID="EncrypTxt" runat="server" Style="display: none;"></asp:Label>
                        </td>
                        <td id="ExportTD" runat="server" style="display: none;" align="left">
                            <asp:Button ID="TxyButton" runat="server" CssClass="altMedium0BlueButtonFormat" OnClick="btnExportTxt_Click" Width="170px"
                                Text="<%$ Resources:WebResources, SuperAdministrator_TxyButton%>" /> <%--ZD 100393--%>
                        </td>
                    </tr>
                </table>
                <table>
                    <%-- added for FF--%>
                    <tr id="ActivationTR" runat="server" style="display: none;">
                        <td colspan="2" width="100%">
                            <p>
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Yoursystemwil%>" runat="server"></asp:Literal></font>
                                <asp:Label ID="DeactLbl" runat="server" CssClass="lblError"></asp:Label><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Please%>" runat="server"></asp:Literal> <span class="contacttext"><a
                                    href='mailto:Support@myvrm.com'>Support@myvrm.com</a></span> <asp:Literal Text="<%$ Resources:WebResources, EncryLicPart%>" runat="server"></asp:Literal></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_OrganizationOp%>" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Organization%>" runat="server"></asp:Literal></td>
            <td align="left" valign="bottom"><%--FB 2678--%>
                <%--<input type="button" id="btnOrg" runat="server"  style="width :150pt" name="btnOrganization" value="Manage Organizations"
                   class="altLongBlueButtonFormat" onclick="javascript:OpenOrg();"/> <%--FB 2664--%> <%--ZD 100420--%>
                   <button id="btnOrg" runat="server"  style="width :150pt" name="btnOrganization" 
                   class="altLongBlueButtonFormat" onclick="javascript:return OpenOrg();">
                   <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageOrganization_lblHeader%>" runat="server"></asp:Literal></button> <%--ZD 100420--%>
                <%--FB 1662--%>
            </td>
            <%--FB 2678 Start--%>
            <%--<td align="left" style="height: 21px; width: 15%;">
            </td>
            <td style="height: 21px;" width="35%">
            </td>--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" nowrap="nowrap"> <%--ZD 100393--%>
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_IndividualOrga%>" runat="server"></asp:Literal>  <%--ZD 100537--%>
            </td>
            <td style="height: 21px;">
                <asp:DropDownList ID="lstOrgExpiry" runat="server" Width="20%" CssClass="alt2SelectFormat">
                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <%--FB 2678 End--%>
        </tr>

        <%--103496 starts; commented for ZD 104482 - Start--%>
        <%--<tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                <asp:Literal Text="<%$ Resources:WebResources, MemcacheOptions%>" runat="server"></asp:literal></td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight:bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" nowrap="nowrap">
                <asp:literal Text="<%$ Resources:WebResources, MemcacheEnabled%>" runat="server"></asp:literal>
            </td>
            <td style="height: 21px;">
                <asp:DropDownlist ID="drpMemEnab" runat="server" width="20%" CssClass="alt2SelectFormat">
                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>">"</asp:ListItem>
                    <asp:ListItem value="1" Text="<%$ Resources:WebResources, Yes%>" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>--%>
        <%--103496 ends; commented for ZD 104482 - Start--%>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_UserInterface%>" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%" colspan="5">
            </td>
        </tr>
        <%-- Code Added for FB 1428--%>
        <tr style="display:none" >
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_CompanyMessage%>" runat="server"></asp:Literal></td>
            <td valign="top">
                <asp:TextBox ID="txtCompanymessage" runat="server" Columns="30" Width="212px" CssClass="altText"
                    Rows="2"></asp:TextBox> <%--FB 2512--%>
                <asp:Label ID="hdnCompanymessage" Text="" Visible="false" runat="server"></asp:Label>
                <asp:Label ID="lblvalidation" Text="" runat="server"></asp:Label>
                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator9" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtCompanymessage" ErrorMessage="<%$ Resources:WebResources, Required%>"
                    Display="dynamic"></asp:RequiredFieldValidator>
            </td>
            <%-- //Commented for FB 1633 start  --%>
            <td align="left" valign="top" height="21" style="font-weight: bold" class="blackblodtext" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_StandardResolu%>" runat="server"></asp:Literal></td>
            <%-- //Commented for FB 1633 end  --%>
            <td valign="top" style="height: 21px;" nowrap>
                <input type="file" id="fleMap2" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" />
                <asp:Label ID="hdnUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map2ImageDt" name="Map1ImageDt" runat="server" />
                <asp:Label ID="hdnImageId1" Text="" Visible="false" runat="server"></asp:Label>
                <span class="blackItalictext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_1024x72pixel%>" runat="server"></asp:Literal></span>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap2" AlternateText="Delete"
                    ToolTip="<%$ Resources:WebResources, Delete %>" OnCommand="RemoveStdBanner" /> <%--FB 2798--%><%--ZD 100419--%>
            </td>
        </tr>
        <tr>
            <%-- //Commented for FB 1633 start  --%>
            <td align="left" style="font-weight: bold" width="2%">
            </td>
            <%--FB 2512 Starts--%>
            <%--</td>
            <td>
            </td>
            <td>
            </td>--%> 
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext" style="height: 9px;"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LoginHistory%>" runat="server"></asp:Literal></td>
            <td align="left" valign="bottom">
                <%--<input type="button" id="btnuser" runat="server" name="btnview" value="View" class="altLongBlueButtonFormat"
                    onclick="javascript:Openuser();" style ="margin-bottom: 5px" /> <%--FB 2512--%><%--ZD 100420 --%>
                     <button id="btnuser" runat="server"  style ="margin-bottom: 5px" name="btnview"
                   class="altLongBlueButtonFormat" onclick="javascript:return Openuser();"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></button> <%--ZD 100420 --%>
            </td>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext" style="height: 9px">
                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, SuperAdministrator_SiteLogo%>" runat="server"></asp:Literal>
               <span class="blackItalictext">&nbsp;<asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, SuperAdministrator_244x88pixels%>" runat="server"></asp:Literal></span><%--(122 x 44)FB 2407--%> <%--ZD 100393--%>
            </td>
            <%--FB 2512 Ends--%>
            <%-- //Commented for FB 1633 end  --%>
            <td valign="top" style="height: 9px;" nowrap="nowrap"><%--FB 2909 Start--%>
             <div>
               <input type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server" style=" height:24px;width:175px"/>  <%--ZD 100393--%>
                <div class="file_input_div"><%--<input type="button" value="Browse" class="file_input_button" style="vertical-align:middle" id="BtnBrowse1"  />--%>
                <button id="BtnBrowse1" class="file_input_button" style="vertical-align:middle" onclick="document.getElementById('fleMap1').click();return false;" ><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button><%--ZD 100420 --%>
                  <input type="file" class="file_input_hidden" accept="image/*" id="fleMap1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/></div><%--FB 3055-Filter in Upload Files--%> <%--ZD 100393--%><%--ZD 102277--%>
                  </div>
                <%--<input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" />--%><%--FB 2909 End--%>
                <%--<span class="blackItalictext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_244x88pixels%>" runat="server"></asp:Literal></span><%--(122 x 44)FB 2407--%> <%--ZD 100393--%>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap1" AlternateText="Delete"
                    ToolTip="<%$ Resources:WebResources, Delete %>" OnCommand="RemoveFile" /> <%--FB 2798--%><%--ZD 100419--%>
                <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                <asp:Label ID="hdnImageId" Text="" Visible="false" runat="server"></asp:Label>
                <br />
                <%--<asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="submit" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator>--%>  <%--ZD 100393--%>
            </td>
            <%-- //Commented for FB 1633 start  --%>
        </tr>
        <%--ZD 100393--%>
        <%--ZD 100902--%> <%--ZD 100381 Start--%>       
        <tr>
            <td colspan="4"></td>
            <td> <asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="submit" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator>
             <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="fleMap1"
                                                    Display="dynamic" runat="server" ValidationGroup="submit" SetFocusOnError="true"
                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>"
                                                    ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
            </td>
        </tr> <%--ZD 100393--%>
        <%-- FB 2719 Starts --%>
        <tr><%-- FB 2779 --%><%-- ZD 100393--%><%-- ZD 100996--%>
        
            <td align="left" style="font-weight: bold" width="2%">
            </td>
            <td align="left" valign="top" style="font-weight: bold;height: 9px;" class="blackblodtext">
                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ReservationHistory%>" runat="server"></asp:Literal>
            </td>
            <td align="left" valign="top"><%-- ZD 103581 Start--%>
                <button id="btnResetvation" runat="server"  style ="margin-bottom: 5px" name="btnview" width="100px;"
                   class="altLongBlueButtonFormat" onclick="javascript:return OpenReservation();"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, View%>" runat="server"></asp:Literal></button> 
            </td>
            <td id="tdlblLoginbg" runat="server" align="left" valign="top" style="font-weight: bold; padding-right:20px; height: 55px" class="blackblodtext" ><%--FB 2977--%>
            <table border="0">
            <tr>
            <td>
            <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, SuperAdministrator_LoginBackgroun%>" runat="server"></asp:Literal>
            </td>
            </tr>
            <tr>
            <td nowrap="nowrap">
            <br />
               <span style="padding:15px"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_WideScreen%>" runat="server"></asp:Literal></span>
               </td></tr>
               </table>
            </td>
                        
            <td id="tdbtnLoginbg" runat="server" valign="bottom" style="height: 9px;" nowrap="nowrap"><%--FB 2909 Start--%><%-- ZD 103581 End--%>
             <div>
               <input id ="txtFile1" type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server" style=" height:24px;width:175px"/>
                <div class="file_input_div">
                    <button id="BtnBrowse2" class="file_input_button" style="vertical-align:middle" onclick="document.getElementById('fleMap4').click();return false;"  ><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button><%--ZD 100420 --%>
                    <input type="file"  class="file_input_hidden" accept="image/*" id="fleMap4" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,2);"/>
                </div> <%--FB 3055-Filter in Upload Files--%>
             </div>               
             
             <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap4" AlternateText="Delete" CommandArgument="WideScreen"
                    ToolTip="<%$ Resources:WebResources, Delete %>" OnCommand="RemoveLoginBackground" /> <%--FB 2798--%><%--ZD 100419--%><%--ZD 103581--%>
             <asp:Label ID="hdnUploadMap4" Text="" Visible="false" runat="server"></asp:Label>
             <input type="hidden" id="Map4ImageDt" name="Map4ImageDt" runat="server" />
             <asp:Label ID="hdnImageId3" Text="" Visible="false" runat="server"></asp:Label>
            </td>
        </tr>
        <tr id="trRegLoginbg" runat="server">
            <td colspan="4"></td>
            <td>
                <asp:RegularExpressionValidator ID="regfleMap4" runat="server" Display="Dynamic" ValidationGroup="submit" ControlToValidate="fleMap4" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="regFNVal2" ControlToValidate="fleMap4"
                                                    Display="dynamic" runat="server" ValidationGroup="submit" SetFocusOnError="true"
                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>"
                                                    ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <%-- FB 2719 Ends --%> <%--ZD 100381 End--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold;display:none" width="2%"><%-- FB 2512--%>
            </td>
            <td valign="top" align="left" style="font-weight: bold;display:none" class="blackblodtext" rowspan="0" width="15%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LoginPageText%>" runat="server"></asp:Literal></td>
            <td style="font-weight: bold;display:none" width="30%"> <%--FB 2512--%>
                <asp:TextBox ID="CompanyInfo" runat="server" rowspan="0" Columns="20" CssClass="altText"
                    Rows="4" TextMode="MultiLine"></asp:TextBox>
            </td>
            <%-- //Commented for FB 1633  
                <td align="right" valign="top" style="font-weight:bold" class="blackblodtext">
                    High Resolution Banner 
                </td>--%>
            <td valign="top" nowrap>
                <input type="file" id="fleMap3" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" visible="false" />
                <span class="blackItalictext">
                    <%--(1600 x 72 pixels)--%></span>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap3" AlternateText="Delete"
                    ToolTip="<%$ Resources:WebResources, Delete %>" OnCommand="RemoveHighBanner" Visible="false" /> <%--FB 2798--%>
                <asp:Label ID="hdnUploadMap3" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map3ImageDt" name="Map1ImageDt" runat="server" />
                <asp:Label ID="hdnImageId2" Text="" Visible="false" runat="server"></asp:Label>
            </td>
            <%-- //Commented for FB 1633 end --%>
        </tr>
        <%--FB 1969 - Start--%> <%--FB 2512 Starts--%>
        <%--<tr>
            <td align="right" style="font-weight: bold">
            </td>
            <td>
            </td>
            <td align="right" valign="top" style="font-weight: bold" class="blackblodtext" style="height: 9px">
                Login History
            </td>
            <td align="left" valign="bottom">
                <input type="button" id="btnuser" runat="server" name="btnview" value="View" class="altLongBlueButtonFormat"
                    onclick="javascript:Openuser();" />
            </td>
        </tr>--%>
        <%--FB 1969 - End--%> <%--FB 2512 Ends--%>
        <tr>
            <%-- ZD 101011 Starts --%>
            <td width="2%">
            </td>
            <td align="left" style="font-weight: bold" class="blackblodtext" >
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_SessionTimeout%>" runat="server"></asp:Literal>
            </td>
            <td>
                <input id="txtSessionTimeout" type="text" maxlength="2" runat="server" value="" class="altText" style="width:50px" /> (mins) 
                <input type="image" id="imgSessionTimeout" title="<%$ Resources:WebResources, MaxTimeoutTooltip%>" runat="server" src="image/info.png" alt="Info" style="border-width:0px; cursor:default; vertical-align:middle" onclick="return false;" /><br /><%-- ZD 102590 --%>
                <asp:requiredfieldvalidator id="reqSessionTimeout" runat="server" controltovalidate="txtSessionTimeout" validationgroup="submit"
                display="dynamic" errormessage="<%$ Resources:WebResources, TimeoutErrMsg%>"></asp:requiredfieldvalidator>
                <asp:RangeValidator ID="RangeSessionTimeout" Type="Integer"
                MinimumValue="5" MaximumValue="60" Display="Dynamic" ControlToValidate="txtSessionTimeout"
                ValidationGroup="submit" runat="server" ErrorMessage="<%$ Resources:WebResources, TimeoutErrMsg%>"></asp:RangeValidator>
                <asp:regularexpressionvalidator id="RegSessionTimeout" validationgroup="submit"
                controltovalidate="txtSessionTimeout" display="dynamic" runat="server"
                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
            </td>
			<%--ZD 103581 Start--%>
            <%--<td colspan="2"></td>--%>
            <td id="td1" runat="server" align="left" valign="top" style="font-weight: bold; height: 9px" class="blackblodtext" ><%--FB 2977--%>
               <span style="padding:19px"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_RegularScreen%>" runat="server"></asp:Literal></span>
            </td>
                        
            <td id="td2" runat="server" valign="top" style="height: 9px;" nowrap="nowrap"><%--FB 2909 Start--%>
             <div>
               <input type="text" id ="txtFile2" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server" style=" height:24px;width:175px"/>
                <div class="file_input_div">
                    <button id="BtnBrowse3" class="file_input_button" style="vertical-align:middle" onclick="document.getElementById('fleMap5').click();return false;"  ><asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button><%--ZD 100420 --%>
                    <input type="file"  class="file_input_hidden" accept="image/*" id="fleMap5" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,2);"/>
                </div> <%--FB 3055-Filter in Upload Files--%>
             </div>               
             
             <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap5" AlternateText="Delete" CommandArgument="NormalScreen"
                    ToolTip="<%$ Resources:WebResources, Delete %>" OnCommand="RemoveLoginBackground" /> <%--FB 2798--%><%--ZD 100419--%>
             <asp:Label ID="hdnUploadMap5" Text="" Visible="false" runat="server"></asp:Label>
             <input type="hidden" id="Map5ImageDt" name="Map5ImageDt" runat="server" />
             <asp:Label ID="hdnImageId4" Text="" Visible="false" runat="server"></asp:Label>
            </td>   
            <%-- ZD 101011 Ends --%><%--ZD 103581 End--%>
        </tr>
        <tr> <%--ZD 104846 start --%>
         <td  width= "2%"> </td>
         <td align="left" style= "font-weight: bold" class="blackblodtext" >
           <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_AutoPurgeLogDuration%>" runat="server"></asp:Literal>
         </td>
         <td>
            <input id="txtpurgeLogDuration" type="text" runat="server" value="" class="altText" style="width:50px" />
            <asp:Literal  Text="<%$ Resources:WebResources, SuperAdministrator_Days%>" runat="server"/>
            <input type="image" id="ImgpurgeLogDuration" title="<%$ Resources:WebResources, SuperAdministrator_MinimumDuration%>" runat="server" src="image/info.png" 
            alt="Info" style="border-width:0px; cursor:default; vertical-align:middle" onclick="return false;" /><br />
            <asp:requiredfieldvalidator id="ReqpurgeLogDuration" runat="server" controltovalidate="txtpurgeLogDuration" validationgroup="submit"
                display="dynamic" errormessage="Required"></asp:requiredfieldvalidator>
            <asp:RangeValidator ID="RangeValidator2" Type="Integer"
             MinimumValue="5" MaximumValue="2000000000" Display="Dynamic" ControlToValidate="txtpurgeLogDuration"
             ValidationGroup="submit" runat="server" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_MinimumDuration%>"></asp:RangeValidator>
              <asp:regularexpressionvalidator id="RegPurgelogDuartion" validationgroup="submit"
                controltovalidate="txtpurgeLogDuration" display="dynamic" runat="server"
                errormessage="<%$ Resources:WebResources, NumericValuesOnly%>" validationexpression="\d+"></asp:regularexpressionvalidator>
            </td>
        </tr> <%--ZD 104846 End --%>
        <tr>
            <td class="subtitleblueblodtext" colspan="5">
                <asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, SuperAdministrator_MailServerConfiguration%>" runat="server"></asp:Literal>                
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_SystemEmail%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtSystemEmail" runat="server" CssClass="altText" Text="support@myvrm.com">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="TestEmail"
                    runat="server" ControlToValidate="txtSystemEmail" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator is="regSystemEmail" runat="server" ControlToValidate="txtSystemEmail"
                    ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <%--FB Case 699--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MailServerLog%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtMailServerLogin" runat="server" CssClass="altText"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MailServerPas%>" runat="server"></asp:Literal></td>
            <td style="height: 37px;" width="30%">
                <asp:TextBox ID="txtMSPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                    CssClass="altText" TextMode="Password" onblur="PasswordChange(1,1)" onfocus=" fnTextFocus(this.id,1,1)">
               </asp:TextBox><asp:CompareValidator ID="CompareValidator2"
                    runat="server" ControlToValidate="txtMSPassword1" ControlToCompare="txtMSPassword2"
                    ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>" Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" Display="Dynamic" />
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_RetypePassword%>" runat="server"></asp:Literal></td>
            <td style="height: 37px;" width="35%">
                <asp:TextBox ID="txtMSPassword2" runat="server" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" onblur="PasswordChange(1,2)" onfocus=" fnTextFocus(this.id,2,1)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold">
                <%--FB 1710--%>
                <%--Window Dressing--%>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_DisplayName%>" runat="server"></asp:Literal></td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtDisplayName" runat="server" CssClass="altText"></asp:TextBox>
                </td>
                <%--Window Dressing--%><%--FB 1710 Starts--%>
                <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_WebsiteURL%>" runat="server"></asp:Literal></td>
                <td style="height: 21px;" width="35%">
                    <asp:TextBox ID="txtWebsiteURL" runat="server" CssClass="altText">
                    </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                        ControlToValidate="txtWebsiteURL" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_WebsiteURLreq%>" Font-Names="Verdana"
                        Font-Size="X-Small" Font-Bold="False"><font color="red" size="1pt"> required</font></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtWebsiteURL"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters3%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                </td>
                <%--FB 1710 ends--%>
        </tr>
        <tr style="height: 35px">
            <%--FB 1710--%>
            <td align="left" height="21" style="font-weight: bold">
                <%--FB 1710--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MailServerAdd%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtServerAddress" runat="server" CssClass="altText" onfocus="SMTPTC()"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="reqEmailServerAddress" ValidationGroup="TestEmail"
                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" CssClass="lblError"
                    ControlToValidate="txtServerAddress"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtServerAddress"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="regPortP" Enabled="false" SetFocusOnError="true"
                    ControlToValidate="txtServerAddress" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
                    ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                <%--FB Case 491: Saima --%>
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MailServerPor%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtServerPort" runat="server" CssClass="altText" Text="25"></asp:TextBox>
                <span><asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, SuperAdministrator_25isdefault%>" runat="server"></asp:Literal></span> <%--FB 2552--%><%--ZD 103053--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="TestEmail"
                    runat="server" ControlToValidate="txtServerPort" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtServerPort"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--FB 2552 Start--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MailRetryCoun%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:TextBox ID="txtRetryCount" CssClass="altText" runat="server"></asp:TextBox>
                <span><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Max30%>" runat="server"></asp:Literal></span><%--ZD 103053--%>
                <asp:RegularExpressionValidator ID="RetryCountValidator" ControlToValidate="txtRetryCount" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="retryCountRangeValid" SetFocusOnError="true" Type="Integer" MinimumValue="0" MaximumValue="30"
                            Display="Dynamic" ControlToValidate="txtRetryCount" ValidationGroup="submit" runat="server" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_Retrycount%>"></asp:RangeValidator>
            </td>
            <%--ZD 100317 Starts--%>
             <td align="left" height="21" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ConnectionTime%>" runat="server"></asp:Literal></td>
             <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtMailTimeout" runat="server" CssClass="altText" Text="10"></asp:TextBox>
                <span><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Seconds%>" runat="server"></asp:Literal></span><%--ZD 103053--%>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtMailTimeout"
                 Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                 ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="RangeValidator1" SetFocusOnError="true" Type="Integer" MinimumValue="10" MaximumValue="300"
                            Display="Dynamic" ControlToValidate="txtMailTimeout" ValidationGroup="submit" runat="server" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_ServerTimeOut%>"></asp:RangeValidator>
            </td>
            <%--ZD 100317 Ends--%>
        </tr>
        <%--FB 2552 Ends--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold">
            </td>
            <%--Commented for FB 1710 start--%>
            <%--Window Dressing //Add Message Text--%>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext" visible="false"
                width="18%">
            </td>
            <td valign="top" width="30%">
                <asp:TextBox ID="txtMailMessage" runat="server" Columns="20" CssClass="altText" Rows="4"
                    TextMode="MultiLine" Width="200px" Wrap="False" Visible="false"></asp:TextBox>
                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtMailMessage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>     Mail Footer      --%>
            </td>
            <%--Commented for FB 1710 End--%>
            <td align="left" height="21">
            </td>
            <td style="height: 21px;" width="35%" class="blackblodtext">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="1" style="font-weight: bold" width="2%">
            </td>
            <td align="center" colspan="4" width="18%">
            <button id="btnTestMailConnection" runat="server" Class="altLongBlueButtonFormat"  style="Width:160pt"  ValidationGroup="TestEmail" Onserverclick="TestMailServerConnection" OnClientClick="javascript:DataLoading('1');" ><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_btnTestMailConnection%>" runat="server"></asp:Literal></button><%--ZD 100420 --%>
                <%--<asp:Button ID="btnTestMailConnection" Text="Test Connection" ValidationGroup="TestEmail" Width="160pt" 
                    CssClass="altLongBlueButtonFormat"  runat="server" OnClick="TestMailServerConnection" OnClientClick="javascript:DataLoading('1');" /> <%--ZD 100176 --%><%--ZD 100420 --%>
                <%--<input id="btnTestMailServer" class="altLongBlueButtonFormat" type="button" value="Test Connection" onclick="javascript:TestMailServerConnection();" />--%>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="font-weight: bold"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MSActiveDirec%>" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ServerAddress%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPServerAddress" runat="server" CssClass="altText" onfocus="LDAPTC()"></asp:TextBox>
                <%--                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" 
                         ErrorMessage="<br>+'&<>%;:( )& / \ ^#$@ and double quotes are invalid characters for this field." ValidationExpression="[A-Za-z0-9._~?!`* \-]+"></asp:RegularExpressionValidator>                    
--%>
                <asp:RegularExpressionValidator runat="server" ID="regAddress" Enabled="false" ControlToValidate="txtLDAPServerAddress"
                    Display="Dynamic" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
                    ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="LDAP" ErrorMessage="<%$ Resources:WebResources, Required%>"
                    ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_PortNumber%>" runat="server"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtLDAPServerPort" runat="server" CssClass="altText" Text="389"></asp:TextBox>
                <span><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_389isdefault%>" runat="server"></asp:Literal></span><%--ZD 103053--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLDAPServerPort"
                    ErrorMessage="Required."></asp:RequiredFieldValidator>
            </td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="LDAP" ErrorMessage="<%$ Resources:WebResources, Required%>"
                ControlToValidate="txtLDAPServerPort" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_AccountLogin%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPAccountLogin" runat="server" CssClass="altText"></asp:TextBox>
                 <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtLDAPAccountLogin"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountLogin" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ConnectionTime%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="35%">
                <asp:DropDownList ID="lstLDAPConnectionTimeout" runat="server" CssClass="altSelectFormat">
                    <asp:ListItem Value="10" Text="<%$ Resources:WebResources, 10Seconds%>"></asp:ListItem>
                    <asp:ListItem Value="20" Text="<%$ Resources:WebResources, 20Seconds%>"></asp:ListItem>
                    <asp:ListItem Value="30" Text="<%$ Resources:WebResources, 30Seconds%>"></asp:ListItem>
                    <asp:ListItem Value="60" Text="<%$ Resources:WebResources, 60Seconds%>"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_AccountPasswor%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPAccountPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" onblur="PasswordChange(2,1)" onfocus=" fnTextFocus(this.id,1,2)"
                    runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="false"
                    ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server"
                    SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator3" Display="dynamic" runat="server" ControlToValidate="txtLDAPAccountPassword1"
                    ControlToCompare="txtLDAPAccountPassword2" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
                <%--<asp:RequiredFieldValidator ID="reqLDAPPass" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_RetypePassword%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtLDAPAccountPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" onblur="PasswordChange(2,2)" onfocus=" fnTextFocus(this.id,2,2)"
                    runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Enabled="false"
                    ControlToValidate="txtLDAPAccountPassword2" Display="dynamic" runat="server"
                    SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtLDAPAccountPassword2"
                    ControlToCompare="txtLDAPAccountPassword1" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LoginKey%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPLoginKey" runat="server" CssClass="altText"></asp:TextBox>
                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtLDAPLoginKey"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_SearchFilter%>" runat="server"></asp:Literal></td>
            <!--Added Regular expression by Vivek to perform junk character validation as a fix for issue number 315 -->
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPSearchFilter" runat="server" CssClass="altText"></asp:TextBox>
                &nbsp;
                <br />
                <span style="color: #666666;"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_excnusersdcdom%>" runat="server"></asp:Literal></span>
                <%--FB 2096--%>
                <%-- <asp:RegularExpressionValidator ID="regLDAPSearchFilter" ControlToValidate="txtLDAPSearchFilter"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_invalidChar%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/+;?|`\[\]{}\x22;^:@#$%~']*$"></asp:RegularExpressionValidator>
               FB Case 491: Saima --%>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_DomainPrefix%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:TextBox ID="txtLDAPPrefix" CssClass="altText" runat="server"></asp:TextBox>
                <%-- <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtLDAPPrefix"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_invalidChars%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^<>+;?|!`,\[\]{}\x22;=^:@#$%&()~']*$"></asp:RegularExpressionValidator>--%>
            </td>
            <%--Window Dressing--%>
            <%--FB 2462 START--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Synchronization%>" runat="server"></asp:Literal></td>
            <td align="left">
                <mbcbb:ComboBox ID="lstLDAPScheduleTime" CssClass="altText" runat="server" Enabled="true" Rows="15" ValidationGroup="submit" AutoPostBack="false"><%--ZD 100284--%> <%--ZD 100420--%>
                    <asp:ListItem Value="01:00 AM" Selected="true"></asp:ListItem>
                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                    <asp:ListItem Value="12:00 AM" Selected="true"></asp:ListItem>
                </mbcbb:ComboBox> <%--ZD 100284--%>
                <asp:RequiredFieldValidator ID="reqlstLDAPScheduleTime" runat="server" ControlToValidate="lstLDAPScheduleTime:Text" ValidationGroup="submit"
                                                                SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reglstLDAPScheduleTime" runat="server" ControlToValidate="lstLDAPScheduleTime:Text" ValidationGroup="submit"
                    SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
            </td>
            <%--FB 2462 END--%>
        </tr>
        <tr style="display:none"> <%--FB 2993 LDAP--%>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" width="18%" class="blackblodtext" style="display: none"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Synchronization%>" runat="server"></asp:Literal></td>
            <td align="left" colspan="4" style="font-weight: normal; color: Blue">
                <%--Window Dressing--%>
                <asp:CheckBoxList ID="chkLstDays" runat="server" RepeatColumns="9" RepeatLayout="Flow"
                    Textalign="left" Style="display: none">
                    <asp:ListItem Text="<%$ Resources:WebResources, Monday%>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Tuesday%>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Wednesday%>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Thursday%>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Friday%>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Saturday%>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Sunday%>" Value="7"></asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>
        <%--FB 2993 LDAP START--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Authentication%>" runat="server"></asp:Literal></td>
			<%--ZD 101443--%>
            <td align="left" style="font-weight: normal; color: Blue">
                <%--Window Dressing--%>
                <asp:DropDownList ID="drpAuthType" runat="server" Width="150px" RepeatColumns="9" RepeatLayout="Flow" Textalign="left">
                    <asp:ListItem Text="<%$ Resources:WebResources, None%>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Signing%>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Sealing%>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Secure%>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Anonymous%>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Delegation%>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, Encryption%>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, FastBind%>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, ReadonlyServer%>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, SecureSocketsLayer%>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:WebResources, ServerBind%>" Value="10"></asp:ListItem>
                </asp:DropDownList>
            </td>
			<%--ZD 101443 Starts--%>
            <td align="left" headers="21" class="blackblodtext" ><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Domains%>" runat="server"></asp:Literal> </td>
            <td nowrap="nowrap"><asp:CheckBox ID="chkDomain" runat="server" CssClass="blackblodtext" onclick="javascript:fnTenantOption()" /> 
            <asp:DropDownList runat="server" ID="drpTenantOption" onchange="javascript:fnTenantOptionchange()">
            <asp:ListItem Value="-1" Text="<%$ Resources:WebResources, PleaseSelect%>"></asp:ListItem>
            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, SuperAdministrator_SCST%>" ></asp:ListItem>
            <asp:ListItem Value="2" Text="<%$ Resources:WebResources, SuperAdministrator_SCMT%>" ></asp:ListItem>
             <asp:ListItem Value="4" Text="<%$ Resources:WebResources, SuperAdministrator_SCMF%>" ></asp:ListItem> 	<%--ZD 102732 Starts--%>
            <asp:ListItem Value="3" Text="<%$ Resources:WebResources, SuperAdministrator_MCMT%>" ></asp:ListItem>
            <asp:ListItem Value="5" Text="<%$ Resources:WebResources, SuperAdministrator_MFGC%>" ></asp:ListItem> <%--ZD 102999--%>
            </asp:DropDownList> 
            <asp:ImageButton ID="imgTenantOption" runat="server" ImageUrl="image/info.png" AlternateText="Info" OnClientClick="return false;" ToolTip="<%$ Resources:WebResources, SuperAdministrator_TenantOption%>" style="cursor:default" /> <%-- ZD 102590 --%>
            <asp:RequiredFieldValidator ID="reqTenantOption" runat="server" InitialValue="-1" ControlToValidate="drpTenantOption" Enabled="false"
             ValidationGroup="submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
            &nbsp; &nbsp; <span class="blackblodtext"><asp:Literal ID="LiteralSSO" Text="<%$ Resources:WebResources, SuperAdministrator_SSO%>" runat="server"></asp:Literal></span><asp:CheckBox ID="chkSSOMode" runat="server" />
			<%--ZD 101443 End--%>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="1" style="font-weight: bold" width="2%">
            </td>
            <td align="left" colspan="4" style="font-weight: bold; font-size: small; color: black;
                font-family: verdana; height: 21px; text-align: center;" >
                <button ID="btnTestLDAPConnection"  runat="server"  style="Width:160pt" Class="altLongBlueButtonFormat" Onserverclick="TestLDAPConnection"  ValidationGroup="LDAP" OnClientClick="javascript:DataLoading('1');" ><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_btnTestLDAPConnection%>" runat="server"></asp:Literal></button><%--ZD 100420 --%>
                <%--<asp:Button runat="server" ID="btnTestLDAPConnection" ValidationGroup="LDAP" Text="Test Connection" Width="160pt" 
                    CssClass="altLongBlueButtonFormat" OnClick="TestLDAPConnection" OnClientClick="javascript:DataLoading('1');" /> <%--ZD 100176 --%><%--ZD 100420 --%>
                <%--               			<input type="button" name="btnTestLDAPServer" value="Test Connection" class="altLongBlueButtonFormat" onclick="javascript:TestLDAPServerConnection();">
--%>
                <asp:Button runat="server" id="btnSyncLdapNow" text="<%$ Resources:WebResources, SuperAdministrator_btnSyncLdapNow%>" cssclass="altLongBlueButtonFormat" onclick="SyncNow" style="display: none;"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" width="18%">
            </td>
            <td style="height: 21px; text-align: center;" width="30%">
            </td>
            <td align="left" style="font-weight: bold; font-size: small; width: 15%; color: black;
                font-family: verdana; height: 21px; text-align: center">
            </td>
            <td style="height: 21; font-weight: bold" width="35%">
            </td>
        </tr>
        <tr>
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ServerOptions%>" runat="server"></asp:Literal></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ServerTimeZon%>" runat="server"></asp:Literal></td>
            <td align="left" style="width: 15%">
                <asp:DropDownList ID="PreferTimezone" runat="server" CssClass="altLong0SelectFormat"
                    DataTextField="timezoneName" OnInit="UpdateTimezones" DataValueField="timezoneID">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="TimeZoneValidator" runat="server" ControlToValidate="PreferTimezone"
                    ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Bold="True" InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <%--FB 2007--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LaunchBufferi%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtLaunchBuffer" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="TxtLaunchBuffer"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_EnterNumber%>"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr> <%--FB 2437--%> 
        <%--FB 2501 starts--%>
         <td style="width: 3%">
            </td>
         <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_StartMode%>" runat="server"></asp:Literal></td>
         <td align="left" style="width: 15%">
                <asp:DropDownList ID="lstStartMode" runat="server" Width="30%" CssClass="alt2SelectFormat">
             <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, Automatic%>"></asp:ListItem>
             <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Manual%>"></asp:ListItem>
           </asp:DropDownList>  
         </td>
         <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
            <asp:Label id="Label8" runat="server" text="<%$ Resources:WebResources, SuperAdministrator_Label8%>"></asp:Label>
         </td>
        <td style="height: 21px;">
           <asp:DropDownList ID="lstEnableLaunchBufferP2P" runat="server" Width="20%" CssClass="alt2SelectFormat">
             <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
             <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
           </asp:DropDownList>
        </td>
        <%--FB 2501 ends--%>
        </tr>
        <%--FB 2670 Start--%>
        <tr>
          <td style="width: 3%">
          </td>
          <td align="left" style="font-weight:bold;width: 18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ConferenceSupp%>" runat="server"></asp:Literal></td>
          <td align="left" style="width:15%">
              <asp:DropDownList ID="lstConciergeSupport" runat="server" Width="30%" CssClass="alt2SelectFormat">
                <asp:ListItem Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                <asp:ListItem Value="2"  Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
              </asp:DropDownList>
          </td>
		 <%--FB 2858 Start--%>
          <td align="left" style="font-weight:bold;width: 18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_DisplayPublic%>" runat="server"></asp:Literal></td>
          <td align="left" style="width:15%">
              <asp:DropDownList ID="lstViewPublic" runat="server" Width="20%" CssClass="alt2SelectFormat">
                <asp:ListItem Value="1"  Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                <asp:ListItem Value="2"  Text="<%$ Resources:WebResources, No%>" Selected="True"></asp:ListItem>
              </asp:DropDownList>
          </td>
         <%--FB 2858 End--%>
        </tr>
        <%--FB 2670 End--%>

        <%--ZD 101837 Start--%>
        <tr valign="top">
            <td style="width: 3%" nowrap="nowrap">
            </td>
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, ConfIDStartValue%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtStartConfID" runat="server" CssClass="altText" MaxLength="9" onblur="javascript:return fnCheckConfID();" ></asp:TextBox>
                <asp:CompareValidator ID="cmpValConfStartVal" runat="server" ValueToCompare="0" ControlToValidate="txtStartConfID" ValidationGroup="submit"
                    Display="dynamic" ErrorMessage="<%$ Resources:WebResources, NumericValuesonly3%>" Operator="GreaterThan" Type="Integer" SetFocusOnError="true"></asp:CompareValidator>
            </td>
            <td align="left" style="font-weight:bold;width: 18%" class="blackblodtext">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ConfRecurLimit%>" runat="server"></asp:Literal>
            </td>
            <td align="left" style="width:15%">
                <asp:TextBox ID="txtRecurLimit" runat="server" MaxLength="3" CssClass="altText"></asp:TextBox>
                <span><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, SuperAdministrator_recurLimit%>" runat="server"></asp:Literal></span><%--ZD 103053--%>
                <asp:RegularExpressionValidator ID="RegRecurLimit" ControlToValidate="txtRecurLimit" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="ValidateRecurLimit" runat="server" ValidationGroup="submit" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_MaxRecurLimit%>"
                    ControlToValidate="txtRecurLimit" MinimumValue="2" Display="Dynamic" Type="Integer" MaximumValue='250'></asp:RangeValidator>
            </td>
        </tr>
        <%--ZD 101837 End--%>
		<%--FB 2659 Starts  --%>
        <tr>
            <td style="width: 3%" nowrap="nowrap">
            </td>
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_CloudInstallat%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="drpCloudInstallation" runat="server" CssClass="alt2SelectFormat" Width="30%" >
                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td id="tdLicense" runat="server" align="left" height="21" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_tdLicense%>" runat="server"></asp:Literal></td>
            <td id="tdBtnLicense" style="height: 21px;" runat="server" >
                <input type="button" id="btnDefaultClick" class="altMedium0BlueButtonFormat" width="250px" runat="server" value="<%$ Resources:WebResources, DefaultLicense%>" /> <%--ZD 100420--%>
            </td>
        </tr>
		<%--ZD 101846 Start--%>  
        <tr id="trReqAcc" runat="server">
            <td style="width: 3%" nowrap="nowrap">
            </td>
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ReqUsrAcc%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="drpEnableReqAcc" runat="server" CssClass="alt2SelectFormat" Width="30%" >
                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                </asp:DropDownList>
            </td> <%--ALLDEV-644 start--%>
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
               <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_DisplayViewRoomcalLink%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:DropDownList ID="drpEnableRoomCalLink" runat="server" CssClass="alt2SelectFormat" Width="30%" >
                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                </asp:DropDownList>
            </td><%--ALLDEV-644 End--%>
        </tr>
		<%--ZD 101846 End--%> 
        <%--ZD 100526--%>        
        <%--FB 2659 ends--%>        
        <%--FB 2501 EM7  Start--%>
        <%-- FB 2598 EnableEM7 Starts tr-id --%>
        <tr id="trEM7Connectivity" runat="server">
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EM7Connectivit%>" runat="server"></asp:Literal></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr id="trEM7URIUsrName" runat="server">
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EM7URI%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtEM7URI" runat="server" CssClass="altText" MaxLength="256"></asp:TextBox>                                
                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ControlToValidate="txtEM7URI" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters30%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                
            </td>
           <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EM7UserName%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtEM7Username" runat="server" CssClass="altText" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" ControlToValidate="txtEM7Username" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_invalidChars%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                
            </td>
        </tr>
        <tr id="trEM7Pwd" runat="server">
            <td align="left">
            </td>
            <td align="left" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EM7Password%>" runat="server"></asp:Literal></td>
            <td style="height: 37px;" width="30%" nowrap="nowrap">
                <asp:TextBox ID="txtEM7Password" runat="server" CssClass="altText" TextMode="Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" MaxLength="20" onblur="PasswordChange(3,1)" onfocus=" fnTextFocus(this.id,1,3)"></asp:TextBox>   
                <asp:CompareValidator ID="cmpValPassword1" runat="server" ValidationGroup="submit" ControlToCompare="txtEM7ConformPassword"
                    ControlToValidate="txtEM7Password" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>"></asp:CompareValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" Enabled="true" ValidationGroup="submit"
                    ControlToValidate="txtEM7Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters4%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>             
            </td>  
             <td align="left" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EM7ConfirmPas%>" runat="server"></asp:Literal></td>
            <td style="height: 37px;" width="30%">
                <asp:TextBox ID="txtEM7ConformPassword" runat="server" CssClass="altText" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" TextMode="Password" MaxLength="20" onblur="PasswordChange(3,2)" onfocus=" fnTextFocus(this.id,2,3)"></asp:TextBox> 
                <asp:CompareValidator ID="CompareValidator6" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtEM7ConformPassword" ControlToCompare="txtEM7Password" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                     Font-Bold="False" />   
                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" Enabled="true" ValidationGroup="submit"
                    ControlToValidate="txtEM7ConformPassword" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters4%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator> 
             </td> 
             
         </tr>
         <tr id="trEM7Port" runat="server">
          <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EM7Port%>" runat="server"></asp:Literal></td> 
          <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtEM7Port" runat="server" CssClass="altText" MaxLength="4" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator19" ControlToValidate="txtEM7Port"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
          </td>
         </tr>
         <%-- FB 2598 EnableEM7 tr-id Ends--%>
         <%--FB 2501 EM7 End--%>
                  
        <%-- FB 2363 - Start--%>
        <%if((Application["External"].ToString() != "")){%>
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ExternalSchedu%>" runat="server"></asp:Literal></span>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_PartnerName%>" runat="server"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtPartnerName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" ControlToValidate="txtPartnerName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="left">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_PartnerEmail%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtPartnerEmail" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" is="regSystemEmail"
                    runat="server" ControlToValidate="txtPartnerEmail" ValidationGroup="submit" ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>"
                    Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <%--FB Case 699--%>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_PartnerURL%>" runat="server"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtPartnerURL" runat="server" CssClass="altText" TextMode="MultiLine"
                    Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtPartnerURL"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters3%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="left">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_UserName%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtPUserName" runat="server" CssClass="altText" Rows="3" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtPUserName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Password%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtP1Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(4,1)" onfocus=" fnTextFocus(this.id,1,4)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Enabled="false"
                    ControlToValidate="txtP1Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator1" Display="dynamic" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtP1Password" ControlToCompare="txtP2Password"
                    ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>" Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" />
            </td>
            <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_RetypePassword%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtP2Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(4,2)" onfocus=" fnTextFocus(this.id,2,4)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" Enabled="false"
                    ControlToValidate="txtP2Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtP2Password" ControlToCompare="txtP1Password" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
        <tr>
            <td style="width: 3%"> <%--FB 2363--%>
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_TimeoutValue%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtTimeoutValue" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regTimeoutVlaue" runat="server" ControlToValidate="txtTimeoutValue" ValidationGroup="submit" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="[\d]+" ></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" style="width: 20%">
                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_EventQueue%>" runat="server"></asp:Literal></span>
            </td>
            <td >
               <%-- <input type="button" id="btnEvent" runat="server" name="btnOrganization" value="Manage Event"
                    class="altMedium0BlueButtonFormat"  onclick="javascript:fnView();"--%>
                    <button id="btnEvent" runat="server" name="btnOrganization" class="altMedium0BlueButtonFormat"  onclick="javascript:return fnView();" style="width:200px;"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageEvent%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
            </td>
            <td colspan="2" ></td>
        </tr>        
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MailingUserRe%>" runat="server"></asp:Literal></span>
            </td>
        </tr>
        <tr>
            <td style="width: 2%">
            </td>
            <td class="blackblodtext" align="left" style="width: 18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_CustomerName%>" runat="server"></asp:Literal></td>
            <td style="width: 30%">
                <asp:TextBox ID="txtUsrRptCustmName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegtxtUsrRptCustmName" ControlToValidate="txtUsrRptCustmName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="left">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Destination%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptDestination" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegtxtUsrRptDestination" ControlToValidate="txtUsrRptDestination"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 3%"></td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_StartTime%>" runat="server"></asp:Literal></td>
            <td>
                <asp:TextBox ID="txtUsrRptStartTime" runat="server" CssClass="altText" Width="70px"></asp:TextBox>
                <a href="" onclick="this.childNodes[0].click();return false;"><img alt ="Date Selector" src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" 
                    style="cursor: pointer; vertical-align: middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=txtUsrRptStartTime.ClientID %>', 'cal_trigger1', 0, '<%=dtFormatType%>');" /></a> <%--ZD 100419--%><%--ZD 104162--%>
            </td>
            <td class="blackblodtext" align="left">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_SentTime%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptSentTime" runat="server" CssClass="altText" Enabled="false" Width="70px"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_FrequencyCount%>" runat="server"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="lstUsrRptFrequencyCount" runat="server" CssClass="altSelectFormat"
                    Width="15%">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="lstUsrRptFrequencyType" runat="server" CssClass="altSelectFormat"
                    Width="30%">
                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Days%>"></asp:ListItem>
                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, Weeks%>"></asp:ListItem>
                    <asp:ListItem Value="3" Text="<%$ Resources:WebResources, Months%>"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr style="display:none">
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_DeliverType%>" runat="server"></asp:Literal></td>
            <td>
                <asp:DropDownList ID="lstUsrRptDeliverType" runat="server" CssClass="altSelectFormat"
                    Width="15%">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>            
          </tr>
        <%} %>
        <%-- FB 2363 - End--%>
          <%-- FB 2392 - Start--%>
        <tr>
            <td colspan="5" align="left">
            </td>
         </tr>
         <tr runat="server" id="trWhygo" >
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_WhyGoIntegrati%>" runat="server"></asp:Literal></span>
            </td>
         </tr>
         <tr runat="server" id="trWhygoURL">
            <td style="width: 2%">
            </td>
            <td class="blackblodtext" align="left" style="width: 18%"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_URL%>" runat="server"></asp:Literal></td>
            <td style="width: 30%">
                <asp:TextBox ID="txtWhyGoURL" runat="server" CssClass="altText" Width="200px" onkeyup="SetButtonStatus(this, 'PollWhyGo') "></asp:TextBox> <%--FB 2594--%>
                <asp:RegularExpressionValidator ID="regttxtWhyGoURL" ControlToValidate="txtWhyGoURL"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
             <td class="blackblodtext" align="left">
                 <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_UserName%>" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:TextBox ID="txtWhygoUsr" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RglrvalidatorWhygo12" ControlToValidate="txtUsrRptCustmName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
         <tr runat="server" id="trWhygoPassword">
             <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Password%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtWhygoPwd" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(5,1)" onfocus=" fnTextFocus(this.id,1,5)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regtxtWhygoPwd" Enabled="false"
                    ControlToValidate="txtWhygoPwd" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="cmptxtWhygoPwd" Display="dynamic" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtWhygoPwd" ControlToCompare="txtWhygoPwd2"
                    ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>" Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" />
            </td>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_RetypePassword%>" runat="server"></asp:Literal>
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtWhygoPwd2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(5,2)" onfocus=" fnTextFocus(this.id,2,5)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regtxtWhygoPwd2" Enabled="false"
                    ControlToValidate="txtWhygoPwd2" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="cmptxtWhygoPwd2" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtWhygoPwd2" ControlToCompare="txtWhygoPwd" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
         <tr runat="server" id="trPoll" >
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Poll%>" runat="server"></asp:Literal></td>
            <td style="height: 21px;">
                <asp:Button ID="PollWhyGo" runat="server" style="width:100px;" ValidationGroup="submit" Enabled="false" CssClass="altShortBlueButtonFormat" Text="<%$ Resources:WebResources, SuperAdministrator_PollWhyGo%>" OnClick="PollWhyGoNow"/>  <%-- OnClick="PollWhyGo"--%>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delPubliRoom" AlternateText="Remove"
                                        ToolTip="<%$ Resources:WebResources, RemovePublicRooms%>" OnClick="DeletePublicRooms" />
            </td>
            <%--ZD 100694 start--%>
            <td class="blackblodtext" align="left">
                <asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, AdminEmailAddress%>" runat="server"></asp:Literal>
            </td>
            <td style="height: 21px;">
            <asp:TextBox ID="txtWhyGoAdminEmail" runat="server" class="altText" Width="200px" ></asp:TextBox>
            <asp:RegularExpressionValidator ID="regWhyGoAdminEmail1" ControlToValidate="txtWhyGoAdminEmail" Display="dynamic" runat="server" 
             ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="regWhyGoAdminEmail" ControlToValidate="txtWhyGoAdminEmail" Display="dynamic" runat="server" SetFocusOnError="true" 
             ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
            </td>
            <%--ZD 100694 End--%>
        </tr>
        <%--ZD 100152 Starts--%>
        <tr id="tr1" runat="server">
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, GoogleConfiguration%>" runat="server"></asp:Literal></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr runat="server">
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                <asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, ClientID%>" runat="server"></asp:Literal>
            </td>
            <td style="height: 21px;" width="30%">
             <asp:TextBox ID="txtGoogleClientID" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                    CssClass="altText" TextMode="Password" Width="200px" onblur="PasswordChange(6,1)" onfocus=" fnTextFocus(this.id,1,6)"></asp:TextBox>
                
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
               <asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, SecretId%>" runat="server"></asp:Literal> 
            </td>
            <td style="height: 21px;" width="30%">
                 <asp:TextBox ID="txtGoogleSecretID" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                    CssClass="altText" TextMode="Password" Width="200px" onblur="PasswordChange(7,1)" onfocus=" fnTextFocus(this.id,1,7)"></asp:TextBox>
            </td>
          
        </tr>
        <tr runat="server">
            <td align="left">
            </td>
            <td align="left" style="font-weight: bold" width="18%" class="blackblodtext">
                <asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, APIKey%>" runat="server"></asp:Literal>
            </td>
            <td style="height: 37px;" width="30%" nowrap="nowrap">
                 <asp:TextBox ID="txtGoogleAPIKey" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                    CssClass="altText" TextMode="Password" Width="200px" onblur="PasswordChange(8,1)" onfocus=" fnTextFocus(this.id,1,8)"></asp:TextBox>
            </td>  
             <td align="left" style="font-weight: bold" width="18%" class="blackblodtext">
                <asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, PushtoGoogle%>" runat="server"></asp:Literal>
            </td>
            <td width="35%">
                <asp:CheckBox ID="chkPushtoGoogle" runat="server" />   
            </td>
             
         </tr>
        <%--ALLDEV-856 Start--%>
        <tr runat="server">
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                <asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, PollCount%>" runat="server"></asp:Literal>
            </td>
            <td style="height: 21px; padding-bottom: 10px;" width="30%"  >
             <asp:TextBox ID="txtPollCount" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
             <asp:RegularExpressionValidator ID="RegExpPollCount" runat="server" ControlToValidate="txtPollCount" ValidationGroup="submit" 
                 Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="[\d]+" ></asp:RegularExpressionValidator>
            </td>
             <td align="left" height="21" style="font-weight: bold" class="blackblodtext" nowrap="nowrap"> 
                <asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_GoogleIntegration%>" runat="server"></asp:Literal> 
            </td>
            <td style="height: 21px;">
                <asp:DropDownList ID="drpGoogleIntegration" runat="server" Width="30%" CssClass="alt2SelectFormat" onchange="JavaScript:EnableGoogleIntegration()">
                    <asp:ListItem Value="0" Text="<%$ Resources:WebResources, SuperAdministrator_oneway%>"></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, SuperAdministrator_twoway%>"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trGoogleIntegration">
           <td align="left" height="21" style="font-weight: bold" width="2%">
           </td>
           <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
               <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, AdminEmailaddress%>" runat="server"></asp:Literal> 
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtAdminEmailAddress" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                 <asp:RegularExpressionValidator ID="RegExpAdminEmailAddress" is="regSystemEmail"
                    runat="server" ControlToValidate="txtAdminEmailAddress" ValidationGroup="submit" ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>"
                    Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--ALLDEV-856 ends--%>
        <%--ZD 100152 ends--%>
        <%-- FB 2392 - End--%>    
        <tr>
            <%--  Commented for FB 1849
            <td colspan="5" align="center"> 
            <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"   PopupControlID="switchOrgPnl" DropShadow="false" Drag="true"  CancelControlID="ClosePUp"></ajax:ModalPopupExtender>
            <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="35%"  CssClass="treeSelectedNode" >
                <table width="100%" align="center" border="0"> 
                    <tr>
                      <td align="center" class="blackblodtext"><SPAN class=subtitleblueblodtext>Switch Organization</SPAN><br />
                      <p>
                      All the transactions performed will be for the below selected organization after the switch.
                     </p>
                      </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center"> 
                        <asp:DropDownList ID="DrpOrganization" DataTextField="OrganizationName" DataValueField="OrgId" runat="server" CssClass="altLong0SelectFormat" Width="205px">
                        </asp:DropDownList>
                      </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                     <asp:button runat="server" ID="BtnChangeOrganization" CssClass="altShortBlueButtonFormat" Text=" Submit " OnClientClick="javascript:return fnChangeOrganization();" OnClick="btnChgOrg_Click" ></asp:button>&nbsp;
                     <input align="middle" type="button" runat="server" style="width:100px;height:21px;" id="ClosePUp" value=" Close " class="altButtonFormat" onclick="javascript:fnClearOrg();" />
                      </td>
                    </tr>
                </table>
              </asp:Panel>
            </td>--%>
        </tr>
        <tr>
            <td align="center" colspan="5" style="font-weight: bold; font-size: small; color: black;
                font-family: verdana;" height="21" style="font-weight: bold">
                <%--<input id="btnReset" type="reset" value="Reset" class="altMedium0BlueButtonFormat" />&nbsp;--%><%--ZD 100420 --%>
                <button type="reset" id="btnReset" class="altMedium0BlueButtonFormat"  ><asp:Literal Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal> </button>&nbsp;<%--ZD 100420 --%>
              <asp:Button runat="server" ID="btnSubmit" CssClass="altMedium0BlueButtonFormat" OnClick="btnSubmit_Click" OnClientClick="javascript:DataLoading('1'); fnCheckConfID();"
                 Text="<%$ Resources:WebResources, SuperAdministrator_btnSubmit%>" ValidationGroup="submit"></asp:Button> <%--ZD 100176 --%><%--ZD 100420 --%><%--ZD 100526 ZD 101011--%>
            </td>
        </tr>
    </table>
    <input type="hidden" name="formname" id="formname" value="frmMainsuperadministrator" />
    <img src="keepalive.asp" name="myPic" alt=Keepalive" width="1" height="1" style="display:none"> <%--ZD 100419--%>

    <script language="javascript">

        //FB 2363
        function fnView() {
            window.location = 'ESEventReport.aspx';
            return false;//ZD 100420
        }

        function fnTransferPage() {
            window.location = 'UITextChange.aspx';
        }

        /*Commented for FB 1849
        function fnClearOrg() 
        {
        var obj1 = document.getElementById('DrpOrganization');
        if(obj1)
        {
        obj1.value = '<%=orgid%>';
        }        
        }*/
        function Validation() {
            var file1 = document.getElementById('fleMap1');
            var msg = document.getElementById('txtCompanymessage');
            var lblval = document.getElementById('lblvalidation');
            var lblfile = document.getElementById('lblfilevalidation');
            if (file1.value == "") {
                lblval.value = "Required";
                return false;
            }
            else if (msg.value == "") {
                lblfile.value = "Required";
                return false;
            }
            else
                return true;

        }

//        SavePassword(); //FB 1896
    </script>

    </form>
    <%--FB 2659 Starts--%>
    <div id="PopupDefaultLicense" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 1000px; display: none;">    
    <iframe src="" id="DefaultLicense" name="DefaultLicense" style="height: 530px; border: 0px; overflow:hidden; width: 1000px; overflow: hidden;"></iframe>
	</div>
	<%--FB 2659 End--%>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->

<script type="text/javascript">

    function refreshImage() {
        var obj = document.getElementById("mainTop");
        if (obj != null) {
            var src = obj.src;
            var pos = src.indexOf('?');
            if (pos >= 0) {
                src = src.substr(0, pos);
            }
            var date = new Date();
            obj.src = src + '?v=' + date.getTime();

            if (obj.width > 804)
                obj.setAttribute('width', '804');
        }
        //refreshStyle(); // Commented for Refresh Issue
        setMarqueeWidth();
        return false;
    }

    function refreshStyle() {
        var i, a, s;
        a = document.getElementsByTagName('link');
        for (i = 0; i < a.length; i++) {
            s = a[i];
            if (s.rel.toLowerCase().indexOf('stylesheet') >= 0 && s.href) {
                var h = s.href.replace(/(&|\\?)forceReload=d /, '');
                s.href = h + (h.indexOf('?') >= 0 ? '&' : '?') + 'forceReload=' + (new Date().valueOf());
            }
        }
    }

    function setMarqueeWidth() {
        var screenWidth = screen.width - 25;
        if (document.getElementById('martickerDiv') != null)
            document.getElementById('martickerDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticDiv') != null)
            document.getElementById('marticDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticker2Div') != null)
            document.getElementById('marticker2Div').style.width = (screenWidth - 15) + 'px';

        if (document.getElementById('martic2Div') != null)
            document.getElementById('martic2Div').style.width = (screenWidth - 15) + 'px';
    }



    window.onload = refreshImage;

    document.getElementById("reglstLDAPScheduleTime").controltovalidate = "lstLDAPScheduleTime_Text"; //FB 2462
    document.getElementById("reqlstLDAPScheduleTime").controltovalidate = "lstLDAPScheduleTime_Text"; //FB 2462
    
    //ZD 100284
    if (document.getElementById("lstLDAPScheduleTime_Text")) {
        var lstLDAPScheduleTime_Text = document.getElementById("lstLDAPScheduleTime_Text");
        lstLDAPScheduleTime_Text.onblur = function() {
        formatTimeNew('lstLDAPScheduleTime_Text', 'reglstLDAPScheduleTime',"<%=Session["timeFormat"]%>")
        };
    }

    //ALLDEV-856 Start
    function EnableGoogleIntegration() {
        if (document.getElementById("drpGoogleIntegration").value == "0") {
            document.getElementById("trGoogleIntegration").style.visibility = 'visible';
        }
        else {
            document.getElementById("trGoogleIntegration").style.visibility = 'hidden';
        }
    }
    //ALLDEV-856 End
    
</script>

<!-- FB 2050 End -->

<script type="text/javascript">
//FB 2659 Start
    $(document).ready(function() {        
        $('#btnDefaultClick').click(function() {            
            $('#popupdiv').fadeIn();
            $("#DefaultLicense").attr("src", "DefaultLicense.aspx");
            $('#PopupDefaultLicense').show();
            $('#PopupDefaultLicense').bPopup({
                fadeSpeed: 'slow',
                followSpeed: 1500,
                modalColor: 'gray'
            });
        });
});
//FB 2659 End
//ZD 101443 start
if (document.getElementById("drpTenantOption").value == "2") {
    document.getElementById("chkSSOMode").checked = true;
    document.getElementById("chkSSOMode").disabled = true;
}
else {
    document.getElementById("chkSSOMode").disabled = false;
}
//ZD 101443 End
//ZD 100420 - Start
if(document.getElementById('TxyButton')!=null)
{
    document.getElementById('lblLicenseDetails').setAttribute("onblur", "document.getElementById('TxyButton').focus()");
    document.getElementById('TxyButton').setAttribute("onfocus", "");
}
document.getElementById('btnuser').setAttribute("onblur", "document.getElementById('BtnBrowse1').focus()");
document.getElementById('BtnBrowse1').setAttribute("onfocus", "");
//ZD 101028 start
//document.getElementById('BtnBrowse1').setAttribute("onblur", "document.getElementById('btnRemoveMap1').focus()");
//document.getElementById('btnRemoveMap1').setAttribute("onfocus", "");
document.getElementById('btnRemoveMap1').setAttribute("onblur", "document.getElementById('btnResetvation').focus()");
document.getElementById('btnResetvation').setAttribute("onfocus", "");
document.getElementById('btnResetvation').setAttribute("onblur", "document.getElementById('BtnBrowse2').focus()");
document.getElementById('BtnBrowse2').setAttribute("onfocus", ""); //ZD 101028 End
document.getElementById('BtnBrowse2').setAttribute("onblur", "document.getElementById('btnRemoveMap4').focus()");

document.getElementById('btnRemoveMap4').setAttribute("onfocus", "");
if (document.getElementById('btnDefaultClick') != null) {
    document.getElementById('drpCloudInstallation').setAttribute("onblur", "document.getElementById('btnDefaultClick').focus()");
    document.getElementById('btnDefaultClick').setAttribute("onfocus", "");
}
if (document.getElementById('PollWhyGo') != null) {
    document.getElementById('txtWhygoPwd2').setAttribute("onblur", "document.getElementById('PollWhyGo').focus()");
    document.getElementById('PollWhyGo').setAttribute("onfocus", "");
}
document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSubmit').focus()");
document.getElementById('btnSubmit').setAttribute("onfocus", "");

$('input[type="text"]').keypress(function(e) {
    var code = e.keyCode || e.which;
    if (code == 13)
        e.preventDefault();
});

$('select').keypress(function(e) {
    var code = e.keyCode || e.which;
    if (code == 13)
        e.preventDefault();
});

$('input[type="password"]').keypress(function(e) {
    var code = e.keyCode || e.which;
    if (code == 13)
        e.preventDefault();
});

//ZD 100420 - Start
</script>