<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Workorder.WorkorderDetails" ValidateRequest="false"  %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<%--ZD 101314 Start--%>
  <%--  <% 
    
if (Request.QueryString["hf"] != null)
if (!Request.QueryString["hf"].ToString().Equals("1"))
   { %>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<% }
   else
   {
%>
    <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<%} %>

<% 
    
if (Request.QueryString["hf"] != null)
if (Request.QueryString["hf"].ToString().Equals("1"))
   { %>
        <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<%} %>--%>

<%--ZD 101314 End--%>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 101388--%>
<script type="text/javascript" src="script/DisplayDIV.js"></script>
<html>
<head>
    <title>myVRM</title> <%--ZD 101314--%>
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>
    <script type="text/javascript" src="script/mousepos.js"></script>
    <script type="text/javascript" src="inc/functions.js"></script> <%--FB 1830--%>
    

<script language="javascript">

    function ShowItems(obj)
    {
        //alert(document.getElementById("tblMenuItems").innerHTML);
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        //Window Dressing
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY><tr class='tableHeader'><td height='25'><font class='tableHeader'>Items list</font></td></tr>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.border = "1";
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
       document.getElementById("tblMenuItems").innerHTML = "";
    }

    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        //getMouseXY();
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'fixed';
        document.getElementById("divPic").style.left = mousedownX + 20 + 'px';
        document.getElementById("divPic").style.top = mousedownY + 'px';
        document.getElementById("divPic").style.display = "block";
        document.getElementById("divPic").style.zIndex = 999; //FB 2214
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }
    
    function pdfReport() //fogbugz case 38
    {
        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("ViewWorkorderDetails.aspx"));

        var htmlString = document.getElementById("tblMain").innerHTML;
        //remove export to pdf button from PDF
        toBeRemoved = document.getElementById("tdButtons");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
            
        //WO Bug Fix - start
        var txtType = document.getElementById("txtType");
        
        if(txtType != null)
        {
          if(txtType.value == "2")
          {
            toBeRemoved = document.getElementById("tblNCater");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
          else
          {
            toBeRemoved = document.getElementById("tblCatering");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
        }  
        //WO Bug Fix - end
        //toBeRemoved = document.getElementById("btnEXCEL");
        //if (toBeRemoved != null)
        //    htmlString = htmlString.replace(toBeRemoved.parentElement.outerHTML, "");
        loc = "http://localhost" + loc.replace(loc.substring(0, loc.indexOf("/", 8)), "");   //fogbugz case 386 Saima
        loc = ""; //Image Project
        //replace all doube " with single '        
        //htmlString = htmlString.replace(new RegExp("\"","g"), "'"); //COmmented for FB 1888
        //remove all image source relative paths with the absolute path
        htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");
        //FB 1830
        htmlString = htmlString.replace(new RegExp("�","g"),"  �");
        //insert the banner on top of page and style sheet on top.
        //htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + "mirror/styles/main.css' /><body><center><table><tr><td><img src='" + loc + "mirror/image/lobbytop1600.jpg' width='100%' height='72'></td></tr></table>" + htmlString + "</center></body></html>";
        if (document.getElementById("tempText") != null)
        {
            document.getElementById("tempText").value = "";
            document.getElementById("tempText").value = htmlString;
        }
    }

    function ExportToExcel()
    {
        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("ViewWorkorderDetails.aspx"));
        var htmlString = document.getElementById("tblMain").innerHTML;
        //remove export to pdf button from PDF
        toBeRemoved = document.getElementById("btnPDF");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, "");//edited for FF
        toBeRemoved = document.getElementById("btnEXCEL");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, ""); //FB 1981

        toBeRemoved = document.getElementById("btnClose");//ZD 101314
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, ""); //FB 1981
        
        //WO Bug Fix - start
        var txtType = document.getElementById("txtType");
        
        if(txtType != null)
        {
          if(txtType.value == "2")
          {
            toBeRemoved = document.getElementById("tblNCater");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
          else
          {
            toBeRemoved = document.getElementById("tblCatering");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
        }  
        //WO Bug Fix - end
        
        //replace all doube " with single '        
        //htmlString = htmlString.replace(new RegExp("\"","g"), "'"); //COmmented for FB 1888
        //remove all image source relative paths with the absolute path
//        htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");//Image Project
        htmlString = htmlString.replace(new RegExp("image/","g"), "image/");//Image Project
        //insert the banner on top of page and style sheet on top.
        //FB 1830
        //htmlString = htmlString.replace(/.00/g,".00&nbsp;"); //FB 1686
        htmlString = "<html><body>" + htmlString + "</body></html>";
        htmlString = htmlString.replace(" style='BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; FONT-WEIGHT: normal; FONT-SIZE: x-small; BORDER-LEFT: black 2px solid; WIDTH: 95%; BORDER-BOTTOM: black 2px solid; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse' borderColor=black cellSpacing=0 border=0", " border=1");
        htmlString = htmlString.replace(" style='COLOR: yellow; HEIGHT: 30px; BACKGROUND-COLOR: black'", "");
        //htmlString = htmlString.replace(" style='COLOR: yellow; HEIGHT: 30px; BACKGROUND-COLOR: black'", "");
        
        
        //alert(document.getElementById("dgItems").innerHTML.replace("style=\"COLOR: yellow; HEIGHT: 30px; BACKGROUND-COLOR: BLACK\"", ""));
        if (document.getElementById("tempText") != null)
            document.getElementById("tempText").value = htmlString;
        return false;
    }
</script>
</head>
<body>

    <form id="frmWorkorderDetails" runat="server" method="post" onsubmit="return true">
      <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
          <input type="hidden" id="helpPage" value="40">

    <div>
        <table width="100%" id="tblMain">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label id="lblType" runat="server" text="">
                        </asp:Label><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_WorkorderDetai%>" runat="server"></asp:Literal></h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Font-Size="Small" ForeColor="Red" Visible="False" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="txtType" runat="server" Width="0px" BackColor="transparent" BorderColor="transparent" BorderStyle="none" style="display:none;" BorderWidth="0px" ForeColor="transparent" ></asp:TextBox>
                    <asp:DropDownList ID="lstServices" Visible="false" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadCateringServices"></asp:DropDownList>  <%--OnLoad="GetServices"--%>                    
                </td>
            </tr>
            <tr>
                <td align="right" id="tdButtons" class="btprint">
				<%--ZD 100429--%>
                    <button id='btnClose' onclick='javascript:window.close();' class='altMedium0BlueButtonFormat'><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal></button><%--ZD 101314--%>
                    <asp:ImageButton ID="btnPrint" AlternateText="Printer" ImageUrl="Image/print.gif" OnClientClick="javascript:window.print();return false;" Text="<%$ Resources:WebResources, ViewWorkorderDetails_btnPrint%>" runat="server" ToolTip="<%$ Resources:WebResources, ViewWorkorderDetails_btnPrint%>"></asp:ImageButton> <%--FB 3024--%>  <%--ZD 100419--%>
                    <asp:ImageButton ID="btnPDF" AlternateText="Export To PDF" runat="server" ImageUrl="Image/adobe.gif" OnClick="ExportToPDF" OnClientClick="javascript:pdfReport();" Text="<%$ Resources:WebResources, ViewWorkorderDetails_btnPDF%>" ToolTip="<%$ Resources:WebResources, ViewWorkorderDetails_btnPDF%>" ></asp:ImageButton><%--FB 3024--%> <%--ZD 100419--%>
                    <asp:ImageButton ID="btnEXCEL" AlternateText="Export to Excel" runat="server" ImageUrl="Image/excel.gif" OnClientClick="javascript:ExportToExcel()" Text="<%$ Resources:WebResources, ViewWorkorderDetails_btnEXCEL%>" OnClick="ExportToEXCEL" ToolTip="<%$ Resources:WebResources, ViewWorkorderDetails_btnEXCEL%>"></asp:ImageButton><%----%><%--FB 3024--%> <%--ZD 100419--%>
                </td>
            </tr>
            <tr id="trNonCatering" runat="server">
                <td align="center">
                  <table width="100%" bgcolor="blue" cellspacing="1" cellpadding="0">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" id="tblNCater">
                                <tr height="30">
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Name%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Rooms%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left" visible="false" id="lablRMLO" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_lablRMLO%>" runat="server"></asp:Literal></td> <%--FB 2214--%>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Startby%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Completedby%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Timezone%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left" id="trDC" runat="server" visible="false"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_trDC%>" runat="server"></asp:Literal><br /> (<%=currencyFormat %>)</td> <%--FB 1830--%>
                                    <td class="tableHeader" align="left" id="trSC" runat="server" visible="false"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_trSC%>" runat="server"></asp:Literal><br /> (<%=currencyFormat %>)</td> <%--FB 1830--%>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Status%>" runat="server"></asp:Literal></td>
                                    <td class="tableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>" runat="server"></asp:Literal></td>
                                </tr>
                                <tr height="50">
                                    <td class="tableBody" align="left"><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblPersonInCharge" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblRooms" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblStartBy" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblCompletedBy" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblTimezone" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left" id="trDC1" runat="server" visible="false"><asp:Label ID="lblDeliveryCost" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left" id="trSC1" runat="server" visible="false"><asp:Label ID="lblServiceCharges" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblComments" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="10" height="20"></td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <asp:DataGrid ID="dgItems"  runat="server" AutoGenerateColumns="False" BackColor="#e1e1e1"
                                        GridLines="Vertical" Width="95%" Visible="true" BorderColor="blue" BorderStyle="Solid" BorderWidth="1" CellPadding="5" CellSpacing="0" OnItemDataBound="BindImages">
                                            <%--Window Dressing - Start--%>
                                            <FooterStyle CssClass="tableBody" />
                                            <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                            <EditItemStyle CssClass="tableBody" />
                                            <AlternatingItemStyle CssClass="tableBody" />
                                            <ItemStyle CssClass="tableBody" />
                                            <HeaderStyle BorderColor="blue" CssClass="tableHeader" BorderStyle="solid" BorderWidth="1" />
                                            <%--Window Dressing - End--%>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="False" ><HeaderStyle CssClass="tableHeader" BorderColor="blue" BorderStyle="solid" BorderWidth="1" HorizontalAlign="Left" Height="30px" /></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName1" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server" Visible='<%# !DataBinder.Eval(Container, "DataItem.QuantityRequested").ToString().Equals("0") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
<%--                                                <asp:BoundColumn DataField="Name" HeaderText="Name"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>--%> 
                                                <asp:BoundColumn DataField="SerialNumber" HeaderText="<%$ Resources:WebResources, SerialNo%>" ItemStyle-HorizontalAlign="Left"><HeaderStyle BorderColor="blue" BorderStyle="solid" BorderWidth="1" HorizontalAlign="Left" CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, EditInventory_tdHImage%>" ItemStyle-CssClass="tableBody"  HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderStyle BorderColor="blue" BorderStyle="solid" BorderWidth="1" CssClass="tableHeader" Height="30px" />
                                                    <ItemTemplate >
                                                       <%--<cc1:ImageControl id="imgItem" Width="30" Height="30"   Runat="server"></cc1:ImageControl>--%>
                                                       <asp:Image ID="imgItem"  AlternateText="Image Item" visible="true" Width="30" Height="30" runat="server" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" /> <%--ZD 100419--%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn> 
                                               
                                                <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody"   HeaderText="<%$ Resources:WebResources, InventoryManagement_Comments%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" ItemStyle-HorizontalAlign="Left"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <%--<asp:BoundColumn DataField="Quantity" HeaderText="Quantity in hand"><HeaderStyle Font-Bold="true" BackColor="black" ForeColor="yellow" Height="30px" /></asp:BoundColumn>--%>                                                
                                                <asp:BoundColumn DataField="DeliveryCost" Visible="false" ItemStyle-CssClass="tableBody"   HeaderText="<%$ Resources:WebResources, EditConferenceOrder_DeliveryCost%>" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ServiceCharge" Visible="false" ItemStyle-CssClass="tableBody"   HeaderText="<%$ Resources:WebResources, EditConferenceOrder_ServiceCharge%>" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Price" ItemStyle-CssClass="tableBody"  HeaderText="<%$ Resources:WebResources, ViewWorkorderDetails_PriceUSD%>" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                  <asp:TemplateColumn ItemStyle-VerticalAlign="bottom" ItemStyle-CssClass="tableBody"   HeaderText="<%$ Resources:WebResources, RequestedQuantity%>" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                                     </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ItemCost" ItemStyle-CssClass="tableBody"   HeaderText="<%$ Resources:WebResources, ItemTotal%>" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, EquipmentID%>" ItemStyle-CssClass="tableBody"   HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderStyle  CssClass="tableHeader" Height="30px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSignature" Text="_______________" runat="server" ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="10" height="20"></td>
                                </tr>
                                <tr>
                                    <td colspan="10" align="center" class="subtitleblueblodtext" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_TotalCost%>" runat="server"></asp:Literal> (<%=currencyFormat %>): &nbsp;&nbsp;<asp:Label ID="lblTotalCharges" runat="server"></asp:Label></td>
                                </tr>
                           </table>
                        </td>
                    </tr>
                  </table>
                </td>
            </tr>
            <tr id="trCatering" runat="server">
                <td>
                    <table width="100%" cellspacing="0" cellpadding="2" >
                        <tr> 
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" id="tblCatering">                        
                                    <tr class="tableHeader">
                                        <%--Window Dressing Start--%>
                                        <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Name%>" runat="server"></asp:Literal></td>
                                        <td class="tableHeader"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ViewWorkorderDetails_Personincharge%>" runat="server"></asp:Literal></td>
                                        <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Room%>" runat="server"></asp:Literal></td>
                                        <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_ServiceType%>" runat="server"></asp:Literal></td>
                                        <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_DeliveryByDat%>" runat="server"></asp:Literal></td>
                                        <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_PriceUSD%>" runat="server"></asp:Literal></td>     <%-- FB 1686 -- ALL $ changed to USD in this Page--%>
                                        <td class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Comments%>" runat="server"></asp:Literal></td>
                                        <td>
                                            <table width="100%" cellpadding="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="75%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Items%>" runat="server"></asp:Literal></td>
                                                    <td width="25%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Quantity%>" runat="server"></asp:Literal></td>
                                                </tr>
                                        <%--Window Dressing End--%>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="tableBody" valign="top"><%--FB 2508--%>
                                        <td width="200px"><asp:Label ID="lblCateringName" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="120px"><asp:Label ID="lblCatAdminName" runat="server" CssClass="tableBody"></asp:Label></td><%--ZD 100757--%>
                                        <td width="110px"><asp:Label ID="lblCateringRoomName" runat="server" CssClass="tableBody" ></asp:Label></td>
                                        <td width="125px"><asp:Label ID="lblCateringServiceType" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="135px"><asp:Label ID="lblDeliverByDateTime" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="100px"><asp:Label ID="lblPrice" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="100px"><asp:Label ID="lblPComments" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td>
                                            <asp:DataGrid ID="dgMenus" BorderWidth="0" BorderStyle="none" CellPadding="2" CellSpacing="0" AutoGenerateColumns="false" ShowHeader="false" runat="server" Width="100%">
                                            <ItemStyle VerticalAlign="top" HorizontalAlign="left" />
                                            <AlternatingItemStyle VerticalAlign="top" HorizontalAlign="left" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn ItemStyle-Width="75%">
                                                        <ItemTemplate>
                                                            <a href="#"><asp:Label ID="lblMenuName" onmouseover="javascript:ShowItems(this)" onmouseout="javascript:HideItems()" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server"></asp:Label></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Quantity" ItemStyle-Width="25%"></asp:BoundColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:DataGrid ShowHeader="false" ID="dgMenuItems" Width="100px" runat="server" AutoGenerateColumns="false" >
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="50">&nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                   <table width="100%" cellpadding="10" cellspacing="4">
                        <tr>
                            <%--Window Dressing Start--%>
                            
                            <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_PickupSignatur%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                ______________________
                            </td>
                            <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Date%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                __________
                            </td>
                            <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_ReturnSignatur%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                ______________________
                            </td>
                            <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ViewWorkorderDetails_Date%>" runat="server"></asp:Literal></td>
                            <%--Window Dressing End--%>
                            <td align="left">
                                __________
                            </td>
                        </tr>
                   </table>
                </td>
            </tr>
        </table>
        
        <div id="tblMenuItems" style="display:none">
            
        </div>
        <br /><br /><%--WO Bug Fix--%>
        <input type="hidden" id="tempText" runat="server" /> 
        <%--<asp:TextBox ID="tempText" runat="server" TextMode="MultiLine" width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>--%>
        <asp:DropDownList ID="lstTimezones" Visible="false" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
     </div>
<div id="divPic" style="display:none;  z-index:1;">
    <img src="" name="myPic" id="myPic" width="200" height="200"> <%--Edited for FF--%>
</div>
<script language="javascript">
    if (document.getElementById("txtType").value == "2")
    {
        document.getElementById("trNonCatering").style.display="none";
        document.getElementById("trCatering").style.display="";
    }
    else
    {
        document.getElementById("trNonCatering").style.display="";
        document.getElementById("trCatering").style.display="none";
    }

    //FB 1830
    changeCurrencyFormat("dgItems",'<%=currencyFormat %>');
    changeCurrencyFormat("tblCatering",'<%=currencyFormat %>');

</script>
 </form>
 
</body></html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>

<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
    <%-- #INCLUDE FILE="inc/mainbottomNET.aspx"  --%><%--ZD 101314--%>
