/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
function DataLoading(val)
{
//        getMouseXY();
    if (document.getElementById("dataLoadingDIV") == null)
        return false;
        //document.getElementById("dataLoadingDIV").style.position = 'absolute'; //FB 2814
        document.getElementById("dataLoadingDIV").style.left = window.screen.width/2 - 100;
        document.getElementById("dataLoadingDIV").style.top = 100;

    if (val=="1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}

function isIPISDN(obj)
{
	var allValid = true;
	//var checkOK = "0123456789.";	
	var checkOK = "0123456789:,\";@.'!#"; //FB 1972 FB 2267
	var str = obj.value;
	for (k = 0;  k < str.length;  k++)
		{
			ch = str.charAt(k);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
				break;
			if (j == checkOK.length)
			{
				alert("Invalid character.");
				obj.value = obj.value.substring(0,str.length-1);
				return false;
			}
		}
		//alert();
	return true;
}

function isNumeric(str)
{
	
	var allValid = true;
	var checkOK = "0123456789";
	for (k = 0;  k < str.length;  k++)
		{
			ch = str.charAt(k);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
				break;
			if (j == checkOK.length)
			{
				return false;
			}
		}
	return true;
}

function isAlphanumeric(str)
{
	
	var allValid = true;
	var checkOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ";
	for (k = 0;  k < str.length;  k++)
		{
			ch = str.charAt(k);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
				break;
			if (j == checkOK.length)
			{
				return false;
			}
		}
		//alert();
	return true;
}

function chkLimit(obj, tpe)
{
    var spCharacters = "";
//	if (obj.type == "textarea")
    //	{
    var spCharacters = "& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $  ~ and \"";  //FB 1888
    if (obj.id == "addPartyFname" || obj.id == "addPartyLname") //FB 1888
    {
        var specialCharacters = new Array("&", "<", ">", "+", "%", "/", "\\", "\(", "\)", "?", "|", "^", "=", "!", "`", "[", "]", "{", "}", "#", "$", "~");
        spCharacters = "& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ and ~";
    }
    else
        var specialCharacters = new Array("&", "<", ">", "'", "+", "%", "/", "\"", "\\", "\(", "\)", ";", "?", "|", "^", "=", "!", "`", ",", "[", "]", "{", "}", ":", "#", "$", "~");
        
	if (tpe == "e") // FB case 505, 457
	{
        specialCharacters = new Array("&", "<", ">", "'", "+", "%", "\"", "\\", ";", "?", "|", "=", "!", "`", ",", "[", "]", "{", "}", ":", "#", "$", "~");
	    spCharacters = "& < > ' + % \ ; ? | = ! ` , [ ] { } : # $ ~ and \"";
	}
	else if (tpe == "u") //FB 615 & 616
	{
	    specialCharacters[8] = "&"; // 8 is the character "\" - BackSlash
	    tpe = "2";
	}
//	}
//	else
//	{
//		var specialCharacters = new Array("&", "<", ">", "'", "+", "%", "/", "\"", "\(", "\)", ";");
//		spCharacters = "& < > ' + % \" \( \)";
//    }
	var temp="";	
//	if (len <= 512)
	for (var i=0;i<specialCharacters.length;i++) {
//		alert(specialCharacters[i]);
		if (obj.value.indexOf(specialCharacters[i])!= -1 )
		{
			//temp = "'" + specialCharacters[i] + "', " + temp;
			alert( specialCharacters[i] + " is an Invalid Character for this field. Removed from the input.\r\n" + spCharacters + " characters are invalid.");
			obj.value = obj.value.replace(specialCharacters[i], "");
			//return false;
		}
	}
	//if (temp != "") alert( temp + " are Invalid Character(s) for this field. Removed from the input.");
// 1 lmtSvrAddr = 15
// 2 lmtFLName = 256
// 3 lmtMessage = 4000
// 4 lmtLogin = 256
// 5 lmtPWD = 256
// 6 lmtAddress = 512
// 7 lmtPhone = 20
// 8 lmtDesc = 2000
// 9 lmtImage = 500
// 10 lmtIPISDN = 512
// 11 lmtSecKey = 256
// 12 lmtURL = 512
// 13 lmtMenuMask = 200
// 14 lmtEmail = 512
// 15 lmtSubject = 2000
// 16 lmtLicKey = 8000
	switch(tpe) {
	case "1":
		len = 14;
		break;
	case "2":
		len = 255;
		break;
	case "3":
		len=3999;
		break;
	case "4":
		len = 255;
		break;
	case "5":
		len = 255;
		break;
	case "6":
		len = 511;
		break;
	case "7":
		len = 19;
		break;
	case "8":
		len = 1999;
		break;
	case "9":
		len = 499;
		break;
	case "10":
		len = 512;
		break;
	case "11":
		len = 255;
		break;
	case "12":
		len = 511;
		break;
	case "13":
		len = 199;
		break;
	case "14":
		len = 511;
		break;
	case "15":
		len = 1999;
		break;
	case "16":
		len = 7999;
		break;
	case "17":
		len = 29;
		break;
	case "e":
	    len = 255;
	    break;
	default:
		len = tpe;
	}										
		
	if (obj.value.length >= len)
	{
		alert("You have reached the maximum limit allowed for this field, " + (parseInt(len,10) + parseInt(1,10)) + " characters. The rest has be truncated."); // ZD 101722
		obj.value = obj.value.substring(0,len+1);
	}
}
//FB 2339 Start
function chkloginLimit(obj, tpe)
{
    var spCharacters = "";
    var spCharacters = "& < and > ";  
    var specialCharacters = new Array("&", "<", ">");
        
	var temp="";	
	for (var i=0;i<specialCharacters.length;i++) {	
		if (obj.value.indexOf(specialCharacters[i])!= -1 )
		{
			alert( specialCharacters[i] + " is an Invalid Character for this field. Removed from the input.\r\n" + spCharacters + " characters are invalid.");
			obj.value = obj.value.replace(specialCharacters[i], "");
			
		}
	}
	
	len = 255;									
		
	if (obj.value.length >= len)
	{
		alert("You have reached the maximum limit allowed for this field, " + (parseInt(len,10) + parseInt(1,10)) + " characters. The rest has be truncated."); // ZD 101722
		obj.value = obj.value.substring(0,len+1);
	}
}
//FB 2339 End
function chkLimit1(obj, len)
{
	var specialCharacters = new Array("&", "<", ">", "'", "+", "%", "/", "\"", "\\", "\(", "\)", "-", ";");
	var temp="";	
//	if (len <= 512)
	for (var i=0;i<specialCharacters.length;i++) {
//		alert(specialCharacters[i]);
		if (obj.value.indexOf(specialCharacters[i])!= -1 )
		{
			//temp = "'" + specialCharacters[i] + "', " + temp;
			alert( specialCharacters[i] + " is an Invalid Character for this field. Removed from the input.");
			obj.value = obj.value.replace(specialCharacters[i], "");
			//return false;
		}
	}
	//if (temp != "") alert( temp + " are Invalid Character(s) for this field. Removed from the input.");

									
		
	if (obj.value.length > len)
	{
		alert("You have reached the maximum limit allowed for this field, " + parseInt(len,10)+1 + " characters."); // ZD 101722
		obj.value = obj.value.substring(0,len+1);
		return false;
	}
	return true;
}
// Remove both leading and trailing spaces from a string
function Trim(str)
{
	while (str.charAt(0)==' ') {
		str = str.slice(1)
	}
	
	while (str.charAt(str.length-1)==' ') {
		str = str.slice(0, -1)
	}
	
	return (str);
}


// transfer vlaue, return suc/fail
// frmform, toform : form name; mod: 0-opener/1-parent; fld: fldname
function dataTransfer(frmform, toform, mod, fld)
{
	var tmpstr = (mod) ? "opener" : "parent";

	if (mod && (frmform != "")) {
		if (!eval("parent.document." + frmform)) {
			alert("no source field.");
			return false;
		}
	}
	if (toform != "") {
		if (!eval("document." + toform)) {
			alert("no target field.");
			return false;
		}
	}
	
	var frmcb = (frmform == "") ? ((mod) ? parent.document.getElementById (fld) : opener.document.getElementById (fld)) : 
			((mod) ? eval("parent.document." + frmform + "." + fld) : eval("opener.document." + frmform + "." + fld));
//alert(frmcb)	

	var tocb = (toform == "") ? document.getElementById (fld) : eval("document." + toform + "." + fld);
	
//alert(tocb)	
	if ( (frmcb == null) || (tocb == null) ) {
		return false;
	} else {
		tocb.value = frmcb.value;
		return false;
	}
}


function delStringRev(str, cha)
{
	if (str != null)
		if (str.lastIndexOf(cha) == str.length - cha.length)
			return( str.substring(0,str.lastIndexOf(cha)) );
		else
			return(str);
}


function isValidDate(theDate) 
{
	arrayDate = theDate.split("/")

	var myDayStr = arrayDate[1];
	var myMonthNo = parseInt(arrayDate[0], 10);
	var myYearStr = arrayDate[2];
    //FB 1073
	if(british)
	{
	    myDayStr = arrayDate[0];
	    myMonthNo = parseInt(arrayDate[1], 10);
	} 
	//FB 1073	
	switch (myMonthNo) {
		case 1:
			myMonthStr = "Jan";
			break;
		case 2:
			myMonthStr = "Feb";
			break;
		case 3:
			myMonthStr = "Mar";
			break;
		case 4:
			myMonthStr = "Apr";
			break;
		case 5:
			myMonthStr = "May";
			break;
		case 6:
			myMonthStr = "Jun";
			break;
		case 7:
			myMonthStr = "Jul";
			break;
		case 8:
			myMonthStr = "Aug";
			break;
		case 9:
			myMonthStr = "Sep";
			break;
		case 10:
			myMonthStr = "Oct";
			break;
		case 11:
			myMonthStr = "Nov";
			break;
		case 12:
			myMonthStr = "Dec";
			break;
		default:
			return (false);
			break;
	}
	
	
	//var myDateStr = myYearStr + ' ' + myMonthStr + ' ' + myDayStr; // Commented for FB 2050
	//var xmyDate = new Date( myDateStr ); // Commented for FB 2050
	var myDate = new Date(parseInt(myYearStr,10), (--myMonthNo), parseInt(myDayStr, 10)); // FB 2050 //FB 2366 ZD 101722
    //myDate.setFullYear(parseInt(myYearStr),(--myMonthNo),parseInt(myDayStr)); // FB 2050
	//myDate.setDate(myDate.getDate() + 1);//FB 1911
	//var myDate_string = myDate.toGMTString(); // Commented for FB 2050
    var myDate_string = myDate.toString(); // FB 2050 //ZD 104854
    var myDate_array = myDate_string.split( ' ' );	

	if ( myDate_array[1] != myMonthStr ) { //ZD 104854
		return (false);
	}	
	
	return (true);
}


function validDate(d)
{
	if (d == "")
		return true;
		
	if (!isValidDate(d)) {
		return false;
	}
			
	return true;
}

	
function isPastDate(theDate) 
{
	var d = new Date()
	arrayDate = theDate.split("/")
	
	strDateYear  = arrayDate[2]
	intDateYear  = parseInt(strDateYear, 10)
	strDateMonth = arrayDate[0]
	intDateMonth = parseInt(strDateMonth, 10)
	strDateDay   = arrayDate[1]
	intDateDay   = parseInt(strDateDay, 10)
	
	if (intDateYear < d.getYear() ) {
		return (true);
	}
	if ( (intDateYear == d.getYear()) && (intDateMonth < (d.getMonth()+1) ) ) {
		return (true);
	}
	if ( (intDateYear == d.getYear()) && (intDateMonth == (d.getMonth()+1)) && (intDateDay < d.getDate()) ) {
		return (true);
	}

	return (false);
}


function caldatecompare(date1, date2)
{
 var d1 = new Date(date1);
 var d2 = new Date(date2);

 if ( (d1.getFullYear() == d2.getFullYear()) && (d1.getMonth()  == d2.getMonth()) && (d1.getDate() == d2.getDate()) ) return 0;
 if ( (d1.getFullYear() < d2.getFullYear()) || (d1.getFullYear() == d2.getFullYear()) && (d1.getMonth()  < d2.getMonth()) || (d1.getFullYear() == d2.getFullYear()) && (d1.getMonth()  == d2.getMonth()) && (d1.getDate() < d2.getDate()) ) return -1;
 if ( (d1.getFullYear() > d2.getFullYear()) || (d1.getFullYear() == d2.getFullYear()) && (d1.getMonth()  > d2.getMonth()) || (d1.getFullYear() == d2.getFullYear()) && (d1.getMonth()  == d2.getMonth()) && (d1.getDate() > d2.getDate()) ) return 1;
 return 1;
}



function datecompare(date1, date2)
{
 var d1 = new Date(date1);
 var d2 = new Date(date2);

 if (d1<d2) return -1;
 if (d1>d2) return 1;
 if (d1==d2) return 0;
}



function y2k(number) 
{ 
	return (number < 1000) ? number + 1900 : number; 
}

function daysElapsed(frmdate, todate) 
{
	date1 = new Date(frmdate)
	date2 = new Date(todate)

    var difference =
        Date.UTC(y2k(date1.getYear()),date1.getMonth(),date1.getDate(),0,0,0)
      - Date.UTC(y2k(date2.getYear()),date2.getMonth(),date2.getDate(),0,0,0);

    return difference/1000/60/60/24;
}



// check emails, work for IE5
//function checkemail(checkThisEmail)
//{
//	var myEMailIsValid = true;
//	var myAtSymbolAt = checkThisEmail.indexOf('@');
//	var myLastDotAt = checkThisEmail.lastIndexOf('.');
//	var mySpaceAt = checkThisEmail.indexOf(' ');
//	var myLength = checkThisEmail.length;
//	var myColon = checkThisEmail.indexOf(';'); //ALLBUGS - 50

//	if (myAtSymbolAt < 1 ) {myEMailIsValid = false}
//	if (myLastDotAt < myAtSymbolAt) {myEMailIsValid = false}
//	if (myLength - myLastDotAt <= 2) {myEMailIsValid = false}
//	if (mySpaceAt != -1) { myEMailIsValid = false }
//	if (myColon > 1) { myEMailIsValid = false } //ALLBUGS-50

//	return myEMailIsValid
//}


//ALLBUGS-50
function checkemail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function dateIsBefore(date1, date2)
{
 // FB 2050
 dt1 = date1.split("/");
 dt2 = date2.split("/");
 //ZD 104854 - Start
 var d1 = new Date(parseInt(dt1[2],10),parseInt(dt1[0],10),parseInt(dt1[1],10)); //FB 2366 ZD 101722 
 var d2 = new Date(parseInt(dt2[2], 10), parseInt(dt2[0], 10), parseInt(dt2[1], 10)); // ZD 101722 
 //ZD 104854 - End
 return (d1<d2);
}


function isPositiveInt(num, str)
{
	if (num == "") {
		alert("Please fill an integer in " +str+ ".");
		return 0;						
	}
	
	if ( (isNaN (num)) || ( (""+num).indexOf(".") != -1 ) ) {
		alert("Please fill in " +str+ " as whole days only.");
		return -1;
	} else {
		if ( parseInt(num, 10)>0 )
			return 1;
		else {
			alert("Value entered must be greater than 0 in " +str+ ".");
			return -1;
		}
	}
}

function isNoNegInt(num)
{
	if (num == "")
		return false;						
	
	if ( (isNaN (num)) || ( (""+num).indexOf(".") != -1 ) ) {
		return false;
	} else {
		if ( parseInt(num, 10)>=0 )
			return true;
		else
			return false;
	}
}

function isMonthDayNo(mondayno)
{
	if (mondayno>31) {
		alert("Day interval for Month is not valid.")
		return false;
	} else
		if (mondayno>28) {
			alert("Some months have fewer than " + mondayno + " days. For these months, the occurence will fall on the last day of the month.");
			//return false;		//FB # 741
		} //else
			return true;
}


function validateInt(theNumber)
{
	if ( isNaN(theNumber) || ( (""+theNumber).indexOf(".") != -1 ) ) {
		return (false);
	} else {
		if (parseInt(theNumber, 10)<0) {
			return (false);
		}
	}
	
	return (true);
}


function validatePositivePrice(theNumber)
{
	if (parseFloat(theNumber,10) != theNumber)
		return false;
		
	if (parseFloat(theNumber,10) < 0)
		return false;

	if ( (pos=theNumber.indexOf(".")) != -1 ) {
		if (pos < theNumber.length - 3)
			return false;
	}
	
	return true;
}


function validateIP(what) {
	var validChars = '.0123456789';

    if (!what)
        return false;

    dots = 0;

    for (var i = 0; i < what.length; i++) {
       var chr = what.substring(i,i+1);
       if (validChars.indexOf(chr) == -1)
           return false;
       if (chr == '.') {
           dots++;
           eval('dot' + dots + ' = ' + i);
       }
    }

    if (dots != 3)
        return false;
    
    if (what.substring(0,1) == '.' || what.substring(what.length,what.length+1) == '.')
        return false;

    ip1 = what.substring(0,dot1);
    if (!ip1 || ip1 >255)
        return false;
    ip2 = what.substring(dot1+1,dot2);
    if (!ip2 || ip2 >255)
        return false;
    ip3 = what.substring(dot2+1,dot3);
    if (!ip3 || ip3 >255)
        return false;
    ip4 = what.substring(dot3+1,what.length+1);
    if (!ip4 || ip4 >255)
        return false;

    if (ip1 == 0 && ip2 == 0 && ip3 == 0 && ip4 == 0)
        return false;

    return true;
}

// NOT USED ANY MORE
function validatePhoneNumber(what) {	// mask is xxx-xxx-xxxx
	var ok = true;
	var temp;
	if (what.length != 12)
		ok = false;
	else {
		for (var i=0; i<3; i++)
			ok = validateInt(what.substring(i, i+1));
		for (var i=4; i<7; i++)
			ok = validateInt(what.substring(i, i+1));
		for (var i=8; i<12; i++)
			ok = validateInt(what.substring(i, i+1));
		if ( (what.substring(3, 4)!= "-") ||(what.substring(7, 8)!= "-") )
			ok = false;
	}

	return ok;
}


function validateISDN(what) {
/*
	var pattern = /^\d{1,2}-(\d{1,3})-(\d+)-(\d+)/;
	return (what.match(pattern));
*/
	return (Trim (what) != "");
}


function isYearMonthDay(mon, day)
{
	switch (mon) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (day>31) {
				alert("Invalid Day for the month.");
				return false;
			} else {
				return true;
			}
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			if (day>30) {
				alert("Invalid Day for the month.");
				return false;
			} else {
				return true;
			}
			break;
		case 2:
			if (day>28) {
				alert("Invalid Day for the month.");
				return false;
			} else {
				return true;
			}
			break;
		default:
			alert("Error(rpx111): Pattern of Recurring Range is not correct. Please notify developer.");
			return false;
			break;
	}
}



var	atint=new Array(5), rpint=new Array(17), rrint=new Array(4);
var monthname = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
var weekdayname = new Array("day","weekday","weekend","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
var weekdayno = new Array("first","second","third","fourth","last");

function AnalyseRecurStr(rstr)
{
	rary = rstr.split("#");
	if (rary.length!=3) {
		alert ("Recurring Part is not correct. Please notify developer.");
	}
	atstr = rary[0];	rpstr = rary[1];	rrstr = rary[2];

	atary = atstr.split("&");
	if (atary.length!=5) {
		alert ("Appointment time is not correct. Please notify developer.atary.length="+atary.length)
	} else {
		for (i=0; i<atary.length; i++) {
			if (isNaN(atary[i])) {
				atint[i] = atary[i];
			} else {
				atint[i] = parseInt(atary[i], 10);
			}
		}
	}

	rpary = rpstr.split("&");
	if (parseInt(rpary[0], 10) == 5) {
		rpint[0] = 5;
		return (rrstr);
	}
	
	if (rpary.length!=17) {
		alert ("Recurring Pattern is not correct. Please notify developer. rpary.length=" + rpary.length)
	} else {
		for (i=0; i<rpary.length; i++) {
			if (isNaN(rpary[i])) {
				rpint[i] = rpary[i];
			} else {
				rpint[i] = parseInt(rpary[i], 10);
			}
		}
	}

	rrary = rrstr.split("&");
	if (rrary.length!=4) {
		alert ("Recurring Part is not correct. Please notify developer.")
	} else {
		for (i=0; i<rrary.length; i++) {
			if (rrary[i] != "") {
				if (isNaN(rrary[i])) {
					rrint[i] = rrary[i];
				} else {
					rrint[i] = parseInt(rrary[i], 10);
				}
			} else
				rrint[i] = "";
		}
	}
	
	return (rpstr);
}


function getweekdaynames(dayintstr, exceptStartDate)
{
	if (dayintstr == "-1") {
		date_str = new Date(exceptStartDate);
		dayname = weekdayname[date_str.getDay()+3];
	} else {
		dayname = "";
		if ((String(dayintstr)).indexOf(",") == -1 ) {
			dayname += ( (String(dayintstr)=="NaN") ? "" : weekdayname[dayintstr+2]);
		} else {
			wdary = dayintstr.split(",");
			for (i = 0; i < wdary.length - 1; i++)
				dayname += weekdayname[parseInt(wdary[i], 10)+2] + ", ";
			dayname += "and " + weekdayname[parseInt(wdary[i], 10)+2];
		}
	}

	return(dayname);
}

/*** *Code change fro FB 1426,1425 **** */


function recur_discription(rv, etext, tztext, exceptsd, tformat, tzdisplay) {
    
	rst = AnalyseRecurStr(rv);

	if (rv.indexOf("#5#") != -1) 
		return "Custom Date Selection: " + (rv.split("#")[2]).replace(/&/gi, ", ");

    rd = "";
	switch (rpint[0]) {
		case 1:
			if (rpint[1] == 1)
				rd += "Occurs every " + ( (rpint[2] == -1) ? "0" : rpint[2]) + " day(s) ";
			else
				rd += "Occurs every weekday ";
			break;
		case 2:
			tmpstr = getweekdaynames(rpint[4], exceptsd);
			rd += "Occurs every " + rpint[3] + " week(s) on " + tmpstr + " ";
			break;
		case 3:
			if (rpint[5] == 1)
				rd += "Occurs day " + rpint[6] + " of every " + rpint[7] + " month(s) ";
			else
				rd += "Occurs the " + weekdayno[rpint[8]-1] + " " + weekdayname[rpint[9]-1] + " of every " + rpint[10] + " month(s) ";
			break;
		case 4:
			if (rpint[11] == 1)
				rd += "Occurs every " + monthname[rpint[12]-1] + " " + rpint[13] + " ";
			else
				rd += "Occurs the " + weekdayno[rpint[14]-1] + " " + weekdayname[rpint[15]-1] + " of " + monthname[rpint[16]-1] + " ";
			break;
	}

	rd += "effective " + rrint[0] + " ";
	switch (rrint[1]) {
		case 1:
			break;
		case 2:
			rd += "occurs " + rrint[2] + " time(s) "
			break;
		case 3:
			rd += "until " + rrint[3] + " "
			break;
	}
	durMin = rv.split("#")[0].split("&")[4];
	etext = Math.floor(durMin / 60) + " hrs "; //ZD 100528
	if (durMin % 60 > 0)
	    etext += durMin % 60 + " mins";
    //alert(rrint[0] + " : " + durMin);
//	rd += "from " + atint[1] + ":" + ((atint[2]==0) ? "00" : atint[2]) + " " + atint[3] + " to " + etext + " " + tztext;
	var period = ":";
	var timeformat = " "; //FB 2588
    if (tformat == "0") {
        if (atint[3] == "PM") {
            if (atint[1] != "12") {
                if (atint[1] == "00")
                    atint[3] = "AM";

                if (eval(atint[1]) < 12)
                    atint[1] = parseInt(atint[1], 10) + 12; // ZD 101722
            }
            else
                atint[1] = "12";
        }
        else {
            if (atint[1] == "12") {
                atint[1] = "00";
                atint[3] = "AM";
            }
        }
        atint[3] = "";
        period = ":";
    } //FB 2588
    else if (tformat == "2") {
    period = "";
    atint[3] = "Z";
    timeformat = "";
    }
    if(tzdisplay == "0")
      tztext = "";

    rd += "from " + ((atint[1] < 10) ? ("0" + atint[1]) : atint[1]) + period + ((atint[2] < 10) ? ("0" + atint[2]) : atint[2]) + timeformat + atint[3] + " for " + etext + " " + tztext;
	return (rd);
}

//FB 1911
function specialRecur_discription(rv, etext, tztext, exceptsd,tformat,tzdisplay, maxRecur) {
    
	rst = AnalyseRecurStr(rv);
	var strDay="";
	var dayStr;
	if (rpint.length > 0)
    {
        var colorDay = document.getElementsByName("rdlstDayColor");//FB 2052                
        if(colorDay)
        {
            for (var c = 0; c < colorDay.length; c++)
                if (colorDay[c].checked)
                {
                    var labelArray = colorDay[c].parentNode.getElementsByTagName('label');
                    strDay = labelArray[0].innerHTML;//Firefox doesn't support innertext property
                }
        }
 
        dayStr = "";
        switch (rpint[2])
        {
            case 1: dayStr = "Monday"; break;
            case 2: dayStr = "Tuesday"; break;
            case 3: dayStr = "Wednesday"; break;
            case 4: dayStr = "Thursday"; break;
            case 5: dayStr = "Friday"; break;
        }
    }
    
	rd = "";	
	switch (rpint[1]) {
	
	 case 1: //every Blue Monday for next 12 weeks
        if (rrint[1] == "2")
            rd += "Every " + strDay + " " + dayStr + " for next " + rrint[2] + " weeks";
        else if (rrint[1] == "3")
            rd += "Every " + strDay + " " + dayStr + " up to " + rrint[3];
        else
            rd += "Every " + strDay + " " + dayStr + " for next " + maxRecur + " weeks";          
        break;
    case 2: //every other Orange Tuesday up to 12/31/2012
        if (rrint[1] == "2")
            rd += "Every other " + strDay + " " + dayStr + " for next " + rrint[2] + " weeks";
        else if (rrint[1] == "3")
            rd += "Every other " + strDay + " " + dayStr + " up to " + rrint[3];
        else
            rd += "Every other " + strDay + " " + dayStr + " for next " + maxRecur + " weeks"
        break;
    case 3: //every first Blue Monday of every month for next 24 weeks
        if (rrint[1] == "2")
            rd += "Every first " + strDay + " " + dayStr + " of every month for next " + rrint[2] + " weeks";
        else if (rrint[1] == "3")
            rd += "Every first " + strDay + " " + dayStr + " up to " + rrint[3];
        else
            rd += "Every first " + strDay + " " + dayStr + " of every month for next " + maxRecur + " weeks";
        break;
    case 4: //every second Orange day of a month for the next 12 months
        if (rrint[1] == "2")
            rd += "Every second " + strDay + " " + dayStr + " of a month for the next " + rrint[2] + " months";
        else if (rrint[1] == "3")
            rd += "Every second " + strDay + " " + dayStr + " up to " + rrint[3];
        else
            rd += "Every second " + strDay + " " + dayStr + " of a month for the next " + maxRecur + " months";
        break;
    case 5: //every last Blue Friday of every month for next 24 month
        if (rrint[1] == "2")
            rd += "Every last " + strDay + " " + dayStr + " of every month for next " + rrint[2] + " months";
        else if (rrint[1] == "3")
            rd += "Every last " + strDay + " " + dayStr + " up to " + rrint[3];
        else
            rd += "Every last " + strDay + " " + dayStr + " of every month for next " + maxRecur + " months";
        break;
    case 6: //every first Blue day of a month for the next 12 months
        if (rrint[1] == "2")
            rd += "Every first " + strDay + " day  of a month for the next " + rrint[2] + " months";
        else if (rrint[1] == "3")
            rd += "Every first " + strDay + " day up to " + rrint[3];
        else
            rd += "Every first " + strDay + " day of a month for the next " + maxRecur + " months";
        break;
     case 7: //every second Orange day of a month for the next 12 months
        if (rrint[1] == "2")
            rd += "Every second " + strDay + " day of a month for the next " + rrint[2] + " months";
        else if (rrint[1] == "3")
            rd += "Every second " + strDay + " day up to " + rrint[3];
        else
            rd += "Every second " + strDay + " day of a month for the next " + maxRecur + " months";
        break;
     case 8: //every last Orange day of a month for the next 12 months
        if (rrint[1] == "2")
            rd += "Every last " + strDay + " of a month for the next " + rrint[2] + " months";
        else if (rrint[1] == "3")
            rd += "Every last " + strDay + " up to " + rrint[3];
        else
            rd += "Every last " + strDay + " of a month for the next " + maxRecur + " months";
        break;
	}

	rd += " effective " + rrint[0] + " ";
	switch (rrint[1]) {
		case 1:
			break; 
		case 2:
			rd += "occurs " + rrint[2] + " time(s) "
			break;
		case 3:
			rd += "until " + rrint[3] + " "
			break;
	}
	durMin = rv.split("#")[0].split("&")[4];
	etext = Math.floor(durMin / 60) + " hrs "; //ZD 100528
	if (durMin % 60 > 0)
	    etext += durMin % 60 + " mins"; 

	var period = ":";
	var timeformat = " "; //FB 2588
    if(tformat == "0")
    {
        if(atint[3] == "PM")
        {
            if(atint[1] != "12")
            {
                if(atint[1] == "00")
                    atint[3] = "AM";
                              
               if(eval(atint[1]) < 12 )         
                    atint[1] = parseInt(atint[1], 10) + 12; // ZD 101722
            }
            else
                atint[1] = "12";    
        }
        else
        {
            if(atint[1] == "12")
            {
                atint[1] = "00";   
                atint[3] = "AM";
            }
        }
      atint[3]= "";
      period = ":";
  } //FB 2588
  else if (tformat == "2") {
      period = "";
      atint[3] = "Z";
      timeformat = "";
  }
  
    if(tzdisplay == "0")
        tztext = "";
    //rd += "from " + atint[1] + period + ((atint[2] < 10) ? ("0" + atint[2]) : atint[2]) + " " + atint[3] + " for " + etext + " " + tztext;
    rd += "from " + ((atint[1] < 10) ? ("0" + atint[1]) : atint[1]) + period + ((atint[2] < 10) ? ("0" + atint[2]) : atint[2]) + timeformat + atint[3] + " for " + etext + " " + tztext;	

	return (rd);
}

/*** *Code change fro FB 1426,1425 **** */



//1``010101``Rm 310^^2``010102``Rm 312^^3``010103``Rm 315^^4``010201``Rm North^^5``010202``Rm South^^6``010203``Rm East^^7``020101``Rm 110^^8``020102``Rm 112^^9``020103``Rm 215^^10``030101``Rm I^^11``030102``Rm II^^12``030103``Rm III^^13``030201``Rm 0101^^14``030202``Rm 0205^^15``030203``Rm 0503^^
function getselrmidnames(rmid, rmsidname)
{
	if ( (rmid == "") || (rmsidname.indexOf("^^") == -1) )
		return "";
	
	rmsary = rmsidname.split("^^");
	for (var i=0; i<rmsary.length-1; i++) {
		rmary = rmsary[i].split("``");
		if (rmid == rmary[1]) 
			return rmary[2];
	}
	
	return "";
}


function calEnd_long(strstart, d)
{
	var myend = new Date(strstart);
	myend.setMinutes(myend.getMinutes() + parseInt(d,10)); // ZD 101722

	with (myend) {
		return([(getMonth() + 1) + "/" + getDate() + "/" + getFullYear(), 1+((H=getHours())+11)%12, getMinutes(), ['AM','PM'][+(H>11)] ]);
	}
}


function calEnd(s, d, needary)
{
	endmin = s + d;
	min_int = endmin%60;
	hor_int = ((endmin%1440) - min_int)/60;
	hor_tmp = (hor_int >= 12) ? (hor_int-12) : hor_int
	endtime_str = ((hor_tmp == 0) ? "12" : hor_tmp) + ":" + ( (min_int < 10) ? ("0" + min_int) : min_int) + " " + ((hor_int >= 12) ? ("PM") : "AM");
//    alert(endtime_str);
	if (needary)
		return [((hor_tmp == 0) ? "12" : hor_tmp), ( (min_int < 10) ? ("0" + min_int) : min_int), ((hor_int >= 12) ? ("PM") : "AM")];
	else
		return (endtime_str);
}


function calStart(sh, sm, ss)
{
	sh = parseInt(sh, 10); // ZD 101722
	sm = parseInt(sm, 10); // ZD 101722
	sint = ((sh==12) ? 0 : sh) * 60 + sm + ((ss == "AM") ? 0 : 720);

	return (sint);
}


function calDur_long(sd, sh, sm, spm, ed, eh, em, epm)
{
    spm = ((!spm)? "" : spm);
    epm = ((!spm)? "" : epm);
	s = Date.parse(sd + " " + sh + ":" + sm + " " + spm);
	e = Date.parse(ed + " " + eh + ":" + em + " " + epm);
    //alert((e-s) / 1000 / 60);
    
	return ( (e-s) / 1000 / 60 );
}

function calDur(sd, ed, sh, sm, spm, eh, em, epm, needtext)
{
////	s = parseInt(sm) + ( (parseInt(sh) == 12) ? 0 : parseInt(sh)) * 60 + ((spm.toUpperCase() == "PM") ? 720 : 0);
////	e = parseInt(em) + ( (parseInt(eh) == 12) ? 0 : parseInt(eh)) * 60 + ((epm.toUpperCase() == "PM") ? 720 : 0);
    spm = ((!spm)? "" : spm);
    epm = ((!spm)? "" : epm);
	s = Date.parse(sd + " " + sh + ":" + sm + " " + spm);
	e = Date.parse(ed + " " + eh + ":" + em + " " + epm);
	
	//var res = ( (e-s) >= 0 ? (e-s) : (e-s) + (24*60) );
	var res = (e-s)/1000/60;
	//alert(res);
	if (needtext)
		return ( Math.floor(res/60) + " hrs " + (res%60) + " mins" ); //ZD 100528
	else
		return res;
}

function calDurAdmin(sh, sm, spm, eh, em, epm, needtext)
{
    spm = ((!spm)? "" : spm);
    epm = ((!spm)? "" : epm);
	s = parseInt(sm,10) + ( (parseInt(sh,10) == 12) ? 0 : parseInt(sh,10)) * 60 + ((spm.toUpperCase() == "PM") ? 720 : 0); // ZD 101722
	e = parseInt(em,10) + ( (parseInt(eh,10) == 12) ? 0 : parseInt(eh,10)) * 60 + ((epm.toUpperCase() == "PM") ? 720 : 0); // ZD 101722
	
	var res = e-s;
	//alert(res);
	if (needtext)
		return ( Math.floor(res/60) + " hrs " + (res%60) + " mins" ); //ZD 100528
	else
		return res;
}




function addopt(cb, txt, val, defsel, sel) 
{
	var defaultSelected = defsel;
	var selected = sel;
//	var option = new Option(txt, val, defaultSelected, selected);
//	var length = cb.length;
//	cb.options[length] = option;
    var elOptNew = document.createElement('option');
    elOptNew.text = txt;
    elOptNew.value = val;
  try {
    cb.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex) {
    cb.add(elOptNew); // IE only
  }
}
	

function delopt(cb, optionNo) 
{
	cb.options[optionNo] = null;
}


function deloptbyval(cb, val) 
{
	optno = -1;
	for (var k=0; k<cb.length; k++) {
		if (cb.options[k].value == val)
			optno = k
	}
	
	if (optno != -1)
		delopt(cb, optno);
}


function RemoveAllOptions (cb)
{
	cblen = cb.length;
	for (i=0; i<cblen; i++)
		delopt(cb, 0);
}


function getUploadFilePath (fpn)
{
	if (fpn == "")
		return "";
	else {
		if (fpn.indexOf("\/") != -1)
			fa = fpn.split("\/");
		else
			fa = fpn.split("\\");
		if (fa.length == 0)
			return "";
		else
			return  fa[fa.length - 2];
	}
}


function getUploadFileName (fpn)
{
	if (fpn == "")
		return "";
	else {
		if (fpn.indexOf("\/") != -1)
			fa = fpn.split("\/");
		else
			fa = fpn.split("\\");
		return  fa[fa.length - 1];
	}
}


function getUploadFileExt (fpn)
{
	if (fpn.indexOf(".") == -1)
		return "";
		
		
	fpnary = fpn.split(".");
	return (fpnary[fpnary.length-1]);
}


function isArray(obj) 
{
//	alert(typeof(obj));
//	if (typeof(obj)
	return ((typeof(obj)=="undefined") ? false : true);
}

function getFieldValue(myobj)
{
	var obj;
//	alert(typeof(myobj));
	if (typeof (myobj) == "object")
		obj = myobj;
	if (typeof (myobj) == "string")		// radio can not use
		if (document.getElementById(myobj) != null)
			obj = document.getElementById(myobj);
	if (obj == null) return false;

	if (isArray(obj) && (typeof(obj.type)=="undefined")) {
		var val = "";
		for(var i=0;i<obj.length;i++) val = getSingleInputValue(obj[i], val);
		return val;
	}
	else return(getSingleInputValue(obj, ""));
}


function setFieldValue(myobj, value)
{
	var obj;
	if (typeof (myobj) == "object")
		obj = myobj;
	if (typeof (myobj) == "string")		// radio can not use
	{
//		alert("out " + document.getElementById(myobj).name);
		if (document.getElementById(myobj) != null)
		{
			obj = document.getElementById(myobj);
//			alert("here " + document.getElementById(myobj));
		}
	}
	
	if (obj == null) return false;
	var use_default = (arguments.length > 1) ? arguments[1] : false;
	if (isArray(obj) && (typeof(obj.type)=="undefined")) {
		for(var i=0;i<obj.length;i++) setSingleInputValue(obj[i],value);
	}
	else setSingleInputValue(obj,value);
}


function set_select_field(cb, txt, isnum)
{
    //alert(typeof(cb));
    if (cb.options)
	    for (i = 0; i < cb.length; i++) {
		    cb.options[i].selected = ( ((isnum) ? parseInt(cb.options[i].text, 10) : (cb.options[i].text).toUpperCase()) == ((isnum) ? parseInt(txt, 10) : txt.toUpperCase()) ) ? true : false;
	    }
	else
    	cb.value = txt;
}



function getSingleInputValue(obj, value)
{
	switch(obj.type){
		case 'radio':
		case 'checkbox':
			if (obj.checked) {return obj.value} else {return value};
			break;
		case 'text': 
		case 'hidden': 
		case 'textarea': 
		case 'password': 
			return obj.value;
			break;
		case 'select-one': 
		case 'select-multiple': 
			var o=obj.options;
			var sel="";
			for(var i=0;i<o.length;i++)
				if (o[i].selected) sel += o[i].value + ", ";
			sel = (sel != "") ? sel.slice(0, -2) : "";
			return sel;
			break;
	}
	return value;
}

function setSingleInputValue(obj, value) 
{
//	alert(obj.type + ":" + value);
	switch(obj.type){
		case 'radio':
		case 'checkbox':
			if(obj.value==value) {obj.checked=true; return true;} else {obj.checked=false; return false;}
			break;
		case 'text': 
		case 'hidden': 
		case 'textarea': 
		case 'password': 
			obj.value=value; return true;
			break;
		case 'select-one':
		case 'select-multiple': 
			var o=obj.options;
			for(var i=0;i<o.length;i++)
				if(o[i].value==value) o[i].selected=true; else o[i].selected=false;
			return true;
			break;
		default:
			return false;
	}
	return false;
}


function addrow(tableid)
{
	var table = document.all ? document.all[tableid] : document.getElementById(tableid);
	if (arguments.length > 1) {
		var row = table.insertRow(table.rows.length);
//		if (document.all) {
			for (var i = 1; i < arguments.length; i++) {
				var cell = row.insertCell(i - 1);
				cell.innerHTML = arguments[i];
			}
/*
		}
		else if (document.getElementById) { 
			//NN6 bug inserting cells in wrong sequence
			for (var i = arguments.length - 1; i >= 1; i--) {
				var cell = row.insertCell(arguments.length - 1 - i);
				cell.appendChild(document.createTextNode(arguments[i]));
			}
		}
*/
	}
}	



function checkInvalidChar(inputVal)
{
    //FB 1640 //FB 2236
	var list = /[\<\&\>\'\"\+\%\(\)\,]/;
	var warningMsg = "You have typed in an invalid character in one of the field.  Please remove it and try again.";
	warningMsg += "\nInvalid characters are: <, >, &, \', (, ), -, \"";

	evalStr = inputVal;
	if (evalStr.match(list)){
		alert(warningMsg);
		return false;
	}
	else
	{
		return true;
	}

}
//FB 2339 Start
function checkInvalidPassChar(inputVal)
{
   var list =  /[\<\&\>]/;
	var warningMsg = "You have typed in an invalid character in one of the field.  Please remove it and try again.";
	warningMsg += "\nInvalid characters are: <, >, &";

	evalStr = inputVal;
	if (evalStr.match(list)){
		alert(warningMsg);
		return false;
	}
	else
	{
		return true;
	}

}
//FB 2339 End
//FB 2236
function checkInvalidCharSpl(inputVal)
{
    //FB 1640 
	var list = /[\\\\'\"\+\%\(\)\,]/;
	var warningMsg = "You have typed in an invalid character in one of the field.  Please remove it and try again.";
	warningMsg += "\nInvalid characters are: \', (, ), -, \"";

	evalStr = inputVal;
	if (evalStr.match(list)){
		alert(warningMsg);
		return false;
	}
	else
	{
		return true;
	}

}
 
//FB 1640 //FB 2321
function checkInvalidCharNew(inputVal)
{

	var list = /[\<\&\>]/;
	var warningMsg = "You have typed in an invalid character in one of the field.  Please remove it and try again.";
	warningMsg += "\nInvalid characters are: < > and &";

	evalStr = inputVal;
	if (evalStr.match(list)){
		alert(warningMsg);
		return false;
	}
	else
	{
		return true;
	}

}


function hov(loc,cls)
{
   if(loc.className)
      loc.className=cls;
}




function addDates(y, m, d, daynum) {
    // ZD 101722 Start
	var year1 = parseInt(y,10); 
	var month1 = parseInt(m,10);
	var day1 = parseInt(d,10);
	var days = parseInt(daynum, 10);
    // ZD 101722 End
	var date1 = new myDate(year1, month1, day1);
	var errmsg = "";

	if( year1 < 1900 )
		errmsg += "invalid year input; year(1900-)\n";
	if( month1 <= 0 || month1 > 12 )
		errmsg += "invalid month input; month(1-12)\n";
	if( day1 <= 0 || day1 > daysOfMonth(year1, month1) )
		errmsg += "invalid day input; day(1-31)\n";
	if( errmsg ) {
		alert(errmsg);
	} else {
		dateAdd(date1, days);
		
		return (date1.month + "/" + date1.day + "/" + date1.year)
//		document.addDate.year2.value = date1.year;
//		document.addDate.month2.value = date1.month;
//		document.addDate.day2.value = date1.day;
	}
}




function subDates(y1, m1, d1, y2, m2, d2) {
	// ZD 101722 Starts
    var year1 = parseInt(y1,10);
	var month1 = parseInt(m1,10);
	var day1 = parseInt(d1,10);
	var year2 = parseInt(y2,10);
	var month2= parseInt(m2,10);
	var day2 = parseInt(d2,10);
    // ZD 101722 Ends

	var date1 = new myDate(year1, month1, day1);
	var date2 = new myDate(year2, month2, day2);
	var errmsg = "";

	if( year1 < 1900 || year2 < 1900 )
		errmsg += "invalid year input; year(1900-)\n";
	if( month1 <= 0 || month1 > 12 ||
		month2 <= 0 || month2 > 12 )
		errmsg += "invalid month input; month(1-12)\n";
	if( day1 <= 0 || day1 > daysOfMonth(year1, month1) ||
		day2 <= 0 || day2 > daysOfMonth(year2, month2) )
		errmsg += "invalid day input; day(1-31)\n";
	if( errmsg ) {
		alert(errmsg);
	} else {
		return dateSub(date1, date2);
	}
}


function dateSub(date1, date2) {
	var n = date1.day - date2.day;
	var date3;
	var sign = 1;

	if(date1.year < date2.year || 
		date1.year == date2.year && date1.month < date2.month ){
		sign = -1;
		date3 = date1;
		date1 = date2;
		date2 = date3;
	}
	while(!(date1.year == date2.year && date1.month == date2.month)) {
		n += daysOfMonth(date2.year, date2.month);
		nextMonth(date2);
	}
	return sign * n;
}

function dateAdd(date, n) {
	date.day += n;
	while(date.day <=0 || date.day > daysOfMonth(date.year, date.month)) {
		if(date.day <= 0){
			prevMonth(date);
			date.day += daysOfMonth(date.year, date.month);
		} else {
			date.day -= daysOfMonth(date.year, date.month);
			nextMonth(date);
		}
	}
}


function daysOfMonth(year, month) {
	var days = new Array;

	days[1] = 31;  days[2] = 28;  days[3] = 31;
	days[4] = 30;  days[5] = 31;  days[6] = 30;
	days[7] = 31;  days[8] = 31;  days[9] = 30; 
	days[10] = 31; days[11] = 30; days[12] = 31;

	if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
		days[2] = 29;

	return days[month];
}

function myDate(year, month, day) {
	this.year = year;
	this.month = month;
	this.day = day;
}

function prevMonth(date) {
	if(date.month == 1) {
		date.year--; date.month = 12;
	} else {
		date.month--;
	}
}

function nextMonth(date) {
	if(date.month == 12) {
		date.year++; date.month = 1;
	} else {
		date.month++;
	}
}

function rlgnstr(str, fixlen)
{
	if (str.length >= fixlen) {
		return str;
	} else {
		var spcstr = "";
		
		for (var i=0; i<fixlen-str.length; i++)
			spcstr += "&nbsp;";
		return (spcstr + str);
	}
}


function USlocaltimeStr(yr, mn, dy, hr, me)
{
	var mydate = new Date(yr,mn,dy,hr,me);
	var h;
	
	with (mydate) {
		return LZ( 1+((H=getHours())+11)%12  ) + ":" + LZ(getMinutes()) + [' AM',' PM'][+(H>11)];
	}
}


function LZ(x) 
{ 
	return ( ((x>=10) || (x<0)) ? "" : "0" ) + x;
}
//Code added by offshore for Issue -- 1073 -- start
function GetDefaultDate(dateVal,format)
{
    if(format == 'MM/dd/yyyy')
        return dateVal;
    else
    {
        var dateSplit = dateVal.split("/")
        if(dateSplit != null)
            return dateSplit[1] + '/' + dateSplit[0] + '/' + dateSplit[2];
    }
}
//Code added by offshore for Issue -- 1073 -- End

// Recurrence Fixes Start
//Date validation Start
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

//This function needs to be called for date validations for date format -- 1073

function IsValidFormatDate(){
	
	var args=IsValidFormatDate.arguments
	
	var dt=args[0]
	
	if(dtCh == '/')
	{
		if (isDate(dt)==false){
			return false
		}
		return true
	}
}

//This function needs to be called for date validations


function IsValidDate(){
	
	var args=IsValidDate.arguments
	
	var dt=args[0].value
	
	if(dtCh == '/')
	{
		if (isDate(dt)==false){
			return false
		}
		return true
	}
}

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	
	strYr=strYear
	
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
    // ZD 101722 Starts
	month=parseInt(strMonth,10)
	day=parseInt(strDay,10)
	year = parseInt(strYr, 10)
    // ZD 101722 Ends
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year");
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}
//Date Validation End
// Recurrence Fixes End

// DateAdd function - added for Buffer zone - start
function dateAddition( start, interval, number )
{
	var buffer = Date.parse(start) ;
	
	if ( isNaN (buffer) ) 
	{ 
	    start = new Date(); 
	    buffer = Date.parse(start);   
	}
		
	if ( interval.charAt == 'undefined' ) 
	{    
		interval = 'm';
	}
		
	if ( isNaN ( number ) )	
	{ 
	    number = 0;
	}

	switch (interval.charAt(0))
	{
	    case 'd': case 'D':
						number *= 24 ; // days to hours
        case 'h': case 'H':
						number *= 60 ; // hours to minutes
        case 'm': case 'M':
						number *= 60 ; // minutes to seconds
        case 's': case 'S':
						number *= 1000 ; // seconds to milliseconds
						break ;
	    default:
						number = 0;  // If we get to here then the interval parameter
	} 
	return new Date( buffer + number ) ;

}
// DateAdd function - added for Buffer zone - end

Date.$VERSION = 1.02;

// Utility function to append a 0 to single-digit numbers
Date.LZ = function(x) {return(x<0||x>9?"":"0")+x};
// Full month names. Change this for local month names
Date.monthNames = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
// Month abbreviations. Change this for local month names
Date.monthAbbreviations = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
// Full day names. Change this for local month names
Date.dayNames = new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
// Day abbreviations. Change this for local month names
Date.dayAbbreviations = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
// Used for parsing ambiguous dates like 1/2/2000 - default to preferring 'American' format meaning Jan 2.
// Set to false to prefer 'European' format meaning Feb 1
Date.preferAmericanFormat = true;

// If the getFullYear() method is not defined, create it
if (!Date.prototype.getFullYear) { 
  Date.prototype.getFullYear = function() { var yy=this.getYear(); return (yy<1900?yy+1900:yy); } ;
} 

// Parse a string and convert it to a Date object.
// If no format is passed, try a list of common formats.
// If string cannot be parsed, return null.
// Avoids regular expressions to be more portable.
Date.parseString = function(val, format) {
  // If no format is specified, try a few common formats
  if (typeof(format)=="undefined" || format==null || format=="") {
    var generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d','MMM-d','d-MMM');
    var monthFirst=new Array('M/d/y','M-d-y','M.d.y','M/d','M-d');
    var dateFirst =new Array('d/M/y','d-M-y','d.M.y','d/M','d-M');
    var checkList=new Array(generalFormats,Date.preferAmericanFormat?monthFirst:dateFirst,Date.preferAmericanFormat?dateFirst:monthFirst);
    for (var i=0; i<checkList.length; i++) {
      var l=checkList[i];
      for (var j=0; j<l.length; j++) {
        var d=Date.parseString(val,l[j]);
        if (d!=null) { 
          return d; 
        }
      }
    }
    return null;
  };

  this.isInteger = function(val) {
    for (var i=0; i < val.length; i++) {
      if ("1234567890".indexOf(val.charAt(i))==-1) { 
        return false; 
      }
    }
    return true;
  };
  this.getInt = function(str,i,minlength,maxlength) {
    for (var x=maxlength; x>=minlength; x--) {
      var token=str.substring(i,i+x);
      if (token.length < minlength) { 
        return null; 
      }
      if (this.isInteger(token)) { 
        return token; 
      }
    }
  return null;
  };
  val=val+"";
  format=format+"";
  var i_val=0;
  var i_format=0;
  var c="";
  var token="";
  var token2="";
  var x,y;
  var year=new Date().getFullYear();
  var month=1;
  var date=1;
  var hh=0;
  var mm=0;
  var ss=0;
  var ampm="";
  while (i_format < format.length) {
    // Get next token from format string
    c=format.charAt(i_format);
    token="";
    while ((format.charAt(i_format)==c) && (i_format < format.length)) {
      token += format.charAt(i_format++);
    }
    // Extract contents of value based on format token
    if (token=="yyyy" || token=="yy" || token=="y") {
      if (token=="yyyy") { 
        x=4;y=4; 
      }
      if (token=="yy") { 
        x=2;y=2; 
      }
      if (token=="y") { 
        x=2;y=4; 
      }
      year=this.getInt(val,i_val,x,y);
      if (year==null) { 
        return null; 
      }
      i_val += year.length;
      if (year.length==2) {
        if (year > 70) { 
          year=1900+(year-0); 
        }
        else { 
          year=2000+(year-0); 
        }
      }
    }
    else if (token=="MMM" || token=="NNN"){
      month=0;
      var names = (token=="MMM"?(Date.monthNames.concat(Date.monthAbbreviations)):Date.monthAbbreviations);
      for (var i=0; i<names.length; i++) {
        var month_name=names[i];
        if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
          month=(i%12)+1;
          i_val += month_name.length;
          break;
        }
      }
      if ((month < 1)||(month>12)){
        return null;
      }
    }
    else if (token=="EE"||token=="E"){
      var names = (token=="EE"?Date.dayNames:Date.dayAbbreviations);
      for (var i=0; i<names.length; i++) {
        var day_name=names[i];
        if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
          i_val += day_name.length;
          break;
        }
      }
    }
    else if (token=="MM"||token=="M") {
      month=this.getInt(val,i_val,token.length,2);
      if(month==null||(month<1)||(month>12)){
        return null;
      }
      i_val+=month.length;
    }
    else if (token=="dd"||token=="d") {
      date=this.getInt(val,i_val,token.length,2);
      if(date==null||(date<1)||(date>31)){
        return null;
      }
      i_val+=date.length;
    }
    else if (token=="hh"||token=="h") {
      hh=this.getInt(val,i_val,token.length,2);
      if(hh==null||(hh<1)||(hh>12)){
        return null;
      }
      i_val+=hh.length;
    }
    else if (token=="HH"||token=="H") {
      hh=this.getInt(val,i_val,token.length,2);
      if(hh==null||(hh<0)||(hh>23)){
        return null;
      }
      i_val+=hh.length;
    }
    else if (token=="KK"||token=="K") {
      hh=this.getInt(val,i_val,token.length,2);
      if(hh==null||(hh<0)||(hh>11)){
        return null;
      }
      i_val+=hh.length;
      hh++;
    }
    else if (token=="kk"||token=="k") {
      hh=this.getInt(val,i_val,token.length,2);
      if(hh==null||(hh<1)||(hh>24)){
        return null;
      }
      i_val+=hh.length;
      hh--;
    }
    else if (token=="mm"||token=="m") {
      mm=this.getInt(val,i_val,token.length,2);
      if(mm==null||(mm<0)||(mm>59)){
        return null;
      }
      i_val+=mm.length;
    }
    else if (token=="ss"||token=="s") {
      ss=this.getInt(val,i_val,token.length,2);
      if(ss==null||(ss<0)||(ss>59)){
        return null;
      }
      i_val+=ss.length;
    }
    else if (token=="a") {
      if (val.substring(i_val,i_val+2).toLowerCase()=="am") {
        ampm="AM";
      }
      else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {
        ampm="PM";
      }
      else {
        return null;
      }
      i_val+=2;
    }
    else {
      if (val.substring(i_val,i_val+token.length)!=token) {
        return null;
      }
      else {
        i_val+=token.length;
      }
    }
  }
  // If there are any trailing characters left in the value, it doesn't match
  if (i_val != val.length) { 
    return null; 
  }
  // Is date valid for month?
  if (month==2) {
    // Check for leap year
    if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
      if (date > 29){ 
        return null; 
      }
    }
    else { 
      if (date > 28) { 
        return null; 
      } 
    }
  }
  if ((month==4)||(month==6)||(month==9)||(month==11)) {
    if (date > 30) { 
      return null; 
    }
  }
  // Correct hours value
  if (hh<12 && ampm=="PM") {
    hh=hh-0+12; 
  }
  else if (hh>11 && ampm=="AM") { 
    hh-=12; 
  }
  return new Date(year,month-1,date,hh,mm,ss);
};

// Check if a date string is valid
Date.isValid = function(val,format) {
  return (Date.parseString(val,format) != null);
};

// Check if a date object is before another date object
Date.prototype.isBefore = function(date2) {
  if (date2==null) { 
    return false; 
  }
  return (this.getTime()<date2.getTime());
};

// Check if a date object is after another date object
Date.prototype.isAfter = function(date2) {
  if (date2==null) { 
    return false; 
  }
  return (this.getTime()>date2.getTime());
};

// Check if two date objects have equal dates and times
Date.prototype.equals = function(date2) {
  if (date2==null) { 
    return false; 
  }
  return (this.getTime()==date2.getTime());
};

// Check if two date objects have equal dates, disregarding times
Date.prototype.equalsIgnoreTime = function(date2) {
  if (date2==null) { 
    return false; 
  }
  var d1 = new Date(this.getTime()).clearTime();
  var d2 = new Date(date2.getTime()).clearTime();
  return (d1.getTime()==d2.getTime());
};

// Format a date into a string using a given format string
Date.prototype.format = function(format) {
  format=format+"";
  var result="";
  var i_format=0;
  var c="";
  var token="";
  var y=this.getYear()+"";
  var M=this.getMonth()+1;
  var d=this.getDate();
  var E=this.getDay();
  var H=this.getHours();
  var m=this.getMinutes();
  var s=this.getSeconds();
  var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
  // Convert real date parts into formatted versions
  var value=new Object();
  if (y.length < 4) {
    y=""+(+y+1900);
  }
  value["y"]=""+y;
  value["yyyy"]=y;
  value["yy"]=y.substring(2,4);
  value["M"]=M;
  value["MM"]=Date.LZ(M);
  value["MMM"]=Date.monthNames[M-1];
  value["NNN"]=Date.monthAbbreviations[M-1];
  value["d"]=d;
  value["dd"]=Date.LZ(d);
  value["E"]=Date.dayAbbreviations[E];
  value["EE"]=Date.dayNames[E];
  value["H"]=H;
  value["HH"]=Date.LZ(H);
  if (H==0){
    value["h"]=12;
  }
  else if (H>12){
    value["h"]=H-12;
  }
  else {
    value["h"]=H;
  }
  value["hh"]=Date.LZ(value["h"]);
  value["K"]=value["h"]-1;
  value["k"]=value["H"]+1;
  value["KK"]=Date.LZ(value["K"]);
  value["kk"]=Date.LZ(value["k"]);
  if (H > 11) { 
    value["a"]="PM"; 
  }
  else { 
    value["a"]="AM"; 
  }
  value["m"]=m;
  value["mm"]=Date.LZ(m);
  value["s"]=s;
  value["ss"]=Date.LZ(s);
  while (i_format < format.length) {
    c=format.charAt(i_format);
    token="";
    while ((format.charAt(i_format)==c) && (i_format < format.length)) {
      token += format.charAt(i_format++);
    }
    if (typeof(value[token])!="undefined") { 
      result=result + value[token]; 
    }
    else { 
      result=result + token; 
    }
  }
  return result;
};

// Get the full name of the day for a date
Date.prototype.getDayName = function() { 
  return Date.dayNames[this.getDay()];
};

// Get the abbreviation of the day for a date
Date.prototype.getDayAbbreviation = function() { 
  return Date.dayAbbreviations[this.getDay()];
};

// Get the full name of the month for a date
Date.prototype.getMonthName = function() {
  return Date.monthNames[this.getMonth()];
};

// Get the abbreviation of the month for a date
Date.prototype.getMonthAbbreviation = function() { 
  return Date.monthAbbreviations[this.getMonth()];
};

// Clear all time information in a date object
Date.prototype.clearTime = function() {
  this.setHours(0); 
  this.setMinutes(0);
  this.setSeconds(0); 
  this.setMilliseconds(0);
  return this;
};

// Add an amount of time to a date. Negative numbers can be passed to subtract time.
Date.prototype.add = function(interval, number) {
  if (typeof(interval)=="undefined" || interval==null || typeof(number)=="undefined" || number==null) { 
    return this; 
  }
  number = +number;
  if (interval=='y') { // year
    this.setFullYear(this.getFullYear()+number);
  }
  else if (interval=='M') { // Month
    this.setMonth(this.getMonth()+number);
  }
  else if (interval=='d') { // Day
    this.setDate(this.getDate()+number);
  }
  else if (interval=='w') { // Weekday
    var step = (number>0)?1:-1;
    while (number!=0) {
      this.add('d',step);
      while(this.getDay()==0 || this.getDay()==6) { 
        this.add('d',step);
      }
      number -= step;
    }
  }
  else if (interval=='h') { // Hour
    this.setHours(this.getHours() + number);
  }
  else if (interval=='m') { // Minute
    this.setMinutes(this.getMinutes() + number);
  }
  else if (interval=='s') { // Second
    this.setSeconds(this.getSeconds() + number);
  }
  return this;
};
//FB 1830
function changeCurrencyFormat(tableName,cFormat)
{        
    var table = document.getElementById(tableName);        
    var elTableCells;
    
    if(table != null)
    {
        elTableCells = table.getElementsByTagName("td");
        var c;
        if(elTableCells != null)
        {
            for(c = 0; c < elTableCells.length; c++)
            {
                if(elTableCells[c].innerHTML.indexOf("USD") != -1)
                {                                
                   elTableCells[c].innerHTML = elTableCells[c].innerHTML.replace("USD",cFormat);
                }
            }
        }            
    }
}

//FB 1911

function get_time_difference(earlierDate,laterDate)
{
    var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
    var oDiff = new Object();

    oDiff.days = Math.floor(nTotalDiff/1000/60/60/24);
    nTotalDiff -= oDiff.days*1000*60*60*24;

    oDiff.hours = Math.floor(nTotalDiff/1000/60/60);
    nTotalDiff -= oDiff.hours*1000*60*60;

    oDiff.minutes = Math.floor(nTotalDiff/1000/60);
    nTotalDiff -= oDiff.minutes*1000*60;

    oDiff.seconds = Math.floor(nTotalDiff/1000);

    dhr = parseInt(oDiff.hours, 10);
    dmi = parseInt(oDiff.minutes, 10);
    oDiff.duration = dhr * 60 + dmi;

    return oDiff;
}

//FB 2998 
function DisplayMCUConnectRow(setup,tear,enableBufferZone)
{
    var isVMR = false;
    
    if(document.getElementById("lstVMR") != null && document.getElementById("lstVMR").selectedIndex > 0)
        isVMR = true;
    
    var MCUConnectRow = document.getElementById("MCUConnectRow");
    var MCUConnectDisplayRow = document.getElementById("MCUConnectDisplayRow");
    var lstConferenceType = document.getElementById("lstConferenceType");
    var MCUConnect = document.getElementById("txtMCUConnect"); //ZD 100085
    var MCUDisConnect = document.getElementById("txtMCUDisConnect");
    var conftype = "";
    
    if(lstConferenceType != null)
        conftype = lstConferenceType.value;
    else
        conftype = "2";
     //ZD 100085
    if ((conftype != "2" && conftype != "6" && conftype != "9") || document.getElementById("chkStartNow").checked || document.getElementById("chkPCConf").checked || isVMR == true) //ZD 100513
    {   
        if(MCUConnectRow)
            MCUConnectRow.style.display = "None";
        if(MCUConnectDisplayRow)
            MCUConnectDisplayRow.style.display = "None";

        if (MCUConnect)
            MCUConnect.value = "0";
        if (MCUDisConnect)
            MCUDisConnect.value = "0";
    }
    else
    {
        if(setup == "1" || tear == "1")
        {
            if(MCUConnectRow)
                MCUConnectRow.style.display = "";
            if(MCUConnectDisplayRow)
                MCUConnectDisplayRow.style.display = "";
            //ZD 100085
            if (document.getElementById("chkMCUConnect") != null &&((MCUConnect && MCUConnect.value > 0) || (MCUDisConnect && MCUDisConnect.value > 0)))
                document.getElementById("chkMCUConnect").checked = true;
                
            openMCUConnectRow(setup,tear);
        }
    }
}

function openMCUConnectRow(setup, tear)
{
    if(document.getElementById("chkMCUConnect") != null)
    {
        document.getElementById("MCUConnectDisplayRow").style.display ="none";
        document.getElementById("ConnectCell").style.display = "none";
        document.getElementById("DisconnectCell").style.display = "none";

        if (document.getElementById("chkMCUConnect").checked)
        {
            document.getElementById("MCUConnectDisplayRow").style.display = "";
            
            if(setup == "1")
                document.getElementById("ConnectCell").style.display = "";
            if(tear == "1")
                document.getElementById("DisconnectCell").style.display = "";
        }
    }
}

//ZD 100284
function formatTimeNew(timeText, regText, tformat) {
    
    var isValidate = true;
    var isColon = true;
    
   if(tformat == "1" || tformat == "0")
   {    
	    var tText = document.getElementById(timeText);
        var timeVal = tText.value;
        
        if(timeVal.length < 8)
        {
            if(timeVal.indexOf(':') < 0)
                isColon = false;
            
            if(isColon == true)
            {
                var timSplit = timeVal.split(":");
                
                if(timSplit[0].length == 1)
                    timeVal = "0" + timSplit[0] + ":" + timSplit[1];

                if (tformat == "1") 
                {
                    timeVal = timeVal.replace('a', 'A').replace('p', 'P').replace('m', 'M');

                    if (timeVal.indexOf(' AM') < 0)
                        timeVal = timeVal.replace('AM', ' AM');

                    if (timeVal.indexOf(' PM') < 0)
                        timeVal = timeVal.replace('PM', ' PM');
                }
            }
        }

        var regEpression = document.getElementById(regText).validationexpression;

        if (tformat == "1") {
            //if (timeVal.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1 && isColon == true) {
            if (timeVal.search(regEpression) != -1 && isColon == true) {
                var a_p = "";
                var t = new Date();
                var d = new Date((t.getMonth() + 1) + "/" + t.getDate() + "/" + t.getFullYear() + " " + timeVal); // ZD 100288
                //ZD 103624
                if (d == "Invalid Date")
                    isValidate = false;

                if (isValidate) {
                    var curr_hour = d.getHours();
                    if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

                    if (curr_hour == 0) curr_hour = 12;
                    if (curr_hour > 12) curr_hour = curr_hour - 12;

                    curr_hour = curr_hour + "";
                    if (curr_hour.length == 1)
                        curr_hour = "0" + curr_hour;

                    var curr_min = d.getMinutes();
                    curr_min = curr_min + "";
                    if (curr_min.length == 1)
                        curr_min = "0" + curr_min;

                    document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;
                    document.getElementById(regText).style.display = "None";
                    return true;
                }
            }
            else 
                isValidate = false;
        }
        else if (tformat == "0") {

            if (timeVal.search(regEpression) != -1 && isColon == true && timeVal.length == 5) {
                var a_p = "";
                var t = new Date();
                var d = new Date((t.getMonth() + 1) + "/" + t.getDate() + "/" + t.getFullYear() + " " + timeVal); // ZD 100288
                //ZD 103624
                if (d == "Invalid Date")
                    isValidate = false;

                if (isValidate) {
                    var curr_hour = d.getHours();
                    curr_hour = curr_hour + "";
                    if (curr_hour.length == 1)
                        curr_hour = "0" + curr_hour;

                    var curr_min = d.getMinutes();
                    curr_min = curr_min + "";
                    if (curr_min.length == 1)
                        curr_min = "0" + curr_min;

                    document.getElementById(timeText).value = curr_hour + ":" + curr_min;
                    document.getElementById(regText).style.display = "None";
                    return true;
                }
            }
            else
                isValidate = false;
        }

        if (isValidate == false) 
        {
            document.getElementById(regText).style.display = "";
            return false;
        }
   }
    return true;
}

//ZD 102277
function fnValidateFileName(filename, ctrlNo) {   
    filename = filename.replace(/^.*[\\\/]/, '');
    var ctrlName = "regFNVal" + ctrlNo;
    
    if (/[^\w\.()\s\-]/gi.test(filename)) { // anything but a-zA-Z0-9 underscore, dot, hyphen, space, ( ).
        if (document.getElementById(ctrlName)) {
            document.getElementById(ctrlName).style.display = 'block';
        }
    }
    else {
        if (document.getElementById(ctrlName)) {
            document.getElementById(ctrlName).style.display = 'None';
        }
    }
}

//ZD 102364
function fnUpload1(obj, btnid, grpname) {

    if (!Page_ClientValidate(grpname))
        return Page_IsValid;

    if (obj == null || obj == undefined || obj == "") 
    {
        alert(WithoutImage);
        return false;
    }
    else {

        __doPostBack(btnid, '');
    }
}

function fnUpload3(obj1, obj2, obj3, btnid, grpname) {

    if (!Page_ClientValidate(grpname))
        return Page_IsValid;

    var val = "";
    if (obj1 != null && obj1.value != "")
        val += obj1.value;
    if (obj2 != null && obj2.value != "")
        val += obj2.value;
    if (obj3 != null && obj3.value != "")
        val += obj3.value;
    
    if (val == "") {
        alert(WithoutImage);
        return false;
    }
    else {
    
        __doPostBack(btnid, '');
    }
}