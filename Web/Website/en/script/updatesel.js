/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100899 End*/
//* UpdateDepSel - This class monitors an independent selection box and updates
//				   the options of a dependent selection.


/* Usage:
	A instance of this object needs an independent and dependent select object
	as part of the constructor. Therefore you cannot create an UpdateDepSel object
	until the selects have been created. The best way to achive this is to create
	the updater from an init method called on the body's onload event. Ex:

	<html>
	<head><title></title></head>

	<script language="JavaScript" src="updatesel.js"></script>
	<script language="JavaScript">

	var updater  = null;

	function init()
	{
			//* Define the dependent parameters.
		var depOpt  =new Array( ["A", "AA", "AAA"],["B", "BB", "BBB"],["C", "CC", "CCC", "CCCC"]);
		var depSelInd = new Array( 0,1,2);

			//* Create the updater for 'int'.
		updater = new UpdateDepSel( document.all['ind'], document.all['dep'], depOpt, depOpt, depSelInd);	
	}

	<body onload = "init()">
	...
	<select name="ind" onChange="updater.updateDependent();">
	<option value="a" selected>a</option>
	<option value="b">b</option>
	<option value="c">c</option>
	</select>

	<select name="dep">
	</select>
	...

	</body>
	</html>


</script>


*/


/*
UpdateDepSel constructor

iSel:	the independent select.
dSel:	the dependent select.
dOpt:	an array of string arrays representing the text of the dependent selects.
dVal:	an array of string arrays representing the values of the dependent select
		options. If null values are the same as the text.
dSelInd:the initially selected options of each dependent select. If null the default
		is unselected.
*/
function UpdateDepSel(iSel, dSel, dOpt, dVal, dSelInd)
{
	this.indSel		= iSel;	
	this.prevIndSel = this.indSel.selectedIndex;
	this.depSel		= dSel;		
	this.depOpt		= dOpt;	
	this.depValues	= dVal;
	this.didInit	= false;

	if( dSelInd == null) {
		var count = this.depOpt.length;

		this.depSelInd = new Array();

		for( i=0; i<count; i++)
			this.depSelInd[i] = -1;

	} else {
		this.depSelInd = dSelInd;
	}
	
	this.updateDependent();
}


function UDS_updateDependent(grandfather, father)
{
	var preCount = 0;
	var fatherprecount = 0;
	
//	if (grandfather != null)
//	alert(grandfather.indSel.selectedIndex);

	
	if (grandfather != null) {
//		alert("grandfather.indSel.selectedIndex=" + grandfather.indSel.selectedIndex);
		if (father != null) {
			for (i=0; i<grandfather.indSel.selectedIndex; i++) {
				for (j=0; j<grandfather.depOpt[i].length; j++) {
					preCount += father.depOpt[j].length;
				}
				fatherprecount += grandfather.depOpt[i].length;
			}
		} else {
			alert("error: has grandfather, but no father!");
		}
	}
	
//	alert("fatherprecount=" + fatherprecount);
//	alert("after grandfather, preCount=" + preCount);

	if (father != null) {
		
//		alert("father.indSel.selectedIndex="+father.indSel.selectedIndex+"; father.depOpt[(fatherprecount+0)].length=" + father.depOpt[(fatherprecount+0)].length);
		for (i=0; i<father.indSel.selectedIndex; i++) {
			preCount += father.depOpt[(fatherprecount+i)].length;
		}
	}

//	alert("after father, preCount=" + preCount);
//	alert("final, preCount=" + (preCount + this.indSel.selectedIndex) );
	
	var newOpt = this.depOpt[(preCount + this.indSel.selectedIndex)];
	var newVal = this.depValues[(preCount + this.indSel.selectedIndex)];
	var newCount = newOpt.length;
	var oldCount = (this.depSel.options == null) ? 0 : this.depSel.options.length;
//	var	maxCount = (oldCount >= newCount) ? oldCount : newCount;
	
	if( this.didInit)
		this.depSelInd[ this.prevIndSel] = this.depSel.selectedIndex;
	else
		this.didInit = true;
	
	for( i=0; i<oldCount; i++)
		this.depSel.options[i] = null;
	
	//this.depSel.options = new Array( newCount);
	
	this.depSel.options.length = 0;
	
	for( i=0; i< newCount; i++)
	{
		this.depSel.options[i] = new Option();
		this.depSel.options[i].text = newOpt[i];
		this.depSel.options[i].value = newVal[i];
	}

	this.depSel.selectedIndex = this.depSelInd[(preCount + this.indSel.selectedIndex)];
	this.prevIndSel = preCount + this.indSel.selectedIndex;
}

/*
Call this method when the independent select changes. It will update the dependent
select to reflect the change.
*/
UpdateDepSel.prototype.updateDependent = UDS_updateDependent;