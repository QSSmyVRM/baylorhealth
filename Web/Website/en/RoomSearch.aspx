﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_RoomSearch" %><%--FB 2694--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--ZD 100393--%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
Namespace="DevExpress.Web.ASPxPager" TagPrefix="devex" %>

<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>

<script type="text/javascript" src="script/errorList.js"></script>

<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>

<script type="text/javascript" src="extract.js"></script>

<script type="text/javascript" src="script/mousepos.js"></script> <%-- ZD 102723 --%>

<script type="text/javascript" src="script/showmsg.js"></script> <%-- ZD 102723 --%>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/Point2Point.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>

<%--ZD 100420 Start--%>
<style type="text/css">

a img { outline:none;
    text-decoration:none;
    border:0;
}

.iemask
{
    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; /* ZD 102404 */
}
</style>
<%--ZD 101918 Start--%>
<style type="text/css">
.tdRoomSearch
{
	padding: 5px;
	color: black; 
	background-color: #E0E0E0;
	border: #C1C1C1  1px solid;
	text-align:Center;
	border-collapse: collapse;
	font:9pt Tahoma;
}

.txtRoomSearch
{
	padding: 5px;
	color: black; 
	background-color: #E0E0E0;
	border: #C1C1C1  1px solid;
	border-collapse:collapse;
	padding-left : 2px;
	padding-right  :6px;
	padding-top :2px;
	padding-bottom:2px;
}
</style>
<%--ZD 101918 End--%>
<%--ZD 100420 - End--%>

<script type="text/javascript">
    // <![CDATA[

    var isIE = false; //ZD 100420
    if (navigator.userAgent.indexOf('Trident') > -1)
        isIE = true;
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //FB 2814 - Starat
    function DataLoading(val) {
        if (document.getElementById("dataLoadingDIV") == null)
            return false;
        document.getElementById("dataLoadingDIV").style.position = 'absolute'; //FB 2814

        document.getElementById("dataLoadingDIV").style.left = window.screen.width / 2 - 100;
        document.getElementById("dataLoadingDIV").style.top = 100;
        
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //FB 2814 - End

    //ZD 102123 - Start
    function pageBarFirstButton_Click() {

        try {

            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }

        }
        catch (exception)
{ }

    }

    function pageBarPrevButton_Click() {
        try {

            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }
        }
        catch (exception)
{ }



    }
    function pageBarNextButton_Click() {
        try {
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }
        }
        catch (exception)
{ }


    }
    function pageBarLastButton_Click(s, e) {
        try {
            
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }
        }
        catch (exception)
{ }

    }
    function pageBarTextBox_Init(s, e) {
        try {
            s.SetText(s.cpText);
        }
        catch (exception)
{ }
    }
    function pageBarTextBox_KeyPress(s, e) {
        try {

            if (e.htmlEvent.keyCode != 13)
                return;
            e.htmlEvent.cancelBubble = true;
            e.htmlEvent.returnValue = false;
            var pageIndex = (parseInt(s.GetText(),10) <= 0) ? 0 : parseInt(s.GetText(),10) - 1; // ZD 101722

            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }
        }
        catch (exception)
{ }



    }
    function pageBarTextBox_ValueChanged(s, e) {
        try {

            var pageIndex = (parseInt(s.GetText(),10) <= 0) ? 0 : parseInt(s.GetText(),10) - 1; // ZD 101722

            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }
        }
        catch (exception)
{ }

    }
    function pagerBarComboBox_SelectedIndexChanged() {
        try {
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else (drp.value == "2")
                grid.GotoPage(0);
            }
        }
        catch (exception)
{ }
    }
    //ZD 102123 - End

    function fnTabNav(cur, nxt, prv, event) {      
     if (event.keyCode == 9) 
        {
            if (event.shiftKey) {
                if (cur != null) {
                    if (cur == "plus") {
                        document.getElementById(prv).focus();
                        return false;
                    }
                }
            }
        }
        var cur = document.getElementById(cur);
        var nxt = document.getElementById(nxt);
        var prv = document.getElementById(prv);

        if (event.keyCode == 9) 
        {
            if (event.shiftKey) {
                if (cur != null) {
                    if (cur.src.toLowerCase().indexOf('plus') > -1)
                        if (prv != null) {
                        prv.focus();
                        return false;
                    }
                }
                
            }
            else {
                if (cur != null) {
                    if (cur.src.toLowerCase().indexOf('plus') > -1)
                        if (nxt != null) {
                        nxt.focus();
                        return false;
                    }
                }   
            }
        }
        
    }
    // ]]>

   //ZD 101918 start
    function fnClear() {
        document.getElementById("txtLsearchtier1").value = "";
        document.getElementById("txtLsearchtier2").value = "";
        document.getElementById("txtLsearchRoomname").value = "";
        document.getElementById("txtLsearchMaxCapacity").value = "";
    }
    function fnDClear() {
        document.getElementById("txtDsearchtier1").value = "";
        document.getElementById("txtDsearchtier2").value = "";
        document.getElementById("txtDsearchRoomname").value = "";
        document.getElementById("txtDsearchMaxCapacity").value = "";
    }
   // ZD 101918 End
</script>

<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script type="text/javascript">
    var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10) - 1, parseInt("<%=DateTime.Now.Day%>", 10),
      parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
</script>

<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>

<script type="text/javascript" src="script/cal-flat.js"></script>

<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>

<script type="text/javascript" src="script/calendar-setup.js"></script>

<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Room Search</title>
    <%--<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1861--%>
    <%--FB 1982--%>
</head>
<%--FB 1861--%>
<!--  #INCLUDE FILE="../en/inc/Holiday.aspx"  -->
<body style="margin: 0 0 0 0">
    <form id="FrmRoomSearch" runat="server" class="tabContents" autocomplete="off"><%--ZD 104844--%>
    <div id="topRoomDiv" runat="server" class="iemask" style="width:1600px; height:1400px; z-index: 1000; position:fixed; left:0px; top:0px; background-color:White; opacity:0.5; display:none;"></div> <%-- ZD 102404 --%>
    <asp:ScriptManager ID="RoomsearchScript" runat="server">
        <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		</Scripts>
    </asp:ScriptManager>
     <%--ZD 100420--%>
     <script type="text/javascript">
         var prm = Sys.WebForms.PageRequestManager.getInstance();
         prm.add_beginRequest(BeginRequestHandler);
         prm.add_endRequest(EndRequestHandler);
         function BeginRequestHandler(sender, args) {
             window.parent.scroll(0, 0);
         }
         function EndRequestHandler(sender, args) {
             window.parent.scroll(0, 0);
         }

         function fnUpdateGridWidth() { // ZD 101276
             if (navigator.userAgent.indexOf('Trident') > -1) {
                 if (document.getElementById('grid2_DXMainTable') != null)
                     document.getElementById('grid2_DXMainTable').width = "100%";
                 if (document.getElementById('grid_DXMainTable').width != null)
                     document.getElementById('grid_DXMainTable').width = "100%";
             }
             if ('<%=Session["language"]%>' == 'sp' && screen.width < 1050) { // ZD 101344
                 var width = Math.round((61.5 / 100) * screen.width); // 61.5% width
                 if (grid != null) {
                     grid.SetWidth(width);
                     grid.AdjustControl();
                 }
                 if (grid2 != null) {
                     grid2.SetWidth(width);
                     grid2.AdjustControl();
                 }
             }
             //if (navigator.userAgent.indexOf('Trident') == -1)
                 //document.getElementById("divLicense").style.marginTop = "10px"
         }
     </script>
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="helpPage" value="84" />
    <input type="hidden" id="hdnRoomIDs" runat="server" />
    <input runat="server" id="IsSettingsChange" type="hidden" />
    <input type="hidden" id="hdnCapacityH" runat="server" />
    <input type="hidden" id="hdnCapacityL" runat="server" />
    <input type="hidden" id="hdnAV" runat="server" value="0" />
    <input type="hidden" id="hdnMedia" runat="server" />
    <input type="hidden" id="hdnLoc" runat="server" value="0" />
    <input type="hidden" id="hdnName" runat="server" />
    <input type="hidden" id="hdnZipCode" runat="server" />
    <input type="hidden" id="hdnAvailable" runat="server" />
    <input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--FB 2588--%>
	<input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--FB 2588--%>
    <input type="hidden" runat="server" id="hdnChgViewType" name="hdnChgViewType"  /><%--ZD 104586--%>
	
	<input name="locstrname" type="hidden" id="RemainingHotdeskingRoomCount" runat="server"  />  <%--FB 2694--%>
	<input type="hidden" runat="server" id="hdnIsChanged" value="0" /> <%--ZD 102315--%>
    <input type="hidden" runat="server" id="hdnSelRoomID" /> <%--ZD 102916--%>
    <input type="hidden" runat="server" id="hdnSelectionVal" value="0" /> <%--ZD 102916--%>
    <div id="dataLoadingDIV" onload="javascript:DataLoading('1');" name="dataLoadingDIV" align="center" style="display:none">
         <img border='0' src='image/wait1.gif' alt='Loading..' />
    </div><%--ZD 100678 End--%>
    <%
        if (Request.QueryString["hf"] != null)
        {
            if (Request.QueryString["hf"].ToString() == "1")
            {
    %>
    <table width="100%" border="0">
        <tr>
            <td align="center">
                <h3>
                    <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_RoomSearch%>" runat="server"></asp:Literal>
                    <button name="close" id="btnClose" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:return ClosePopup();">
						<asp:Literal Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal></button> <%--ZD 100642--%>
                </h3>
            </td>
        </tr>
    </table>
    <%              
                
        }
        }   
    %>
    <div class="tabContents" style="height: 545px; vertical-align: super; width: 100%;">
        <table width="100%" class="tabContents" border="0px">
            <tr valign="top">
                <td style="width: 22%">
                    <br />
                    <asp:Panel ID="Filters" runat="server" Height="590px" CssClass="treeSelectedNode"
                        BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Auto"><%-- 101918--%>
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Panel ID="FavPnl" runat="server">
                                        <table width="94%">
                                            <tr id="trActDct" runat="server" style="display: none;">
                                                <td>
                                                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, RoomSearch_Show%>" runat="server"></asp:Literal></span>
                                                    <asp:DropDownList ID="DrpActDct" CssClass="altText" Width="125" runat="server" AutoPostBack="false"
                                                        onchange="javascript:ShowActDct()">
                                                        <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, OnlyActive%>"></asp:ListItem> <%--FB 2565--%>
                                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, OnlyDeactived%>"></asp:ListItem>
                                                        <%--<asp:ListItem Value="2">All</asp:ListItem>--%> <%--FB 2565--%>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="chkFavourites" Text=" <%$ Resources:WebResources, RoomSearch_chkFavourites%> "
                                                        onclick="javascript:ChkFavorites()" />
                                                </td>
                                            </tr>
                                            <%--FB 2426 Start--%>
                                            <tr id="trGuestRooms" runat="server" style="display: block; margin-left:-2px "> <%--ZD 100393--%>
                                                <td>
                                                    <asp:CheckBox runat="server" id="chkGuestRooms" text="<%$ Resources:WebResources, RoomSearch_chkGuestRooms%>" onclick="javascript:ChkGuestRooms()"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <%--FB 2426 End--%>
                                            <tr id="trchkVMR" runat="server">
                                                <%--FB 2448--%>
                                                <td>
                                                    <asp:CheckBox runat="server" id="chkIsVMR" text="<%$ Resources:WebResources, RoomSearch_chkIsVMR%>" onclick="javascript:ChkVirtualMeetingRooms()"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr id="trAvlChk" runat="server">
                                                <td>
                                                    <asp:CheckBox runat="server" id="Available" text="<%$ Resources:WebResources, RoomSearch_Available%>" onclick="javascript:EndDateValidation()"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <%--FB 2694 Start--%>
                                            <tr id="trHotdesking" runat="server">
                                                <td>
                                                    <asp:CheckBox runat="server" id="chkHotdesking" text="<%$ Resources:WebResources, RoomSearch_chkHotdesking%>" onclick="javascript:HotdeskingRooms()"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <%--FB 2694 End--%>
                                            <tr id="trDateFromTo" runat="server">
                                                <td>
                                                    <table>
                                                        <tr><%--ZD 101714--%>
                                                            <td align="left" class="blackblodtext" nowrap>
                                                                <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_From%>" runat="server"></asp:Literal>
                                                            </td>
                                                            <td nowrap="nowrap">
                                                                <asp:TextBox ID="txtRoomDateFrom" runat="server" Width="65px" Text="" CssClass="altText" />
                                                                <a href="" onclick="this.childNodes[0].click();return false;"> <%--ZD 104097 Start--%>
                                                                  <%--<img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerFrom" alt="Date Selector"
                                                                    style="cursor: pointer;vertical-align:middle;" title="<%$ Resources:WebResources, Dateselector%>" 
                                                                    runat="server" onclick="return showCalendar('txtRoomDateFrom', 'cal_triggerFrom', 0, '<%=format%>');" />--%>
                                                                 <img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_triggerFrom" 
                                                                    style='cursor: pointer;<%=((txtRoomDateFrom.Enabled)? "" : "display:none;")%>'
                                                                    title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" 
                                                                    onclick="return showCalendar('<%=txtRoomDateFrom.ClientID %>', 'cal_triggerFrom', 1, '<%=format%>');" /></a> <%--ZD 104097 End--%>
                                                                <asp:RequiredFieldValidator ID="reqRoomFrom" Enabled="false" ControlToValidate="txtRoomDateFrom"
                                                                    Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="DateSubmit" runat="server" />
                                                                <mbcbb:ComboBox ID="confRoomStartTime" runat="server" CssClass="altSelectFormat"
                                                                    Rows="10" CausesValidation="True" Width="80px" AutoPostBack="false"><%-- ZD 100393--%>
                                                                    <asp:ListItem Text="12:00 AM">
                                                                    </asp:ListItem>
                                                                </mbcbb:ComboBox>
                                                                <asp:RequiredFieldValidator ID="reqRoomStartTime" runat="server" ControlToValidate="confRoomStartTime"
                                                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>" />
                                                                <asp:RegularExpressionValidator ID="regRoomStartTime" runat="server" ControlToValidate="confRoomStartTime"
                                                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" />
                                                                <%-- FB Case 371 Saima --%>
                                                            </td>
                                                        </tr>
                                                        <tr id="TrRoomAvaible" runat="server">
                                                            <td align="left" class="blackblodtext" nowrap valign="top">
                                                                <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_To%>" runat="server"></asp:Literal>
                                                            </td>
                                                            <td nowrap="nowrap">
                                                                <asp:TextBox ID="txtRoomDateTo" Width="65px" runat="server" Text="" CssClass="altText" />
                                                                 <a href="" onclick="this.childNodes[0].click();return false;"> <%--ZD 104097 Start--%>
                                                                    <%--<img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerTo"  alt="Date Selector"
                                                                    style="cursor: pointer;vertical-align:middle;" title="<%$ Resources:WebResources, Dateselector%>" 
                                                                    runat="server" onclick="return showCalendar('txtRoomDateTo', 'cal_triggerTo', 0, '<%=format%>');" />--%>
                                                                    <img src="image/calendar.gif" alt="Date Selector" border="0" width="20" height="20" id="cal_triggerTo" 
                                                                    style='cursor: pointer;<%=((txtRoomDateTo.Enabled)? "" : "display:none;")%>'
                                                                    title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" 
                                                                    onclick="return showCalendar('<%=txtRoomDateTo.ClientID %>', 'cal_triggerTo', 1, '<%=format%>');" />
                                                                 </a>  <%--ZD 104097 End--%>
                                                                <mbcbb:ComboBox ID="confRoomEndTime" runat="server" CssClass="altSelectFormat"
                                                                    Rows="10" Width="80px" CausesValidation="True" AutoPostBack="false"> <%--ZD 100393--%>
                                                                    <asp:ListItem Text="12:00 AM">
                                                                    </asp:ListItem>
                                                                </mbcbb:ComboBox>
                                                                <asp:RequiredFieldValidator ID="reqRoomEndTime" runat="server" ControlToValidate="confRoomEndTime"
                                                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>" />
                                                                <asp:RegularExpressionValidator ID="regRoomEndTime" runat="server" ControlToValidate="confRoomEndTime"
                                                                    Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] [A|a|P|p][M|m]" />
                                                                <asp:RequiredFieldValidator ID="reqRoomTo" Enabled="false" ControlToValidate="txtRoomDateTo"
                                                                    Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="DateSubmit" runat="server" />
                                                                <br />
                                                                <input type="button" name="DateSubmit" runat="server" id="DateSubmit" value="<%$ Resources:WebResources, Submit%>"
                                                                    class="altMedium0BlueButtonFormat" style="width: 90px;" onclick="javascript:EndDateValidation('1')">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr><%--ZD 104482--%>
                            <tr>
                                <td align="center">
                                    <hr style="height: 2px; color: Black;" />
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="ExtenderName" runat="server" TargetControlID="NameTable"
                                        ImageControlID="RmNameImg" CollapseControlID="RmNameImg" ExpandControlID="RmNameImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" CollapsedImage="image/loc/nolines_plus.gif"
                                        Collapsed="false" CollapsedSize="30" />
                                    <asp:Panel ID="NameTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="" onclick="this.childNodes[0].click();return false;"><img id="RmNameImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse" /></a>
                                                    <span class=""><asp:Literal Text="<%$ Resources:WebResources, RoomSearch_RoomName%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TxtNameSearch" CssClass="altText" runat="server" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input id="Button1" type="button" name="NameSubmit" runat="server" value="<%$ Resources:WebResources, Submit%>" class="altMedium0BlueButtonFormat"
                                                        onfocus="this.blur()" style="width: 80px;" onclick="javascript:NameSearch()"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr align="center" style="display: none;">
                                <td align="center">
                                    <span class="blackbigblodtext"><asp:Literal Text="<%$ Resources:WebResources, RoomSearch_OR%>" runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                            <%--101175 Start--%>
                            <tr id="trRmName" runat="server"> <%--ZD 104482--%>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="RoomPanel"
                                        ImageControlID="RoomImg" CollapseControlID="RoomImg" ExpandControlID="RoomImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif" CollapsedSize="30" />
                                    <asp:Panel ID="RoomPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%" style="height:200px;">
                                            <tr valign="top">
                                                 <td class="tableHeader" nowrap="nowrap" colspan="2">
                                                    <a href="" id="ancRoomImg" onkeydown="javascript:return fnTabNav('RoomImg','ancCap','null',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="RoomImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a> 
                                                    <span class=""><asp:Literal  Text="<%$ Resources:WebResources, TierRoom%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    <span class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, TopTier%>" runat="server"></asp:Literal></span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTier1" Width="100" runat="server" CssClass="altText" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, MiddleTier%>" runat="server"></asp:Literal></span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTier2" Width="100" runat="server" CssClass="altText" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, RoomName%>" runat="server"></asp:Literal></span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRoomName" Width="100" runat="server" CssClass="altText" ></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <button name="RoomSubmit" class="altMedium0BlueButtonFormat" type="button" style="width: 80px;" onclick="javascript:TierSearch();"> <asp:Literal  runat="server" Text="<%$ Resources:WebResources, Submit%>" /></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <%--101175 End--%>
                            <tr id="trCapacity" runat="server"> <%--ZD 104482--%>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="CapacityExtender" runat="server" TargetControlID="CapacityPanel"
                                        ImageControlID="CapacitImg" CollapseControlID="CapacitImg" ExpandControlID="CapacitImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="CapacityPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="" id="ancCap" onkeydown="javascript:return fnTabNav('CapacitImg','ancRoomImg','null',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="CapacitImg" runat="server" src="image/loc/nolines_plus.gif" alt="<%$ Resources:WebResources, SettingSelect2_Expand%>"/></a><%--101175 End--%>
                                                    <span class=""><asp:Literal  Text="<%$ Resources:WebResources, RoomSearch_Capacity%>" runat="server"></asp:Literal></span><%--ZD 102314--%>                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink NavigateUrl="#" ID="Any" Style="cursor: pointer;" runat="server" Text="<%$ Resources:WebResources, RoomSearch_Any%>" onclick="javascript:ChangeLbl('Any','Any')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink NavigateUrl="#" ID="Ten" Style="cursor: pointer;" runat="server" Text="<%$ Resources:WebResources, RoomSearch_Ten%>" onclick="javascript:ChangeLbl('Ten','0 - 10')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink NavigateUrl="#" ID="Twenty" Style="cursor: pointer;" runat="server" Text="<%$ Resources:WebResources, RoomSearch_Twenty%>"
                                                        onclick="javascript:ChangeLbl('Twenty','11 - 20')"></asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink NavigateUrl="#" ID="TwentFive" Style="cursor: pointer;" runat="server" Text="<%$ Resources:WebResources, RoomSearch_TwentFive%>"
                                                        onclick="javascript:ChangeLbl('TwentFive','20+')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TxtSearchL" CssClass="altText" onkeyup="javascript:chkLimit(this,'u');"
                                                        Width="30px" runat="server" />
                                                    &nbsp;&nbsp;-&nbsp;&nbsp;
                                                    <asp:TextBox ID="TxtSearchH" CssClass="altText" onkeyup="javascript:chkLimit(this,'u');"
                                                        Width="30px" runat="server" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input id="btnSubmitCapacity" runat="server" type="button" name="Submit" value="<%$ Resources:WebResources, Submit%>" class="altMedium0BlueButtonFormat"
                                                        style="width: 90px;" onclick="javascript:ChangeLbl('','')" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr style="display:none";> <%--ZD 104482--%>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="AvExtender" runat="server" TargetControlID="AVTable"
                                        CollapseControlID="AVImg" ImageControlID="AVImg" ExpandControlID="AVImg" ExpandedImage="image/loc/nolines_minus.gif"
                                        Collapsed="true" CollapsedImage="image/loc/nolines_Plus.gif" CollapsedSize="30" />
                                    <asp:Panel ID="AvTable" runat="server" >
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a id="ancAVImg" href="" onkeydown="javascript:return fnTabNav('AVImg','ancCntryImg','ancCap',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="AVImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal  Text="<%$ Resources:WebResources, RoomSearch_AVItems%>" runat="server"></asp:Literal></span>
                                                    <%--ZD 101225 Starts--%>
                                                    <br />
                                                    <br />
                                                    <input name="group1" type="radio" id="Av_and" runat="server" style="margin-left:22px;" /> <%--ZD 101175--%>
                                                    <span class="" ><asp:Literal ID="Literal20" Text='<%$ Resources:WebResources, AND%>' runat='server'></asp:Literal></span> 
                                                    <input name="group1" id="Av_or" checked runat="server" type="radio" /> <%--ZD 101175--%>
                                                    <span class=""><asp:Literal ID="Literal21"  Text='<%$ Resources:WebResources, RoomSearch_OR%>' runat='server'></asp:Literal></span> 
                                                    <%--ZD 101225 Ends--%>
                                                    
                                                 </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBoxList runat="server" ID="AVlist" DataValueField="Name" DataTextField="Name" /> <%--ZD 101175--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="btnSubmitAVlist" type="button" name="Submit" value="<%$ Resources:WebResources, Submit%>" runat="server" class="altMedium0BlueButtonFormat"
                                                        style="width: 90px;" onclick="javascript:AVItemChanged()">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trCountry" runat="server"> <%--ZD 104482--%>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="BtnUpdateStates" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <ajax:CollapsiblePanelExtender ID="LocExtender" runat="server" TargetControlID="LocPanel"
                                                ImageControlID="CntryImg" CollapseControlID="CntryImg" ExpandControlID="CntryImg"
                                                CollapsedImage="image/loc/nolines_plus.gif" Collapsed="true" ExpandedImage="image/loc/nolines_Minus.gif"
                                                CollapsedSize="30" />
                                            <asp:Panel ID="LocPanel" runat="server">
                                                <table class="treeSelectedNode" width="100%" border="0">
                                                    <tr> <%--ZD 101047 - Done UI Changes--%>
                                                        <td class="tableHeader" nowrap="nowrap" colspan="2"><%--FB 2657--%>
                                                            <a id="ancCntryImg" href="" onkeydown="javascript:return fnTabNav('CntryImg','ancMediaImg','ancAVImg',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="CntryImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                            <span class=""><asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, RoomSearch_CountryStatePro%>" runat="server"></asp:Literal></span><%--FB 2657--%>
                                                        </td>
                                                    </tr>
                                                    <tr></tr>
                                                    <tr></tr>
                                                    <tr>
                                                        <td>
                                                            <span class="blackblodtext"><asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, RoomSearch_Country%>" runat="server"></asp:Literal></span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="lstCountry" CssClass="altText" Width="125" runat="server" DataTextField="Name"
                                                                DataValueField="ID" AutoPostBack="false" onchange="javascript:ChangeCountryorState()" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="blackblodtext"><asp:Literal ID="Literal24" Text="<%$ Resources:WebResources, RoomSearch_StateProvince%>" runat="server"></asp:Literal></span><%--FB 2657--%> 
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="lstStates" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                                AutoPostBack="false" DataValueField="ID" onchange="javascript:ChangeCountryorState('1')" />
                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:DropDownList ID="lstStates2" Visible="false" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                                AutoPostBack="false" DataValueField="ID" onchange="javascript:ChangeCountryorState('1')" />
                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:DropDownList ID="lstStates3" Visible="false" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                                AutoPostBack="false" DataValueField="ID" onchange="javascript:ChangeCountryorState('1')" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class="blackblodtext" align="center" >
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button name="ZipSubmit" class="altMedium0BlueButtonFormat" type="button" style="width: 80px;" onclick="javascript:RefreshRooms();">
                                                            <asp:Literal ID="Literal25" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            <span class="blackblodtext"><asp:Literal ID="Literal26" Text="<%$ Resources:WebResources, RoomSearch_OR%>" runat="server"></asp:Literal></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="blackblodtext"><asp:Literal ID="Literal27" Text="<%$ Resources:WebResources, RoomSearch_PostalCode%>" runat="server"></asp:Literal></span><%--FB 2657--%>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText" onkeyup="javascript:chkZip();"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                                runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters38%>"
                                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$" /><%--FB 2222--%>
                                                            &nbsp;&nbsp;
                                                            <button name="ZipSubmit" class="altMedium0BlueButtonFormat" type="button" style="width: 80px;" onclick="javascript:ZipCodeCheck();">
                                                            <asp:Literal ID="Literal28" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Button ID="BtnUpdateStates" Style="display: none;" runat="server" OnClick="BindCountry" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr  id="trMedia" runat="server"> <%--ZD 104482--%>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="MediaExtender" runat="server" TargetControlID="MediaPanel"
                                        ImageControlID="MediaImg" CollapseControlID="MediaImg" ExpandControlID="MediaImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="MediaPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="" id="ancMediaImg" onkeydown="javascript:return fnTabNav('MediaImg','ancPhotImg','ancCntryImg',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="MediaImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a> 
                                                    <span class=""><asp:Literal ID="Literal29" Text="<%$ Resources:WebResources, ManageRoomProfile_Media%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><br />
                                                    <asp:CheckBox runat="server" ID="MediaNone" Text=" <%$ Resources:WebResources, None%>" Checked="true" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="MediaAudio" Text=" <%$ Resources:WebResources, Audio%>" Checked="true" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="MediaVideo" Text=" <%$ Resources:WebResources, Video%>" Checked="true" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trPhoto" runat="server"> <%--ZD 104482--%>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="PhotoExtender" runat="server" TargetControlID="PhotoTable"
                                        ImageControlID="PhotImg" CollapseControlID="PhotImg" ExpandControlID="PhotImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="PhotoTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="" id="ancPhotImg" onkeydown="javascript:return fnTabNav('PhotImg','ancHandAccs','ancMediaImg',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="PhotImg" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal ID="Literal30" Text="<%$ Resources:WebResources, RoomSearch_Photos%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><br />
                                                    <asp:CheckBox runat="server" ID="PhotosOnly" Text=" <%$ Resources:WebResources, RoomSearch_PhotosOnly%>" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr id="trHandi" runat="server"> <%--ZD 104482--%>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="HandiExtend" runat="server" TargetControlID="HandicapTable"
                                        CollapseControlID="HandAccs" ImageControlID="HandAccs" ExpandControlID="HandAccs"
                                        ExpandedImage="image/loc/nolines_Minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="45" /> <%--ZD 101714--%>
                                    <asp:Panel ID="HandicapTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <a href="" id="ancHandAccs" onkeydown="javascript:return fnTabNav('HandAccs','Reset1','ancPhotImg',event);" onmouseup="if(!isIE){this.childNodes[0].click();return false;}" onclick="this.childNodes[0].click();return false;"><img id="HandAccs" runat="server" src="image/loc/nolines_plus.gif" alt="Expand/Collapse"/></a>
                                                    <span class=""><asp:Literal ID="Literal31" Text="<%$ Resources:WebResources, RoomSearch_HandicappedAcc%>" runat="server"></asp:Literal></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><br />
                                                    <asp:CheckBox runat="server" ID="HandiCap" Text=" <%$ Resources:WebResources, RoomSearch_HandiCap%>" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <button type="button" name="reset" class="altMedium0BlueButtonFormat" onkeydown="javascript:return fnTabNav('plus','null','ancHandAccs',event);"
                                        id="Reset1" onclick="javascript: document.location.href = document.location.href.replace('#','');" style="width: 90px;">
                                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button> <%-- ZD 100369 508 Issue ZD 104482--%>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td style="width: 78%; height: 540px;">
                    <asp:UpdatePanel ID="RoomsUpdate" UpdateMode="Conditional" runat="server" RenderMode="Inline">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnRefreshRooms" />
                        </Triggers>
                        <ContentTemplate>
                            <%--alignment fix--%>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <center>
                                            <asp:Label ID="LblError" CssClass="lblError" runat="server"></asp:Label></center>
                                            <label id="errormsg" style="text-align: center; font-family: Verdana; color: Red; display:none;" >Only one Endpoint can be selected as Caller</label>
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 70%">
                                                    <%--ZD 101175 Starts--%>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblViewType" Text="<%$ Resources:WebResources, RoomSearch_lblViewType%>" Visible="false" CssClass="blackblodtext"></asp:Label>&nbsp;
                                                                <asp:CheckBox ID="ChkUserRoomViewType" Visible="false" runat="server" AutoPostBack="true" OnCheckedChanged="ChangeRoomViewType" style="vertical-align:bottom" /> <%--ZD 100621 End--%>                                                                    
                                                                 <%--ZD 102123 Starts--%>
                                                                <asp:DropDownList ID="DrpDwnListView" CssClass="altText" runat="server" AutoPostBack="true" onchange="javascript:return fnHotdesking();"
                                                                    OnSelectedIndexChanged="DrpDwnListView_SelectedIndexChanged">
                                                                    <asp:ListItem Text="<%$ Resources:WebResources, ListView%>" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="<%$ Resources:WebResources, DetailsView%>" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="<%$ Resources:WebResources, GraphicView%>" Value="3"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%--ZD 102123 Ends--%>
                                                                <div style="float:right" >
                                                                    <asp:Label ID="lblrecord" runat="server" Text="<%$ Resources:WebResources, RecordsPerPage%>" CssClass="blackblodtext" ></asp:Label>
                                                                    <asp:DropDownList ID="DrpRecordsperPage" CssClass="altText" runat="server" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="DrpRecordsperPage_SelectedIndexChanged" Visible="true">
                                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                   </table>
                                                    <%--ZD 101175 Ends--%>
                                                    <input type="hidden" id="hdnView" runat="server" />
                                                    <input type="hidden" id="addroom" value="0" runat="server" />
                                                    <input type="hidden" id="hdnDelRoom" value="0" runat="server" />
                                                    <input type="hidden" id="hdnDelRoomID" runat="server" />
                                                    <input type="hidden" id="hdnEditroom" value="0" runat="server" />
                                                    <input type="hidden" id="hdnImportErr" value="0" runat="server" /> <%--ZD 100619--%>
                                                    <%--FB 1796--%>
                                                    <input runat="server" id="hdnTimeZone" type="hidden" />
                                                    <input runat="server" id="hdnServiceType" type="hidden" /><%--FB 2219--%>
                                                    <input runat="server" id="selectedlocframe" type="hidden" />
                                                    <input runat="server" id="SelectedRoomNameValue" type="hidden" /> <%--ZD 101175--%>
                                                    <input runat="server" id="SelectedRoomNew" type="hidden" /><%--ZD 102123--%>
                                                    <input  id="SelectedRoomNameValuetemp" type="hidden" /> <%--ZD 101175--%>
                                                    <input type="hidden" id="hdnVMRRoomadded" runat="server" /><%--FB 2448--%>
                                                    <input type="hidden" id="locstr" name="locstr" value="" runat="server" />
                                                    <input type="hidden" id="Tierslocstr" name="Tierslocstr" value="" runat="server" />
                                                    <input type="hidden" id="hdnTierIDlocStr" name="hdnTierIDlocStr" value="" runat="server" /> <%--ZD 102458--%>
                                                    <asp:Panel ID="PanelRooms" runat="server" Height="560px" CssClass="treeSelectedNode"
                                                        BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"> <%--ZD 101918--%>
                                                        <div align="center" id="conftypeDIV" style="width: 100%;" class="treeSelectedNode">
                                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                <tr id="DetailsView" runat="server" style="display: none;">
                                                                    <td width="40%" align="left" style="font-weight: bold; font-size: small; color: green;
                                                                        font-family: arial" valign="middle">
                                                                        <%--Edited for FF--%>
                                                                        <%--ZD 101175 - Start --%>
                                                                        <table width="100%" border ="0">
                                                                            <tr>
                                                                                <td style="width:60%"> <%--101918--%>
                                                                                    <devex:ASPxPager ID="ASPxPager1" runat="server" ItemsPerPage="1" Width="100px" OnPageIndexChanged="ASPxPager_PageIndexChanged"
                                                                                        style="float: left; clear: both; margin-top: 16px"> 
                                                                                        <LastPageButton Visible="True"></LastPageButton>
                                                                                        <FirstPageButton Visible="True"></FirstPageButton>
                                                                                        <Summary Position="Inside" Text="<%$ Resources:WebResources, GridViewPageText%>" />
                                                                                    </devex:ASPxPager>
                                                                                </td>
                                                                                <td  style="padding-top:16px;width:40%" > <%--101918--%>
                                                                                    <div style="float:right" >
                                                                                        <asp:Label  ID="Label2" Text="<%$ Resources:WebResources, InventoryManagement_TotalRecords%>" runat="server" Font-Bold="true" ></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblTotalRecords" ></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                             <tr> <%--ZD 101918 - Start --%>
                                                                                <td colspan="2" >
                                                                                    <table width="100%" border ="0" style="border-collapse:collapse;margin-left:-3px">
                                                                                        <tr>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                               <asp:Literal Text="<%$ Resources:WebResources, Tier1%>" runat="server"></asp:Literal> 
                                                                                            </td>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal  Text="<%$ Resources:WebResources, Tier2%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, approvalstatus_RoomName%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_MaximumCapacit%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td>
                                                                                                
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtDsearchtier1" runat="server" style="width:100%" />
                                                                                            </td>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtDsearchtier2" runat="server" style="width:100%" />
                                                                                            </td>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtDsearchRoomname" runat="server" style="width:100%" />
                                                                                            </td>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtDsearchMaxCapacity" runat="server" style="width:100%" />
                                                                                                <asp:RegularExpressionValidator ID="RegDSearchMaxCapacity" ControlToValidate="txtDsearchMaxCapacity" ValidationGroup="SearchSubmit" 
                                                                                                Display="dynamic" runat="server"  ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                                            </td>
                                                                                            <td nowrap="nowrap" valign="top" align="center">
                                                                                                <asp:Button ID="btnDSearch" Text='<%$ Resources:WebResources, Search%>' runat="server" CssClass ="altMedium0BlueButtonFormat"  Width="85px" ValidationGroup="SearchSubmit" OnClick ="searchresult" /> &nbsp;&nbsp;
                                                                                                <asp:Button Text='<%$ Resources:WebResources, MasterChildReport_btnCClear%>' ID="btnDClear" runat="server" CssClass ="altMedium0BlueButtonFormat"  Width="80px" OnClick ="searchresult" OnClientClick="javascript:return fnDClear();" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr><%--ZD 101918 - End --%>
                                                                        </table>      
                                                                        <%--ZD 101175 - End --%>
                                                                        <dxwgv:ASPxGridView AllowSort="true" OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated"
                                                                            ID="grid" ClientInstanceName="grid" runat="server" KeyFieldName="RoomID" Width="100%"
                                                                            EnableRowsCache="True" OnCustomCallback="Grid_CustomCallback" OnDataBound="Grid_DataBound">
                                                                            <ClientSideEvents Init="function (s,e) { fnUpdateGridWidth(); }" EndCallback="function (s,e) { fnUpdateGridWidth(); }" />
                                                                            <Columns><%--101918 start--%>
                                                                                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="50px" Visible="false"> 
                                                                                    <ClearFilterButton Visible="True" />
                                                                                </dxwgv:GridViewCommandColumn>
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1Name" Caption="<%$ Resources:WebResources, Tier1%>" VisibleIndex="1" Visible ="false"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier2Name" Caption="<%$ Resources:WebResources, Tier2%>" VisibleIndex="2" Visible ="false"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomName" Caption="<%$ Resources:WebResources, ManageRoomProfile_RoomName%>" VisibleIndex="3" Visible ="false"
                                                                                    HeaderStyle-HorizontalAlign="Center" /><%--ZD 104482--%>
                                                                                <dxwgv:GridViewDataColumn FieldName="Capacity" Caption="<%$ Resources:WebResources, ManageRoomProfile_MaximumCapacit%>"
                                                                                    VisibleIndex="4" Visible ="false" HeaderStyle-HorizontalAlign="Center" Width="32%" /> <%--101918 End--%>
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomID" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="IsVMR" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1ID" Visible="False" /> <%--FB 2637--%>
                                                                                <%--FB 2448 --%>
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomCategory" Visible="False" /> 
                                                                                <%--FB 2694--%>
                                                                            </Columns>
                                                                            <Styles>
                                                                                <CommandColumn Paddings-Padding="1" />
                                                                            </Styles>
                                                                            <Settings ShowColumnHeaders="false" ShowFilterRow="false" /> <%--ZD  101918--%>
                                                                            <SettingsBehavior AllowMultiSelection="false" />
                                                                            <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" />
                                                                            <SettingsPager Mode="ShowAllRecords" PageSize="5" AlwaysShowPager="false" Position="Top">  <%--ZD 101175 --%>
                                                                            </SettingsPager>
                                                                            <Templates>
                                                                                <DataRow>
                                                                                    <div style="padding: 5px">
                                                                                        <table class="templateTable" cellpadding="0" cellspacing="1" width="100%">
                                                                                            <tr>
                                                                                                <td rowspan="4" width="70px">
                                                                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%#  DataBinder.Eval(Container, "DataItem.ImageName")%> '
                                                                                                        Height="65px" AlternateText="<%$ Resources:WebResources, RoomImg%>" Width="70px" /><%--ZD 100420--%>
                                                                                                </td>
                                                                                                <td class="templateCaption" colspan="2">
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.Tier1Name")%>
                                                                                                    >
                                                                                                    <%#  DataBinder.Eval(Container,"DataItem.Tier2Name")%>
                                                                                                    >
                                                                                                    <asp:HyperLink NavigateUrl="#" ID="btnViewDetailsDev" Style="cursor: pointer;" runat="server" Text='<%#  DataBinder.Eval(Container, "DataItem.RoomName") %>' />                                                                                                    
                                                                                                    <%--FB 2694 Starts--%>                                                                                                    
                                                                                                   <%--<span id="hideImg"><img src="<%#DataBinder.Eval(Container,"DataItem.ImgHotDeskingRoom")%>" id="imfHotDeskingRoom" height="15px" width="15px" alt="" style="visibility:hidden" /></span> --%><%--FB 2065--%>  
                                                                                                    <%--FB 2694 Ends--%> 
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="templateCaption" width="50%">
                                                                                                    <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_Capacity%>" runat="server"></asp:Literal>:
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.Capacity")%><%--ZD 104482--%>
                                                                                                </td>
                                                                                                <td class="templateCaption">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="templateCaption" width="50%">
                                                                                                    <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_Media%>" runat="server"></asp:Literal> :
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.Video")%>
                                                                                                </td>
                                                                                                <td class="templateCaption">
                                                                                                    <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_Approval%>" runat="server"></asp:Literal> :
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.ApprovalReq")%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="white-space: normal">
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.City")%>
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.StateName")%>
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.ZipCode")%>
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.CountryName")%>
                                                                                                </td>
                                                                                                <td align="center">
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="Hyper1" Text="<%$ Resources:WebResources, RoomSearch_Hyper1%>" Style="cursor: pointer;"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="DelRoom" Text="<%$ Resources:WebResources, RoomSearch_DelRoom%>" Style="cursor: pointer; "
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                        <input type="hidden" id="hdnEdit<%#  DataBinder.Eval(Container, "DataItem.RoomID")%>" value="<%#  DataBinder.Eval(Container, "DataItem.RoomCategory")%>" /> <%--FB 2694--%>
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="Editroom" Text="<%$ Resources:WebResources, RoomSearch_Editroom%>" Style="cursor: pointer;" runat="server" />
                                                                                                    <%--FB 2426 Start--%>
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="importRoom" Text="<%$ Resources:WebResources, RoomSearch_importRoom%>" Style="cursor: pointer; color: Green"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="DelGuestRoom" Text="<%$ Resources:WebResources, RoomSearch_DelGuestRoom%>" Style="cursor: pointer;"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <%--FB 2426 End--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </DataRow>
                                                                            </Templates>
                                                                        </dxwgv:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                                <tr id="ListView" runat="server">
                                                                    <td width="40%" align="left" style="font-weight: bold; font-size: small; color: green;
                                                                        font-family: arial" valign="middle">
                                                                        <%--ZD 101175 101918 Start--%>
                                                                        <table width="100%" border ="0">
                                                                            <tr>
                                                                                <td style="width:60%">
                                                                                    <devex:ASPxPager ID="ASPxPager2" runat="server" ItemsPerPage="1"  Width ="100px" OnPageIndexChanged="ASPxPager_PageIndexChanged2"
                                                                                        style="float: left; clear: both; margin-top: 16px"> 
                                                                                        <LastPageButton Visible="True"></LastPageButton>
                                                                                        <FirstPageButton Visible="True"></FirstPageButton>
                                                                                        <Summary Position="Inside" Text="<%$ Resources:WebResources, GridViewPageText%>" />
                                                                                    </devex:ASPxPager>
                                                                                </td>
                                                                                <td style="padding-top:16px;width:40%" >
                                                                                    <div style="float:right" >
                                                                                        <asp:Label  ID="Label3" Text="<%$ Resources:WebResources, InventoryManagement_TotalRecords%>" runat="server" Font-Bold="true" ></asp:Label>
                                                                                        <asp:Label runat="server" ID="lblTotalRecords1" ></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" >
                                                                                    <table width="100%" border ="0" style="border-collapse:collapse;margin-left:-3px">
                                                                                        <tr>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal  Text="<%$ Resources:WebResources, Tier1%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, Tier2%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, approvalstatus_RoomName%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td  class="tdRoomSearch" valign="top"  align="center">
                                                                                                <asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_MaximumCapacit%>" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td>
                                                                                                
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtLsearchtier1" runat="server" style="width:100%"/>
                                                                                            </td>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtLsearchtier2" runat="server"  style="width:100%"/>
                                                                                            </td>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtLsearchRoomname" runat="server" style="width:100%" />
                                                                                            </td>
                                                                                            <td valign="top" align="left" class="txtRoomSearch">
                                                                                                <input type="text" class="altText" id="txtLsearchMaxCapacity" runat="server" style="width:100%" /><br />
                                                                                                <asp:RegularExpressionValidator ID="RegSearchMaxCapacity" ControlToValidate="txtLsearchMaxCapacity" ValidationGroup="SearchSubmit"
                                                                                                Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                                            </td>
                                                                                            <td nowrap="nowrap" valign="middle" align="center">
                                                                                                <asp:Button Text='<%$ Resources:WebResources, Search%>' ID="btnLSearch" runat="server" CssClass ="altMedium0BlueButtonFormat"  Width="85px" ValidationGroup="SearchSubmit" OnClick ="searchresult" /> &nbsp;&nbsp;
                                                                                                <asp:Button Text='<%$ Resources:WebResources, MasterChildReport_btnCClear%>' ID="btnLClear" runat="server" CssClass ="altMedium0BlueButtonFormat"  Width="80px" OnClick ="searchresult" OnClientClick="javascript:return fnClear();" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr> <%--ZD 101918 End--%>
                                                                        </table>
                                                                        <%--ZD 101175 End--%>
                                                                        <%--Edited for FF--%>
                                                                        <dxwgv:ASPxGridView AllowSort="true" OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated"
                                                                            ID="grid2" ClientInstanceName="grid2" runat="server" KeyFieldName="RoomID" Width="100%"
                                                                            EnableRowsCache="True" OnCustomCallback="Grid2_CustomCallback" OnDataBound="Grid2_DataBound">
                                                                            <ClientSideEvents Init="function (s,e) { fnUpdateGridWidth(); }" EndCallback="function (s,e) { fnUpdateGridWidth(); }" />
                                                                            <Columns> <%--101918 start--%>
                                                                                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="50px" Visible="false"> <%--ZD 101918--%>
                                                                                    <ClearFilterButton Visible="True" />
                                                                                </dxwgv:GridViewCommandColumn>
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1Name" Caption="<%$ Resources:WebResources, Tier1%>" VisibleIndex="1" Visible ="false"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier2Name" Caption="<%$ Resources:WebResources, Tier2%>" VisibleIndex="2" Visible ="false"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomName" Caption="<%$ Resources:WebResources, ManageRoomProfile_RoomName%>" VisibleIndex="3" Visible ="false"
                                                                                    HeaderStyle-HorizontalAlign="Center" /><%--ZD 104482--%>
                                                                                <dxwgv:GridViewDataColumn FieldName="Capacity" Caption="<%$ Resources:WebResources, ManageRoomProfile_MaximumCapacit%>"
                                                                                    VisibleIndex="4" Visible ="false" HeaderStyle-HorizontalAlign="Center" Width="32%" /> <%--101918 End--%>
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomID" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="IsVMR" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1ID" Visible="False" /> <%--FB 2637--%>
                                                                                <%--FB 2448 --%>
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomCategory" Visible="False" />
                                                                                <%--FB 2694--%>
                                                                            </Columns>
                                                                            <Styles>
                                                                                <CommandColumn Paddings-Padding="1" />
                                                                            </Styles>
                                                                            <Settings />
                                                                            <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" />
                                                                            <Settings ShowColumnHeaders ="false" ShowFilterRow="false"/> <%--ZD 101918--%>
                                                                            <SettingsBehavior AllowMultiSelection="false" />
                                                                            <SettingsPager Mode="ShowPager" PageSize="100" AlwaysShowPager="false" Position="Top"><%--ZD 101175--%>
                                                                            </SettingsPager>
                                                                            <Templates>
                                                                                <DataRow>
                                                                                    <div style="padding: 5px">
                                                                                        <table class="templateTable" cellpadding="0" cellspacing="1" width="100%">
                                                                                            <tr><%--FB 2037 Start--%>
                                                                                                <td class="templateCaption" style="width: 55%;">
                                                                                                    <%#DataBinder.Eval(Container,"DataItem.Tier1Name")%>
                                                                                                    >
                                                                                                    <%#DataBinder.Eval(Container,"DataItem.Tier2Name")%>
                                                                                                    >
                                                                                                    <asp:HyperLink NavigateUrl="#" ID="btnViewDetailsDev" Style="cursor: pointer;" runat="server" Text='<%#  DataBinder.Eval(Container, "DataItem.RoomName") %>' />
                                                                                                    <%--FB 2065 Starts--%>                                                                                                        
                                                                                                   <img src='<% #DataBinder.Eval(Container,"DataItem.ImgRoomIcon")%>' visible='<% #(DataBinder.Eval(Container,"DataItem.ImgRoomIcon") != "")%>' id="ImgRoomIcon" runat="server" border="0"  height="15" width="15" alt='<%#DataBinder.Eval(Container,"DataItem.ImageName1")%>' title='<%#DataBinder.Eval(Container,"DataItem.ImageName1")%>' style='visibility:<%#DataBinder.Eval(Container,"DataItem.RoomIconTypeId").ToString().Equals("0") ? "hidden" :"visible" %>'  /><%--ZD 100419--%><%--ZD 100152--%>
                                                                                                    <%--FB 2065 Ends--%> 
                                                                                                </td>
                                                                                                <td class="templateCaption" width="18%">
                                                                                                    <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_Approval%>" runat="server"></asp:Literal> :
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.ApprovalReq")%>
                                                                                                </td>
                                                                                                <td align="center">                                                                                                
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="Hyper1" Text="<%$ Resources:WebResources, RoomSearch_Hyper1%>" Style="cursor: pointer;"
                                                                                                        runat="server" />&nbsp;&nbsp;
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="DelRoom" Text="<%$ Resources:WebResources, RoomSearch_DelRoom%>" Style="cursor: pointer;"
                                                                                                        runat="server" />&nbsp;&nbsp;
                                                                                                        <input type="hidden" id="hdnEdit<%#  DataBinder.Eval(Container, "DataItem.RoomID")%>" value="<%#  DataBinder.Eval(Container, "DataItem.RoomCategory")%>" /> <%--FB 2694--%>
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="Editroom" Text="<%$ Resources:WebResources, RoomSearch_Editroom%>" Style="cursor: pointer;" runat="server" />
                                                                                                    <%--FB 2426 Start--%>
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="importRoom" Text="<%$ Resources:WebResources, RoomSearch_importRoom%>" Style="cursor: pointer; color: Green"
                                                                                                        runat="server" />&nbsp;&nbsp;
                                                                                                    <asp:HyperLink NavigateUrl="javascript:return false;" ID="DelGuestRoom" Text="<%$ Resources:WebResources, RoomSearch_DelGuestRoom%>" Style="cursor: pointer;"
                                                                                                        runat="server" />&nbsp;
                                                                                                    <%--FB 2426 End--%><%--FB 2037 End--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </DataRow>
                                                                            </Templates>
                                                                        </dxwgv:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr id="TrLicense" runat="server" style="display: none;margin-top: 15px;"> <%--ZD 101175--%>
                                                <td align="left" nowrap="nowrap"><%--FB 2694 Starts--%> 
                                                    <div id="divLicense" style="margin-top:33px"> <%--ZD 101918--%>
                                                    <asp:Label ID="lblTtlRooms" CssClass="blackblodtext" runat="server" ><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, RoomSearch_lblTtlRooms%>" runat="server"></asp:Literal> :</asp:Label>
                                                    <span class="blackblodtext">
                                                        <asp:Label ID="totalNumber" runat="server" />
														 <%--FB 2694 Starts--%>  
                                                        <asp:Label ID="Label1" CssClass="blackblodtext" runat="server" >
                                                        ; <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, RoomSearch_Label1%>" runat="server"></asp:Literal> :</asp:Label>
                                                        <asp:Label ID="ttlnvidLbl" runat="server" />

                                                        <%if (Session["language"].ToString() != "en")
                                                                             {%> ;<br /> <%} %>

                                                        <asp:Label ID="lblVMRRooms" CssClass="blackblodtext" runat="server">
                                                        <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, RoomSearch_lblVMRRooms%>" runat="server"></asp:Literal>:</asp:Label><%--FB 2586--%>
                                                        <asp:Label ID="tntvmrrooms" runat="server" />&nbsp
                                                        
                                                        <%if (Session["language"].ToString().Equals("en"))
                                                                             {%><br /> <%} %>
                                                        
                                                        <%--ZD 101098 START--%>
                                                        <asp:Label ID="lbliControlRooms" CssClass="blackblodtext" runat="server">
                                                        <asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, RoomSearch_lbliControlRooms%>" runat="server"></asp:Literal>:</asp:Label>
                                                        <asp:Label ID="tntiControlRooms" runat="server" />
                                                        <%--ZD 101098 END--%>
                                                        
                                                        <asp:Label ID="lblPublicRoom" CssClass="blackblodtext" runat="server" >
                                                        ; <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, RoomSearch_lblPublicRoom%>" runat="server"></asp:Literal>:</asp:Label> 
                                                        <asp:Label ID="ttlPublicLbl" runat="server" /> 
                                                    </span><b>;</b> &nbsp; 
                                                    
                                                    <%if (Session["language"].ToString() !="en")
                                                                             {%> <br /> <%} %>

                                                    <span class="blackblodtext"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, RoomSearch_TotalROHotdes%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="ttlROHotdeskingRooms" runat="server" CssClass="blackblodtext" />
                                                    </span><b>;</b>&nbsp;
                                                    <span class="blackblodtext"><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, RoomSearch_TotalVCHotdes%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="ttlVCHotdeskingRooms" runat="server" CssClass="blackblodtext" />
                                                    </span>&nbsp;<br />  

                                                    <span class="blackblodtext"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, RoomSearch_VideoRoomsRem%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="vidLbl" runat="server" CssClass="blackblodtext" />
                                                    </span><b>;</b> 

                                                    <span class="blackblodtext"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ManageRoom_NonVideoRooms%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="nvidLbl" runat="server" CssClass="blackblodtext" />
                                                    </span><br />
                                                    <span class="blackblodtext"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, ManageRoom_VMRRoomsRemai%>" runat="server"></asp:Literal></span> <%--ZD 100806--%>
                                                    <span class="summaryText"><%--FB 2586--%>
                                                    <asp:Label ID="vmrvidLbl" runat="server" CssClass="blackblodtext" />
                                                    </span><b>;</b><%-- ZD 100806--%> 
                                                    
                                                    <%if (Session["language"].ToString().Equals("en"))
                                                                             {%>  <%} %>

                                                    <%--ZD 101098 START--%>
                                                    <span class="blackblodtext"><asp:Literal  Text="<%$ Resources:WebResources, ManageRoom_iControlRoomsRemai%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="icontrolvidlbl" runat="server" CssClass="blackblodtext" />
                                                    </span><br />
                                                    <%--ZD 101098 END--%>
                                                    
                                                    <span class="blackblodtext"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, RoomSearch_ROHotdeskingR%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="ROHotdeskingRooms" runat="server" CssClass="blackblodtext" />
                                                    </span><b>;</b><%--ZD 100806--%>
                                                    <span class="blackblodtext"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, RoomSearch_VCHotdeskingR%>" runat="server"></asp:Literal></span>
                                                    <span class="summaryText">
                                                    <asp:Label ID="VCHotdeskingRooms" runat="server" CssClass="blackblodtext" />
                                                    </span>
                                                    </br>
													<%--FB 2694 Ends--%> 
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Button ID="btnRefreshRooms" Style="display: none;" runat="server" OnClick="GetAllData" /><%-- 101175 --%>
                                    </td>
                                    <td style="vertical-align: top" width="30%" id="TDSelectedRoom" runat="server">
                                        <br />
                                        <asp:Panel ID="SelectedRooms" runat="server" Height="540px" CssClass="treeSelectedNode"
                                            ScrollBars="Auto">
                                            <div width="100%" style="border-style: solid; border-width: 1px; border-color: Blue;">
                                                <table width="100%">
                                                    <tr class="tableHeader">
                                                        <td class="tableHeader">
                                                            <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_SelectedRooms%>" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' src='image/deleteall.gif' title="<%$ Resources:WebResources, LDAPImport_RemoveAll%>" runat="server" id="ImageDel" alt="Remove All"
                                                                width='18' height='18' onclick="JavaScript:ClearAllSelection()"  style="cursor:pointer;" /></a> <%--FB 2798--%>
                                                            <span class="treeRootNode" onclick="JavaScript:ClearAllSelection()"><asp:Literal Text="<%$ Resources:WebResources, RoomSearch_RemoveAll%>" runat="server"></asp:Literal></span>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td>
                                                            <asp:DataGrid ShowHeader="false" Width="100%" ID="SelectedGrid" runat="server" AutoGenerateColumns="False"
                                                                OnItemDataBound="SetRoomAttributes" Font-Names="Verdana" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="RoomName" Visible="false"></asp:BoundColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <table width="100%" cellspacing="3">
                                                                                <tr>
                                                                                    <td align="left" width="80%"> <%--ZD 102916--%>
                                                                                        <asp:HyperLink NavigateUrl="#" ID="btnViewDetails" Style="cursor: pointer;" runat="server" Text='<%#DataBinder.Eval(Container,"DataItem.RoomName") %>' />
                                                                                    </td>
                                                                                    <td align="left" width="10%"> <%--ZD 102916 start--%>
                                                                                           <%--<a href="" onclick="this.childNodes[0].click();return false;">--%>

                                                                                           <a href="" onclick="javascript:this.childNodes[0].click();return false;">
                                                                                           <img id="ImgRoomAttendee" border='0'  width='18' height='18' alt="Edit"  runat='server' style="cursor:pointer;" src="image/RoomAttendee.jpg"  />
                                                                                           </a>
                                                                                    </td><%--ZD 102916 End--%>
                                                                                    <td align="left" width="10%">
                                                                                        <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' src='image/btn_delete.gif' runat="server" id="ImageDel" width='18'
                                                                                            height='18' style="cursor:pointer;" title="<%$ Resources:WebResources, Delete%>"  alt="Delete"/></a> <%--FB 2798--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>

<script type="text/javascript">
    var roomNamesStr, roomIdsStr
    function chkresources(id) {
        if (id != "") {
            if (id.indexOf(",") < 0)
                id += ",";
            url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + id;
            rmresPopup = window.open(url, 'roomresource', 'status=no,width=450,height=480,resizable=yes,scrollbars=yes');
            rmresPopup.focus();
            if (!rmresPopup.opener) {
                rmresPopup.opener = self;
            }
        }
    }


    function EndRequestHandler() // ZD 102404
    {
          document.getElementById("topRoomDiv").style.display = "none";
    }
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

    function Addroms() {
        document.getElementById("topRoomDiv").style.display = "block"; // ZD 102404
        CorrectHdnString();
        var VMRDialoutLocs="", isSearch="0"; //ZD 100522
        var args = Addroms.arguments;
        args = args[0].split(';');
        
        VMRDialoutLocs = args[2]; //ZD 100522
        var locs = document.getElementById("selectedlocframe");
        var locsname=document.getElementById("SelectedRoomNameValue");//ZD 101175
        var locsnew = document.getElementById("SelectedRoomNew"); // ZD 102123
        var adlocs = document.getElementById("addroom");
        var vmrRoomadded = document.getElementById("hdnVMRRoomadded"); //FB 2448
        var chkboxVMR = document.getElementById("chkIsVMR"); //FB 2448
        var prnt;
        var prntroomname; //ZD 101175
        //locs.value = 28;
        roomIdsStr = locs.value.split(',');
        //ZD 100522 Starts
        isSearch= "0";
        if (getQueryVariable('frm') != null) 
            if(getQueryVariable('frm') == "frmSearchConference") 
                isSearch ="1";
        //ZD 100522 Ends
       

        if ('<%=Parentframe%>' == "frmCalendarRoom" || '<%=Parentframe%>' == "frmUserProfile") {

            if (roomIdsStr.length > 20) {
                alert(RoomsearchMaxi);
                return false;
            }
        }
        if (!Loccontains(roomIdsStr, args[0])) {

            //FB 2637 Starts
            var tier1Alert = "<%=Session["AlertforTier1"]%>";
            //ZD 103059
            if(tier1Alert != "" && args[5] != 8)
            {
                var firstTier = "";
                if(args[1] != "" && args[1] != "0")
                {
                    firstTier = args[1].split(':')[0];
                    if (Loccontains(tier1Alert, firstTier)) {
                        alert(RoomsearchDialin);
                    }
                }
            }
            tier1Alert = tier1Alert.split('|');
            //FB 2637 Ends
            if(args[4] == 1)
            {
                if(!confirm(RoomUnavailable))
                {
                    if(document.getElementById("topRoomDiv") != null)
                        document.getElementById("topRoomDiv").style.display = "none";
                    return false;
                }
            }
            //FB 2448 Starts
            if (getQueryVariable('isVMR') != null) {

                if (getQueryVariable('isVMR') == "1") {
                    var locdummy = "";
                    locdummy = vmrRoomadded.value;
                    if (chkboxVMR.checked) {
                        if (parent && locdummy == "")
                            if (parent.document.getElementById("hdnSelectVMRRoom"))
                            locdummy = parent.document.getElementById("hdnSelectVMRRoom").value;

                        if (locdummy != "") {
                            if(isSearch =="0") //ZD 100522
                            {
                                alert(OnlyoneVMR); //ZD 100806
                                if(document.getElementById("topRoomDiv") != null)
                                    document.getElementById("topRoomDiv").style.display = "none";
                                return false;
                            }
                        }
                        else {
                            vmrRoomadded.value = args[0];
                        }
                    }
                }
            }

            //FB 2448 Ends
            
            if (locs.value == "")
            {
                locs.value = args[0];
                locsname.value= args[0]+"|"+args[3];
                locsnew.value = args[0]+"||"+args[3]+"||"+args[1].split(':')[0]+","+args[1].split(':')[1]; // ZD 102123
                }
                
            else
            {
                if(window.location.href.indexOf("RoomFloor") > -1) // ZD 102123
                {
                    alert(OnlyoneRoomSel);
                    if(document.getElementById("topRoomDiv") != null)
                        document.getElementById("topRoomDiv").style.display = "none";
                    return false;
                }
                locs.value += "," + args[0];
                locsname.value += "ô" + args[0]+"|"+args[3];
                locsnew.value += "~~" + args[0]+"||"+args[3]+"||"+args[1].split(':')[0]+","+args[1].split(':')[1]; // ZD 102123
                }
            
            if(VMRDialoutLocs != "" && VMRDialoutLocs != "0")//ZD 100522 //ZD 102650
                locs.value += "," + args[2];

            if (adlocs)
                adlocs.value = "1";

            if (opener) {
                prnt = opener.document.getElementById("selectedList");
                prntTier = opener.document.getElementById("selectedTierList"); //ZD 102358          
                prntroomname=opener.document.getElementById("selectedListRoomName");
                
                if(prntTier)//ZD 102358
                    prntTier.value = args[1] + ":" + args[0] + ":A";
                if (prnt)
                    prnt.value = locs.value;
                if (prntroomname)
                    prntroomname.value=locsname.value;

                if(prntroomname != null)
                {
                    if(prntroomname.value !="")
                        opener.document.getElementById("tdSelectRooms").style.display='none';
                    else
                        opener.document.getElementById("tdSelectRooms").style.display='block';
                }

                if (opener.document.getElementById("btnfrmSearch"))
                    opener.document.getElementById("btnfrmSearch").click();

                var selprnt = opener.document.getElementById("selectedloc");

                if (selprnt)
                    selprnt.value = locs.value;

                    
            }
            else if (parent) {
                prnt = parent.document.getElementById("selectedloc");
                if (prnt)
                    prnt.value = locs.value;

                prntTier = parent.document.getElementById("selectedTierList"); //ZD 102358          
                if(prntTier)//ZD 102358
                    prntTier.value = args[1] + ":" + args[0] + ":A";
            }

            //ZD 102008 - Start
            if(opener != null && opener.document.getElementById("selStatus") != null)
            {
             var SelectedWeekdays, Selectedday, str="", selectedRooms1;
             var isClosed = new Boolean();
             var selDays = new Array();             
             var confdate = new Date();

             isClosed = false;
             confdate = (confdate.getMonth() + 1) + "/" + confdate.getDate() + "/" + confdate.getFullYear();
             SelectedWeekdays = new Date(confdate); 
             Selectedday = SelectedWeekdays.getDay()+1

             if( '<%=Session["DaysClosed"]%>' != null)
             {
                str = '<%=Session["DaysClosed"].ToString()%>';       
        
                selDays = str.split(',');
        
                for(var i =0; i<selDays.length; i++)
                {
                    if( selDays[i] == Selectedday)        
                        isClosed = true;
                }                
                if(isClosed)
                {  
                    if(opener.document.getElementById("selStatus") != null)
                        opener.document.getElementById("selStatus").value = "0";
                }
                else
                {   
                    if(opener.document.getElementById("selStatus") != null)                      
                        opener.document.getElementById("selStatus").value = "1";
                }
             }
             //ZD 102008 - End
            }
        }
        else
            alert(RoomalAdded);
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();
    }

    function CorrectHdnString() {
   
        var locs = document.getElementById("selectedlocframe");

        
        var vlue = "";
        //var valu1="";

        roomIdsStr = locs.value.split(',');

        var i = roomIdsStr.length;

        while (i--) {
            if (roomIdsStr[i] != "") {
                if (vlue == "")
                    vlue = roomIdsStr[i].trim();
                else
                    vlue += "," + roomIdsStr[i].trim();

            }

        }

        locs.value = vlue;
        
        

    }


    function delRoom() {
    
        var args = delRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");
        var prnt;
        if (locs.value == "")
            locs.value = args[0];

        var lang = "<%=Session["language"] %>";

        if(adlocs)
        {
            if (lang == "en")
                adlocs.value = "Deactivate";
            else if (lang == "sp")
                adlocs.value = "Desactivar";
            else if (lang == "fr")
                adlocs.value = "Désactiver";
        }
        if (opener) {
            prnt = opener.document.getElementById("selectedList");
            if (prnt)
                prnt.value = locs.value;

            if (opener.document.getElementById("btnfrmSearch"))
                opener.document.getElementById("btnfrmSearch").click();
        }
        else if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();


    }

    function ActivateRoom() {
    
        var args = ActivateRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");

        if (locs.value == "")
            locs.value = args[0];

        var lang = "<%=Session["language"] %>";

        if(adlocs)
        {
            if (lang == "en")
                adlocs.value = "Activate";
            else if (lang == "sp")
                adlocs.value = "Activar";
            else if (lang == "fr")
                adlocs.value = "Activer";
        }

        if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();


    }

    //ZD 102916 start
     function getPartyList(id) 
     {
        var ConfOrg = '<%=Session["OrganizationID"] %>';
        if ('<%=Session["multisiloOrganizationID"] %>' != '')
            ConfOrg = '<%=Session["multisiloOrganizationID"] %>';

        if (id != "" && id.indexOf(",") < 0)
        {
            //id += ",";
            url = "ParticipantAssignment.aspx?Rmid=" + id;
            if (!window.winrtc) 
            {
                winrtc = window.open(url, "", "width=500,height=400,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
                if(!winrtc.opener)
                  winrtc.opener = self;
            }
            else 
            { 
	            if (!winrtc.closed) // still open
                {     
	    	        winrtc.close();
	                //winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                    winrtc = window.open(url, "", "width=500,height=400,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
		        }
                else 
                {
	                winrtc = window.open(url, "", "width=500,height=400,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	                winrtc.focus();
    	        }
            }
        }
        return false;
    }
    //ZD 102916  End

    function Delroms() {
        document.getElementById("topRoomDiv").style.display = "block"; // ZD 102404
        CorrectHdnString();

        var args = Delroms.arguments;
        var locs = document.getElementById("selectedlocframe");
        var data=document.getElementById("SelectedRoomNameValue"); //ZD 101175
        var adlocs = document.getElementById("addroom");
        var hdNm = document.getElementById("locstr");
        var vmrRoomadded = document.getElementById("hdnVMRRoomadded"); //FB 2448
        var vmrRoomdeleted = "";
        if (parent.document.getElementById("hdnSelectVMRRoom") != null) {
            vmrRoomdeleted = parent.document.getElementById("hdnSelectVMRRoom").value; //FB 2448
        }
        var prnt;

        roomIdsStr = locs.value.split(',');

        if(document.getElementById('hdnSelRoomID') != null)//ZD 102916
        {
            document.getElementById('hdnSelRoomID').value = args[0];
            document.getElementById('hdnSelectionVal').value = "1";            
        }
        // FB2448
        if (vmrRoomadded.value.trim() == args[0]) {
            vmrRoomadded.value = "";
        }
        if (vmrRoomdeleted.trim() == args[0]) {
            parent.document.getElementById("hdnSelectVMRRoom").value = "";
        }
        //FB 2448

        var splitroom1=null; var locsname="";
        if(data.value!=""&& data.value.indexOf("ô") > -1)
        {
            splitroom1=data.value.split('ô'); //ALT 147
            
        }
        
            


        if (Loccontains(roomIdsStr, args[0])) {

            var i = roomIdsStr.length;

            while (i--) {

            if(splitroom1!=null)
            {
                if(splitroom1[i].split('|')[0]!=args[0])
                {
                if(locsname=="")
                   locsname=splitroom1[i];
                   else
                   locsname+="ô"+splitroom1[i];
                    }
            }

                if (roomIdsStr[i] == args[0]) {
                    roomIdsStr[i] = "";
                }

            }

            i = roomIdsStr.length;
            locs.value = "";
            while (i--) {
                if (roomIdsStr[i] != "") {
                    if (locs.value == "")
                        locs.value = roomIdsStr[i];
                    else
                        locs.value += "," + roomIdsStr[i];
                }
            }
            if (adlocs)
                adlocs.value = "1";

            if (locs.value == "")
                hdNm.value = "";

            if (opener) {
                prnt = opener.document.getElementById("selectedList");
                prntroomname=opener.document.getElementById("selectedListRoomName");

                prntTier = opener.document.getElementById("selectedTierList"); //ZD 102358          
                if(prntTier)
                    prntTier.value = args[1] + ":D" ;

                if (prnt)
                    prnt.value = locs.value;
                if (prntroomname)
                    prntroomname.value = locsname;

                if(prntroomname != null)
                {
                    if(prntroomname.value !="")
                        opener.document.getElementById("tdSelectRooms").style.display='none';
                    else
                        opener.document.getElementById("tdSelectRooms").style.display='block';
                 }

                if (opener.document.getElementById("btnfrmSearch"))
                    opener.document.getElementById("btnfrmSearch").click();

                prntsellocs = opener.document.getElementById("selectedloc");
                if (prntsellocs)
                    prntsellocs.value = locs.value;
            }
            else if (parent) {
                prnt = parent.document.getElementById("selectedloc");
                if (prnt)
                    prnt.value = locs.value;
            }

            DataLoading("1") //ZD 100429
            
            var refrsh = document.getElementById("btnRefreshRooms");
            if (refrsh)
                refrsh.click();

            //ZD 101175 Starts

//            if(roomIdsStr.length == 1)
//                setTimeout("fnremove()",1000);

        }
    }

    function fnremove()
    {    
        if(document.getElementById('SelectedGrid') != null)
               document.getElementById('SelectedGrid').innerHTML = "";
        
    }
    //ZD 101175 Ends

    function Loccontains(a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }


</script>

<script type="text/javascript">
    // FB 1797
    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }

        return "";

    }
    // FB 1797

    function ChangeLbl() {
    
        var args = ChangeLbl.arguments;

        if (args) {


            var txt = document.getElementById(args[0]);
            var lbl = document.getElementById("LBLCapacity");
            var hdnH = document.getElementById("hdnCapacityH");
            var hdnL = document.getElementById("hdnCapacityL");
            var txtL = document.getElementById("TxtSearchL");
            var txtH = document.getElementById("TxtSearchH");
            if (txt) {
                if (lbl)
                    lbl.innerHTML = args[1];
            }

            if (args[1] != "") {
                txtL.value=""; //ZD 104482
                txtH.value="";
                if (args[1] == "Any") {
                    if (hdnL)
                        hdnL.value = "";
                    if (hdnH)
                        hdnH.value = "";
                }
                else {
                    var vlues = args[1].split(' ');
                    if (vlues) {
                        if (vlues.length > 1) {
                            if (hdnH)
                                hdnL.value = vlues[0];
                            if (hdnL)
                                hdnH.value = vlues[2];
                        }
                        else {
                            if (hdnH)
                                hdnL.value = "20";
                            if (hdnL)
                                hdnH.value = "";
                        }
                    }
                }
                if (txt)
                    RefreshRooms(txt);
            }
            else {
                if (txtL && txtH) {  //ZD 104482
                    var Hval = txtH.value;
                    if (hdnH)
                        hdnH.value = txtH.value;
                    if (hdnL)
                        hdnL.value = txtL.value;


                    if (Hval == "")
                        Hval = "Any";


                    try {
                        if (Hval != "Any")
                            eval(hdnH.value);

                        eval(hdnL.value);
                    }
                    catch (exception) {
                        alert(RoomCheckvalue);
                        return false;
                    }

                    if (Hval != "Any") {
                        if (eval(hdnH.value) < eval(hdnL.value)) {
                            alert(RoomCheckvalue);
                            return false;
                        }
                    }


                    //lbl.innerHTML = txtL.value + " - " + Hval;  //ZD 104482

                }

                RefreshRooms(txtL);
            }

        }
    }

    function RefreshRooms() {
        var args = RefreshRooms.arguments;

        if (args) {
            if (args[0]) {
                if (args[0].value == "") {
                    alert(RSValidValue);
                    return false;
                }

                var isfilter = document.getElementById("hdnIsFilterChanged");
                if (isfilter)
                    isfilter.value = "Y";
            }
        }

        var mnone = document.getElementById("MediaNone");
        var maud = document.getElementById("MediaAudio");
        var mvid = document.getElementById("MediaVideo");

        if (!mnone.checked && !maud.checked && !mvid.checked) {
            alert(RoomSelectmedia);
            return false;
        }

        var hdNm = document.getElementById("hdnName");
        if (hdNm)
            hdNm.value = "0";

        var hdNm = document.getElementById("hdnView");
        if (hdNm)
            hdNm.value = "0";

        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();

    }


    //ZD 101175 Start
    
    function TierSearch() {
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();

    }
    //ZD 101175 End


    function AVItemChanged() {
        var avchg = document.getElementById("hdnAV");
        if (avchg)
            avchg.value = "1";
        RefreshRooms();

    }



    function ChkFavorites() {
        var avchg = '<%=favRooms%>';

        var chkfav = document.getElementById("chkFavourites");

        if (avchg == "0") {
            alert(RoomFavour);
            if (chkfav)
                chkfav.checked = false;

        }
        else
            RefreshRooms();

    }
    //FB 2426 Start
    function ChkGuestRooms() {
        var avchg = '<%=GuestRooms%>';

        var chkfav = document.getElementById("chkGuestRooms");

        if (avchg == "0") {
            alert(GuestRoom);
            if (chkfav)
                chkfav.checked = false;

        }
        else
            RefreshRooms();

    }
    //FB 2426 End

    //FB 2694 Starts
    function HotdeskingRooms() {        
        var Hotdesking = '<%=HotdeskingRooms%>';        
        var chkHotdesking = document.getElementById("chkHotdesking");
        if (Hotdesking == "0") {
            alert(HotDeskRoom);
            if (chkHotdesking)
                chkHotdesking.checked = false;
        }
        else
            RefreshRooms();
    }
    //FB 2694 Ends

    //FB 2448 Start
    function ChkVirtualMeetingRooms() {
        var avchg = '<%=VMR%>';

        var chkfav = document.getElementById("chkIsVMR");

        if (avchg == "0") {
            alert(VMRRoom);
            if (chkfav)
                chkfav.checked = false;

        }
        else
            RefreshRooms();

    }
    //FB 2448 End
    function ZipCodeCheck() {
        var zphg = document.getElementById("hdnZipCode");
        if (zphg)
            zphg.value = "1";
        RefreshRooms(zphg);
    }

    function chkZip() {
        var zpTxt = document.getElementById("txtZipCode");
        var cntry = document.getElementById("lstCountry");
        var stt1 = document.getElementById("lstStates");
        //var stt2 = document.getElementById("lstStates2"); //ZD 104482 - In exiting it was visible false
        //var stt3 = document.getElementById("lstStates3");
        var zphg = document.getElementById("hdnZipCode");
        if (zpTxt) {
            if (zpTxt.value == "") {
                cntry.disabled = false;
                stt1.disabled = false;
                //stt2.disabled = false;
                //stt3.disabled = false;
                if (zphg)
                    zphg.value = "0";

                RefreshRooms(zphg);

            }
            else {
                cntry.disabled = true;
                stt1.disabled = true;
                //stt2.disabled = true;
                //stt3.disabled = true;
                chkLimit(zpTxt, 'u');

            }
        }

    }

    function ChangeCountryorState() {
        var zphg = document.getElementById("hdnZipCode");
        if (zphg)
            zphg.value = "0";
        var loc = document.getElementById("hdnLoc");
        if (loc)
            loc.value = "0";

        var arg = ChangeCountryorState.arguments;

        if (arg) {
            if (arg[0] == "1") {
                if (loc)
                    loc.value = "1";
            }

        }

        var btnst = document.getElementById("BtnUpdateStates");

        if (btnst)
            btnst.click();
    }

    function NameSearch() {
        var hdNm = document.getElementById("hdnName");
        if (hdNm)
            hdNm.value = "1";

        var txtNm = document.getElementById("TxtNameSearch");

        if (txtNm.value == "") {
            alert(RSValidValue);
            return false;
        }

        var refrshNm = document.getElementById("btnRefreshRooms");
        if (refrshNm)
            refrshNm.click();


    }

    function ViewChng() {
        var hdNm = document.getElementById("hdnView");
        if (hdNm)
            hdNm.value = "1";

    }



    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_initializeRequest(initializeRequest);

    prm.add_endRequest(endRequest);

    var postbackElement;

    function initializeRequest(sender, args) {
        document.body.style.cursor = "wait";
        DataLoading(1);
        //document.getElementById("btnCompare").disabled = true;



    }



    function endRequest(sender, args) {
        document.body.style.cursor = "default"; DataLoading(0);
        //document.getElementById("btnCompare").disabled =  false;



    }

    function EndDateValidation() {
        ChangeTimeFormat("D"); //FB 2588
        var args = EndDateValidation.arguments;
        var endb = document.getElementById("Available");


        var sDate = Date.parse(document.getElementById("txtRoomDateFrom").value + " " + document.getElementById("hdnStartTime").value);
        var eDate = Date.parse(document.getElementById("txtRoomDateTo").value + " " + document.getElementById("hdnEndTime").value);


        if (args) {
            if (args[0] == "1")
                endb.checked = true;
        }

        if (endb.checked) {

            if ((sDate >= eDate)) {

                if (document.getElementById("txtRoomDateFrom").value == document.getElementById("txtRoomDateTo").value) {
                    if (sDate > eDate)
                        alert(RoomsearchTime);
                    else if (eDate == sDate)
                        alert(RoomsearchTime);
                }
                else
                    alert(RoomsearchCheckTime);

                endb.checked = false;

                return;

            }
        }

        if (args)// FB 1797
        {
            if (args[0] == "1") {

                if (getQueryVariable('confID') != "" && document.getElementById("hdnIsChanged").value == "1") // ZD 102315
                {
                    alert(RoomChangeDate); //FB 2367
                    document.getElementById("hdnIsChanged").value = "0";
                }
            }
        }
        //FB 2588
        ChangeTimeFormat("O");
        document.getElementById("confRoomEndTime_Text").value = document.getElementById("hdnEndTime").value;
        RefreshRooms();
    }

    function ClosePopup() {
        try {
            var url = window.location.href;
			//ZD 100602 Starts
            var roomCnt = 0;
			
			// ZD 102123 Starts
            if(url.indexOf("RoomFloor") > -1)
            {
                var prnt = document.getElementById("selectedlocframe").value;
                if(prnt == "")
                {
                    document.getElementById("SelectedRoomNew").value = "";
                }
                if(prnt != "")
                    roomCnt = prnt.split(',');
                if (roomCnt.length > 1) {
                    alert(OnlyoneRoomSel);
                    return false;
                }

                //window.opener.document.getElementById("hdnSelectedRoom").value = selec;
                window.opener.fnResumeSelection(document.getElementById("SelectedRoomNew").value);
                window.close();
                return false;
            }
			// ZD 102123 Ends

            if(url.indexOf("AddRoomEndpoint") >= 0 )
            {
                var prnt = document.getElementById("selectedlocframe").value;
                if(prnt != "")
                    roomCnt = prnt.split(',');
                if (roomCnt.length > 1) {
                    alert(OnlyoneRoomSel);
                    return false;
                }
                
                if(url.indexOf("ManageConf") >= 0 )
                {
                    window.parent.document.getElementById("hdnRoomID").value = prnt;
                    var add = window.parent.document.getElementById("SelectRoom");
                    
                    if (add && roomCnt != 0 && roomCnt.length == 1)
                    {
                     if(window.parent.document.getElementById("chkUnlistedEndpoint"))
                        window.parent.document.getElementById("chkUnlistedEndpoint").checked = false;
                     add.click();
                    }
                    else
                    {
                     if(window.parent.document.getElementById("chkListedEndpoint"))
                        window.parent.document.getElementById("chkListedEndpoint").checked = false;
                     
                      var obj = window.parent.document.getElementById("chkUnlistedEndpoint");
                      obj.checked = true;
                      window.parent.OpenEndpointlist(obj);
                    }
                    window.parent.fnTriggerFromPopup();
                }
                else
                {
                    parent.opener.document.getElementById("hdnRoomID").value = prnt;
                    var add = parent.opener.document.getElementById("SelectRoom");
                    
                    if (add && roomCnt != 0 && roomCnt.length == 1)
                    {
                     if(parent.opener.document.getElementById("chkUnlistedEndpoint"))
                        parent.opener.document.getElementById("chkUnlistedEndpoint").checked = false;
                     //add.click();
                     window.opener.fnInvokeSelectRoom(); // ZD 101174
                    }
                    else
                    {
                     if(parent.opener.document.getElementById("chkListedEndpoint"))
                        parent.opener.document.getElementById("chkListedEndpoint").checked = false;
                     
                      var obj = parent.opener.document.getElementById("chkUnlistedEndpoint");
                      obj.checked = true;
                      parent.opener.OpenEndpointlist(obj);
                    }
                    window.close();
                }
			    //ZD 100602 End
            }
			else if (url.indexOf("hdnLnkBtnId") >= 0 && url.indexOf("AddRoomEndpoint") < 0 ) {//ZD 100642 Start
                var hdNm = document.getElementById("locstr");
                var locsmain = document.getElementById("selectedlocframe");
                if(url.indexOf("hdnLnkBtnId") >= 0)
                {
                    var xprnt = hdNm.value.split("|");
                    if (xprnt.length > 2) {
                    alert(RoomalterApprover);
                    window.parent.document.getElementById('errormsg').style.display = 'block';
                    }
                }
                if (window.parent) {
                    var add = window.parent.document.getElementById("addRooms"); //Edited for FF   START        
                    var prnt = window.parent.document.getElementById("locstrname");
                    if (prnt)
                        prnt.value = hdNm.value;
                    if (add && prnt.value != "")
                        add.click();
                }
                window.close;
            } //ZD 100642 End
            //ZD 100522 Starts
             else if (url.indexOf("VMRfrmSettings2") >= 0) {
                var hdNm = document.getElementById("locstr");
                var locsmain = document.getElementById("selectedlocframe");
                var add = window.parent.document.getElementById("addRooms");
                var prnt = window.parent.document.getElementById("locstrname");
                if (prnt)
                    prnt.value = hdNm.value;
                if (add)
                    add.click();

                window.parent.document.getElementById("hdnDialLoc").value = hdNm.value;
                window.parent.fnTriggerFromPopup();

            }
            //ZD 100522 End
            else if (url.indexOf("pageID") == -1) {
                var hdNm = document.getElementById("locstr");
                var locsmain = document.getElementById("selectedlocframe");
                if (opener) {
                    var f = top.opener.document.forms['<%=Parentframe%>'];
                    var add = parent.opener.document.getElementById("addRooms"); //Edited for FF   START        
                    var prnt = parent.opener.document.getElementById("locstrname");
                    var calen = parent.opener.document.getElementById("btnDate");
                    var calensettings = parent.opener.document.getElementById("IsSettingsChange"); //Edited for FF End
                    if (prnt)
                        prnt.value = hdNm.value;
                    if (add)
                        add.click();
                }
                window.close()
            }
            else {
                var prnt = document.getElementById("selectedlocframe").value; //"12,13";////parent.opener.document.getElementById("locstrname");
                var xprnt = prnt.split(",");
                if (xprnt.length == "1") {
                    window.close()
                }
                else if (xprnt.length > 2) {
                    window.parent.document.getElementById('errormsg').style.display = 'block';
                }
                else {
                    alert(ExistingEndpoint);
                    window.close()
                }
            }
        }
        catch (exception) {
            window.parent.document.getElementById('errormsg').style.display = 'block';
        }
    }

    function EditRoom(val) {        
        var rmids = EditRoom.arguments;

        var rmid = "";

        if (rmids) {
            rmid = rmids[0];
        }        
        DataLoading("1");//ZD 100429
        //FB 2694 Starts
        var RoomCategory = document.getElementById("hdnEdit" + rmid).value;        
        if (RoomCategory == 4)
            parent.location.replace("ManageRoomProfile.aspx?cal=2&rID=" + rmid + "&pageid=Hotdesking");            
        else{        
            if (parent) {
                var isReplace = true;  //FB 2448 start
                if (document.getElementById("chkIsVMR"))
                    if (document.getElementById("chkIsVMR").checked) {
                    isReplace = false;
                    parent.location.replace("ManageVirtualMeetingRoom.aspx?rID=" + rmid);
                }

                if (isReplace) //FB 2448 end
                    parent.location.replace("ManageRoomProfile.aspx?cal=2&rid=" + rmid);
            }
        }        
        //FB 2694 Ends
    }
    //FB 2426 Start
    function ImportRoom() {
    
        var args = ImportRoom.arguments;
        var ErrLbl= document.getElementById("hdnImportErr"); //ZD 100619
        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");

        if (locs.value == "")
            locs.value = args[0];

        var lang = "<%=Session["language"] %>";

        if(adlocs)
        {
            if (lang == "en")
                adlocs.value = "Import";
            else if(lang == "sp")
                adlocs.value = "Importar";
            else if (lang == "fr")
                adlocs.value = "Importer";
        }

        if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
       DataLoading("1"); //ZD 100429
        //ZD 100619 Starts
        var isRemove = confirm(RSAssDepartment)
        var refrsh = document.getElementById("btnRefreshRooms");
            if (refrsh)
                refrsh.click();
        DataLoading("1");
		if (isRemove == true) 
        {
            if(ErrLbl!= null)
            {
                if(ErrLbl.value =="1")
                   return false;
               else
                    parent.location.replace("ManageRoomProfile.aspx?cal=2&rid=" + locs.value);
            }
            else
                parent.location.replace("ManageRoomProfile.aspx?cal=2&rid=" + locs.value);
        }
	   //ZD 100619 Ends
    }


    function delGuestRoom() {
        var args = delGuestRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");

        if (locs.value == "")
            locs.value = args[0];

        var lang = "<%=Session["language"] %>";

        if(adlocs)
        {
        if (lang == "en")
            adlocs.value = "Delete";
        else if(lang == "sp")
            adlocs.value = "Eliminar";
        else if (lang == "fr")
                adlocs.value = "Effacer";

        }
        if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        
        DataLoading("1"); //ZD 100429
        
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();

    }
    //FB 2426 End
    function ClearAllSelection() {
        try {
            var locs = document.getElementById("selectedlocframe");
            var adlocs = document.getElementById("addroom");
            var data=document.getElementById("SelectedRoomNameValue"); //ZD 101175
            var hdNm = document.getElementById("locstr");  
            document.getElementById("hdnVMRRoomadded").value=""; //ZD 10052
            if (parent.document.getElementById("hdnSelectVMRRoom")) //ZD 100522
                parent.document.getElementById("hdnSelectVMRRoom").value="";

            locs.value = "";
            hdNm.value = "";
            document.getElementById('hdnSelectionVal').value = "2";//ZD 102916

            if (adlocs)
                adlocs.value = "1";

            if (opener) {
                prnt = opener.document.getElementById("selectedList");
                 prntroomname=opener.document.getElementById("selectedListRoomName");

                 var hdnTierIDlocStr = document.getElementById("hdnTierIDlocStr");
                 prntTier = opener.document.getElementById("selectedTierList"); //ZD 102358          
                 if(prntTier)
                    prntTier.value = hdnTierIDlocStr.value;

                if (prnt)
                    prnt.value = locs.value;
                if (prntroomname)
                    prntroomname.value = "";

                if(prntroomname != null)
                {
                    if(prntroomname.value !="")
                        opener.document.getElementById("tdSelectRooms").style.display='none';
                    else
                        opener.document.getElementById("tdSelectRooms").style.display='block';
                }

                if (opener.document.getElementById("btnfrmSearch"))
                    opener.document.getElementById("btnfrmSearch").click();

                var selprnt = opener.document.getElementById("selectedloc");

                if (selprnt)
                    selprnt.value = "";
            }
            else if (parent) {
                prnt = parent.document.getElementById("selectedList");

                if (prnt)
                    prnt.value = locs.value;


                var selprnt = parent.document.getElementById("selectedloc");

                if (selprnt)
                    selprnt.value = "";

            }



            var refrsh = document.getElementById("btnRefreshRooms");
            if (refrsh)
                refrsh.click();
//              if(roomIdsStr.length == 1)
//                setTimeout("fnremove()",1000);

        }
        catch (exception) {
            window.close()
        }
    }

    function ChangeViewType() {

        var tr2 = document.getElementById("DetailsView");
        var tr1 = document.getElementById("ListView");
        var drp = document.getElementById("DrpDwnListView");

        if (drp) {
            tr1.style.display = 'none';
            tr2.style.display = 'none';

            if (drp.value == "1")
                tr1.style.display = 'block';
            else
                tr2.style.display = 'block';
        }


    }
    //FB 2426 Start 
    function ShowActDct() {

        document.getElementById("trGuestRooms").style.display = 'block';
        document.getElementById("chkGuestRooms").checked = false;

        var DrpActDct = document.getElementById("DrpActDct");
        if (DrpActDct)
            if (DrpActDct.value == "1") // || DrpActDct.value == "2") //FB 2565
            document.getElementById("trGuestRooms").style.display = 'none';

        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();
    }


    function getQueryString(par) {
        par = par + '=';
        var url = window.location.href;
        var splited = url.split(par);
        var extracted = splited[1].split('&');
        return extracted[0];
    }

    //FB 2426 End

    //FB 2588
    function ChangeTimeFormat() {

        var args = ChangeTimeFormat.arguments;
        var stime = document.getElementById("confRoomStartTime_Text").value;
        var etime = document.getElementById("confRoomEndTime_Text").value;
        var hdnsTime = document.getElementById("hdnStartTime");
        var hdneTime = document.getElementById("hdnEndTime");

        if ('<%=Session["timeFormat"]%>' == "2") {
            if (args[0] == "D") {
                stime = stime.replace('Z', '')
                stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);

                etime = etime.replace('Z', '')
                etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);

            }
            else {
                if (stime.indexOf("Z") < 0)
                    stime = stime.replace(':', '') + "Z";
                if (etime.indexOf("Z") < 0)
                    etime = etime.replace(':', '') + "Z";
            }
        }

        hdnsTime.value = stime
        hdneTime.value = etime
    }
    
    //ZD 100284
    if (document.getElementById("confRoomStartTime_Text")) {
        var confstarttime_text = document.getElementById("confRoomStartTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('confRoomStartTime_Text', 'regRoomStartTime',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("confRoomEndTime_Text")) {
        var confstarttime_text = document.getElementById("confRoomEndTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('confRoomEndTime_Text', 'regRoomEndTime',"<%=Session["timeFormat"]%>")
        };
    }
    
    //alert(window.opener.parent.document.getElementById("selectedloc").value);
</script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            if(!calendar)
                ClosePopup();
        }
    }

    document.getElementById("chkFavourites").setAttribute("onfocus", "window.parent.scrollTo(0,0);");
    //ZD 101316
    if(document.getElementById('grid2_tcPagerBarT') != null)
        document.getElementById('grid2_tcPagerBarT').style.width = "100%";
    if (document.getElementById('grid_tcPagerBarT') != null)
        document.getElementById('grid_tcPagerBarT').style.width = "100%";

    fnUpdateGridWidth(); // ZD 101276

    //if(navigator.userAgent.indexOf('Trident') == -1)
        //document.getElementById("divLicense").style.marginTop = "10px"

    // ZD 102315 Starts
    function fnSetChangeStatus() {
        document.getElementById("hdnIsChanged").value = "1";
    }

    document.getElementById("txtRoomDateFrom").setAttribute("onfocus", "fnSetChangeStatus();");
    document.getElementById("txtRoomDateTo").setAttribute("onfocus", "fnSetChangeStatus();");
    document.getElementById("confRoomStartTime_Text").setAttribute("onchange", "fnSetChangeStatus();");
    document.getElementById("confRoomEndTime_Text").setAttribute("onchange", "fnSetChangeStatus();");
    // ZD 102315 Ends
    //ZD 102123 - Start
    function fnHotdesking() {
        //ZD 104586 - Start
//        if (document.getElementById('DrpDwnListView').value == "3") {
//            DataLoading("1");
//            var str = window.location.href;
//            str = str.replace("RoomSearch.aspx", "RoomSearchHotdesking.aspx");
//            str = str.replace("rmsframe=", "");
//            str += "&rmsframe= " + document.getElementById("selectedlocframe").value;
//            window.location.href = str;
//            return false;
        //        }
        //ZD 104586 - End
        document.getElementById('hdnChgViewType').value = 1;
        __doPostBack('DrpDwnListView', '');
    }
    
    //ZD 102123 - End

</script>
<%--ZD 100428 END--%>

</html>
