<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_Allocation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNet.aspx" -->
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>

<script language="JavaScript1.2" src="inc/functions.js" type="text/javascript" />

 
    <script src="inc/menuinc.js" type="text/javascript"></script>
<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
//ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server">
    <title>Bulk Tool</title>
       <link rel="stylesheet" type="text/css" media="all" href="css/calendar-win2k-1.css" title="win2k-1" />
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

    <script type="text/javascript">
      var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Date%>", 10),
      parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
    </script>
<%--FB 1861--%>
  <%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js" ></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <script language="JavaScript" type="text/javascript">
<!--

	tabs=new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","ALL")
	
//	sortimg = "";

	function setOprValue()
	{
		var m1 = "", m2="", oprval="", cb = document.frmAllocation.DdlOperation;
		document.getElementById("AllocationSubmit").disabled = false; //FB 2262	//FB 2599	
		switch (cb.options[cb.selectedIndex].value) {

            case "21"://ZD 104850 start
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, UserDomain%>' runat='server'></asp:Literal></label>"
				m2 = "<input type='text' name='OprValue' id='OprValue' size='28' value='' class='altText'>";
				break; //ZD 104850 End
           
           case "20"://ZD 101122
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, EnableFacilityWO%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.enableFacilityWO.value;
				break;
           case "19":
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, EnableCateringWO%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.enableCateringWO.value;
				break;

           case "18":
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, EnableAudioVisualWO%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.enableAVWO.value;
				break;
           case "17"://ZD 101093
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, EnableAdditionalOptions%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.enableAO.value;
				break;
		    
            case "16"://ZD 101015
		        m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, EnableWebExConferencing%>' runat='server'></asp:Literal></label>"
		        oprval = document.frmAllocation.enableMobile.value;
		        break;
		    case "15"://FB 1979
		        m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageUserProfile_LblMob%>' runat='server'></asp:Literal></label>"
		        oprval = document.frmAllocation.enableMobile.value;
		        break;
		    case "14"://FB 1599
			m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageUserProfile_LblExc%>' runat='server'></asp:Literal></label>" //FB 2941
				oprval = document.frmAllocation.enableExchange.value;
				break;
		    case "13"://FB 1599
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageUserProfile_LblDom%>' runat='server'></asp:Literal></label>"  //FB 2941
				oprval = document.frmAllocation.enableDomino.value;
				break;
		    case "12"://window dressing
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, EnableAVSettings%>' runat='server'></asp:Literal> </label>"  //FB 2941
				oprval = document.frmAllocation.enableAV.value;
				break;
		    case "11": //window dressing
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, DisplayTimezoneonallScreens%>' runat='server'></asp:Literal></label>" 
				oprval = document.frmAllocation.tzdisplay.value;
				if ("<%=Session["EnableZulu"]%>" == "1") //FB 2588
				    document.getElementById('AllocationSubmit').disabled = true;
				break;
			case "10"://window dressing//FB 2588
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, TimeFormat%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.tformat.value;
				if ("<%=Session["EnableZulu"]%>" == "1")
				    document.getElementById('AllocationSubmit').disabled = true;
				break;
			case "9"://window dressing
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageUserProfile_DateFormat%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.dtformat.value;
				break;
			case "6"://window dressing//FB 2588
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, ManageUserProfile_TzTD1%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.timezones.value;
				if ("<%=Session["EnableZulu"]%>" == "1")
				    document.getElementById('AllocationSubmit').disabled = true;
				break;
            case "7": //FB 2262 //FB 2599 //FB 2698
            if ("<%=Session["Cloud"]%>" == "1")
                document.getElementById('AllocationSubmit').disabled = true;
                break;
            case "8": //FB 2599 //FB 2698
            if ("<%=Session["Cloud"]%>" == "1")
                document.getElementById('AllocationSubmit').disabled = true;
                break;
			case "5"://window dressing
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, UserRole%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.roles.value;
				break;
			case "4"://window dressing
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_PreferredLangu%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.languages.value;
				break;
			case "3"://window dressing
				m1 = "<label class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, AssignMCU%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.bridges.value;
				break;
			case "0":
                //Window Dressing
				m1 = "<label for='OprValue' class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, AddMinutestoWallet%>' runat='server'></asp:Literal></label>"
				m2 = "<input type='text' name='OprValue' id='OprValue' size='28' value='' class='altText'>";
				break;
			case "2":
                //Window Dressing
				m1 = "<label for='OprValue' class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, NewAccountExpirationDate%>' runat='server'></asp:Literal></label>" //ZD 100537
				m2 = "<input type='text' name='OprValue' id='OprValue' size='16' value='' class='altText' readonly='true'> ";
				m2 += "<a href='' onclick='this.childNodes[0].click();return false;'>";//ZD 100420
				m2 += "<img src='image/calendar.gif' valign='centre' border='0' alt='Date Selector' width='20' height='20' id='cal_trigger' style='cursor: pointer; z-index:10' title='<asp:Literal Text='<%$ Resources:WebResources, Dateselector%>' runat='server'></asp:Literal>' " ; // FB 2050 //ZD 100419
                m2 += " onclick='return showCalendar(\"OprValue\", \"cal_trigger\", 1, \"<%=format%>\");' />";//FB 1073
				break;
			case "1":
                //Window Dressing
				m1 = "<label for='OprValue' class='blackblodtext'><asp:Literal Text='<%$ Resources:WebResources, AssigntoDepartments%>' runat='server'></asp:Literal></label>"
				oprval = document.frmAllocation.departments.value;
				break;
		}

		if (oprval != "") {
			valsary = oprval.split("||"); // FB 1888
			if (cb.options[cb.selectedIndex].value == "1")
				m2  = "<select size='5' name='OprValue' id='OprValue' class='altLong0SelectFormat' multiple>"
			else
				m2  = "<select size='1' name='OprValue' id='OprValue' class='altLong0SelectFormat'>"
			for (i = 0; i < valsary.length - 1; i++) {
				valary = valsary[i].split("``");
				
				m2 += "  <option value='" + valary[0] + "'>" + valary[1] + "</option>"
			}
			m2 += "</select>"
			
		}
		
		document.getElementById ("OprValueDIV0").innerHTML = m1;
		document.getElementById ("OprValueDIV1").innerHTML = m2;

		switch (cb.options[cb.selectedIndex].value) {
			//Code Removed temporarily to be added
			case "0":
				setFieldValue ("OprValue", "28800"); 
				break;
		}
	}
	
	
	
	function chgpageoption(totalpagenum)
	{
	
		tpn = parseInt(totalpagenum, 10)
		cb = document.frmAllocation.SelPage;
		
		RemoveAllOptions (cb);
		
		for (var i=1; i<=tpn; i++) {
			addopt(cb, i, i, false, false);
		}
		
		
	}



	function typeimg(ifrmname, sb)
	{
	    
		imgsrc = document.getElementById("img_" + ifrmname + "_" + sb).src;
		sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
		
		for (i = 1; i < 5; i++) {
			if (i != 3)
				document.getElementById("img_" + ifrmname + "_" + i).src = "image/bg.gif";
		}
		document.getElementById("img_" + ifrmname + "_" + sb).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_up.gif" : "image/sort_up.gif")) ;
	}
	

	function nameimg(prename, sel)
	{
	    
		for (i in tabs) {
			document.getElementById(prename + tabs[i]).style.backgroundColor = ""; 
		}

		document.getElementById(prename + sel).style.backgroundColor = "#FF6699"; 
	}
	
	
	function pageimg(selectedpn)
	{
	
		document.frmAllocation.pageno.value = selectedpn;
		
		if (document.frmAllocation.SelPage.options.length == 1)
			document.frmAllocation.SelPage.options.selected = true;
		else
			document.frmAllocation.SelPage.options[parseInt(selectedpn, 10)-1].selected = true;
	}
	

	function seltype(ifrmname, sb)
	{	
		document.frmAllocation.sortby.value = sb;
		
		document.frmAllocation.pageno.value = 1;
		if (document.frmAllocation.SelPage.options.length>0)
			document.frmAllocation.SelPage.options[0].selected = true; 

        eval(ifrmname).window.location.href = "ifrmaduserlist.aspx?f=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value+"&wintype=ifr";
		//eval(ifrmname).window.location.href = "dispatcher/admindispatcher.asp?cmd=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value;

		document.frmAllocation.CheckAllAdUser.checked = false;
	}


	function selname(ifrmname, sel)
	{
        fnResetFrames(); // ZD 101362

		document.frmAllocation.alphabet.value = sel;
		document.frmAllocation.pageno.value = 1;
		if (document.frmAllocation.SelPage.options.length>0)
			document.frmAllocation.SelPage.options[0].selected = true;
			
		eval(ifrmname).window.location.href = "ifrmaduserlist.aspx?f=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value+"&wintype=ifr";
		//eval(ifrmname).window.location.href = "dispatcher/admindispatcher.asp?cmd=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value;
		document.frmAllocation.CheckAllAdUser.checked = false;
	}
	

	function selpage(ifrmname, cb)
	{
        fnResetFrames(); // ZD 101362

		document.frmAllocation.pageno.value = cb.options[cb.selectedIndex].value;

		eval(ifrmname).window.location.href = "ifrmaduserlist.aspx?f=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value+"&wintype=ifr";
		//eval(ifrmname).window.location.href = "dispatcher/admindispatcher.asp?cmd=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value;

		document.frmAllocation.CheckAllAdUser.checked = false;
		
		
	}
	

	function Sort(ifrmname, id, totalnum)
	{
		if ( document.getElementById(ifrmname).contentWindow.sortlist ) {
		    if(id==4 && ifrmname =="ifrmADuserlist") // FB 2942 
			    document.getElementById(ifrmname).contentWindow.sortlist(3);
			else
			    document.getElementById(ifrmname).contentWindow.sortlist(id);

			imgsrc = document.getElementById("img_" + ifrmname + "_" + id).src;
			sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
			for (i = 1; i <= totalnum; i++) {
			
			    if(i==3&&ifrmname=="ifrmADuserlist") // FB 2942
			        continue;
				
				document.getElementById("img_" + ifrmname + "_" + i).src = "image/bg.gif";
			}
			document.getElementById("img_" + ifrmname + "_" + id).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_down.gif" : "image/sort_up.gif")) ;
		}
	}


	function checkallusr(chk)
	{
        fnResetFrames(); // ZD 101362

		var needstop = false;
					
		if (ifrmADuserlist.document.frmIfrmuserlist.adusers) {
			xmlstr = ifrmADuserlist.document.frmIfrmuserlist.adusers.value;
			usersary = xmlstr.split("||"); // FB 1888
			
			cb = ifrmADuserlist.document.frmIfrmuserlist.seladuser

			if (usersary.length == 2) {
				usersary[0] = usersary[0].split("``");
				if (cb.checked != chk) {
					cb.checked = chk;
					needstop = ifrmADuserlist.adUserClicked(cb, usersary[0][0], usersary[0][1], usersary[0][2], usersary[0][3]);
				}
			}
			
			if (usersary.length > 2) {
				for (i = 0; i < cb.length; i++) {
					usersary[i] = usersary[i].split("``");
					if (cb[i].checked != chk) {
						cb[i].checked = chk;
						needstop = ifrmADuserlist.adUserClicked(cb[i], usersary[i][0], usersary[i][1], usersary[i][2], usersary[i][3]);
						
						if (needstop)
							break;
					}
				}
			}

		}
	}
	
	
	function checkallgrp(chk)
	{
		var needstop = false;
		if (ifrmAllogrouplist.document.frmIfrmgrouplist.selgrp) {
			cb = ifrmAllogrouplist.document.frmIfrmgrouplist.selgrp

			if (cb.length > 0)
				for (i = 0; i < cb.length; i++) {
					
					if (cb[i].checked != chk) {
						cb[i].checked = chk;
						needstop = ifrmAllogrouplist.grpClicked(cb[i], i);
						if (needstop) break;
					}
					
				}
			else {
				if (cb.checked != chk) {
					cb.checked = chk;
					ifrmAllogrouplist.grpClicked(cb, 0);
				}
			}
		}
	}
	
	
	function deleteall()
	{
        fnResetFrames(); // ZD 101362
        
		if (ifrmVRMuserlist.delUser) {
			xmlstr = document.frmAllocation.SelectedUser.value;
			usersary = xmlstr.split("||"); // FB 1888
			
			for (i = 0; i < usersary.length; i++) {
				usersary[i] = usersary[i].split("``");
				
				ifrmVRMuserlist.delUser(usersary[i][0]);
			}
			
			document.frmAllocation.CheckAllAdUser.checked = false;
		}
	}


	function frmAllocation_Validator ()
	{
		if (document.frmAllocation.SelectedUser.value == "" && document.getElementById("CheckAllAdUser_Org").checked == false) { // ZD 101362
			alert(AlllocationUser); 
			return false;
		}
		
		var cb = document.frmAllocation.DdlOperation;
		switch (cb.options[cb.selectedIndex].value) {
		    case "0": 
		        var obj = document.getElementById("OprValue");
		        if (obj != null)
		        {
		            if (isNaN(obj.value))
		            {
		                    alert(AllocWalletMin);
		                    return false;  
		            }
		            else if (obj.value <= 0)
		            {
		                    alert(AllocWalletMin);
		                    return false;  
                    }
                    else if (obj.value > 999999)
                    {
                        alert(AllocWallet);
		                return false;  
                    }
		        }
		        break;
		    case "2":
		       
		        var objDt = document.getElementById("OprValue");
		        if(objDt != null)
		        {
		            
		            if(objDt.value == "")
		            {
		                alert(AllocDate);
		                return false;
		            } 
		        
		        }
		        break;
		        case "1":
		        var objDt = document.getElementById("OprValue");
		        if(objDt != null)
		        {
		            
		            if(objDt.value == "")
		            {
		                alert(Allocdept);
		                return false;
		            } 
		        
		        }
		        break;
		    case "3": 
                // Disable submit button if there are no MCU present
                var obj = document.getElementById("OprValue");
                if (obj == null)
                {
                    alert(AllocMCU);
                    document.getElementById('AllocationSubmit').disabled=true;
                    return false;
                }
                break;

			case "7":
				var isConfirm = confirm(AllocDelete)
				return isConfirm;
				break;
			case "8":
				var isConfirm = confirm(AllocLock)
				return isConfirm;
				break;
          case "21": //ZD 104850 start
             var objDo = document.getElementById("OprValue").value;
             var ValidateEx =/^[^<>&]*$/;
             if(!objDo != null)
              {
                  if (objDo.trim() == "")
		          {
		             alert(RequiredUsrDomain);
		             return false;  
		          }
                  else if(!ValidateEx.test(objDo))
                  {
                    alert(invalidcharacter);
                    return false;
                  }
              }
             break; //ZD 104850 End 
		}
      DataLoading(1);//ZD 100176
      return true;
	}
	//ZD 100420  start
	function fnReset()
	{
	    window.location = 'allocation.aspx';
	    return false; 
	}
	//ZD 100420 End

    // ZD 101362 Starts
    function fnResetFrames()
    {
        document.getElementById("CheckAllAdUser_Org").checked = false;
        window.frames['ifrmADuserlist'].fnHideIframe("0");
        window.frames['ifrmVRMuserlist'].fnHideIframe("0");
    }
    // ZD 101362 Ends
//-->
</script>
    
</head>
<body>
    <form lang="JavaScript" method="post" id="frmAllocation" onsubmit="return frmAllocation_Validator()" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <div>
            <center>
                <h3><asp:Literal Text="<%$ Resources:WebResources, Allocation_BulkUserManag%>" runat="server"></asp:Literal></h3><!-- FB 2570 -->
            </center>
            <table width="95%">
                <tr align="center">
                    <td align="center">
                        <asp:Label ID="LblError" runat="server" CssClass="lblError"></asp:Label> <%--FB 1599--%>
                        <div id="dataLoadingDIV" style="z-index: 1; display :none" align="center">
                            <img border='0' src='image/wait1.gif'  alt='Loading..' />
                        </div> <%--ZD 100176--%>
                    </td>
                </tr>
            </table>
            
                <input name="cmd" id="cmd" type="hidden" value="SetAllocation" />
                <input name="users" id="hdnusers" type="hidden" runat="server" />
                <input name="Action" id="Action" type="hidden" />
                <input name="SelectedUser" id="SelectedUser" type="hidden" runat="server" />
                <input name="sortby" id="sortby" type="hidden"  runat="server" />
                <input name="alphabet" id="alphabet" type="hidden"  runat="server" />
                <input name="pageno" id="pageno" type="hidden"  runat="server" />
                <input name="timezones" id="timezones" type="hidden"  runat="server" />
                <input name="roles" id="roles" type="hidden"  runat="server" />
                <input name="languages" id="languages" type="hidden"  runat="server" />
                <input name="bridges" id="bridges" type="hidden"  runat="server" />
                <input name="departments" id="departments" type="hidden"  runat="server" />
                <input name="canAll" id="canAll" type="hidden"  runat="server" />
                <input name="totalPages" id="totalPages" type="hidden"  runat="server" />
                <input name="totalNumber" id="totalNumber" type="hidden"  runat="server" />
                <input name="dtformat" id="dtformat" type="hidden"  runat="server" />
                <input name="tformat" id="tformat" type="hidden"  runat="server" />
                <input name="tzdisplay" id="tzdisplay" type="hidden"  runat="server" />
                <input name="enableAV" id="enableAV" type="hidden"  runat="server" />
                <input name="enableAO" id="enableAO" type="hidden"  runat="server" /><%--ZD 101093--%>
                <input name="enableAVWO" id="enableAVWO" type="hidden"  runat="server" />
                <input name="enableCateringWO" id="enableCateringWO" type="hidden"  runat="server" />
                <input name="enableFacilityWO" id="enableFacilityWO" type="hidden"  runat="server" />
                <%--FB 1599--%>
                <input name="enableDomino" id="enableDomino" type="hidden"  runat="server" />
                <input name="enableExchange" id="enableExchange" type="hidden"  runat="server" />
                <%--FB 1979--%>
                <input name="enableMobile" id="enableMobile" type="hidden"  runat="server" />

                <input id="helpPage" type="hidden" value="94" />
                <center>
                    <table border="0" cellpadding="0" cellspacing="0" height="104" width="80%">
                        <tr align="left">
                            <td valign="top" style="width: 2%; height: 53px;">
                                &nbsp;
                            </td>
                            <td style="width: 1%; height: 53px">
                                &nbsp;</td>
                            <td style="width: 97%; height: 53px">
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_SelectUsers%>" runat="server"></asp:Literal></span><br />
                                <span class="blackblodtext">
                                <%--Window Dressing--%>
                                <font size="2"><span class="cmtfldstarText">-</span> <asp:Literal Text="<%$ Resources:WebResources, Allocation_Toaddusersch%>" runat="server"></asp:Literal><br />
                                    <span class="cmtfldstarText">-</span><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Allocaton_Remove1%>" runat="server"></asp:Literal> 
                                    <img style="border: 0; height: 12; width: 12" src="image/btn_delete.gif" alt="Delete Button" />
                                    <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Allocaton_Remove2%>" runat="server"></asp:Literal> </font></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 10">
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 651px">
                            </td>
                            <td style="height: 651px">
                            </td>
                            <td align="center" valign="middle" style="height: 651px">
                                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                    <tr>
                                        <td style="width: 1%; height: 624px">
                                        </td>
                                        <td style="width: 49%;height: 624px; vertical-align: top" >
                                            <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                <tr>
                                                    <td colspan="2" style="height: 15" valign="top">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="right">
                                                                    <table width="100%" style="height: 100%">
                                                                        <tr>
                                                                        <%--Window Dressing--%>
                                                                            <td valign="top" class="blackblodtext">
                                                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_StartsWith%>" runat="server"></asp:Literal></span> <%--Edited for FF--%><%--FB 2579--%>
                                                                            </td>
                                                                            <td>
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'A')"><span id="uA" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uA%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'B')"><span id="uB" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uB%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'C')">
                                                                                            <span id="uC" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uC%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'D')">
                                                                                                <span id="uD" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uD%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'E')">
                                                                                                    <span id="uE" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uE%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'F')">
                                                                                                        <span id="uF" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uF%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'G')">
                                                                                                            <span id="uG" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uG%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'H')">
                                                                                                                <span id="uH" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uH%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'I')">
                                                                                                                    <span id="uI" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uI%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'J')">
                                                                                                                        <span id="uJ" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uJ%>" runat="server"></asp:Literal></span></a>
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'K')"><span id="uK" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uK%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'L')"><span id="uL" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uL%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'M')">
                                                                                            <span id="uM" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uM%>" runat="server"></asp:Literal></span></a><br />
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'N')"><span id="uN" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uN%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'O')"><span id="uO" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uO%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'P')">
                                                                                            <span id="uP" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uP%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'Q')">
                                                                                                <span id="uQ" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uQ%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'R')">
                                                                                                    <span id="uR" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uR%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'S')">
                                                                                                        <span id="uS" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uS%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'T')">
                                                                                                            <span id="uT" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uT%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'U')">
                                                                                                                <span id="uU" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uU%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'V')">
                                                                                                                    <span id="uV" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uV%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'W')">
                                                                                                                        <span id="uW" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uW%>" runat="server"></asp:Literal></span></a>
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'X')"><span id="uX" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uX%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'Y')"><span id="uY" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uY%>" runat="server"></asp:Literal></span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'Z')">
                                                                                            <span id="uZ" class="tabtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_uZ%>" runat="server"></asp:Literal></span></a>
                                                                            </td>
                                                                            <td id="tabAll" align="right" style="width:10%">
                                                                                <a  href="#" onclick="JavaScript: selname('ifrmADuserlist', 'ALL')"><span id="uALL" class="tabtext">
                                                                                    <asp:Literal Text="<%$ Resources:WebResources, Allocation_uALL%>" runat="server"></asp:Literal></span></a><%--100420 End--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext">
                                                                    <asp:Literal Text="<%$ Resources:WebResources, Allocation_GoTo%>" runat="server"></asp:Literal>
                                                                    <select name="SelPage" id="SelPage" class="altText" onchange="selpage('ifrmADuserlist', this)" size="1" runat="server">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="height: 18px; width: 7%"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, Select%>" runat="server"></asp:Literal>
                                                                </td>
                                                                <td align="center" style="height: 18px; width: 25%"> <%--FB 2942 Starts--%>
                                                                    <a class="e" href="javascript:Sort('ifrmADuserlist', 1,4)"><asp:Literal Text="<%$ Resources:WebResources, Allocation_FirstName%>" runat="server"></asp:Literal></a><img id="img_ifrmADuserlist_1" 
                                                                        height="10" src="image/bg.gif" width="10"  alt="Sort"/></td> <%--ZD 100419--%>
                                                                <td align="center" style="height: 18px; width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmADuserlist', 2,4)"><asp:Literal Text="<%$ Resources:WebResources, Allocation_LastName%>" runat="server"></asp:Literal></a><img id="img_ifrmADuserlist_2"
                                                                        height="10" src="image/bg.gif" width="10" alt="Sort" /></td><%--ZD 100419--%>
                                                                <td align="center" style="height: 18px; width: 43%">
                                                                    <a class="e" href="javascript:Sort('ifrmADuserlist', 4,4)"><asp:Literal Text="<%$ Resources:WebResources, Allocation_Email%>" runat="server"></asp:Literal></a><img id="img_ifrmADuserlist_4"
                                                                        height="10" src="image/bg.gif" width="10" alt="Sort" /></td><%--FB 2942 Ends--%><%--ZD 100419--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   <%--Window Dressing--%>
                                                    <td align="right" valign="top" style="width: 10%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_Active%>" runat="server"></asp:Literal><br /><%--FB 2579--%>
                                                            <span class="reqfldText">*</span>
                                                    </td>
                                                   <%--Window Dressing--%>
                                                     <td align="left" class="blackblodtext">
                                                      <iframe src="ifrmaduserlist.aspx?f=frmAllocation&wintype=ifr" name="ifrmADuserlist" id="ifrmADuserlist" width="100%" height="500" align="left" valign="top"><%--FB 2942 ZD 101362 --%>
                                                        <p><asp:Literal Text="<%$ Resources:WebResources, Allocation_goto%>" runat="server"></asp:Literal><a href=""><asp:Literal Text="<%$ Resources:WebResources, Allocation_ActiveUserLis%>" runat="server"></asp:Literal></a></p>
                                                      </iframe> 

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px" align="right">
                                                        <input type="image" onclick="return false;" title="<asp:Literal Text='<%$ Resources:WebResources, SelectAllActiveUsr%>' runat='server'></asp:Literal>" src="image/info.png" alt="Info" style="border-width:0px;cursor:default" /><%-- ZD 101362 --%><%-- ZD 102590 --%>
                                                    </td>
                                                    <td align="left" style="height: 20px" class="blackblodtext"> <%--window dressing--%>
                                                        <input name="CheckAllAdUser" id="CheckAllAdUser" onclick="JavaScript: checkallusr(this.checked);" type="checkbox" runat="server" value="1" /><asp:Literal Text="<%$ Resources:WebResources, Allocation_all%>" runat="server"></asp:Literal></td>
                                                </tr>
                                                <%-- ZD 101362 Starts --%>
                                                <tr>
                                                    <td align="right">
                                                        <input type="image" onclick="return false;" title="<asp:Literal Text='<%$ Resources:WebResources, SelectAllOrguUsr%>' runat='server'></asp:Literal>" src="image/info.png" alt="Info" style="border-width:0px;cursor:default" /><%-- ZD 102590 --%>
                                                    </td>
                                                    <td class="blackblodtext" align="left">
                                                        <input type="checkbox" id="CheckAllAdUser_Org" runat="server" onclick="fnSelectAll(this.checked)" /><asp:Literal Text="<%$ Resources:WebResources, Allocation_allOrg%>" runat="server" />
                                                    </td>
                                                </tr>
                                                <%-- ZD 101362 Ends --%>
                                            </table>
                                        </td>
                                        <td align="center" valign="middle" style="width: 1%;height: 624px">
                                        </td>
                                        <td align="center" valign="middle" style="width: 49%;height: 624px" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                <tr><td style="height: 22px;" valign="top" colspan="2"> </td></tr>
                                                <tr><td  colspan="2"></td></tr>
                                                <tr>
                                                    <td>
                                                        <br /> <%-- ZD 101362  --%>
                                                    </td>
                                                    <td align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="width: 7%"><asp:Literal Text="<%$ Resources:WebResources, Delete%>" runat="server"></asp:Literal>
                                                                </td>
                                                                <td align="center" style="width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmVRMuserlist', 1, 3)"><asp:Literal Text="<%$ Resources:WebResources, Allocation_FirstName%>" runat="server"></asp:Literal></a><img id="img_ifrmVRMuserlist_1"
                                                                        height="10" src="image/bg.gif" width="10" alt="ifrmVRMuserlist1" /></td>
                                                                <td align="center" style="width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmVRMuserlist', 2, 3)"><asp:Literal Text="<%$ Resources:WebResources, Allocation_LastName%>" runat="server"></asp:Literal></a><img id="img_ifrmVRMuserlist_2"
                                                                        height="10" src="image/bg.gif" width="10" alt="ifrmVRMuserlist2" /></td>
                                                                <td align="center" style="width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmVRMuserlist', 3, 3)"><asp:Literal Text="<%$ Resources:WebResources, Allocation_Email%>" runat="server"></asp:Literal></a><img id="img_ifrmVRMuserlist_3"
                                                                        height="10" src="image/bg.gif" width="10" alt="ifrmVRMuserlist3" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                <%--Window Dressing--%>
                                                    <td align="right" valign="top" style="width: 10%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_Selected%>" runat="server"></asp:Literal><br /> <%--FB 2579--%>
                                                    </td> 
                                                      <td style="width:100%" align="left" valign="top">

                                                      <iframe src="ifrmvrmuserlist.aspx?f=frmAllocation&wintype=ifr" name="ifrmVRMuserlist" id="ifrmVRMuserlist" width="100%" height="500" align="left" valign="top">
                                                        <p><asp:Literal Text="<%$ Resources:WebResources, Allocation_goto%>" runat="server"></asp:Literal><a href=""><asp:Literal Text="<%$ Resources:WebResources, Allocation_SelectedUserL%>" runat="server"></asp:Literal></a></p>
                                                      </iframe> 

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="right"><%--FB 2262 FB 2599--%>
                                                        <input class="altMedium0BlueButtonFormat" name="deleteAll" onclick="JavaScript: deleteall();"
                                                            type="button" value="<%$ Resources:WebResources, ViewBlockedMails_LnkDeleteAll%>" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="left">
                            <td valign="top">
                                &nbsp;
                            </td>
                            <td >
                                &nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, Allocation_ConfigureSelec%>" runat="server"></asp:Literal></span><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td align="right">
                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right" style="height: 10px; width: 12%">
                                            <%--Window Dressing--%>
                                            <label class="blackblodtext">
                                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Action%>" runat="server"></asp:Literal></label>
                                        </td>
                                        <td align="left" style="height: 26px; width: 30%">
                                            <asp:DropDownList ID="DdlOperation" CssClass="altLong0SelectFormat" runat="server" onChange="JavaScript:setOprValue();">
                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, AddMinutestoWallet%>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, AssigntoDepartments%>"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ChangeAccountExpirationDate%>"></asp:ListItem> <%--ZD 100537--%>
                                                <asp:ListItem Value="3" Text="<%$ Resources:WebResources, ChangeAssignedMCU%>"></asp:ListItem>
                                                <%--FB 1404 <asp:ListItem Value="4">Change Preferred Language</asp:ListItem> --%>
                                                <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ChangePreferredLanguage%>"></asp:ListItem> <%--FB 2027--%>
                                                <asp:ListItem Value="21" Text= "<%$ Resources:WebResources, ChangeUserDomain%>"></asp:ListItem> <%--ZD 104850--%>
                                                <asp:ListItem Value="5" Text="<%$ Resources:WebResources, ChangeUserRole%>"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="<%$ Resources:WebResources, ManageUserProfile_TzTD1%>"></asp:ListItem> <%--FB 2938--%>
                                                <asp:ListItem Value="7" Text="<%$ Resources:WebResources, DeleteUsers%>"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="<%$ Resources:WebResources, LockUsers%>"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="<%$ Resources:WebResources, ManageUserProfile_DateFormat%>"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="<%$ Resources:WebResources, TimeFormat%>"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="<%$ Resources:WebResources, DisplayTimezoneonallScreens%>"></asp:ListItem> <%--FB 2938--%>
                                                <asp:ListItem Value="12" Text="<%$ Resources:WebResources, EnableAVSettings%>"></asp:ListItem> <%--FB 2938--%>
                                                <%--FB 1599--%>
                                                <asp:ListItem Value="13" Text="<%$ Resources:WebResources, ManageUserProfile_LblDom%>"></asp:ListItem><%--FB 2164--%> <%--FB 2939--%>
                                                <asp:ListItem Value="14" Text="<%$ Resources:WebResources, ManageUserProfile_LblExc%>"></asp:ListItem> <%--FB 2939--%>
                                                <asp:ListItem Value="15" Text="<%$ Resources:WebResources, ManageUserProfile_LblMob%>"></asp:ListItem> <%--FB 1979--%> <%--FB 2939--%>
                                                <asp:ListItem Value="16" Text="<%$ Resources:WebResources, EnableWebExConferencing%>"></asp:ListItem> <%--ZD 101015--%> 
                                                <asp:ListItem Value="17" Text="<%$ Resources:WebResources, EnableAdditionalOptions%>"></asp:ListItem> <%--ZD 101093--%>
                                                <asp:ListItem Value="18" Text="<%$ Resources:WebResources, EnableAudioVisualWO%>"></asp:ListItem>
                                                <asp:ListItem Value="19" Text="<%$ Resources:WebResources, EnableCateringWO%>"></asp:ListItem>
                                                <asp:ListItem Value="20" Text="<%$ Resources:WebResources, EnableFacilityWO%>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="height: 26px; width: 5%">
                                        </td>
                                        <td id="OprValueDIV0" align="right" style="width: 25%; height: 26px;" nowrap="nowrap">
                                        </td>
                                        <td id="OprValueDIV1" align="left" style="width: 32%; height: 26px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table border="0" cellpadding="2" cellspacing="0" width="600">
                        <tr>
                            <td align="center">
                              <%--<input id="btnReset" class="altMedium0BlueButtonFormat" name="reset"  onclick="javascript:window.location.hash = '';history.go(0);" value="Reset" /><%--ZD 100176--%>
                                    <button  name="reset" id="btnReset" class="altMedium0BlueButtonFormat" onclick="javascript:return fnReset()" ><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button> <%--ZD 100420--%>
                            </td>
                            <td align="center">
                                    <%--code added for Soft Edge button--%>
                                <asp:Button  CssClass="altMedium0BlueButtonFormat" onfocus="this.blur()" ID="AllocationSubmit" Text="<%$ Resources:WebResources, Allocation_AllocationSubmit%>" runat="server" OnClick="Submit_Click" />
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </center>
            </form>

            <script type="text/javascript" >
            <!--
                setOprValue();
                //-->
            </script>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 start--%>
<script type="text/javascript">
    // ZD 101362 Start
    function fnSelectAll(chk) {
        if (chk == true) {
            DataLoading('1');
            document.getElementById("CheckAllAdUser").checked = false;
            window.frames['ifrmADuserlist'].fnHideIframe("1");
            window.frames['ifrmVRMuserlist'].fnHideIframe("1");
            //alert("All users in the organization are selected");
            setTimeout("DataLoading('0')", 1000);
        }
        else {
            //document.getElementById("CheckAllAdUser_Org").checked = false;
            window.frames['ifrmADuserlist'].fnHideIframe("0");
            window.frames['ifrmVRMuserlist'].fnHideIframe("0");
        }
    }
    // ZD 101362 Ends
document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('AllocationSubmit').focus()");
document.getElementById('AllocationSubmit').setAttribute("onfocus", "");
</script>
<%--ZD 100420 End--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
