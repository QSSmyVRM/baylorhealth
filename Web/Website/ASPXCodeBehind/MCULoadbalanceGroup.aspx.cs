﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
* ZD 100040 Smart MCU
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using System.Data;


namespace ns_MyVRM
{
    public partial class MCULoadbalanceGroup : System.Web.UI.Page
    {
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #region protected Members

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox GroupName;
        protected System.Web.UI.WebControls.RequiredFieldValidator ReqFieldGName;
        protected System.Web.UI.WebControls.RegularExpressionValidator regGName;
        protected System.Web.UI.WebControls.RegularExpressionValidator regGrpDescription;
        protected System.Web.UI.WebControls.CheckBox ChkLoadBalance;
        protected System.Web.UI.WebControls.CheckBox Chkpooled;
        protected System.Web.UI.WebControls.CheckBox ChkLCR;
        protected System.Web.UI.WebControls.TextBox GroupDescription;
        protected System.Web.UI.WebControls.TextBox MCUsStr;
        protected System.Web.UI.WebControls.TextBox MCUsInfo;
        protected System.Web.UI.WebControls.Button Managegroup2Submit; 

        #endregion

        #region Private Members
        public Boolean searchgroup = false;
        private Int32 mcuGroupID = Int32.MinValue;
        private String strpartysInfo = "";

        #endregion

        #region MCULoadbalanceGroup
        public MCULoadbalanceGroup()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                obj.AccessandURLConformityCheck("MCULoadbalanceGroup.aspx", Request.Url.AbsoluteUri.ToLower());
                errLabel.Text = "";

                if (Request.QueryString["groupID"] != null)
                {
                    if (Request.QueryString["groupID"].ToString() != "")
                    {
                        mcuGroupID = Convert.ToInt32(Request.QueryString["groupID"]);
                    }
                }

                if (!IsPostBack)
                    BindData();

            }
            catch (Exception ex)
            {
                errLabel.Attributes.Add("style", "display:block");
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load" + ex.Message);
            }
        }

        #region BindData

        private void BindData()
        {
            String inXML = "";
            
            Session.Remove("MCUGroupID"); //Remove active mcu group id which is used to fetch un assigned virtual mcus

            if (mcuGroupID == Int32.MinValue)
                lblHeader.Text = obj.GetTranslatedText("Create MCU Load Balance Group");
            else
                lblHeader.Text = obj.GetTranslatedText("Edit MCU Load Balance Group");

            if (mcuGroupID > 0)
            {
                inXML += "<GetMCUGroup>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><groupID>" + mcuGroupID + "</groupID></GetMCUGroup>";
                String outXML = obj.CallMyVRMServer("GetMCUGroup", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNode node = xmldoc.SelectSingleNode("//groups/group");

                    if (mcuGroupID > 0)
                    {
                        Session.Add("MCUGroupID", mcuGroupID); //this is to display active mcu group mcu details
                        LoadPageDetails(node);
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
        }

        #endregion

        #region LoadGroupDetailsTable

        protected void LoadGroupDetailsTable(XmlNodeList nodes)
        {
            string strUser = "";
            try
            {
                //lstGroup.Items.Clear();
                strUser = "";
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (!nodes[i].SelectSingleNode("groupID").InnerText.Equals(mcuGroupID.ToString()))
                    {
                       // lstGroup.Items.Add(new ListItem(nodes[i].SelectSingleNode("groupName").InnerText, nodes[i].SelectSingleNode("groupID").InnerText));
                        XmlNodeList subnotes = nodes[i].SelectNodes("mcus/mcu");

                        strUser += BindMembers(subnotes);
                        strUser += "``"; 
                    }
                }

                if (strUser.Length > 0)
                    MCUsStr.Text = strUser.Substring(0, strUser.Length - 2);
               
            }
            catch (Exception ex)
            {
                log.Trace("LoadGroupDetailTable" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }

        #endregion

        #region LoadPageDetails

        protected void LoadPageDetails(XmlNode node)
        {
            try
            {
                XmlNode groupNode = null;
                int pooled = 0, LCR = 0, loadBal = 0;

                groupNode = node.SelectSingleNode("//groups/group/groupName");
                if (groupNode != null)
                    GroupName.Text = groupNode.InnerText.Trim();

                groupNode = node.SelectSingleNode("//groups/group/description");
                if (groupNode != null)
                    GroupDescription.Text = groupNode.InnerText.Trim();

                groupNode = node.SelectSingleNode("//groups/group/pooled");
                if (groupNode != null)
                    Int32.TryParse(groupNode.InnerText.Trim(),out pooled);

                groupNode = node.SelectSingleNode("//groups/group/lcr");
                if (groupNode != null)
                    Int32.TryParse(groupNode.InnerText.Trim(), out LCR);

                groupNode = node.SelectSingleNode("//groups/group/loadBalance");
                if (groupNode != null)
                    Int32.TryParse(groupNode.InnerText.Trim(), out loadBal);

                if (loadBal > 0)
                    ChkLoadBalance.Checked = true;
                else
                    ChkLoadBalance.Checked = false;

                if (pooled > 0)
                    Chkpooled.Checked = true;
                else
                    Chkpooled.Checked = false;

                if (LCR > 0)
                    ChkLCR.Checked = true;
                else
                    ChkLCR.Checked = false;

                XmlNodeList subnotes = node.SelectNodes("mcus/mcu");
                MCUsInfo.Text = BindMembers(subnotes);
            }
            catch (Exception ex)
            {
                log.Trace("LoadPageDetails" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindMembers

        private String BindMembers(XmlNodeList subNodes)
        {
            String uStr = "";
            int sublength = subNodes.Count;
            
            for (int j = 0; j < sublength; j++)
            {
                string mcuID = subNodes[j].SelectSingleNode("mcuID").InnerText;
                string mcuName = subNodes[j].SelectSingleNode("mcuName").InnerText;
                string mcuAdminEmail = subNodes[j].SelectSingleNode("mcuAdminEmail").InnerText;
                string mcuTimezone = subNodes[j].SelectSingleNode("mcuTimezone").InnerText;
                string mcuLoadBalance = subNodes[j].SelectSingleNode("mcuLoadBalance").InnerText;
                string mcuOverflow = subNodes[j].SelectSingleNode("mcuOverflow").InnerText;
                string mcuLeadMCU = subNodes[j].SelectSingleNode("mcuLeadMCU").InnerText;

                uStr += mcuID + "!!" + mcuName + "!!" + mcuAdminEmail + "!!" + mcuTimezone + "!!" + mcuLoadBalance + "!!" + mcuOverflow + "!!" + mcuLeadMCU + "||";
            }

            return uStr;
        }
        #endregion

        #region Submit Button Event Handler

        protected void Managegroup2Submit_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<setMCUGroup>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<group>");
                String grpID = "new";
                
                if (mcuGroupID > 0)
                    grpID = mcuGroupID.ToString();

                inXML.Append("<groupID>" + grpID + "</groupID>");
                inXML.Append("<groupName>" + GroupName.Text + "</groupName>");
                inXML.Append("<description>" + GroupDescription.Text + "</description>");
                String pooled = "0";
                if (Chkpooled.Checked) pooled = "1";
                inXML.Append("<pooled>" + pooled + "</pooled>");
                
                string lcr = "0";
                if (ChkLCR.Checked) lcr = "1";
                inXML.Append("<lcr>" + lcr + "</lcr>");

                string loadbalance = "0";
                if (ChkLoadBalance.Checked) loadbalance = "1";
                inXML.Append("<loadBalance>" + loadbalance + "</loadBalance>");

                Boolean isLeadMCUSelected = false;

                if (MCUsInfo.Text.Trim() != "")
                {
                    String[] delimiterArr = { "||" };
                    String[] delimiterArr1 = { "!!" };
                    String[] mcuInfoSplit = MCUsInfo.Text.Split(delimiterArr, StringSplitOptions.RemoveEmptyEntries);
                    String[] mInfoDetails = null;

                    for (Int32 s = 0; s < mcuInfoSplit.Length; s++)
                    {
                        mInfoDetails = null;
                        mInfoDetails = mcuInfoSplit[s].Split(delimiterArr1, StringSplitOptions.RemoveEmptyEntries);
                        mInfoDetails[0] = mInfoDetails[0].Replace("|", ""); 
                        inXML.Append("<mcu>");
                        inXML.Append("<mcuID>" + mInfoDetails[0] + "</mcuID>");
                        inXML.Append("<mcuAdmin>" + mInfoDetails[1] + "</mcuAdmin>");
                        inXML.Append("<loadBalance>" + mInfoDetails[4] + "</loadBalance>");
                        inXML.Append("<overFlow>" + mInfoDetails[5] + "</overFlow>");
                        if (mInfoDetails[6] == "1")
                            isLeadMCUSelected = true;
                        inXML.Append("<leadMCU>" + mInfoDetails[6] + "</leadMCU>");
                        inXML.Append("</mcu>");
                    }
                }
                inXML.Append("</group>");
                inXML.Append("</setMCUGroup>");

                if (!isLeadMCUSelected)
                {
                    errLabel.Text = obj.GetTranslatedText("Please select the lead MCU.");
                    errLabel.Visible = true;
                    return;
                }

                log.Trace("SetMCUGroup inXML: " + inXML.ToString());
                String outXML = obj.CallMyVRMServer("SetMCUGroup", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetMCUGroup Outxml: " + outXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                    Response.Redirect("ManageMCUGroups.aspx?m=1");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion
    }
}