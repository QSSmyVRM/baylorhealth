SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

BEGIN TRANSACTION

-- Add 1 row to [dbo].[Acc_Balance_D]
--ZD 101905
--INSERT INTO [dbo].[Acc_Balance_D] ([UserID], [TotalTime], [TimeRemaining], [InitTime], [ExpirationTime]) VALUES (11, 100000, 66327, '19000101 00:00:00.000', '19000101 00:00:00.000')
INSERT INTO [dbo].[Acc_Balance_D] ([UserID], [TotalTime], [TimeRemaining], [InitTime], [ExpirationTime]) VALUES (11, 100000, 100000, GETDATE(), GETDATE())

-- Add 3 rows to [dbo].[Acc_Scheme_S]
INSERT INTO [dbo].[Acc_Scheme_S] ([id], [name]) VALUES (0, N'none')
INSERT INTO [dbo].[Acc_Scheme_S] ([id], [name]) VALUES (1, N'Bill to host')
INSERT INTO [dbo].[Acc_Scheme_S] ([id], [name]) VALUES (2, N'Bill to individual')

-- Add 4 rows to [dbo].[Cat_Category_S]
INSERT INTO [dbo].[Cat_Category_S] ([id], [name], [description], [deleted]) VALUES (100, N'Appetizer', NULL, NULL)
INSERT INTO [dbo].[Cat_Category_S] ([id], [name], [description], [deleted]) VALUES (101, N'Snacks', NULL, NULL)
INSERT INTO [dbo].[Cat_Category_S] ([id], [name], [description], [deleted]) VALUES (102, N'Entree', NULL, NULL)
INSERT INTO [dbo].[Cat_Category_S] ([id], [name], [description], [deleted]) VALUES (103, N'Beverage', NULL, 0)

-- Add 3 rows to [dbo].[Conf_CreateType_S]
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (0, N'VRM', N'18')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (1, N'Exchange', N'2000')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (2, N'Lotus Domino', N'6.0')

-- Add 8 rows to [dbo].[Email_Type_S]
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'Conference Create/Modify Notification', N'VRM Conference Setup/Modify Notification', 1)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'Invitation Acceptance Confirmation', N'VRM Invitation Acceptance Confirmation', 2)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'Room Approval Request', N'VRM Conference Room approval request', 4)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'MCU Approval Request', N'VRM Conference MCU approval request', 8)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'System Approval Request', N'VRM Conference System approval request', 16)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'Conference Deletion Notification', N'VRM Conference Deletion Notification', 32)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'Forgotten Password Reminder', N'VRM Forgotten Login Information', 64)
INSERT INTO [dbo].[Email_Type_S] ([emailtype], [emailsubject], [emailid]) VALUES (N'Conference Invitation Reminder', N'VRM Conference Invitation Reminder', 128)

-- Add 107 rows to [dbo].[Err_List_S]
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (200, N'System Error.Please contact your VRM Administrator and supply this error code.', N'System Error.Please contact your VRM Administrator and supply this error code.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (201, N'UserID missing.Operation aborted or UserID not supplied.', N'UserID missing.Operation aborted or UserID not supplied.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (202, N'Please enter a Conference Name.', N'Conference Name missing.Operation aborted.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (203, N'Please enter a valid Date and Time.', N'The value of monthWeekDayNo is invalid.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (205, N'User is not registered in VRM.', N'User does not exist.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (206, N'Please enter a unique login name.', N'Duplicate Logins', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (207, N'Invalid login or password.Please try again.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (208, N'Your account is locked due to multiple invalid password attempts.Please contact your VRM administrator.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (209, N'Please enter a valid e-mail address.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (210, N'Not enough IP or T1 ports.', N'Not enough IP or T1 ports.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (211, N'Not enough Mux T1 ports.', N'Not enough Mux T1 ports.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (212, N'Please enter a unique Conference Name.', N'Duplicate conference name', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (213, N'Not enough T1 ports.', N'Not enough T1 ports.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (214, N'Not enough audio cards.', N'Not enough audio cards.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (215, N'Not enough video cards.', N'Not enough video cards.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (216, N'Complicated error in resource computation.', N'Complicated error in resource computation.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (217, N'Please enter a unique group name.', N'You entered a dup group name, no new group created.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (218, N'Your account balance is insufficient for this conference change. Please contact your VRM Administrator and supply this error code.', N'The remaining money or time available is not enough to change the conference.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (219, N'Please enter First Name, Last Name, or E-mail address.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (220, N'Duplicated User Name, Login, E-mail address. Please check and re-enter.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (221, N'Conference was successfully created. "New User" with duplicate participant e-mail <email address> not invited -- user already in VRM database was invited.', N'', N'I         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (222, N'Please enter a unique template name.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (223, N'Please enter a valid conference start date / time.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (224, N'Super Administrator function only. Please contact VRM Administrator for more information.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (225, N'Please accept your conference invitation before logging in.', N'Only the user accepted the conference at first can login.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (226, N'Insufficient system resources for this particular conference. Please try reducing the number of participants or rooms included in the conference. For more assistance please contact your VRM Administrator and supply this error code.', N'Not enough ip avaliable.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (227, N'Please enter a valid Login or Password.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (228, N'Please check recurring conference settings and re-enter.', N'query recurrent error', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (229, N'You are not permissioned for this function. Please contact your VRM Administrator and supply this error code for assistance.', N'Error, you have no right to use this function.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (230, N'Your account balance is insufficient to set up this conference. Please contact your VRM Administrator and supply this error code.', N'Your account balance is insufficient to set up this conference. Please contact your VRM Administrator and supply this error code.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (231, N'Insufficient account balance to set up a conference. Please contact your VRM Administrator and supply this error code to obtain a usage acccount.', N'The host <name> did not open account before. There is no user tuple in financial table.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (233, N'System Error. Please close and restart your browser, and then login to VRM again to reset the application.', N'Unknown or unrecognized command.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (234, N'There is no Group that matches your search criteria.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (236, N'System Error. Please contact your VRM Administrator and supply this error code.', N'asp connect2 data error.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (237, N'Please enter a unique Bridge Name.', N'', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (238, N'Billing Error. Please contanct your VRM Administrator and supply this error code.', N'No row in accountingRef.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (239, N'System Error. Please try again. If this problem persists, please contact Expedite VCS, Inc.', N'User couldnot be deleted.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (240, N'Insufficient system resources for this particular conference. No usable ISDN phone number was found for the bridge. For more assistance please contact your VRM Administrator and supply this error code.', N'Either ISDN list is empty or the list is completely in use.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (241, N'One or more selected rooms have already been booked.', N'Room conflict occurred', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (242, N'The requested conference has
either passed or does not exist', N'A message shown for email response when
the conference is not found', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (243, N'The requested conference has  been deleted by the host or administrator.', N'Conference has previuosuly been deleted ', N'I         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (244, N'Point to Point conferences have been disabled by the administrator. Please choose from one of the other sections  to setup a conference.', N'P2P confs are disabled.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (250, N'Invalid Security Key or Time Expired.Please contact your VRM Administrator for further assistance.', N'Security key is wrong.', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (252, N'Your VRM License doesn''t permit this operation', N'Your VRM License doesn''t permit this operation', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (253, N'VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again.', N'VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again.', N'C         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (254, N'Your account has been locked. Please contact your VRM administrator for assistance.', N'VRM account locked manually', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (255, N'The requested conference is pending approval. Please contact your VRM Administrator for further assistance', N'Conference has been modified and has gone to pending mode.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (256, N'The requested  name  has already been taken. Please choose another name.', N'Duplicate room name for this location', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (257, N'The requested building  name for this city  has already been taken. Please choose another name for this building', N'Duplicate building  name for this city', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (258, N'The requested city name  has already been taken. Please choose another name.', N'Duplicate city name', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (259, N'The requested party is already present in the conference', N'The requested party is already present in the conference', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (260, N'The requested bridgename has already been assigned to another bridge', N'Duplicate bridge name', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (261, N'The requested username/email is already in use', N'Duplicate user', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (262, N'One or more selected rooms have no endpoint associated.', N'Endpointless room scheduled', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (263, N'Your VRM account has expired and has been locked. Please contact you local VRM administrator for further assistance', N'Your VRM account has expired and has been locked. Please contact you local VRM administrator for further assistance', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (264, N'Your VRM account is about to expire on %s. Please contact your VRM Administrator for further assistance', N'VRM Account Expiration warning" 

', N'M         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (301, N'System Error. Please contact your VRM Administrator and supply this error code.', N'DB or DB connection error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (302, N'Invalid Security Key or Time Expired.', N'', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (305, N'Unable to execute sql . Error in text.', N'Unable to execute sql . Error in text.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (306, N'Unable to read DB.', N'Unable to read DB.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (307, N'Invalid account expiration date. Please check the license.
', N'Account expiry date > license date
', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (308, N'Conference cannot be deleted. It is either expired or has been terminated.', N'Past conference', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (309, N'User account info missing or corrupted in database.', N'User account info missing or corrupted in database.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (310, N'Error in user operation', N'Query user info error.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (311, N'Error occurred, roomid error in confuser for user <name>', N'Error occurred, roomid error in confuser for user <name>', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (312, N'Time Zone data error1 of the certain conference.', N'Time Zone data error1 of the certain conference.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (313, N'Time Zone table error', N'Time Zone table error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (314, N'Query error<database query>', N'Query error<database query>', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (315, N'Replace User error', N'Replace User error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (316, N'Unable to query [user].', N'Unable to query [user].', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (317, N'Unable to query Bounded Operator.', N'Unable to query Bounded Operator.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (318, N'select foodid,foodname from menufood', N'select foodid,foodname from menufood', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (319, N'FetchGroupList', N'FetchGroupList', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (320, N'query count from GroupParticipant', N'query count from GroupParticipant', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (321, N'query from GroupParticipant , [User]', N'query from GroupParticipant , [User]', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (322, N'insert confgroup<stmt>', N'insert confgroup<stmt>', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (323, N'Unable insert confgroup', N'Unable insert confgroup', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (325, N'delete error.', N'delete error.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (326, N'Unable to insert Confroom.', N'Unable to insert Confroom.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (327, N'Unable to insert other locations into Confroom.', N'Unable to insert other locations into Confroom.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (328, N'Unable to query [ConferenceRoom].', N'Unable to query [ConferenceRoom].', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (330, N'FetchLocations', N'FetchLocations', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (331, N'select count(*) from confuser where confid.', N'select count(*) from confuser where confid.', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (334, N'select operator error', N'select operator error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (400, N'XML Validation Error', N'XML Validation Error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (401, N'System Error. Please contact your VRM Administrator and supply this error code.', N'Error loading XML Schema', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (402, N'Maximum Groups Exceeded.', N'Tried to add more than MaxGoupsPerUser in license.xml', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (403, N'Maximum Templates Exceeded.', N'Tried to add more than MaxTemplatesPerUser in license.xml', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (404, N'Error reading License.', N'Get License could not retrieve license Xml', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (405, N'VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again.', N'Same as Error 253 with level ->', N'Z         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (406, N'Dept is in use. Cannot delete!', N'User or Room still attached ', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (407, N'License check failed. Please contact your local administrator or myVRM for further assistance.  ', N'CriticalError License check failed', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (408, N'Invalid date range selected', N'Error in recurring conf', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (409, N'AES error in encryption/decryption ', N'AES Error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (410, N'Conference creation error. Split room can only be NON VIDEO!', N'Conference Error', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (411, N'Cannot delete MCU. Remove all attached resources and try again.', N'MCU delete eroor', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (412, N'Duplicate department name. Cannot create/edit!', N'Department Add/Edit error', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (413, N'Endpoint name already exists. Please choose another name.', N'Duplicate Endpoint name', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (414, N'Link FAILED. Transaction rolled back! ', N'Link failed, transaction rollback', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (415, N'Duplicate Group Name.', N'Group Name', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (416, N'Command is not supported.', N'no support', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (417, N'Tier is in use and cannot be deleted.', N'Tier in use', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (418, N'Error End Point must be assigned a bridge', N'Unable to add End Point', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (419, N'Conference has ended.', N'End Conference', N'E         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (420, N'Invalid category type', N'work order ', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (421, N'Record not found', N'generic', N'S         ')
INSERT INTO [dbo].[Err_List_S] ([ID], [Message], [Description], [Level]) VALUES (422, N'Invalid Operation', N'Selected operation invalid', N'S         ')

-- Add 4 rows to [dbo].[Err_LogPrefs_S]
INSERT INTO [dbo].[Err_LogPrefs_S] ([modulename], [loglevel], [loglife], [LogModule]) VALUES (N'Lotus Plugin', 0, 0, N'Lotus')
INSERT INTO [dbo].[Err_LogPrefs_S] ([modulename], [loglevel], [loglife], [LogModule]) VALUES (N'Outlook Plugin', 0, 0, N'Outlook')
INSERT INTO [dbo].[Err_LogPrefs_S] ([modulename], [loglevel], [loglife], [LogModule]) VALUES (N'Services', 0, 0, N'RTC')
INSERT INTO [dbo].[Err_LogPrefs_S] ([modulename], [loglevel], [loglife], [LogModule]) VALUES (N'Website', 0, 0, N'COM')

-- Add 5 rows to [dbo].[Gen_AddressType_S]
INSERT INTO [dbo].[Gen_AddressType_S] ([id], [name]) VALUES (1, N'IP Address')
INSERT INTO [dbo].[Gen_AddressType_S] ([id], [name]) VALUES (2, N'H323 ID')
INSERT INTO [dbo].[Gen_AddressType_S] ([id], [name]) VALUES (3, N'E.164')
INSERT INTO [dbo].[Gen_AddressType_S] ([id], [name]) VALUES (4, N'ISDN Phone number')
INSERT INTO [dbo].[Gen_AddressType_S] ([id], [name]) VALUES (5, N'MPI')

-- Add 11 rows to [dbo].[Gen_AlertType_S]
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (1, N'Conference setup failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (2, N'Add Endpoint operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (3, N'Edit Endpoint operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (4, N'Delete Endpoint operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (5, N'Conference display layout operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (6, N'Conference extend end time operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (7, N'Conference could not be terminated on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (8, N'Endpoint display layout could not be changed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (9, N'Endpoint mute operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (10, N'Endpoint redial operation failed on mcu.')
INSERT INTO [dbo].[Gen_AlertType_S] ([AlertTypeID], [Description]) VALUES (11, N'')

-- Add 6 rows to [dbo].[Gen_AudioAlg_S]
INSERT INTO [dbo].[Gen_AudioAlg_S] ([AudioID], [AudioType]) VALUES (0, N'Auto')
INSERT INTO [dbo].[Gen_AudioAlg_S] ([AudioID], [AudioType]) VALUES (1, N'16 [G728/Siren7]')
INSERT INTO [dbo].[Gen_AudioAlg_S] ([AudioID], [AudioType]) VALUES (2, N'24 [G722/Siren7]')
INSERT INTO [dbo].[Gen_AudioAlg_S] ([AudioID], [AudioType]) VALUES (3, N'32 [G722/Siren7]')
INSERT INTO [dbo].[Gen_AudioAlg_S] ([AudioID], [AudioType]) VALUES (4, N'56 [G722/G711]')
INSERT INTO [dbo].[Gen_AudioAlg_S] ([AudioID], [AudioType]) VALUES (5, N'56 [G711]')

-- Add 3 rows to [dbo].[Gen_ConfVideoProtocol_S]
INSERT INTO [dbo].[Gen_ConfVideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (1, N'IP')
INSERT INTO [dbo].[Gen_ConfVideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (2, N'IP-ISDN')
INSERT INTO [dbo].[Gen_ConfVideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (3, N'ID-ISDN-SIP')

-- Add 3 rows to [dbo].[Gen_DialingOption_S]
SET IDENTITY_INSERT [dbo].[Gen_DialingOption_S] ON
INSERT INTO [dbo].[Gen_DialingOption_S] ([ID], [DialingOption]) VALUES (1, N'Dial-in to MCU')
INSERT INTO [dbo].[Gen_DialingOption_S] ([ID], [DialingOption]) VALUES (2, N'Dial-out from MCU')
INSERT INTO [dbo].[Gen_DialingOption_S] ([ID], [DialingOption]) VALUES (3, N'Direct to MCU')
SET IDENTITY_INSERT [dbo].[Gen_DialingOption_S] OFF

-- Add 2 rows to [dbo].[Gen_DualStreamMode_S]
INSERT INTO [dbo].[Gen_DualStreamMode_S] ([ID], [DualStreamModeType]) VALUES (0, N'None      ')
INSERT INTO [dbo].[Gen_DualStreamMode_S] ([ID], [DualStreamModeType]) VALUES (1, N'H.239     ')

-- Add 3 rows to [dbo].[Gen_InterfaceType_S]
INSERT INTO [dbo].[Gen_InterfaceType_S] ([ID], [Interface]) VALUES (1, N'IP')
INSERT INTO [dbo].[Gen_InterfaceType_S] ([ID], [Interface]) VALUES (2, N'IP,ISDN')
INSERT INTO [dbo].[Gen_InterfaceType_S] ([ID], [Interface]) VALUES (3, N'IP,ISDN,SIP')

-- Add 5 rows to [dbo].[Gen_Language_S]
INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country]) VALUES (1, N'English', N'US')
INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country]) VALUES (2, N'English', N'UK')
INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country]) VALUES (3, N'Spanish', N'US')
INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country]) VALUES (4, N'Mandarin', N'China')
INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country]) VALUES (5, N'French', N'France')

-- Add 11 rows to [dbo].[Gen_LineRate_S]
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (56, N'56 Kbps / 1 channel')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (64, N'64 Kbps / 1 channel')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (128, N'128 Kbps / 2 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (256, N'256 Kbps / 4 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (384, N'384 Kbps / 6 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (512, N'512 Kbps / 8 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (768, N'768 Kbps / 12 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (1024, N'1.00 Mbps / 18 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (1472, N'1.472 Mbps / 23 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (1500, N'1.5 Mbps / 24 channels')
INSERT INTO [dbo].[Gen_LineRate_S] ([LineRateID], [LineRateType]) VALUES (2048, N'2.0 Mbps / 30 channels')

-- Add 3 rows to [dbo].[Gen_MediaType_S]
INSERT INTO [dbo].[Gen_MediaType_S] ([ID], [MediaType]) VALUES (1, N'None')
INSERT INTO [dbo].[Gen_MediaType_S] ([ID], [MediaType]) VALUES (2, N'Audio-only')
INSERT INTO [dbo].[Gen_MediaType_S] ([ID], [MediaType]) VALUES (3, N'Audio,Video')

-- Add 74 rows to [dbo].[Gen_TimeZone_S]
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (1, N'Afghanistan Standard Time', -4.5, N'Kabul', N'(GMT+04:30)', -270, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (2, N'Alaskan Standard Time', 9, N'Alaska', N'(GMT-09:00)', 540, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 1, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070401 03:00:00.000', '20071028 02:00:00.000', '20080406 03:00:00.000', '20081026 02:00:00.000', '20090405 03:00:00.000', '20091025 02:00:00.000', '20100404 03:00:00.000', '20101031 02:00:00.000', '20110403 03:00:00.000', '20111030 02:00:00.000', '20120401 03:00:00.000', '20121028 02:00:00.000', '20130407 03:00:00.000', '20131027 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (3, N'Arab Standard Time', -3, N'Kuwai', N'(GMT+03:00)', -180, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (4, N'Arabian Standard Time', -4, N'Abu', N'(GMT+04:00)', -240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (5, N'Arabic Standard Time', -3, N'Baghdad', N'(GMT+03:00)', -180, 0, -60, 0, 10, 0, 1, 4, 0, 0, 0, 0, 4, 0, 1, 3, 0, 0, 0, 0, 1, '20040404 04:00:00.000', '20041003 05:00:00.000', '20050403 04:00:00.000', '20051002 05:00:00.000', '20060402 04:00:00.000', '20061001 05:00:00.000', '20070401 04:00:00.000', '20071007 05:00:00.000', '20080406 04:00:00.000', '20081005 05:00:00.000', '20090405 04:00:00.000', '20091004 05:00:00.000', '20100404 04:00:00.000', '20101003 05:00:00.000', '20110403 04:00:00.000', '20111002 05:00:00.000', '20120401 04:00:00.000', '20121007 05:00:00.000', '20130407 04:00:00.000', '20131006 05:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (6, N'Atlantic Standard Time', 4, N'Atlantic', N'(GMT-04:00)', 240, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 0, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070401 03:00:00.000', '20071028 02:00:00.000', '20080406 03:00:00.000', '20081026 02:00:00.000', '20090405 03:00:00.000', '20091025 02:00:00.000', '20100404 03:00:00.000', '20101031 02:00:00.000', '20110403 03:00:00.000', '20111030 02:00:00.000', '20120401 03:00:00.000', '20121028 02:00:00.000', '20130407 03:00:00.000', '20131027 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (7, N'AUS Central Standard Time', -9.5, N'Darwin', N'(GMT+09:30)', -570, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (8, N'AUS Eastern Standard Time', -10, N'Sydney', N'(GMT+10:00)', -600, 0, -60, 0, 3, 0, 5, 2, 0, 0, 0, 0, 10, 0, 5, 2, 0, 0, 0, 0, 1, '20041031 02:00:00.000', '20050327 03:00:00.000', '20051030 02:00:00.000', '20060326 03:00:00.000', '20061029 02:00:00.000', '20070325 03:00:00.000', '20071028 02:00:00.000', '20080330 03:00:00.000', '20081026 02:00:00.000', '20090329 03:00:00.000', '20091025 02:00:00.000', '20100328 03:00:00.000', '20101031 02:00:00.000', '20110327 03:00:00.000', '20111030 02:00:00.000', '20120325 03:00:00.000', '20121028 02:00:00.000', '20130331 03:00:00.000', '20131027 02:00:00.000', '20140330 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (9, N'Azores Standard Time', 1, N'Azores', N'(GMT-01:00)', 60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (10, N'Canada Central Standard Time', 6, N'Saskatchewan', N'(GMT-06:00)', 360, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (11, N'Cape Verde Standard Time', 1, N'Cape', N'(GMT-01:00)', 60, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (12, N'Caucasus Standard Time', -4, N'Baku', N'(GMT+04:00)', -240, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (13, N'Cen. Australia Standard Time', -9.5, N'Adelaide', N'(GMT+09:30)', -570, 0, -60, 0, 3, 0, 5, 2, 0, 0, 0, 0, 10, 0, 5, 2, 0, 0, 0, 0, 1, '20041031 02:00:00.000', '20050327 03:00:00.000', '20051030 02:00:00.000', '20060326 03:00:00.000', '20061029 02:00:00.000', '20070325 03:00:00.000', '20071028 02:00:00.000', '20080330 03:00:00.000', '20081026 02:00:00.000', '20090329 03:00:00.000', '20091025 02:00:00.000', '20100328 03:00:00.000', '20101031 02:00:00.000', '20110327 03:00:00.000', '20111030 02:00:00.000', '20120325 03:00:00.000', '20121028 02:00:00.000', '20130331 03:00:00.000', '20131027 02:00:00.000', '20140330 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (14, N'Central America Standard Time', 6, N'Central', N'(GMT-06:00)', 360, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (15, N'Central Asia Standard Time', -6, N'Astana', N'(GMT+06:00)', -360, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (16, N'Central Europe Standard Time', -1, N'Belgrade', N'(GMT+01:00)', -60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (17, N'Central European Standard Time', -1, N'Sarajevo', N'(GMT+01:00)', -60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (18, N'Central Pacific Standard Time', -11, N'Magadan', N'(GMT+11:00)', -660, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (19, N'Central Standard Time', 5, N'Central Time', N'(GMT-06:00)', 360, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 1, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070311 02:00:00.000', '20071104 02:00:00.000', '20080309 02:00:00.000', '20081102 02:00:00.000', '20090308 02:00:00.000', '20091108 02:00:00.000', '20100314 02:00:00.000', '20101107 02:00:00.000', '20110313 02:00:00.000', '20111106 02:00:00.000', '20120311 02:00:00.000', '20121104 02:00:00.000', '20130310 02:00:00.000', '20131103 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (20, N'China Standard Time', -8, N'Beijing', N'(GMT+08:00)', -480, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (21, N'Dateline Standard Time', 12, N'Eniwetok', N'(GMT-12:00)', 720, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (22, N'E. Africa Standard Time', -3, N'Nairobi', N'(GMT+03:00)', -180, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (23, N'E. Australia Standard Time', -10, N'Brisbane', N'(GMT+10:00)', -600, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (24, N'E. Europe Standard Time', -2, N'Bucharest', N'(GMT+02:00)', -120, 0, -60, 0, 9, 0, 5, 1, 0, 0, 0, 0, 3, 0, 5, 0, 0, 0, 0, 0, 1, '20040328 00:00:00.000', '20041031 01:00:00.000', '20050327 00:00:00.000', '20051030 01:00:00.000', '20060326 00:00:00.000', '20061029 01:00:00.000', '20070325 00:00:00.000', '20071028 01:00:00.000', '20080330 00:00:00.000', '20081026 01:00:00.000', '20090329 00:00:00.000', '20091025 01:00:00.000', '20100328 00:00:00.000', '20101031 01:00:00.000', '20110327 00:00:00.000', '20111030 01:00:00.000', '20120325 00:00:00.000', '20121028 01:00:00.000', '20130331 00:00:00.000', '20131027 01:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (25, N'E. South America Standard Time', 3, N'Brasilia', N'(GMT-03:00)', 180, 0, -60, 0, 2, 0, 2, 2, 0, 0, 0, 0, 10, 0, 3, 2, 0, 0, 0, 0, 1, '20041017 03:00:00.000', '20050213 02:00:00.000', '20051016 03:00:00.000', '20060212 02:00:00.000', '20061015 03:00:00.000', '20070211 02:00:00.000', '20071014 03:00:00.000', '20080210 02:00:00.000', '20081019 03:00:00.000', '20090208 02:00:00.000', '20091018 03:00:00.000', '20100207 02:00:00.000', '20101017 03:00:00.000', '20110213 02:00:00.000', '20111016 03:00:00.000', '20120212 02:00:00.000', '20121014 03:00:00.000', '20130210 02:00:00.000', '20131020 03:00:00.000', '20140209 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (26, N'Eastern Standard Time', 4, N'Eastern Time ', N'(GMT-05:00)', 300, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 1, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070311 02:00:00.000', '20071104 02:00:00.000', '20080309 02:00:00.000', '20081102 02:00:00.000', '20090308 02:00:00.000', '20091108 02:00:00.000', '20100314 02:00:00.000', '20101107 02:00:00.000', '20110313 02:00:00.000', '20111106 02:00:00.000', '20120311 02:00:00.000', '20121104 02:00:00.000', '20130310 02:00:00.000', '20131103 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (27, N'Egypt Standard Time', -2, N'Cairo', N'(GMT+02:00)', -120, 0, -60, 0, 9, 3, 5, 2, 0, 0, 0, 0, 5, 5, 1, 2, 0, 0, 0, 0, 1, '20040507 03:00:00.000', '20040929 03:00:00.000', '20050506 03:00:00.000', '20050928 03:00:00.000', '20060505 03:00:00.000', '20060927 03:00:00.000', '20070504 03:00:00.000', '20070926 03:00:00.000', '20080502 03:00:00.000', '20081001 03:00:00.000', '20090501 03:00:00.000', '20090930 03:00:00.000', '20100507 03:00:00.000', '20100929 03:00:00.000', '20110506 03:00:00.000', '20110928 03:00:00.000', '20120504 03:00:00.000', '20120926 03:00:00.000', '20130503 03:00:00.000', '20130925 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (28, N'Ekaterinburg Standard Time', -5, N'Ekaterinburg', N'(GMT+05:00)', -300, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (29, N'Fiji Standard Time', -12, N'Fiji', N'(GMT+12:00)', -720, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (30, N'FLE Standard Time', -2, N'Helsinki', N'(GMT+02:00)', -120, 0, -60, 0, 10, 0, 5, 4, 0, 0, 0, 0, 3, 0, 5, 3, 0, 0, 0, 0, 1, '20040328 03:00:00.000', '20041031 04:00:00.000', '20050327 03:00:00.000', '20051030 04:00:00.000', '20060326 03:00:00.000', '20061029 04:00:00.000', '20070325 03:00:00.000', '20071028 04:00:00.000', '20080330 03:00:00.000', '20081026 04:00:00.000', '20090329 03:00:00.000', '20091025 04:00:00.000', '20100328 03:00:00.000', '20101031 04:00:00.000', '20110327 03:00:00.000', '20111030 04:00:00.000', '20120325 03:00:00.000', '20121028 04:00:00.000', '20130331 03:00:00.000', '20131027 04:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (31, N'GMT Standard Time', 0, N'Dublin', N'(GMT)', 0, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 3, 0, 5, 1, 0, 0, 0, 0, 1, '20040328 01:00:00.000', '20041031 02:00:00.000', '20050327 01:00:00.000', '20051030 02:00:00.000', '20060326 01:00:00.000', '20061029 02:00:00.000', '20070325 01:00:00.000', '20071028 02:00:00.000', '20080330 01:00:00.000', '20081026 02:00:00.000', '20090329 01:00:00.000', '20091025 02:00:00.000', '20100328 01:00:00.000', '20101031 02:00:00.000', '20110327 01:00:00.000', '20111030 02:00:00.000', '20120325 01:00:00.000', '20121028 02:00:00.000', '20130331 01:00:00.000', '20131027 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (32, N'Greenland Standard Time', 3, N'Greenland', N'(GMT-03:00)', 180, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 0, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070401 03:00:00.000', '20071028 02:00:00.000', '20080406 03:00:00.000', '20081026 02:00:00.000', '20090405 03:00:00.000', '20091025 02:00:00.000', '20100404 03:00:00.000', '20101031 02:00:00.000', '20110403 03:00:00.000', '20111030 02:00:00.000', '20120401 03:00:00.000', '20121028 02:00:00.000', '20130407 03:00:00.000', '20131027 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (33, N'Greenwich Standard Time', 0, N'Casablanca', N'(GMT)', 0, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (34, N'GTB Standard Time', -2, N'Athens', N'(GMT+02:00)', -120, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (35, N'Hawaiian Standard Time', 10, N'Hawaii', N'(GMT-10:00)', 600, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (36, N'India Standard Time', -5.5, N'New', N'(GMT+05:30)', -330, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (37, N'Iran Standard Time', -3.5, N'Tehran', N'(GMT+03:30)', -210, 0, -60, 0, 9, 2, 4, 2, 0, 0, 0, 0, 3, 0, 1, 2, 0, 0, 0, 0, 1, '20040307 02:00:00.000', '20040921 03:00:00.000', '20050306 02:00:00.000', '20050927 03:00:00.000', '20060305 02:00:00.000', '20060926 03:00:00.000', '20070304 02:00:00.000', '20070925 03:00:00.000', '20080302 02:00:00.000', '20080923 03:00:00.000', '20090301 02:00:00.000', '20090922 03:00:00.000', '20100307 02:00:00.000', '20100921 03:00:00.000', '20110306 02:00:00.000', '20110927 03:00:00.000', '20120304 02:00:00.000', '20120925 03:00:00.000', '20130303 02:00:00.000', '20130924 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (38, N'Israel Standard Time', -2, N'Jerusalem', N'(GMT+02:00)', -120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (39, N'Korea Standard Time', -9, N'Seoul', N'(GMT+09:00)', -540, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (40, N'Mexico Standard Time', 6, N'Mexico', N'(GMT-06:00)', 360, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 0, 1, '20040502 03:00:00.000', '20040926 03:00:00.000', '20050501 03:00:00.000', '20050925 03:00:00.000', '20060507 03:00:00.000', '20061001 03:00:00.000', '20070506 03:00:00.000', '20070930 03:00:00.000', '20080504 03:00:00.000', '20080928 03:00:00.000', '20090503 03:00:00.000', '20090927 03:00:00.000', '20100502 03:00:00.000', '20100926 03:00:00.000', '20110501 03:00:00.000', '20110925 03:00:00.000', '20120506 03:00:00.000', '20120930 03:00:00.000', '20130505 03:00:00.000', '20130929 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (41, N'Mid-Atlantic Standard Time', 2, N'Mid-Atlantic', N'(GMT-02:00)', 120, 0, -60, 0, 9, 0, 5, 2, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20040926 03:00:00.000', '20050327 02:00:00.000', '20050925 03:00:00.000', '20060326 02:00:00.000', '20061001 03:00:00.000', '20070325 02:00:00.000', '20070930 03:00:00.000', '20080330 02:00:00.000', '20080928 03:00:00.000', '20090329 02:00:00.000', '20090927 03:00:00.000', '20100328 02:00:00.000', '20100926 03:00:00.000', '20110327 02:00:00.000', '20110925 03:00:00.000', '20120325 02:00:00.000', '20120930 03:00:00.000', '20130331 02:00:00.000', '20130929 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (42, N'Mountain Standard Time', 6, N'Mountain Time', N'(GMT-07:00)', 420, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 1, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070311 02:00:00.000', '20071104 02:00:00.000', '20080309 02:00:00.000', '20081102 02:00:00.000', '20090308 02:00:00.000', '20091108 02:00:00.000', '20100314 02:00:00.000', '20101107 02:00:00.000', '20110313 02:00:00.000', '20111106 02:00:00.000', '20120311 02:00:00.000', '20121104 02:00:00.000', '20130310 02:00:00.000', '20131103 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (43, N'Myanmar Standard Time', -6.5, N'Rangoon', N'(GMT+06:30)', -390, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (44, N'N. Central Asia Standard Time', -6, N'Almaty', N'(GMT+06:00)', -360, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (45, N'Nepal Standard Time', -5.75, N'Kathmandu', N'(GMT+05:45)', -345, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (46, N'New Zealand Standard Time', -12, N'Auckland', N'(GMT+12:00)', -720, 0, -60, 0, 3, 0, 3, 2, 0, 0, 0, 0, 10, 0, 1, 2, 0, 0, 0, 0, 1, '20041003 03:00:00.000', '20050320 02:00:00.000', '20051002 03:00:00.000', '20060319 02:00:00.000', '20061001 03:00:00.000', '20070318 02:00:00.000', '20071007 03:00:00.000', '20080316 02:00:00.000', '20081005 03:00:00.000', '20090315 02:00:00.000', '20091004 03:00:00.000', '20100314 02:00:00.000', '20101003 03:00:00.000', '20110320 02:00:00.000', '20111002 03:00:00.000', '20120318 02:00:00.000', '20121007 03:00:00.000', '20130317 02:00:00.000', '20131006 03:00:00.000', '20140316 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (47, N'Newfoundland Standard Time', 3.5, N'Newfoundland', N'(GMT-03:30)', 210, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 0, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070401 03:00:00.000', '20071028 02:00:00.000', '20080406 03:00:00.000', '20081026 02:00:00.000', '20090405 03:00:00.000', '20091025 02:00:00.000', '20100404 03:00:00.000', '20101031 02:00:00.000', '20110403 03:00:00.000', '20111030 02:00:00.000', '20120401 03:00:00.000', '20121028 02:00:00.000', '20130407 03:00:00.000', '20131027 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (48, N'North Asia East Standard Time', -8, N'Irkutsk', N'(GMT+08:00)', -480, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (49, N'North Asia Standard Time', -7, N'Krasnoyarsk', N'(GMT+07:00)', -420, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (50, N'Pacific SA Standard Time', 4, N'Santiago', N'(GMT-04:00)', 240, 0, -60, 0, 3, 6, 2, 0, 0, 0, 0, 0, 10, 6, 2, 0, 0, 0, 0, 0, 1, '20041009 01:00:00.000', '20050312 00:00:00.000', '20051008 01:00:00.000', '20060311 00:00:00.000', '20061007 01:00:00.000', '20070310 00:00:00.000', '20071013 01:00:00.000', '20080308 00:00:00.000', '20081011 01:00:00.000', '20090307 00:00:00.000', '20091010 01:00:00.000', '20100313 00:00:00.000', '20101009 01:00:00.000', '20110312 00:00:00.000', '20111008 01:00:00.000', '20120310 00:00:00.000', '20121013 01:00:00.000', '20130309 00:00:00.000', '20131012 01:00:00.000', '20140308 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (51, N'Pacific Standard Time', 7, N'Pacific Time', N'(GMT-08:00)', 480, 0, -60, 0, 10, 0, 5, 2, 0, 0, 0, 0, 4, 0, 1, 2, 0, 0, 0, 1, 1, '20040404 03:00:00.000', '20041031 02:00:00.000', '20050403 03:00:00.000', '20051030 02:00:00.000', '20060402 03:00:00.000', '20061029 02:00:00.000', '20070311 02:00:00.000', '20071104 02:00:00.000', '20080309 02:00:00.000', '20081102 02:00:00.000', '20090308 02:00:00.000', '20091108 02:00:00.000', '20100314 02:00:00.000', '20101107 02:00:00.000', '20110313 02:00:00.000', '20111106 02:00:00.000', '20120311 02:00:00.000', '20121104 02:00:00.000', '20130310 02:00:00.000', '20131103 02:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (52, N'Romance Standard Time', -1, N'Brussels', N'(GMT+01:00)', -60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (53, N'Russian Standard Time', -3, N'Moscow', N'(GMT+03:00)', -180, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (54, N'SA Eastern Standard Time', 3, N'Buenos', N'(GMT-03:00)', 180, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (55, N'SA Pacific Standard Time', 5, N'Bogota', N'(GMT-05:00)', 300, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (56, N'SA Western Standard Time', 4, N'Caracas', N'(GMT-04:00)', 240, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (57, N'Samoa Standard Time', 11, N'Samoa', N'(GMT-11:00)', 660, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (58, N'SE Asia Standard Time', -7, N'Jakarta', N'(GMT+07:00)', -420, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (59, N'Singapore Standard Time', -8, N'Singapore', N'(GMT+08:00)', -480, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (60, N'South Africa Standard Time', -2, N'Pretoria', N'(GMT+02:00)', -120, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (61, N'Sri Lanka Standard Time', -6, N'Sri', N'(GMT+06:00)', -360, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (62, N'Taipei Standard Time', -8, N'Taipei', N'(GMT+08:00)', -480, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (63, N'Tasmania Standard Time', -10, N'Hobart', N'(GMT+10:00)', -600, 0, -60, 0, 3, 0, 5, 2, 0, 0, 0, 0, 10, 0, 1, 2, 0, 0, 0, 0, 1, '20041003 03:00:00.000', '20050327 03:00:00.000', '20051002 03:00:00.000', '20060326 03:00:00.000', '20061001 03:00:00.000', '20070325 03:00:00.000', '20071007 03:00:00.000', '20080330 03:00:00.000', '20081005 03:00:00.000', '20090329 03:00:00.000', '20091004 03:00:00.000', '20100328 03:00:00.000', '20101003 03:00:00.000', '20110327 03:00:00.000', '20111002 03:00:00.000', '20120325 03:00:00.000', '20121007 03:00:00.000', '20130331 03:00:00.000', '20131006 03:00:00.000', '20140330 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (64, N'Tokyo Standard Time', -9, N'Tokyo', N'(GMT+09:00)', -540, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (65, N'Tonga Standard Time', -13, N'Nuku alofa', N'(GMT+13:00)', -780, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (66, N'US Eastern Standard Time', 5, N'Indiana', N'(GMT-05:00)', 300, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (67, N'US Mountain Standard Time', 7, N'Mountain - Arizona', N'(GMT-07:00)', 420, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (68, N'Vladivostok Standard Time', -10, N'Vladivostok', N'(GMT+10:00)', -600, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (69, N'W. Australia Standard Time', -8, N'Perth', N'(GMT+08:00)', -480, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (70, N'W. Central Africa Standard Time', -1, N'West', N'(GMT+01:00)', -60, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (71, N'W. Europe Standard Time', -1, N'Amsterdam', N'(GMT+01:00)', -60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (72, N'West Asia Standard Time', -5, N'Karachi', N'(GMT+05:00)', -300, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (73, N'West Pacific Standard Time', -10, N'Guam', N'(GMT+10:00)', -600, 0, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000', '19800101 00:00:00.000')
INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (74, N'Yakutsk Standard Time', -9, N'Yakutsk', N'(GMT+09:00)', -540, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')

-- Add 24 rows to [dbo].[Gen_VideoEquipment_S]
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (1, N'Polycom VSX 8000 series')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (2, N'Polycom VSX 7000 series')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (3, N'Polycom VSX 5000')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (4, N'Polycom VSX 3000')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (5, N'Polycom V 500')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (6, N'Polycom PVX')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (7, N'Tandberg 8000 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (8, N'Tandberg 7000 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (9, N'Tandberg 6000 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (10, N'Tandberg 3000 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (11, N'Tandberg 2000 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (12, N'Tandberg 1500 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (13, N'Tandberg 1000 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (14, N'Tandberg 990 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (15, N'Tandberg 880 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (16, N'Tandberg 770 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (17, N'Tandberg 550 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (18, N'Tandberg 150 MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (19, N'Tandberg Director MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (20, N'Tandberg Maestro MXP')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (21, N'Codian IP VCR 2210')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (22, N'Codian IP VCR 2220')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (23, N'Codian IP VCR 2240')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (24, N'Other...')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (25, N'Polycom HDX 4000')
INSERT INTO [dbo].[Gen_VideoEquipment_S] ([VEid], [VEname]) VALUES (26, N'Polycom HDX 9000')

-- Add 4 rows to [dbo].[Gen_VideoLayout_S]
INSERT INTO [dbo].[Gen_VideoLayout_S] ([ID], [VideoLayout]) VALUES (1, N'1x1')
INSERT INTO [dbo].[Gen_VideoLayout_S] ([ID], [VideoLayout]) VALUES (2, N'1x2')
INSERT INTO [dbo].[Gen_VideoLayout_S] ([ID], [VideoLayout]) VALUES (3, N'1x1')
INSERT INTO [dbo].[Gen_VideoLayout_S] ([ID], [VideoLayout]) VALUES (4, N'2x2')

-- Add 3 rows to [dbo].[Gen_VideoMode_S]
INSERT INTO [dbo].[Gen_VideoMode_S] ([id], [modeName]) VALUES (1, N'Switched Video')
INSERT INTO [dbo].[Gen_VideoMode_S] ([id], [modeName]) VALUES (2, N'Transcoding')
INSERT INTO [dbo].[Gen_VideoMode_S] ([id], [modeName]) VALUES (3, N'Continuous Presence')

-- Add 4 rows to [dbo].[Gen_VideoProtocol_S]
INSERT INTO [dbo].[Gen_VideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (1, N'IP')
INSERT INTO [dbo].[Gen_VideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (2, N'ISDN')
INSERT INTO [dbo].[Gen_VideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (3, N'SIP')
INSERT INTO [dbo].[Gen_VideoProtocol_S] ([VideoProtocolID], [VideoProtocolType]) VALUES (4, N'MPI')

-- Add 4 rows to [dbo].[Gen_VideoSession_S]
INSERT INTO [dbo].[Gen_VideoSession_S] ([VideoSessionID], [VideoSessionType]) VALUES (0, N'Auto')
INSERT INTO [dbo].[Gen_VideoSession_S] ([VideoSessionID], [VideoSessionType]) VALUES (1, N'H.261')
INSERT INTO [dbo].[Gen_VideoSession_S] ([VideoSessionID], [VideoSessionType]) VALUES (2, N'H.263')
INSERT INTO [dbo].[Gen_VideoSession_S] ([VideoSessionID], [VideoSessionType]) VALUES (3, N'H.264')

-- Add 5 rows to [dbo].[Inv_DeliveryType_S]
INSERT INTO [dbo].[Inv_DeliveryType_S] ([id], [deliveryType]) VALUES (0, N'Custom')
INSERT INTO [dbo].[Inv_DeliveryType_S] ([id], [deliveryType]) VALUES (1, N'Delivery and Pickup')
INSERT INTO [dbo].[Inv_DeliveryType_S] ([id], [deliveryType]) VALUES (2, N'Delivery Only')
INSERT INTO [dbo].[Inv_DeliveryType_S] ([id], [deliveryType]) VALUES (3, N'Pick up only')
INSERT INTO [dbo].[Inv_DeliveryType_S] ([id], [deliveryType]) VALUES (4, N'Equipment pickup')

-- Add 9 rows to [dbo].[Inv_ItemService_S]
SET IDENTITY_INSERT [dbo].[Inv_ItemService_S] ON
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (1, 2, N'Breakfast')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (2, 2, N'Lunch')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (3, 2, N'Dinner')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (4, 2, N'Snack')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (5, 2, N'Coffee Service')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (6, 2, N'Vegetarian')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (7, 2, N'Kosher')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (8, 2, N'Low-Cal.')
INSERT INTO [dbo].[Inv_ItemService_S] ([id], [type], [name]) VALUES (9, 2, N'Halal')
SET IDENTITY_INSERT [dbo].[Inv_ItemService_S] OFF

-- Add 1 row to [dbo].[Loc_Tier2_D]
SET IDENTITY_INSERT [dbo].[Loc_Tier2_D] ON
INSERT INTO [dbo].[Loc_Tier2_D] ([Id], [Name], [Address], [L3LocationId], [Comment], [disabled]) VALUES (0, N'Phantom', NULL, 0, N'Phantom Room Tier', 1)
SET IDENTITY_INSERT [dbo].[Loc_Tier2_D] OFF

-- Add 1 row to [dbo].[Loc_Tier3_D]
SET IDENTITY_INSERT [dbo].[Loc_Tier3_D] ON
INSERT INTO [dbo].[Loc_Tier3_D] ([Id], [Name], [Address], [disabled]) VALUES (0, N'Tier3', N'', 1)
SET IDENTITY_INSERT [dbo].[Loc_Tier3_D] OFF

-- Add 13 rows to [dbo].[Mcu_CardDetail_S]
SET IDENTITY_INSERT [dbo].[Mcu_CardDetail_S] ON
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (1, N'HDLC', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (2, N'ISDN', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (3, N'ATM', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (4, N'IP', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (5, N'MUX', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (6, N'AUDIO', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (7, N'VIDEO', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (8, N'DATA', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (9, N'ISDN-8', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (10, N'MPI', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (11, N'AUDIO-PLUS', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (12, N'VIDEO-PLUS', NULL)
INSERT INTO [dbo].[Mcu_CardDetail_S] ([id], [CardName], [Description]) VALUES (13, N'MUX-PLUS', NULL)
SET IDENTITY_INSERT [dbo].[Mcu_CardDetail_S] OFF

-- Add 3 rows to [dbo].[Mcu_CardStatus_S]
INSERT INTO [dbo].[Mcu_CardStatus_S] ([id], [status]) VALUES (0, N'Active')
INSERT INTO [dbo].[Mcu_CardStatus_S] ([id], [status]) VALUES (1, N'Maintenance')
INSERT INTO [dbo].[Mcu_CardStatus_S] ([id], [status]) VALUES (2, N'Disabled')

-- Add 11 rows to [dbo].[Mcu_CardType_S]
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (1, N'IP                                                                                                                                                                                                                                                              ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (2, N'T1                                                                                                                                                                                                                                                              ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (3, N'audio                                                                                                                                                                                                                                                           ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (4, N'mux8                                                                                                                                                                                                                                                            ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (5, N'mux16                                                                                                                                                                                                                                                           ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (6, N'Video6                                                                                                                                                                                                                                                          ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (7, N'Video12                                                                                                                                                                                                                                                         ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (8, N'None                                                                                                                                                                                                                                                            ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (9, N'V35                                                                                                                                                                                                                                                             ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (10, N'CPU                                                                                                                                                                                                                                                             ')
INSERT INTO [dbo].[Mcu_CardType_S] ([CardTypeID], [CardTypeName]) VALUES (11, N'Net8                                                                                                                                                                                                                                                            ')

-- Add 3 rows to [dbo].[Mcu_Status_S]
INSERT INTO [dbo].[Mcu_Status_S] ([id], [status]) VALUES (1, N'Active')
INSERT INTO [dbo].[Mcu_Status_S] ([id], [status]) VALUES (2, N'Maintenance')
INSERT INTO [dbo].[Mcu_Status_S] ([id], [status]) VALUES (3, N'Disabled')

-- Add 6 rows to [dbo].[Mcu_Tokens_D]
INSERT INTO [dbo].[Mcu_Tokens_D] ([McuID], [McuToken], [McuUserToken], [LastUpdate]) VALUES (1, N'710954656', N'130874400', '20070222 20:25:59.803')
INSERT INTO [dbo].[Mcu_Tokens_D] ([McuID], [McuToken], [McuUserToken], [LastUpdate]) VALUES (2, N'221215575', N'641877720', '20070222 20:25:59.833')
INSERT INTO [dbo].[Mcu_Tokens_D] ([McuID], [McuToken], [McuUserToken], [LastUpdate]) VALUES (3, N'526838886', N'456808203', '20070222 20:25:59.867')
INSERT INTO [dbo].[Mcu_Tokens_D] ([McuID], [McuToken], [McuUserToken], [LastUpdate]) VALUES (4, N'768780873', N'187435571', '20070222 20:25:59.897')
INSERT INTO [dbo].[Mcu_Tokens_D] ([McuID], [McuToken], [McuUserToken], [LastUpdate]) VALUES (5, N'173350632', N'88120140', '20070222 20:25:59.943')
INSERT INTO [dbo].[Mcu_Tokens_D] ([McuID], [McuToken], [McuUserToken], [LastUpdate]) VALUES (6, N'51735645', N'164369688', '20070222 20:25:59.973')

-- Add 8 rows to [dbo].[Mcu_Vendor_S]
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (1, N'Polycom MGC 25', 1, 25, 25)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (2, N'Polycom MGC 50', 1, 50, 50)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (3, N'Polycom MGC 100', 1, 100, 100)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (4, N'Codian MCU 4200', 3, 40, 40)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (5, N'Codian MCU 4500', 3, 40, 40)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (6, N'Codian  MSE 8000 Series', 3, 40, 40)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (7, N'Tandberg MPS 800 Series', 4, 160, 48)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (8, N'Polycom RMX', 1, 20, 20)
INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (9, N'Radvision Scopia', 5, 20, 20)

-- Add 9 rows to [dbo].[Rpt_Input_D]
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (1, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59''')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (2, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59'' AND U.userid  IN (<<selUsers>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (3, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59'' AND D.departmentId IN(<<selDepartments>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (4, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59''  AND R.roomId IN (<<selRooms>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (5, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59'' AND L2.ID IN (<<selTier2s>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (6, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59'' AND L3.ID IN (<<selTier1s>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (7, N' WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59'' AND E.endpointid IN (<<selEndpoints>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (8, N'WHERE C.confdate  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59''  AND MCU.BridgeID IN (<<selMCUs>>)')
INSERT INTO [dbo].[Rpt_Input_D] ([InputID], [InputClause]) VALUES (9, N'WHERE WK.CompletedBy  BETWEEN ''<<selFromDate>> 00:00:00'' AND ''<<selToDate>> 23:59:59''')

-- Add 32 rows to [dbo].[Rpt_Template_D]
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (1, 11, 1, '20070207 00:00:00.000', N'Test Report', N'Time total in Minutes', N'Select C.confnumname, C.confdate, C.confid, C.instanceid, C.externalname', N' From myVRM.DataLayer.vrmConference C', N' ', N' ORDER BY C.confdate', N'NULL', N'group by C.confnumname, C.confdate, C.confid, C.instanceid, C.externalname', N'Conference ID, Date, Confid, Conf Instance, Conf Name', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (2, 11, 2, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected Users', N'Time total in Minutes', N'Select U.FirstName,U.LastName,  sum(C.duration)', N' From myVRM.DataLayer.vrmConference C,myVRM.DataLayer.vrmUser U, myVRM.DataLayer.vrmConfUser Cu', N' AND C.deleted = 0 AND Cu.userid = U.userid AND Cu.confid = C.confid AND Cu.instanceid = C.instanceid  ', N' ORDER BY U.LastName,U.FirstName', N'NULL', N'group by  U.FirstName,U.LastName ', N'First Name, Last Name, Duration', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (3, 11, 3, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected Departments', N'Time total in Minutes', N'Select D.departmentName,Sum(C.duration)', N' From myVRM.DataLayer.vrmConference C, myVRM.DataLayer.vrmDept D,  myVRM.DataLayer.vrmConfRoom CR,  myVRM.DataLayer.vrmLocDepartment L', N' AND L.departmentId = D.departmentId AND C.deleted = 0 AND CR.roomId = L.roomId AND CR.confid = C.confid  AND CR.instanceid = C.instanceid  ', N' ORDER BY D.departmentName', N'NULL', N'group by  D.departmentName ', N'Department Name, Duration', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (4, 11, 4, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected Rooms', N'Time total in Minutes', N'Select R.Name , sum(C.duration) ', N' From myVRM.DataLayer.vrmConference C,myVRM.DataLayer.vrmRoom R,myVRM.DataLayer.vrmConfRoom CR', N' AND C.deleted = 0 AND R.roomId = CR.roomId AND (CR.confid = C.confid  and CR.instanceid = C.instanceid)', N' ORDER BY R.Name', N'NULL', N'group by  R.Name  ', N'Name, Duration(Min.)', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (5, 11, 5, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected Tier2''s', N'Time total in Minutes', N'Select L2.Name, sum(C.duration) ', N' From myVRM.DataLayer.vrmConference C, myVRM.DataLayer.vrmTier2 L2, myVRM.DataLayer.vrmConfRoom CR, myVRM.DataLayer.vrmRoom R', N' AND C.deleted = 0 AND L2.id = R.L2LocationId AND R.roomId = CR.roomId AND (CR.confid = C.confid and CR.instanceid = C.instanceid)', N' ORDER BY L2.Name', N'NULL', N'group by  L2.Name', N'Name, Duration(Min.)', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (6, 11, 6, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected Tier1''s', N'Time total in Minutes', N'Select L3.Name, sum(C.duration) ', N' From myVRM.DataLayer.vrmConference C,myVRM.DataLayer.vrmTier3 L3, myVRM.DataLayer.vrmConfRoom CR, myVRM.DataLayer.vrmRoom R', N' AND C.deleted = 0 AND L3.id = R.L3LocationId AND R.RoomID = CR.roomId AND (CR.confid = C.confid and CR.instanceid = C.instanceid)', N' ORDER BY L3.Name', N'NULL', N'group by  L3.Name ', N'Name, Duration(Min.)', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (7, 11, 7, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected Endpoints', N'Time total in Minutes', N'Select E.name , sum(C.duration) ', N' From myVRM.DataLayer.vrmConference C,myVRM.DataLayer.vrmEndPoint E, myVRM.DataLayer.vrmConfRoom CR, myVRM.DataLayer.vrmRoom R', N' AND C.deleted = 0 AND (C.confid = CR.confid AND C.instanceid = CR.instanceid) AND CR.roomId = R.RoomID AND R.endpointid = E.endpointid  ', N' ORDER BY E.name', N'NULL', N'group by  E.name ', N'Name, Duration(Min.)', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (8, 11, 8, '20070209 00:00:00.000', N'Total Scheduled Minutes for Selected MCUs', N'Time total in Minutes', N'Select MCU.BridgeName, sum(C.duration) ', N' From myVRM.DataLayer.vrmConference C,myVRM.DataLayer.vrmEndPoint E,myVRM.DataLayer.vrmConfRoom CR,myVRM.DataLayer.vrmRoom R,myVRM.DataLayer.vrmMCU MCU', N' AND C.deleted = 0 AND MCU.BridgeID = E.bridgeid AND E.endpointid = R.endpointid AND R.RoomID = CR.roomId AND (CR.confid = C.confid and CR.instanceid = C.instanceid)', N' ORDER BY MCU.BridgeName', N'NULL', N'group by  MCU.BridgeName', N'Bridge Name, Duration(Min.)', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (9, 11, 2, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected Users', N'Time total in Minutes', N'Select U.FirstName,U.LastName, count(M) ', N' From myVRM.DataLayer.vrmConfMonitor M, myVRM.DataLayer.vrmUser U, myVRM.DataLayer.vrmConference C', N' AND M.status <> 3 AND C.userid = U.userid AND (C.confid = M.confid AND C.instanceid = M.instanceid)', N' ORDER BY U.LastName,U.FirstName', N'NULL', N'group by  U.FirstName,U.LastName ', N'First Name, Last Name,Actual Minutes ', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (10, 11, 3, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected Departments', N'Time total in Minutes', N'Select D.departmentName, count(M)  ', N' From myVRM.DataLayer.vrmConfMonitor M,myVRM.DataLayer.vrmDept D,  myVRM.DataLayer.vrmConference C,  myVRM.DataLayer.vrmConfRoom CR,  myVRM.DataLayer.vrmLocDepartment L', N' AND M.status <> 3 AND (C.confid = M.confid AND C.instanceid = M.instanceid) AND  L.departmentId = D.departmentId AND C.deleted = 0 AND CR.roomId = L.roomId AND CR.confid  = C.confid  AND CR.instanceid = C.instanceid', N' ORDER BY D.departmentName', N'NULL', N'group by  D.departmentName ', N'Department Name,  Actual Minutes', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (11, 11, 4, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected Rooms', N'Time total in Minutes', N'Select R.Name, count(M)  ', N' From myVRM.DataLayer.vrmConfMonitor M, myVRM.DataLayer.vrmRoom  R,myVRM.DataLayer.vrmConfRoom CR,  myVRM.DataLayer.vrmConference C', N' AND (C.confid = CR.confid AND C.instanceid = CR.instanceid) AND M.status <> 3 AND R.RoomID = CR.roomId AND (CR.confid = M.confid and CR.instanceid = M.instanceid) ', N' ORDER BY R.Name', N'NULL', N'group by  R.Name ', N'Name, Actual Minutes', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (12, 11, 5, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected Tier2''s', N'Time total in Minutes', N'Select L2.Name, count(M)   ', N' From myVRM.DataLayer.vrmConfMonitor M, myVRM.DataLayer.vrmTier2 L2, myVRM.DataLayer.vrmConfRoom CR, myVRM.DataLayer.vrmRoom R,  myVRM.DataLayer.vrmConference C', N' AND (C.confid = M.confid AND C.instanceid = M.instanceid) AND M.status <> 3 AND R.RoomID = CR.roomId AND (CR.confid = C.confid and CR.instanceid  = C.instanceid) AND R.L2LocationId = L2.id ', N' ORDER BY L2.Name', N'NULL', N'group by  L2.Name ', N'Name, Actual Minutes', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (13, 11, 6, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected Tier1''s', N'Time total in minutes', N'Select L3.Name, count(M)    ', N' From myVRM.DataLayer.vrmConfMonitor M, myVRM.DataLayer.vrmTier3 L3, myVRM.DataLayer.vrmConfRoom CR,  myVRM.DataLayer.vrmRoom R, myVRM.DataLayer.vrmConference C', N' AND M.status <> 3 AND L3.id = R.L3LocationId AND R.RoomID = CR.roomId AND (CR.confid = C.confid and CR.instanceid  = C.instanceid) AND (M.confid = C.confid AND M.instanceid = C.instanceid)  ', N' ORDER BY L3.Name', N'NULL', N'group by  L3.Name ', N'Name, Actual Minutes', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (14, 11, 7, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected Endpoints', N'Time total in minutes', N'Select E.name, count(M)  ', N' From myVRM.DataLayer.vrmConfMonitor M, myVRM.DataLayer.vrmConference C,myVRM.DataLayer.vrmEndPoint E, myVRM.DataLayer.vrmConfRoom CR, myVRM.DataLayer.vrmRoom R', N' AND (C.confid = M.confid AND C.instanceid = M.instanceid) AND M.status <> 3 AND (M.confid = CR.confid AND M.instanceid = CR.instanceid) AND CR.roomId = R.RoomID AND R.endpointid = E.endpointid  ', N' ORDER BY E.name', N'NULL', N'group by  E.name ', N'Name,  Actual Minutes', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (15, 11, 8, '20070209 00:00:00.000', N'Total Real-time Minutes for Selected MCU''s', N'Time total in minutes', N'Select MCU.BridgeName, count(M)', N' From myVRM.DataLayer.vrmConfMonitor M,myVRM.DataLayer.vrmEndPoint E, myVRM.DataLayer.vrmConfRoom CR, myVRM.DataLayer.vrmRoom R,myVRM.DataLayer.vrmMCU MCU, myVRM.DataLayer.vrmConference C', N' AND (C.confid = M.confid AND C.instanceid = M.instanceid) AND M.status <> 3 AND MCU.BridgeID = E.bridgeid AND E.endpointid = R.endpointid AND R.RoomID = CR.roomId AND (CR.confid = M.confid and CR.instanceid = M.instanceid)  ', N' ORDER BY MCU.BridgeName', N'NULL', N'group by  MCU.BridgeName ', N'Bridge Name, Actual Minutes', N'NULL', 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (16, 11, 2, '20070209 00:00:00.000', N'Total IP Minutes for Selected Users', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = C.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (17, 11, 3, '20070209 00:00:00.000', N'Total IP Minutes for Selected Departments', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N' ', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (18, 11, 4, '20070209 00:00:00.000', N'Total IP Minutes for Selected Rooms', N'NULL', N'dumm', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dumm', N'dumm', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (19, 11, 5, '20070209 00:00:00.000', N'Total IP Minutes for Selected Tier2''s', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (20, 11, 6, '20070209 00:00:00.000', N'Total IP Minutes for Selected Tier1''s', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (21, 11, 7, '20070209 00:00:00.000', N'Total IP Minutes for Selected Endpoints', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (22, 11, 8, '20070209 00:00:00.000', N'Total IP Minutes for Selected MCU''s', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (23, 11, 2, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected Users', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (24, 11, 3, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected Departments', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (25, 11, 4, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected Rooms', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (26, 11, 5, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected Tier2''s', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (27, 11, 6, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected Tier1''s', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (28, 11, 7, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected Endpoints', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (29, 11, 8, '20070209 00:00:00.000', N'Total ISDN Minutes for Selected MCU''s', N'NULL', N'dummy', N'', N' AND (c.confid = m.confid AND c.instanceid = m.instanceid) AND M.Status <> 3 AND R.RoomID = CR.RoomID AND (CR.confid = c.confid and CR.instanceid = c.instanceid) GROUP BY l2.[name] ORDER BY l2.[name]', NULL, N'NULL', N'dummy', N'dummy', N'NULL', 1, 1)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (30, 11, 9, '20070707 00:00:00.000', N'Outstanding AV Work Order(s) ', N'Pending A/V Work Order(s) Report', N'Select WK.Name, C.externalname,  R.Name, U.LastName, U.FirstName,  WK.CompletedBy', N'From  myVRM.DataLayer.vrmConference C,  myVRM.DataLayer.vrmRoom R, myVRM.DataLayer.vrmUser U,   myVRM.DataLayer.WorkOrder WK  ', N' AND  WK.Type = 1 AND WK.Status = 0 AND WK.deleted = 0 AND  C.confid = WK.ConfID AND C.instanceid = WK.InstanceID  AND U.userid = WK.AdminID AND R.RoomID = WK.roomid  ', N'ORDER BY WK.CompletedBy', NULL, N'group by WK.Name, C.externalname,  R.Name, U.LastName, U.FirstName,  WK.CompletedBy', N'Title, Conference Name, Room Name, Admin Last Name, Admin First Name, Complete by Date', NULL, 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (31, 11, 9, '20070707 00:00:00.000', N'Outstanding Catering Work Order(s)', N'Pending Catering Work Order(s) Report', N'Select WK.Name, C.externalname,  R.Name, U.LastName, U.FirstName,  WK.CompletedBy', N'From  myVRM.DataLayer.vrmConference C,  myVRM.DataLayer.vrmRoom R, myVRM.DataLayer.vrmUser U,   myVRM.DataLayer.WorkOrder WK', N' AND WK.Type = 2 AND  WK.Status = 0 AND WK.deleted = 0 AND C.confid = WK.ConfID AND C.instanceid = WK.InstanceID  AND U.userid = WK.AdminID  AND  R.RoomID = WK.roomid  ', N'ORDER BY WK.CompletedBy', NULL, N'group by WK.Name, C.externalname,  R.Name, U.LastName, U.FirstName,  WK.CompletedBy', N'Title, Conference Name, Room Name, Admin Last Name, Admin First Name, Complete by Date', NULL, 1, 0)
INSERT INTO [dbo].[Rpt_Template_D] ([ID], [userID], [inputID], [lastModified], [templateTitle], [templateDescription], [query], [tables], [filter], [orderby], [outputFormat], [totalQuery], [columnHeadings], [outputTotal], [Public], [deleted]) VALUES (32, 11, 9, '20070707 00:00:00.000', N'Outstanding Housekeeping  Work Order(s) ', N'Housekeeping  Work Order Report', N'Select WK.Name, C.externalname,  R.Name, U.LastName, U.FirstName,  WK.CompletedBy', N'From  myVRM.DataLayer.vrmConference C,  myVRM.DataLayer.vrmRoom R, myVRM.DataLayer.vrmUser U,   myVRM.DataLayer.WorkOrder WK', N' AND WK.Type = 3 AND WK.Status = 0 AND WK.deleted = 0 AND C.confid = WK.ConfID AND C.instanceid = WK.InstanceID  AND U.userid = WK.AdminID   AND R.RoomID = WK.roomid  ', N'ORDER BY WK.CompletedBy', NULL, N'group by WK.Name, C.externalname,  R.Name, U.LastName, U.FirstName,  WK.CompletedBy', N'Title, Conference Name, Room Name, Admin Last Name, Admin First Name, Complete by Date', NULL, 1, 0)

-- Add 22 rows to [dbo].[Sys_DatabaseVersion_S]
SET IDENTITY_INSERT [dbo].[Sys_DatabaseVersion_S] ON
EXEC(N'INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (1, N''1.2.2'', N''05/23/06 Added user defined function SysDate(indate) to change system date to server timzone(GMT)

05/26/06 Added xchange-specific conference info in conf_exchange_d (FogBuz 652)
	 Added LastUpdate in Sys_Exchange_Settings_D

06/12/06 Due to the Unicode conversion and the need for consistency the following standard have          been applied to database field lengths;

         Standard field lengths for v18_sql

          Logical Type            Type/Length
          --------------          ---------------

          Name                     nvarchar   256 
          Login                    nvarchar  1024 
          Password                 nvarchar   256 
          Address                  nvarchar   512 
          Phone                    nvarchar    20 
          Description              nvarchar  2000 
          Image                    nvarchar   500 
          Comment/message          nvarchar  4000
          IP/ISDN                  nvarchar   512
          Security Key    	   nvarchar   256
          URL			   nvarchar   512
          Menu Mask                nvarchar   200
          Email   to/from          nvarchar   512
          Subject                  nvarchar  2000        


06/19/06 Added fields p2pConfEnabled (bool), IMEnabled(bool) IMRefreshConn(float),                       IMMaxUnitConn(int and IMMaxSysConn(int) to Sys_Settings_D

06/23/06 3 New tables were added to support custom attributes:
		
		Conf_CustomAttr_D
		Dept_CustomAttr_D
		Dept_CustomAttr_Option_D

06/27/06 Added caption filed to Dept_CustomAttr_Option_D
	 Defalut IMEnabled off (0) in Sys_System_D

07/06/06 Increased size of MCU_LIST_D password to 1024

07/07/06 The Following tables were removed:

	Conf_Custom1_D
	Conf_Custom2_D
	Conf_Custom3_D
	Conf_Custom4_D
	Conf_Custom5_D
	Conf_Custom6_D
	Dept_Security_D
	Gen_Audio_D
	Gen_Mute_D
	Gen_StaticCustom1_D
	Gen_StaticCustom2_D
	Gen_StaticCustom3_D
	Gen_StaticCustom4_D
	Gen_StaticCustom5_D	
	Loc_Level3Locations_D
	Loc_Other_D
	Usr_Accord_D
	Usr_Security_D

	Err_LogPrefs_D  was renamed to Err_LogPrefs_S

07/07/06 Two customer specific tables were added for HKLaw
  
	Custom_HKLaw_ReprotRoom_D 
		RoomID, 	int, 	 4,   Allow Nulls 
		TimezoneID, 	int,	 4,   Allow Nulls 
		FilePath, 	nvarchar,255, Allow Nulls 

	Custom_HKlaw_eportTier2_D 
		Tier2ID, 	int,	 4,   Allow Nulls 		
		TimezoneID,	int, 	 4,   Allow Nulls 
		Filepath, 	nvarchar,255, Allow Nulls 

The Following convention will be used for all customer specific custom tables in the future
	Custom_<Client>_<TableName>_<D/S> 

07/10/06 Added new table Sys_LoginAudit_D
	 Changed error message 261

07/12/06 Added new error to err_List_S (407 license check failed)
	 Added new error to err_List_S (408 invalid selection critera)

08/01/06 Changed all encrypted fields to AES. This includes ALL passwords and the license key.
	 Added ERROR 409 AES Ecryption failure 

08/15/06 v1.1 
	 Added field Mcu_List_D deleted
	 Added field Ept_List_D endptURL
	 Added errors 411,412,413 to Err_List_S

08/23/06 Added flag to Sys_Settings_D  
	 DefaultToPublic (bool)

08/31/06 2 new tables for nested custom attributes were added.
         Dept_CustomAttr_Ex_D
	 Conf_CustomAttrEx_D 

	 ConfDeptID was added to Conf_Conference_D
         
         A new Error (414) was added to Err_List_S for Link Trans Rollback

09/11/06 Lost changes for 09/06 check v1.5
	 Many new custom attributes. 
	 
09/12/06 v18.1.8
	 Mcu_ISDNServices_D field LastNumberUsed allow nulls
	 New errors in error llist
	 added responsetime and repsonsemessage to dept_List_D
 	 Change stmt in Sys_TDF_D to ntext

09/15/08 v18.1.9
	 Email_Queue_D fields RetryCount and LastRetryDateTime
	 Dept_CustomAtt_Option_D & Dept_CustomAtt_Ex_D added DeptID
 	 Usr_Level_D added DeptID
	  
09/18/06 Latest no changes...

09/19/06 v.18.2.1 
	 User list approver set to 0 default

09/19/06 Primary keys add'', ''20071114 00:00:00.000'', N''Alex'')')
EXEC(N'DECLARE @pv binary(16)
'+N'SELECT @pv=TEXTPTR([changeLog]) FROM [dbo].[Sys_DatabaseVersion_S] WHERE [id]=1
UPDATETEXT [dbo].[Sys_DatabaseVersion_S].[changeLog] @pv NULL NULL N''ed to tables. 
(KM)	 Spelling changes on last 6 error messages in Err_List_D.
	 New tables added - Gen_VideoLayout_S, Gen_DualStreamMode_S, Gen_MediaType_S.
	 New column "Attachment" added to "Email_Queue_D". 
	 New rows added in Gen_VideoSession_S, Gen_LineRate_S,Gen_VideoEquipment_S. 

09/22/06 Added and reorganized custom attributes...

09/25/06 Added new table Gen_ConfVideoProtocol_S for conference video protocol restrictions. 
         Added lectureMode to ConfAdvAVParams. 

09/25/06 Added new table Conf_Alerts_D,Gen_AlertType_S for conference alerts. 
(KM) 	 
(ag)	 Removed Gen_ConfVideoProtocol_S

09/26/06 Removed Mcu_Log_D, Mcu_Polycom_D tables. These became redundant in v18.
(KM)

09/29/06 v18.2.4
	 Fixed scripts, changed admin email.

10/03/06 v18.2.6
	 Fixed scripts yet again!

10/05/06 v18.2.7 
	 Fixed scripts
	 Changed default settings in Sys_Settings

10/06/06 v18.2.8
	 Changed script to change doublebookingenabled to 0 ALWAYS!

10/16/06 V18.2.9 
	 Added constraints to tables in key places. 
	 Added UUID entity in Email_Queue_D

10/17/06 V18.2.9
	 Changed Telephone field length due to LDAP requirments.

10/26/06 V18.3.0
	 Remove UUID entity (It causes errors, will add during conversions)
         Usr_GuestList_D deleted field defaults to 0
         Err_List_D added error 418 Endpoint must have bridge assigned

11/09/06 V18.3.1 
	 Small changes to fields (and constraints) to make conversion easier

11/13/06 V18.3.2 
	 changed err message 412
	 set BridgeID in Usr_Inactive_D and Usr_GuestList_D to default to 1

12/08/06 v18.3.3
	 Changed field definitions to make conersion eaiser.

12/18/06 v18.3.4
	 added error 419. 
	 changed defaults to make dbconversion easier. 
01/11/07 v18.3.5
	 added default for accountexpiry in usr_list_d (this is a temporary fix and will not be in v19)
	 changed message in error 412
01/25/07 v18.3.6
	 added 3 tables for report processing
	 ReportInput
	 ReportSchedule
    	 ReportTemplate
	
02/06/07 v18.3.6 
	 Made changes to report tables. 
	 Kept version number the same

02/14/07 v18.3.7 
	 moved HardwareIssue from v17 to Gen_HardwareIsuue_D
02/20/07 v18.3.8
	 Made changes to rpt_schedule_d to to allow paging in SQL-2005.
	 there are 2 sets of table data for pre-loaded report templates. 
         These files are in the scripts directory. Load the 2005 templates 
         the server is 2005. Sql-2000 is the default.
02/26/07 V.18.3.9
	Added DST update
03/19/07 v.18.4.0
	Added new error message. 
	Added Ldap_Serverconfig_D fields. 

04/23/07 v.18.4.1
        Changed time zones. 
	Added cataloge for full text search.
	Fixed report query.

04/23/07 v.18.4.2
	Fixed template field int Usr_SearchTemplate_D
05/13/07 v.18.4.3
	Added tables for Inv (work order management)
05/25/07 v.18.4.3
	Updated tables for Inv (work order management)
06/01/07 v.18.4.4
	More Inv updates
        Added Inv tables to the clean script. 
06/07/07 v.18.5.0
	Added type field to WorkOrder to support templates.
	Removed unnecessary field from template Tmp_List_D
	Altered table Tmp_AdvAVParams_D to correspond to Conf_AdvAVParams_D
06/12/07 v.18.5.1
	Put back unnecessary fields into Tmp_List_D
06/12/07 v.18.5.2
	Put back unnecessary fields into Tmp_List_D
06/19/2007 v.18.5.2
	UUID in Email_Queue_D is type identity
	smallint in Conf_Conference_D were changed to int (avoid type cast issues in mapping)
	LoginKey in LDAP_ServerConfig_D was increased to nvarchar 50
	strData (nvarchar 2000) was added to Inv_WorkOrder_D to act as a general purpose field
07/01/2007 v.18.5.3
	UUID field in Conf_Room_D and necessary for hHibernace.
	clean data script does not clear Inv_List_D
        Roles moved from .11 test to Usr_ROles_D
	added columnHeadings to rpt_Template_D
07/09/2007 v.18.5.4
	Fixed reports in Rpt_Template_D	
07/20/2007 v.18.5.5
	Fixed reports in Rpt_Template_D	
07/30/2007 same version....
	Fixed spelling erro''
UPDATETEXT [dbo].[Sys_DatabaseVersion_S].[changeLog] @pv NULL NULL N''rs
	set default date for LastLogin in usr_list_d
09/14/07 
	5.6a Restored, 5.6 is corrupt
09/14/07 5.7
	changes to usr_template_d table.
10/10/07
	v18.Sql.1.2
	Added video mode tables and endpoint fields.
10/12/07
	v18.Sql.1.2
	Added videoMode field to conf_advAvParams_D.
10/15/07
	v18.Sql.1.2
	Fixed query strings in Rpt_Template_D and Rpt_Input_D as persistance layer is case 	sesitive.
10/31/07 v18.Sql.1.2.1
	Added id(identity) to Usr_list_d and usr_guestlist_d . 

10/31/07 v18.Sql.1.2.1
	Added id(identity) to Usr_list_d and usr_guestlist_d . 

11/01/07 v18.Sql.1.2.2
        Dropped Mcu_Type_S and update Mcu_Vendor_S
''
')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (2, N'1.8.3.a', N'Change static table Gen_VideoMode_S, Added table Sys_DatabaseVersion_S', '20071114 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (3, N'1.8.3.b', N'Added startDate, timezone fields to Inv_WorkOrder_D; id to Usr_Inactive_D', '20071120 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (4, N'1.8.3.b', N'fixed misspelled bridgeid in Usr_Inactive_D table', '20071126 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (5, N'1.8.3.b', N'added work oder columns, new table inv_DelieryTyps_S', '20071127 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (6, N'1.8.3.b', N'added domain prefix to LDAP_Server and Identity ID to conf_attahments', '20071129 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (7, N'1.8.3.b', N'Changed the Gen_VideoMode_S table values - removed id=0 as its redundant, corrected spelling for rest of record values.', '20071205 13:54:00.000', N'Kapil')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (8, N'1.8.3.b', N'Added maxConcurrentAudioCalls and maxConcurrentVideoCalls in Mcu_List_D', '20071206 15:15:00.000', N'Kapil')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (9, N'1.8.3.b', N'Added rows in Gen_LineRate_S table', '20071212 10:00:00.000', N'Kapil')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (10, N'1.8.3.b', N'Added 2 new ldap fields - scheduleTime, scheduleDays', '20071231 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (12, N'1.8.3.c', N'Added confOrigin to Conf_conference_D; added chargeId to Inv_Item_List_D; added new table; added new table Inv_ItemCharge_D for item charges', '20070111 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (13, N'18.3.c', N'Changed usr search template id to identitiy ', '20080204 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (14, N'18.3.c', N'Changed user tables, merged guest and inactive with user, userid now primary key/identityh', '20080205 00:00:00.000', N'Alex')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (15, N'18.3.c', N'Added userid to Usr_SearchTemplates', '20080214 00:00:00.000', NULL)
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (16, N'18.3.c', N'Change gen_timezone_S', '20080220 00:00:00.000', NULL)
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (17, N'18.3.c', N'uId in conf_Cascade_D', '20080310 00:00:00.000', N'AG')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (18, N'18.3.d', N'MPI Record in Gen_AddressType_S', '20080604 00:00:00.000', N'AG')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (19, N'18.3.d', N'MPI Record in Gen_VideoProtocol_S', '20080604 00:00:00.000', N'AG')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (20, N'18.3.d', N'Table MCISerices_S added', '20080604 00:00:00.000', N'AG')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (22, N'18.3.d', N'Table Gen_Dialing_Options_S + data', '20080604 00:00:00.000', N'AG')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (23, N'18.3.d', N'Conf_RecurInfo_D ad recurring patttern field', '20080612 00:00:00.000', N'AG')
INSERT INTO [dbo].[Sys_DatabaseVersion_S] ([id], [version], [changeLog], [changeDate], [changeAuthor]) VALUES (24, N'18.3.d', N'Usr_Role_D changed menu mask for general user', '20080618 00:00:00.000', N'AG')
SET IDENTITY_INSERT [dbo].[Sys_DatabaseVersion_S] OFF

-- Add 4 rows to [dbo].[Sys_LoginAudit_D]
INSERT INTO [dbo].[Sys_LoginAudit_D] ([UserId], [LoginDate]) VALUES (11, '20081112 20:39:03.000')
INSERT INTO [dbo].[Sys_LoginAudit_D] ([UserId], [LoginDate]) VALUES (11, '20081112 20:47:57.000')
INSERT INTO [dbo].[Sys_LoginAudit_D] ([UserId], [LoginDate]) VALUES (11, '20081113 09:27:30.000')
INSERT INTO [dbo].[Sys_LoginAudit_D] ([UserId], [LoginDate]) VALUES (11, '20081113 09:46:56.000')

-- Add 2 rows to [dbo].[Sys_TimeZonePref_S]
INSERT INTO [dbo].[Sys_TimeZonePref_S] ([systemID], [name]) VALUES (0, N'World Wide Time Zones')
INSERT INTO [dbo].[Sys_TimeZonePref_S] ([systemID], [name]) VALUES (1, N'U.S Time Zones')

-- Add 6 rows to [dbo].[Usr_Roles_D]
INSERT INTO [dbo].[Usr_Roles_D] ([roleID], [roleName], [roleMenuMask], [locked], [level]) VALUES (1, N'General User', N'6*63-13*7687+3*7+2*3+3*7+8*255+2*1+2*1+2*1-6*63', 1, 0)
INSERT INTO [dbo].[Usr_Roles_D] ([roleID], [roleName], [roleMenuMask], [locked], [level]) VALUES (2, N'Conferencing Administrator', N'6*63-13*7663+3*7+2*3+3*7+8*255+2*3+2*3+2*3-6*63', 1, 1)
INSERT INTO [dbo].[Usr_Roles_D] ([roleID], [roleName], [roleMenuMask], [locked], [level]) VALUES (3, N'System Administrator', N'6*63-13*7679+3*7+2*3+3*7+8*255+2*3+2*3+2*3-6*63', 1, 2)
INSERT INTO [dbo].[Usr_Roles_D] ([roleID], [roleName], [roleMenuMask], [locked], [level]) VALUES (4, N'Catering Administrator', N'6*33-13*2+3*7+2*3+3*7+8*255+2*3+2*3+2*3-6*63', 1, 1)
INSERT INTO [dbo].[Usr_Roles_D] ([roleID], [roleName], [roleMenuMask], [locked], [level]) VALUES (5, N'AV Administrator', N'6*33-13*4+3*7+2*3+3*7+8*255+2*3+2*3+2*3-6*63', 1, 1)
INSERT INTO [dbo].[Usr_Roles_D] ([roleID], [roleName], [roleMenuMask], [locked], [level]) VALUES (6, N'Housekeeping Administrator', N'6*33-13*1+3*7+2*3+3*7+8*255+2*3+2*3+2*3-6*63', 1, 1)

-- Add 1 row to [dbo].[Conf_Conference_D]
SET IDENTITY_INSERT [dbo].[Conf_Conference_D] ON
INSERT INTO [dbo].[Conf_Conference_D] ([confid], [instanceid], [userid], [externalname], [internalname], [password], [owner], [confdate], [conftime], [timezone], [immediate], [audio], [videoprotocol], [videosession], [linerate], [recuring], [duration], [description], [public], [deleted], [continous], [transcoding], [deletereason], [advanced], [totalpoints], [connect2], [settingtime], [videolayout], [manualvideolayout], [conftype], [confnumname], [status], [lecturemode], [lecturer], [dynamicinvite], [CreateType], [ConfDeptID], [ConfOrigin]) VALUES (11, 1, 11, N'phantom', N'phantom', NULL, 11, '19800101 00:00:00.000', '19800101 00:00:00.000', 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'phantom', 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 1, 9, 0, NULL, NULL, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Conf_Conference_D] OFF

-- Add 11 rows to [dbo].[Inv_List_D]
SET IDENTITY_INSERT [dbo].[Inv_List_D] ON
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (1, N'Table', 1, N'image/resource/table.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (2, N'Chair', 1, N'image/resource/chair.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (3, N'Plasma TV', 1, N'image/resource/projector.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (4, N'Misc', 1, N'image/space.gif', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (5, N'Pizza', 2, N'image/food/pizza.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (6, N'Salad', 2, N'image/food/salad.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (7, N'Sandwiches', 2, N'image/food/sandwich.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (8, N'Misc', 2, N'image/space.gif', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (9, N'Cleaning Supplies', 3, N'image/housekeeping/Cleaning.jpg', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (10, N'Misc', 3, N'image/space.gif', 0)
INSERT INTO [dbo].[Inv_List_D] ([ID], [Name], [Type], [Image], [Deleted]) VALUES (11, N'Test', 2, NULL, 1)
SET IDENTITY_INSERT [dbo].[Inv_List_D] OFF

-- Add 1 row to [dbo].[Loc_Room_D]
SET IDENTITY_INSERT [dbo].[Loc_Room_D] ON
INSERT INTO [dbo].[Loc_Room_D] ([RoomID], [Name], [RoomBuilding], [RoomFloor], [RoomNumber], [RoomPhone], [Capacity], [Assistant], [AssistantPhone], [ProjectorAvailable], [MaxPhoneCall], [AdminID], [videoAvailable], [DefaultEquipmentid], [DynamicRoomLayout], [Caterer], [L2LocationId], [L3LocationId], [Disabled], [responsetime], [responsemessage], [roomimage], [setuptime], [teardowntime], [auxattachments], [endpointid], [costcenterid], [notifyemails]) VALUES (11, N'Phantom Room', N'None', N'None', N'None', N'None', N'0', N'11', N'', 0, 0, 11, 0, 0, 0, 0, 0, 0, 1, 0, N'0', N'0', 0, 0, N'0', 0, 0, N'0')
SET IDENTITY_INSERT [dbo].[Loc_Room_D] OFF
-- Add 1 row to [dbo].[Sys_Settings_D]
INSERT INTO [dbo].[Sys_Settings_D] ([LastModified], [Admin], [TimeZone], [Logo], [CompanyTel], [CompanyEmail], [CompanyURL], [Connect2], [DialOut], [AccountingLogic], [BillPoint2Point], [AllLocation], [SecurityKey], [superAdmin], [tzsystemid], [RealtimeStatus], [BillRealTime], [MultipleDepartments], [overAllocation], [Threshold], [autoApproveImmediate], [adminEmail1], [adminEmail2], [adminEmail3], [AutoAcceptModConf], [recurEnabled], [responsetime], [responsemessage], [SystemStartTime], [SystemEndTime], [Open24hrs], [Offdays], [ISDNLineCost], [ISDNPortCost], [IPLineCost], [IPPortCost], [ISDNTimeFrame], [foodsecuritykey], [resourcesecuritykey], [dynamicinviteenabled], [doublebookingenabled], [emaildisclaimer], [license], [IMEnabled], [IMRefreshConn], [IMMaxUnitConn], [IMMaxSysConn], [DefaultToPublic], [DefaultConferenceType], [EnableRoomConference], [EnableAudioVideoConference], [EnableAudioOnlyConference], [DefaultCalendarToOfficeHours], [RoomTreeExpandLevel]) VALUES ('20071010 16:21:56.420', 11, 26, N'ABC', NULL, N'vrmadmin@myVrm.com', N'', 1, 1, 0, 1, 0, N'2552364769726247697880893139999', 1, 1, 1, 1, 0, 0, NULL, 0, 0, 0, 0, 1, 1, 60, N'', '19000101 00:00:00.000', '19000101 23:59:00.000', 1, N'1,7', 2, 1, 33, 3, 1, N'', N'', 1, 0, N'', N'CkwYfj4KBDfNi12D+KC1WWvX7M9/KKZyI0kWa5KHSg6ebed0ntCD+EnzCJOBjeO6m4f+vqgw1dFTuE/VpPZsnxWgGxj5pPwlGiCF564NRFDtWmjfk4UJTxShM5UCv+J9IuiBuDkN5pa9IQc1hKNW7M2Y2m4AetfGat0APQ4UgxpQ774zx+60DpcYrLY80/hnaQzeSjx9Sz0WTWNtpcWqIOfv6TjEyR91jnt1AgIaEVNoNBPDi/SPPb84DcSagJi34coKXp2El7nefm+eFK7g/M4YMJ7SnnuUaqwWvqSBk3hqaHcG5ad/vxgv67PYTD0vCZT35FfRzII3OcIuZ3s4Dx8pDJcCgZGso4muRcHp6digL/LlYKGtjjrLigdEs04imQpiTag2rN5Jfi9BZ2vOo/Mu08GVWhTgPWmn4+q4T/QqlIL66GoJjtmciYC0P/zHjv88bcyBCI89HeK5uj15Azl8JQ+2JYHQzV01Zz3zdBg=', 0, 30, 3, 30, 0,7, 1, 1, 1, 1, 3)

-- Add 1 row to [dbo].[Sys_TechSupport_D]
INSERT INTO [dbo].[Sys_TechSupport_D] ([name], [email], [phone], [info], [feedback], [id]) VALUES (N'Support', N'support@myvrm.com', N'888-698-6798', NULL, NULL, 1)

-- Add 11 rows to [dbo].[Usr_Level_S]
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (1, N'CEO/CMO/CFO', 1)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (2, N'VP or above', 1)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (3, N'Director/DMM', 1)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (4, N'Category Manager/Buyer', 1)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (5, N'Other', 1)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (1, N'NONE', 2)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (1, N'Agency', 3)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (2, N'Research', 3)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (3, N'Other', 3)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (1, N'Open', 4)
INSERT INTO [dbo].[Usr_Level_S] ([LevelID], [LevelName], [DeptID]) VALUES (2, N'Restricted', 4)

-- Add 1 row to [dbo].[Usr_List_D]
SET IDENTITY_INSERT [dbo].[Usr_List_D] ON
INSERT INTO [dbo].[Usr_List_D] ([UserID], [Id], [FirstName], [LastName], [Email], [Financial], [Admin], [Login], [Password], [Company], [TimeZone], [Language], [PreferedRoom], [AlternativeEmail], [DoubleEmail], [PreferedGroup], [CCGroup], [Telephone], [RoomID], [PreferedOperator], [lockCntTrns], [DefLineRate], [DefVideoProtocol], [Deleted], [DefAudio], [DefVideoSession], [MenuMask], [initialTime], [EmailClient], [newUser], [PrefTZSID], [IPISDNAddress], [DefaultEquipmentId], [connectionType], [companyId], [securityKey], [LockStatus], [LastLogin], [outsidenetwork], [emailmask], [roleID], [addresstype], [accountexpiry], [approverCount], [BridgeID], [Title], [LevelID], [preferredISDN], [portletId], [searchId], [endpointId]) VALUES (11, 1, N'VRM', N'Administrator', N'admin@myvrm.com', 0, 2, N'admin', N'odXTn94M8rRfGEyF6nwoKQ==', N'', 26, 1, 1, N'another@anyplace.com', 1, 0, 0, N'', 1, 2, 0, 384, 1, 0, 1, 1, N'6*63-13*7679+3*7+2*3+3*7+8*255+2*3+2*3+2*3-6*63', 0, 1, 0, 0, N'127.0.0.1', 21, 1, 0, N'11111', 0, '20081113 14:46:56.203', 0, 0, 3, 1, '20110606 00:00:00.000', 0, 0, NULL, 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[Usr_List_D] OFF

COMMIT TRANSACTION

SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

BEGIN TRANSACTION

-- Add 238 rows to [dbo].[Gen_Country_S]
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (1, N'Afghanistan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (2, N'Albania', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (3, N'Algeria', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (4, N'American Samoa', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (5, N'Andorra', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (6, N'Angola', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (7, N'Anguilla', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (8, N'Antarctica', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (9, N'Antigua and Barbuda', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (10, N'Argentina', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (11, N'Armenia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (12, N'Aruba', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (13, N'Australia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (14, N'Austria', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (15, N'Azerbaijan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (16, N'Bahamas', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (17, N'Bahrain', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (18, N'Bangladesh', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (19, N'Barbados', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (20, N'Belarus', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (21, N'Belgium', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (22, N'Belize', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (23, N'Benin', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (24, N'Bermuda', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (25, N'Bhutan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (26, N'Bolivia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (27, N'Bosnia and Herzegovina', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (28, N'Botswana', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (29, N'Bouvet Island', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (30, N'Brazil', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (31, N'British Indian Ocean Territory', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (32, N'Brunei Darussalam', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (33, N'Bulgaria', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (34, N'Burkina Faso', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (35, N'Burundi', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (36, N'Cambodia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (37, N'Cameroon', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (38, N'Canada', 1)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (39, N'Cape Verde', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (40, N'Cayman Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (41, N'Central African Republic', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (42, N'Chad', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (43, N'Chile', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (44, N'China', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (45, N'Christmas Island', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (46, N'Cocos (Keeling) Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (47, N'Colombia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (48, N'Comoros', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (49, N'Congo', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (50, N'Congo, The Democratic Republic of The', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (51, N'Cook Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (52, N'Costa Rica', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (53, N'Croatia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (54, N'Cuba', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (55, N'Cyprus', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (56, N'Czech Republic', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (57, N'Denmark', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (58, N'Djibouti', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (59, N'Dominica', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (60, N'Dominican Republic', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (61, N'Ecuador', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (62, N'Egypt', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (63, N'El Salvador', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (64, N'Equatorial Guinea', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (65, N'Eritrea', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (66, N'Estonia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (67, N'Ethiopia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (68, N'Falkland Islands (Malvinas)', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (69, N'Faroe Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (70, N'Fiji', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (71, N'Finland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (72, N'France', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (73, N'French Guiana', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (74, N'French Polynesia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (75, N'French Southern Territories', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (76, N'Gabon', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (77, N'Gambia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (78, N'Georgia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (79, N'Germany', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (80, N'Ghana', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (81, N'Gibraltar', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (82, N'Greece', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (83, N'Greenland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (84, N'Grenada', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (85, N'Guadeloupe', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (86, N'Guam', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (87, N'Guatemala', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (88, N'Guinea', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (89, N'Guinea-bissau', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (90, N'Guyana', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (91, N'Haiti', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (92, N'Heard Island and Mcdonald Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (93, N'Holy See (Vatican City State)', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (94, N'Honduras', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (95, N'Hong Kong', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (96, N'Hungary', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (97, N'Iceland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (98, N'India', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (99, N'Indonesia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (100, N'Iran, Islamic Republic of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (101, N'Iraq', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (102, N'Ireland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (103, N'Israel', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (104, N'Italy', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (105, N'Jamaica', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (106, N'Japan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (107, N'Jordan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (108, N'Kazakhstan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (109, N'Kenya', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (110, N'Kiribati', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (111, N'Korea, Democratic People''s Republic of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (112, N'Korea, Republic of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (113, N'Kuwait', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (114, N'Kyrgyzstan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (115, N'Lao People''s Democratic Republic', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (116, N'Latvia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (117, N'Lebanon', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (118, N'Lesotho', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (119, N'Liberia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (120, N'Libyan Arab Jamahiriya', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (121, N'Liechtenstein', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (122, N'Lithuania', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (123, N'Luxembourg', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (124, N'Macao', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (125, N'Macedonia, The Former Yugoslav Republic of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (126, N'Madagascar', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (127, N'Malawi', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (128, N'Malaysia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (129, N'Maldives', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (130, N'Mali', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (131, N'Malta', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (132, N'Marshall Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (133, N'Martinique', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (134, N'Mauritania', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (135, N'Mauritius', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (136, N'Mayotte', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (137, N'Mexico', 1)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (138, N'Micronesia, Federated States of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (139, N'Moldova, Republic of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (140, N'Monaco', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (141, N'Mongolia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (142, N'Montserrat', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (143, N'Morocco', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (144, N'Mozambique', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (145, N'Myanmar', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (146, N'Namibia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (147, N'Nauru', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (148, N'Nepal', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (149, N'Netherlands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (150, N'Netherlands Antilles', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (151, N'New Caledonia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (152, N'New Zealand', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (153, N'Nicaragua', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (154, N'Niger', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (155, N'Nigeria', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (156, N'Niue', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (157, N'Norfolk Island', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (158, N'Northern Mariana Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (159, N'Norway', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (160, N'Oman', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (161, N'Pakistan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (162, N'Palau', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (163, N'Palestinian Territory, Occupied', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (164, N'Panama', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (165, N'Papua New Guinea', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (166, N'Paraguay', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (167, N'Peru', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (168, N'Philippines', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (169, N'Pitcairn', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (170, N'Poland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (171, N'Portugal', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (172, N'Puerto Rico', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (173, N'Qatar', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (174, N'Reunion', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (175, N'Romania', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (176, N'Russian Federation', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (177, N'Rwanda', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (178, N'Saint Helena', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (179, N'Saint Kitts and Nevis', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (180, N'Saint Lucia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (181, N'Saint Pierre and Miquelon', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (182, N'Saint Vincent and The Grenadines', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (183, N'Samoa', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (184, N'San Marino', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (185, N'Sao Tome and Principe', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (186, N'Saudi Arabia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (187, N'Senegal', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (188, N'Serbia and Montenegro', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (189, N'Seychelles', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (190, N'Sierra Leone', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (191, N'Singapore', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (192, N'Slovakia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (193, N'Slovenia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (194, N'Solomon Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (195, N'Somalia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (196, N'South Africa', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (197, N'South Georgia and The South Sandwich Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (198, N'Spain', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (199, N'Sri Lanka', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (200, N'Sudan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (201, N'Suriname', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (202, N'Svalbard and Jan Mayen', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (203, N'Swaziland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (204, N'Sweden', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (205, N'Switzerland', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (206, N'Syrian Arab Republic', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (207, N'Taiwan, Province of China', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (208, N'Tajikistan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (209, N'Tanzania, United Republic of', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (210, N'Thailand', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (211, N'Timor-leste', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (212, N'Togo', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (213, N'Tokelau', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (214, N'Tonga', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (215, N'Trinidad and Tobago', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (216, N'Tunisia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (217, N'Turkey', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (218, N'Turkmenistan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (219, N'Turks and Caicos Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (220, N'Tuvalu', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (221, N'Uganda', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (222, N'Ukraine', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (223, N'United Arab Emirates', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (224, N'United Kingdom', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (225, N'United States', 1)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (226, N'United States Minor Outlying Islands', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (227, N'Uruguay', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (228, N'Uzbekistan', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (229, N'Vanuatu', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (230, N'Venezuela', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (231, N'Viet Nam', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (232, N'Virgin Islands, British', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (233, N'Virgin Islands, U.S.', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (234, N'Wallis and Futuna', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (235, N'Western Sahara', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (236, N'Yemen', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (237, N'Zambia', 0)
INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (238, N'Zimbabwe', 0)

-- Add 99 rows to [dbo].[Gen_State_S]
SET IDENTITY_INSERT [dbo].[Gen_State_S] ON
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1, 225, N'AL', N'Alabama ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2, 225, N'AK', N'Alaska ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (3, 225, N'AZ', N'Arizona ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (4, 225, N'AR', N'Arkansas ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (5, 225, N'CA', N'California ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (6, 225, N'CO', N'Colorado ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (7, 225, N'CT', N'Connecticut ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (8, 225, N'DE', N'Delaware ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (9, 225, N'DC', N'District of Columbia ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (10, 225, N'FL', N'Florida ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (11, 225, N'GA', N'Georgia ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (12, 225, N'GU', N'Guam ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (13, 225, N'HI', N'Hawaii ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (14, 225, N'ID', N'Idaho ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (15, 225, N'IL', N'Illinois ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (16, 225, N'IN', N'Indiana ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (17, 225, N'IA', N'Iowa ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (18, 225, N'KS', N'Kansas ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (19, 225, N'KY', N'Kentucky ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (20, 225, N'LA', N'Louisiana ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (21, 225, N'ME', N'Maine ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (22, 225, N'MD', N'Maryland ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (23, 225, N'MA', N'Massachusetts ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (24, 225, N'MI', N'Michigan ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (25, 225, N'MN', N'Minnesota ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (26, 225, N'MS', N'Mississippi ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (27, 225, N'MO', N'Missouri ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (28, 225, N'MT', N'Montana ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (29, 225, N'NE', N'Nebraska ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (30, 225, N'NV', N'Nevada ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (31, 225, N'NH', N'New Hampshire ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (32, 225, N'NJ', N'New Jersey ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (33, 225, N'NM', N'New Mexico ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (34, 225, N'NY', N'New York ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (35, 225, N'NC', N'North Carolina ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (36, 225, N'ND', N'North Dakota ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (37, 225, N'OH', N'Ohio ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (38, 225, N'OK', N'Oklahoma ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (39, 225, N'OR', N'Oregon ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (40, 225, N'PA', N'Pennyslvania ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (41, 225, N'PR', N'Puerto Rico ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (42, 225, N'RI', N'Rhode Island ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (43, 225, N'SC', N'South Carolina ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (44, 225, N'SD', N'South Dakota ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (45, 225, N'TN', N'Tennessee ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (46, 225, N'TX', N'Texas ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (47, 225, N'UT', N'Utah ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (48, 225, N'VT', N'Vermont ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (49, 225, N'VA', N'Virginia ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (50, 225, N'VI', N'Virgin Islands ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (51, 225, N'WA', N'Washington ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (52, 225, N'WV', N'West Virginia ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (53, 225, N'WI', N'Wisconsin ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (54, 225, N'WY', N'Wyoming ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (55, 38, N'AB', N'Alberta ', 1)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (56, 38, N'BC', N'British Columbia ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (57, 38, N'MB', N'Manitoba ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (58, 38, N'NB', N'New Brunswick ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (59, 38, N'NL', N'Newfoundland and Labrador ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (60, 38, N'NT', N'Northwest Territories ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (61, 38, N'NS', N'Nova Scotia ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (62, 38, N'NU', N'Nunavut ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (63, 38, N'PE', N'Prince Edward Island ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (64, 38, N'SK', N'Saskatchewan ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (65, 38, N'ON', N'Ontario ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (66, 38, N'QC', N'Quebec ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (67, 38, N'YT', N'Yukon ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (68, 137, N'AGS', N'Aguascalientes ', 1)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (69, 137, N'BCN', N'Baja California Norte ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (70, 137, N'BCS', N'Baja California Sur ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (71, 137, N'CAM', N'Campeche ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (72, 137, N'CHIS', N'Chiapas ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (73, 137, N'CHIH', N'Chihuahua ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (74, 137, N'COAH', N'Coahuila ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (75, 137, N'COL', N'Colima ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (76, 137, N'DF', N'Distrito Federal ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (77, 137, N'DGO', N'Durango ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (78, 137, N'GTO', N'Guanajuato ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (79, 137, N'GRO', N'Guerrero ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (80, 137, N'HGO', N'Hidalgo ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (81, 137, N'JAL', N'Jalisco ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (82, 137, N'EDM', N'México - Estado de ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (83, 137, N'MICH', N'Michoacán ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (84, 137, N'MOR', N'Morelos ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (85, 137, N'NAY', N'Nayarit ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (86, 137, N'NL', N'Nuevo León ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (87, 137, N'OAX', N'Oaxaca ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (88, 137, N'PUE', N'Puebla ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (89, 137, N'QRO', N'Querétaro ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (90, 137, N'QROO', N'Quintana Roo ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (91, 137, N'SLP', N'San Luis Potosí ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (92, 137, N'SIN', N'Sinaloa ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (93, 137, N'SON', N'Sonora ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (94, 137, N'TAB', N'Tabasco ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (95, 137, N'TAMPS', N'Tamaulipas ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (96, 137, N'TLAX', N'Tlaxcala ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (97, 137, N'VER', N'Veracruz ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (98, 137, N'YUC', N'Yucatán ', 0)
INSERT INTO [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (99, 137, N'ZAC', N'Zacatecas ', 0)
SET IDENTITY_INSERT [dbo].[Gen_State_S] OFF

Update Usr_List_D set [DateFormat]='MM/dd/yyyy',TimeFormat = 1, TimeZoneDisplay = 1,enableAV = 0,enableparticipants = 0 
Update Usr_Inactive_D set [DateFormat]='MM/dd/yyyy',TimeFormat = 1, TimeZoneDisplay = 1,enableAV = 0,enableparticipants = 0 

COMMIT TRANSACTION
GO

-- Reseed identity on [dbo].[Gen_State_S]
DBCC CHECKIDENT('[dbo].[Gen_State_S]', RESEED, 99)
GO

/* Default data to MailServer_D */

INSERT INTO [dbo].[Sys_MailServer_D]
           ([IsRemoteServer]
           ,[ServerAddress]
           ,[Login]
           ,[password]
           ,[portNo]
           ,[ConTimeOut]
           ,[CompanyMail]
           ,[displayname]
           ,[messagetemplate]
           ,[websiteURL]
           ,[Id]
           )
VALUES
           (1
           ,'127.0.0.1'
           ,''
           ,''
           ,25
           ,10
           ,'support1@myvrm1.com'
           ,'myVRM'
           ,''
           ,'http://localhost'
	   ,1
           )

update mcu_vendor_s set name = 'Polycom RMX 2000' where id = 8


update [dbo].[Usr_Roles_D] set [roleMenuMask] = '6*63-13*7232+3*0+2*0+3*0+8*0+2*0+2*0+2*0-6*14' where [roleID] = 1
update [dbo].[Usr_List_D] set [MenuMask] = '6*63-13*7232+3*0+2*0+3*0+8*0+2*0+2*0+2*0-6*14' where [roleID] = 1