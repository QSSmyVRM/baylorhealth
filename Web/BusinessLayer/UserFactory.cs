//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using NHibernate;
using NHibernate.Criterion;
using System.Reflection; //ZD 101026
using System.Xml.Linq;
using System.Linq;
using System.DirectoryServices;
using log4net;
using System.DirectoryServices.Protocols;//101812

using myVRM.DataLayer;
using NS_OPERATIONS; //ZD 104116
using NS_MESSENGER; //ZD 104116

using System.Data;
using System.Text.RegularExpressions;//ZD 103635

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Data Layer Logic for loading/saving Reports(Templates/Schedule)
    /// </summary>
    /// 
    #region enum WebService Client Defination

    //01 - Outlook York
    //02 - Outlook Generic
    //03 - Lotus York 
    //04 - Lotus Generic
    //05 - Ipad - AppleDevices
    //06 - Other mobile devices
    //07 - Exchange - EWS

    public enum clientFrom { None = 0, OutlookYork, OutlookGeneric, LotusYork, LotusGeneric, AppleDevices, AndroidDevices, Exchange }; //FB 1899, 1900

    public enum userOrigin { Web = 1, Outlook }; //ZD 102052

    public enum bulkoperations { DateFormat = 9, TimeFormat, Timezonedisplay, enableAV, enableDomino, enableExchange, enableMobile, enableWebexUser, enableAdditionalOption, EnableAVWO, EnableCateringWO, EnableFacilityWO }; //ZD 104204
    public enum bulkoperationslicnese { DominoUserLimit = 13, ExchangeUserLimit, MobileUserLimit, WebexUserLimit }; //ZD 104204
    public enum bulkoperationLicenseserror { errcode461 = 13, errcode460 = 14, errcode526 = 15, errcode718 = 16, errcode766 = 18 }; //ZD 104204 //ALLBUGS-40
    

    #endregion
    #region UserFactory

    public class UserFactory
    {
        #region Private Members

        private static int m_PAGE_SIZE = 20;

        private static int m_isUser = 1;
        private static int m_isLDAP = 2;
        private static int m_isGustUser = 3;//FB 2027
        private static int m_isInActiveUser = 4;//FB 2027
        private static int CREATENEW = 0;  // create a new user FB 2027S
        private static int MODIFYOLD = 1;  // modify an existing user FB 2027S
        private static int TOTALTIMEFORNEWUSER = 100000; // FB 2027S

        private userDAO m_userDAO;
        private deptDAO m_deptDAO;
        private GeneralDAO m_generalDAO; //NewLobby
        private conferenceDAO m_confDAO; //FB 2027
        //FB 2027 Start
        private ILanguageDAO m_langDAO;
        private IDeptDao m_IdeptDAO;
        private hardwareDAO m_hardwareDAO;
        private IUserLotusDao m_userlotusDAO;
        private IMCUDao m_ImcuDao;
        private SystemDAO m_systemDAO;//FB 2027
        private ISystemDAO m_IsystemDAO;//FB 2027
        private IEptDao m_IeptDao;
        //FB 2027 End
        private IUserDao m_IuserDao;
        private IGuestUserDao m_IGuestUserDao;
        private IInactiveUserDao m_IinactiveUserDao;
        private IVRMLdapUserDao m_IldapDao;
        private IUserRolesDao m_IUserRolesDao;
        private IUserDeptDao m_IuserDeptDAO;
        private IUserTemplateDao m_ItempDao;
        private IGrpDetailDao m_IGrpDetailDao;
        private IGrpParticipantsDao m_IGrpParticipantsDao;
        private IUserLobbyDAO m_IUserLobbyDao; //NewLobby
        private IIconsRefDAO m_IIconsRefDAO;//NewLobby
        private IConfUserDAO m_IConfUserDAO; //FB 2027
        private IConferenceDAO m_IConferenceDAO; //FB 2027
        private ILDAPGroupsDAO m_ILDAPGroupsDAO; //ZD 101525
        private ILDAPConfigDAO m_ILDAPConfigDAO; //ZD 101525
        private IUserDeleteListDAO m_UserDeleteListDAO; //ZD 103878

        private myVRMSearch m_Search;
        private static log4net.ILog m_log;
        private string m_configPath;

        private int m_iMaxRecords;
        private int m_iMaxUsrRecords;//ZD 101525
        private int m_userType;
        private int isAudioAddOn = 2; //FB 2023

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        private int organizationID = 0;
        private int multiDepts = 1;
        private emailFactory m_emailFactory;// FB 1860
        private myVRMException myvrmEx; //FB 1881
        private ns_SqlHelper.SqlHelper m_rptLayer = null; //FB 1497

        //FB 1959 - Start
        private IRoomDAO m_IRoomDAO;
        private LocationDAO m_locDAO;
        //FB 1959 - End
        private ILanguageTextDao m_ILanguagetext;//FB 1830 - Translation
        //FB 2027 - Starts
        private ILocApprovDAO m_ILocApprovDAO;
        private ISysApproverDAO m_ISysApproverDAO;
        private IAccountGroupListDAO m_IGroupAccountDAO;
        private ITempUserDAO m_TempUserDAO;
        private IMCUApproverDao m_IMCUapprover;
        private IUserAccountDao m_IuserAccountDAO;
        private IOrgDAO m_IOrgDAO;
        private ISysTechDAO m_ISysTechDAO;
        private IEmailLanguageDao m_IEmailLanguageDAO;
        //FB 2027 - End
        private IUserPCDAO m_IUserPCDAO; //FB 2693 
        private UtilFactory m_UtilFactory; //FB 2721

        // ZD 100157 Start  
        XmlWriter xWriter = null;
        XmlWriterSettings xSettings = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        StringBuilder OUTXML = null;
        // ZD 100157 End

        private string macID = "";//ZD 100263
        cryptography.Crypto crypto = null; //ZD 100263
        //ZD 100152 starts

        private IUserTokenDAO m_IUserTokenDAO;

        XmlWriter _xWriter = null;
        XmlWriterSettings _xSettings = null;
        StringBuilder _OUTXML = null;
        //ZD 100152 Ends

        private HardwareFactory m_HardwareFactory; //ZD 101026
        private WorkOrderFactory m_WorkOrderFactory; //ZD 101443
        private IEmailDomainDAO m_IEmailDomainDAO;// ZD 101865

        string outRTCXML = "", msg = "", BJNxml = "", configPath = "";//ZD 104116
        VRMRTC.VRMRTC RTCobj = null; //ZD 104116
        
        #endregion

        //ZD 101443 Start
        #region UserFilterType

        private class UserFilterType
        {
            public const int Conferences = 1;
            public const int Rooms = 2;
            public const int MCUs = 3;
            public const int AV = 4;
            public const int Menu = 5;
            public const int Facilities = 6;
            public const int AVWO = 7;
            public const int MenuWO = 8;
            public const int FacilitiesWO = 9;
        }
        #endregion
        //ZD 101443 End
        //ZD 101525
        #region vrmSortBy
        private class vrmSortBy
        {
            public const int UniqueID = 1;
            public const int ConfName = 2;
            public const int SiloName = 3;
            public const int ConfDate = 4;
        }
        #endregion

        #region User Factory
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        /// 
        public UserFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;

                m_userDAO = new userDAO(obj.ConfigPath, obj.log);
                m_deptDAO = new deptDAO(obj.ConfigPath, obj.log);
                m_Search = new myVRMSearch(obj.ConfigPath, obj.log);
                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log); //FB 2027
                m_generalDAO = new GeneralDAO(m_configPath, m_log); //NewLobby

                m_IuserDao = m_userDAO.GetUserDao();
                m_IGuestUserDao = m_userDAO.GetGuestUserDao();
                m_IinactiveUserDao = m_userDAO.GetInactiveUserDao();
                m_IldapDao = m_userDAO.GetLDAPUserDao();
                m_IUserRolesDao = m_userDAO.GetUserRolesDao();
                m_ItempDao = m_userDAO.GetUserTemplateDao();
                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao();
                m_IGrpDetailDao = m_userDAO.GetGrpDetailDao();
                m_IGrpParticipantsDao = m_userDAO.GetGrpParticipantDAO();
                m_IConferenceDAO = m_confDAO.GetConferenceDao(); //FB 2027
                m_IConfUserDAO = m_confDAO.GetConfUserDao(); //FB 2027
                m_UserDeleteListDAO = m_userDAO.GetUserDeleteListDAO();//ZD 103878
                m_OrgDAO = new orgDAO(obj.ConfigPath, obj.log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();

                m_userType = vrmUserConstant.TYPE_USER;
                m_iMaxRecords = 10;
                m_iMaxUsrRecords = 20;//ZD 101525

                //FB 1959 - Start
                m_locDAO = new LocationDAO(m_configPath, obj.log);
                m_IRoomDAO = m_locDAO.GetRoomDAO();
                m_IuserDao = m_userDAO.GetUserDao();
                //FB 1959 - End
                m_ILanguagetext = m_userDAO.GetLanguageTextDao();//FB 1830 - Translation

                m_IUserLobbyDao = m_userDAO.GetUserLobbyIconsDAO(); //NewLobby
                m_IIconsRefDAO = m_generalDAO.GetIconRefDAO(); //NewLobby
                //FB 2027 Start
                m_generalDAO = new GeneralDAO(m_configPath, m_log);
                m_langDAO = m_generalDAO.GetLanguageDAO();
                m_IdeptDAO = m_deptDAO.GetDeptDao();
                m_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);
                m_ImcuDao = m_hardwareDAO.GetMCUDao();
                m_userlotusDAO = m_userDAO.GetUserLotusDao();
                m_ILocApprovDAO = m_locDAO.GetLocApprovDAO();
                m_ISysApproverDAO = m_OrgDAO.GetSysApproverDao();
                m_IGroupAccountDAO = m_userDAO.GetGroupAccountDao();
                m_IMCUapprover = m_hardwareDAO.GetMCUApproverDao();
                m_IuserAccountDAO = m_userDAO.GetUserAccountDao();
                m_TempUserDAO = m_confDAO.GetTempUserDAO();
                m_systemDAO = new SystemDAO(m_configPath, m_log);//FB 2027
                m_IsystemDAO = m_systemDAO.GetSystemDao();//FB 2027
                m_IOrgDAO = m_OrgDAO.GetOrgDao();
                m_ISysTechDAO = m_OrgDAO.GetSysTechDao();
                m_IeptDao = m_hardwareDAO.GetEptDao();
                m_confDAO = new conferenceDAO(m_configPath, m_log);
                m_IEmailLanguageDAO = m_confDAO.GetEmailLanguageDao();
                //FB 2027 End
                m_IUserPCDAO = m_userDAO.GetUserPCDAO(); //FB 2693
                m_UtilFactory = new UtilFactory(ref obj); //FB 2721
                m_IUserTokenDAO = m_userDAO.GetUserTokenDAO(); //ZD 100152 
                m_HardwareFactory = new HardwareFactory(ref obj); //ZD 101026
                m_WorkOrderFactory = new WorkOrderFactory(ref obj); //ZD 101433
                m_ILDAPConfigDAO = m_OrgDAO.GetLDAPConfigDao();  //ZD 101525
                m_ILDAPGroupsDAO = m_OrgDAO.GetLDAPGroupDao(); //ZD 101525
                m_IEmailDomainDAO = m_OrgDAO.GetEmailDomainDAO(); //ZD 101865

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        public UserFactory()
        {
            //Default Constructor
        }

        #endregion

        #region GetMultipleUsers
        public bool GetMultipleUsers(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/alphabet");
                string alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                string pageno = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/sortBy");
                string field = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/searchFor");
                string searchFor = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (field == "1")
                    field = "FirstName";
                else if (field == "3")
                    field = "Login";
                else if (field == "4")
                    field = "Email";
                else
                    field = "LastName"; // Also default

                obj.outXml = "<getMultipleUsers>";

                List<vrmUserTemplate> templateList = new List<vrmUserTemplate>();
                List<vrmLDAPUser> userList = new List<vrmLDAPUser>();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes
                criterionList.Add(Expression.Eq("deleted", 0)); //Fixed during Organization Module

                templateList = m_ItempDao.GetByCriteria(criterionList);

                obj.outXml += "<templates>";
                if (templateList.Count > 0)
                {
                    foreach (vrmUserTemplate temp in templateList)
                    {
                        obj.outXml += "<template>";
                        obj.outXml += "<ID>" + temp.id.ToString() + "</ID>";
                        obj.outXml += "<name>" + temp.name + "</name>";
                        obj.outXml += "<role>" + temp.roleId.ToString() + "</role>";
                        obj.outXml += "<language>" + temp.languageId.ToString() + "</language>";
                        obj.outXml += "</template>";
                    }
                }
                obj.outXml += "</templates>";
                m_userDAO.pageSize = 20;
                m_userDAO.pageNo = int.Parse(pageno);
                m_userDAO.pageEnabled = true;

                int MaxRecords = m_userDAO.pageSize;
                int MaxSearchRecords = 0;

                obj.outXml += "<users>";
                if (!GetLDAPUserList(userID, alphabet, field, searchFor, ref MaxRecords, ref MaxSearchRecords, ref userList))
                {
                    m_log.Error("Error in get user list");
                    obj.outXml = "";
                    return false;
                }

                foreach (vrmLDAPUser lUser in userList)
                {
                    obj.outXml += "<user>";
                    obj.outXml += "<userID>" + lUser.userid.ToString() + "</userID>";
                    obj.outXml += "<firstName>" + lUser.FirstName + "</firstName>";
                    obj.outXml += "<lastName>" + lUser.LastName + "</lastName>";
                    obj.outXml += "<login>" + lUser.Login + "</login>";
                    obj.outXml += "<email>" + lUser.Email + "</email>";
                    obj.outXml += "<telephone>" + lUser.Telephone + "</telephone>";
                    obj.outXml += "</user>";
                }

                obj.outXml += "</users>";

                int iMaxPage = 0;

                iMaxPage = MaxSearchRecords / m_userDAO.pageSize;

                // and modulus remainder...
                if (MaxSearchRecords % m_userDAO.pageSize > 0)
                    iMaxPage++;
                if (iMaxPage == 0)
                    iMaxPage = 1;

                obj.outXml += "<pageNo>" + pageno + "</pageNo>";
                obj.outXml += "<totalPages>" + string.Format("{0:d}", iMaxPage) + "</totalPages>";
                obj.outXml += "<totalNumber>" + string.Format("{0:d}", MaxRecords) + "</totalNumber>";

                obj.outXml += "</getMultipleUsers>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region DeleteLDAPUser
        public bool DeleteLDAPUser(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                int LDAPID = 0;
                node = xd.SelectSingleNode("//login/LDAPUserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out LDAPID);

                vrmLDAPUser user = new vrmLDAPUser();
                user.userid = LDAPID;
                m_IldapDao.Delete(user);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SetMultipleUsers
        public bool SetMultipleUsers(ref vrmDataObject obj)
        {
            bool bRet = true, ret = false;
            cryptography.Crypto cPwd = null; //ZD 100263
            ArrayList arrDuplicateUsr = new ArrayList(); //Added for LDAP 
            string firstName = "", lastName = "", loginName = "", emailId = "", telephone = "", UserDomain = "", sPwd = "";
            string testLogin = "", testEmail = "",uName="",strDup="";
            int userCount = 0, sID = 0, epid = 0, id = 0, testOrgId = 0;
            DateTime accountExpiry = DateTime.UtcNow;

            vrmUserTemplate template = new vrmUserTemplate();
            vrmUserRoles roles = new vrmUserRoles();
            vrmLDAPUser vrmLDAP = new vrmLDAPUser();
            vrmUser user = new vrmUser();
             vrmUserDepartment ud  = new vrmUserDepartment();

            List<ICriterion> criUsrList = new List<ICriterion>();
            List<vrmUser> results = null;
            List<vrmLDAPUser> LDAPRecords = new List<vrmLDAPUser>();
            List<vrmUser> UserRecords = new List<vrmUser>();
            List<ICriterion> criterionList = new List<ICriterion>();

            hardwareDAO m_hardwareDAO = new hardwareDAO(m_configPath, m_log);
            IEptDao eptDao = m_hardwareDAO.GetEptDao();
            try
            {

                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                //ZD 104116 Starts
                if (RTCobj == null)
                    RTCobj = new VRMRTC.VRMRTC();

                NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                ret = RTCobj.LoadConfigParams(configPath, ref configParams);
                //ZD 104116 Ends


                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                int userTemplateID = 0;
                node = xd.SelectSingleNode("//login/userTemplateID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userTemplateID);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                XmlElement root = xd.DocumentElement;
                XmlNodeList elemList = root.SelectNodes(@"/login/users/userID");
                ArrayList ldapID = new ArrayList();

                //Edited for LDAP Browser START
                XmlNodeList usrList = root.SelectNodes(@"/login/users/user");

                foreach (XmlNode xlNde in usrList)
                {
                    node = xlNde.SelectSingleNode("FirstName");
                    firstName = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("LastName");
                    lastName = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("Login");
                    loginName = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("Email");
                    emailId = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("Telephone");
                    telephone = node.InnerXml.Trim();
                    node = xlNde.SelectSingleNode("userDomain");//Zd 103784
                    UserDomain = node.InnerXml.Trim();//Zd 103784
                    //New LDAP Design
                    sPwd = "";
                    if (xlNde.SelectSingleNode("Password") != null)
                    {
                        node = xlNde.SelectSingleNode("Password");
                        cPwd = new cryptography.Crypto();

                        sPwd = cPwd.encrypt(node.InnerXml.Trim());
                    }
                    //Edited for LDAP Browser END

                    //START

                    //New LDAP Design Start
                    template = new vrmUserTemplate();
                    roles = new vrmUserRoles();

                    template = m_ItempDao.GetById(userTemplateID);
                    organizationID = template.companyId;
                    //New LDAP Design End
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes
                    criUsrList.Add(Expression.Eq("Deleted", 0)); //Fixed during Organization Module

                    results = m_IuserDao.GetByCriteria(criUsrList);

                    userCount = 0;

                    userCount = results.Count;
                    //m_userDAO.GetUserCount(ref userCount, organizationID);
                    //END      
                    accountExpiry = sysSettings.ExpiryDate;

                    //New LDAP Design Start
                    //vrmUserTemplate template = new vrmUserTemplate();
                    //vrmUserRoles roles = new vrmUserRoles();

                    //template = m_ItempDao.GetById(int.Parse(userTemplateID));
                    //New LDAP Design End
                    if (template.id == 0)
                    {
                        myvrmEx = new myVRMException(476);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    roles = m_IUserRolesDao.GetById(template.roleId);
                    if (roles.roleID == 0)
                    {
                        myvrmEx = new myVRMException(477);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    //ZD 101525
                    if (sysSettings.IsLDAP == 1 || orgdt.UserLimit >= usrList.Count + userCount)
                    {
                        sID = 0;
                        for (int i = 0; i < elemList.Count; i++)
                        {
                            node = elemList[i];
                            int.TryParse(node.InnerXml, out sID);
                            ldapID.Add(sID);
                        }
                        LDAPRecords = new List<vrmLDAPUser>();
                        UserRecords = new List<vrmUser>();

                        criterionList = new List<ICriterion>();

                        epid = 0;
                        m_hardwareDAO = new hardwareDAO(m_configPath, m_log);
                        eptDao = m_hardwareDAO.GetEptDao();

                        //Edited for LDAP Browser START
                        vrmLDAP = new vrmLDAPUser();
                        vrmLDAP.FirstName = firstName;
                        vrmLDAP.LastName = lastName;
                        vrmLDAP.Email = emailId;
                        vrmLDAP.Login = loginName;
                        vrmLDAP.Telephone = telephone;
                        LDAPRecords.Add(vrmLDAP);
                        //Edited for LDAP Browser END

                        //if (m_userDAO.GetMultipleLDAP(ldapID, LDAPRecords))//Edited for LDAP Browser
                        //{

                        id = 0;

                        foreach (vrmLDAPUser ldap in LDAPRecords)
                        {
                            user = new vrmUser();

                            user.LastLogin = DateTime.Now;

                            user.userid = 0;
                            user.PreferedRoom = template.location;
                            user.DefLineRate = template.lineRateId;
                            user.DefVideoProtocol = template.videoProtocol;
                            user.connectionType = template.connectiontype;
                            user.IPISDNAddress = template.ipisdnaddress;
                            user.initialTime = template.initialtime;
                            user.Language = template.languageId;
                            user.BridgeID = template.bridgeId;
                            user.roleID = template.roleId;
                            user.emailmask = template.emailNotification;// FB 1592
                            //ZD 102052 START
                            user.enableParticipants = template.enableParticipants;
                            user.enableAV = template.enableAV;
                            user.enableAdditionalOption = template.enableAdditionalOption;
                            user.EnableAVWO = template.enableAVWO;
                            user.EnableCateringWO = template.enableCateringWO;
                            user.EnableFacilityWO = template.enableFacilityWO;
                            user.enableExchange = template.enableExchange;
                            user.enableDomino = template.enableDomino;
                            user.enableMobile = template.enableMobile;
                            //ZD 102052 END
                            //ZD 100318
                            user.PerCalStartTime = DateTime.Today;
                            user.PerCalEndTime = DateTime.Today;
                            user.RoomCalStartTime = DateTime.Today;
                            user.RoomCalEndime = DateTime.Today;

                            //ZD 100318

                            if (template.expirationDate <= DateTime.Today)//New LDAP Design
                            {
                                myvrmEx = new myVRMException(429);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            else
                                user.accountexpiry = template.expirationDate;

                            user.PasswordTime = DateTime.UtcNow; //ZD 100781
                            user.IsPasswordReset = 0; //ZD 100781
                            user.MenuMask = roles.roleMenuMask;
                            user.Admin = roles.level;
                            user.searchId = -1; // default value (FB 381)

                            user.EndPoint.linerateid = template.lineRateId;
                            user.EndPoint.protocol = template.videoProtocol;
                            user.EndPoint.connectiontype = template.connectiontype;
                            user.EndPoint.address = template.ipisdnaddress;
                            user.EndPoint.bridgeid = template.bridgeId;
                            user.EndPoint.profileId = 0; // no profile
                            user.EndPoint.isDefault = 0; // no default

                            if (template.timezone == 0)
                                user.TimeZone = sysSettings.TimeZone;
                            else
                                user.TimeZone = template.timezone;

                            user.roleID = roles.roleID;
                            user.MenuMask = roles.roleMenuMask;
                            user.UserRolename = roles.roleName; //ZD 103405 
                            user.Admin = roles.level;
                            //user.EndPoint.endpointid = epid++;
                            // we do it this way for now. (endpoint added as a seperate transaction)
                            user.EndPoint.endpointid = 0;
                            user.FirstName = ldap.FirstName;
                            user.LastName = ldap.LastName;
                            user.Email = ldap.Email;
                            user.Telephone = ldap.Telephone;
                            user.Login = ldap.Login;
                            user.EndPoint.name = user.LastName;

                            //Edited for LDAP Browser START
                            //user.DateFormat = "MM/dd/yyyy"; //Default Format
                            //user.TimeFormat = "1";
                            user.DateFormat = template.DateFormat;//ZD 103673
                            user.TimeFormat = template.TimeFormat;
                            user.Timezonedisplay = "1";
                            user.TimeZone = template.timezone;
                            user.initialTime = template.initialtime;
                            if (sPwd != "")
                            {
                                user.Password = sPwd;
                                sPwd = "";
                            }
                            user.companyId = template.companyId;
                            user.PreferedRoom = "0";//FB 2027 - Starts
                            user.TickerStatus = 1;
                            user.TickerStatus1 = 1;
                            user.TickerSpeed = 6;
                            user.TickerSpeed1 = 6;//FB 2027 - End
                            //Edited for LDAP Browser END

                            //FB 2320 - Starts
                            user.Company = "";
                            user.AlternativeEmail = "";
                            user.TickerBackground = "#3399ff";
                            user.TickerBackground1 = "#ffff99";
                            user.Audioaddon = "0";
                            user.RSSFeedLink = "";
                            user.RSSFeedLink1 = "";
                            user.WorkPhone = "";
                            user.CellPhone = "";
                            user.ConferenceCode = "";
                            user.LeaderPin = "";
                            user.ParticipantCode = ""; //ZD 101446
                            user.InternalVideoNumber = "";
                            user.ExternalVideoNumber = "";
                            user.HelpReqEmailID = "";
                            user.HelpReqPhone = "";
                            //FB 2320 - End
                            user.UserDomain = UserDomain;//Zd 103784
                            testLogin = user.Login;
                            testEmail = user.Email;
                            testOrgId = user.companyId;

                            //ZD 104116 Starts
                            if (sysSettings.MaxBlueJeansUsers == -2)
                            {
                                user.EnableBJNConf = 1;
                                user.BJNUserEmail = ldap.Email;
                            }
                            //ZD 104116 Ends

                            // first check user and guest table for this login name OR email
                            // if found DO NOT ADD, log error and continue

                            //FB 2119 Start
                            m_userType = m_isLDAP;
                            crypto = new cryptography.Crypto();
                            user.Password = crypto.encrypt("pwd");
                            //FB 2119 End

                            // cant delete this one, it is in use....
                            if (!checkUser(0, testLogin, testEmail, testOrgId, UserDomain))
                            {
                                myVRMException e = new myVRMException("Error user name " + user.Login + " or email " + user.Email + " already exist as either user or guest");
                                m_log.Error(e.Message);
                                uName = ""; //FB 1623, 1624
                                uName = user.FirstName + " " + user.LastName;
                                if (uName.Trim() == "")
                                    uName = user.Email;

                                arrDuplicateUsr.Add(uName); //Added for LDAP
                            }
                            else
                            {
                                if (id == 0)
                                    id = m_userDAO.GetID();
                                else
                                    id++;
                                // set  up endpoiont, find max endpointid....
                                user.EndPoint.uId = 0;
                                if (epid == 0)
                                {
                                    if (epid == 0)
                                    {
                                        eptDao.addProjection(Projections.Max("endpointid"));
                                        IList maxId = eptDao.GetObjectByCriteria(new List<ICriterion>());
                                        if (maxId[0] != null)
                                            epid = ((int)maxId[0]) + 1;
                                        else
                                            epid = 1;
                                        eptDao.clearProjection();
                                    }
                                }
                                else
                                    epid++;
                                user.userid = id;
                                user.newUser = 1;
                                user.endpointId = epid;
                                user.EndPoint.orgId = user.companyId;//ZD 100288
                                user.EndPoint.endpointid = epid;
                                UserRecords.Add(user);
                                ldap.newUser = 0;
                            }
                        }
                        //Edited for LDAP Browser START                        
                        //if (!m_userDAO.SetMutipleUsers(UserRecords, LDAPRecords))
                        if (!m_userDAO.SetMutipleLDAPUsers(UserRecords))
                        {
                            myvrmEx = new myVRMException(509);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //else // for now we have to set the endpoints manually when we shit to .net we
                        //// will implement this through or mapping
                        //{
                        //Edited for LDAP Browser END



                        foreach (vrmUser ur in UserRecords)
                        {
                            ur.endpointId = ur.EndPoint.endpointid;
                            eptDao.Save(ur.EndPoint);
                            //ZD 102052 Starts	
                            if (template.deptId != "")
                            {
                                ud = new vrmUserDepartment();
                                string[] deptId = template.deptId.Split(',');
                                for (int d = 0; d < deptId.Length; d++)
                                {
                                    ud.userId = ur.userid;
                                    ud.departmentId = Convert.ToInt16(deptId[d]);
                                    m_IuserDeptDAO.Save(ud);
                                }
                            }
                            //ZD 102052 End

                            //ZD 104116 Starts
                            if (ur.EnableBJNConf == 1 && !string.IsNullOrEmpty(ur.BJNUserEmail))
                            {
                                if (ret)
                                {
                                    BJNxml = "<GetUserDetails><UserID>" + ur.userid.ToString() + "</UserID><organizationID>" + ur.companyId.ToString() + "</organizationID><BJNUserEmail>" + ur.BJNUserEmail + "</BJNUserEmail></GetUserDetails>";

                                    NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);
                                    ops.GetUserBJNDetails(BJNxml, ref outRTCXML, ref msg);

                                    obj.outXml = msg;
                                }
                            }
                            //ZD 104116 Ends
                        }
                        if (arrDuplicateUsr.Count > 0) //Added for LDAP
                        {
                            strDup = "";
                            obj.outXml = "<error><message>";
                            foreach (string str in arrDuplicateUsr)
                            {
                                if (strDup == "")
                                {
                                    obj.outXml += "User's email address or login (" + str; //FB 1921
                                    strDup = "1";
                                }
                                else
                                    obj.outXml += "," + str;

                            }

                            obj.outXml += ") already exists either as Active/Inactive/Guest user in same/other organization</message></error>"; //Added for LDAP
                            bRet = false;

                        }
                    }
                    else if (sysSettings.IsLDAP == 0) //ZD 101443
                    {
                        throw new myVRMException(252);
                    }
                } //Added for LDAP Browser

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region UserSearch
        private bool UserSearch(string alphabet, string field, string search,
            ref List<ICriterion> criterionList)
        {
            try
            {
                if (search.Length > 0)
                {
                    search = search.ToLower();
                    ICriterion criterium;
                    criterium = Expression.Or(
                         Expression.Like("FirstName", search + "%%"),
                         Expression.Like("LastName", search + "%%"));
                    criterium = Expression.Or(criterium, Expression.Like("Email", search + "%%"));
                    criterium = Expression.Or(criterium, Expression.Like("Login", search + "%%"));
                    criterionList.Add(criterium);
                }
                else
                {
                    if (alphabet.Length > 0)
                        if (alphabet == "a")
                            criterionList.Add(Expression.Le(field, alphabet));
                        else
                            if (alphabet.ToLower() != "all")
                                criterionList.Add(Expression.Like(field, alphabet.ToLower() + "%%"));
                }
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        #endregion

        #region GetUserList
        private bool GetUserList(string userID, string alphabet, string field, string search,
           ref int MaxRecords, ref List<vrmUser> userList, string BulkUser)//ZD 100263
        {
            List<ICriterion> criterionList = new List<ICriterion>();

            try
            {
                IList Result = new ArrayList();

                criterionList.Add(Expression.Eq("userType", m_userType));
                UserSearch(alphabet, field, search, ref criterionList);
                m_IuserDao.addProjection(Projections.RowCount());

                IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                int.TryParse(list[0].ToString(), out MaxRecords);
                m_IuserDao.clearProjection();

                if (m_userDAO.maxResultsReturned > 0)
                {
                    m_IuserDao.pageNo(1);
                    m_IuserDao.pageSize(m_userDAO.maxResultsReturned);
                }

                userList = m_IuserDao.GetByCriteria(criterionList);
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        #endregion

        #region GetLDAPUserList
        private bool GetLDAPUserList(string userID, string alphabet, string field, string search,
                            ref int MaxRecords, ref int MaxSearchRecords, ref List<vrmLDAPUser> userList)
        {
            List<ICriterion> criterionList = new List<ICriterion>();

            try
            {
                IList Result = new ArrayList();

                criterionList.Add(Expression.Eq("newUser", 1));
                m_IldapDao.addProjection(Projections.RowCount());

                IList list = m_IldapDao.GetObjectByCriteria(criterionList);
                int.TryParse(list[0].ToString(), out MaxRecords);
                UserSearch(alphabet, field, search, ref criterionList);

                IList list_2 = m_IldapDao.GetObjectByCriteria(criterionList);
                int.TryParse(list_2[0].ToString(), out MaxSearchRecords);
                m_IldapDao.clearProjection();
                // FB 330 use user pageing informatioin (same change as in v18 branch AG 5/12/2008)
                m_IldapDao.pageNo(m_userDAO.pageNo);
                m_IldapDao.pageSize(m_userDAO.pageSize);
                //FB 509 sort order

                if (field == "FirstName")
                {
                    m_IldapDao.addOrderBy(Order.Asc("FirstName"));
                    m_IldapDao.addOrderBy(Order.Asc("LastName"));
                }
                else if (field == "Login")
                {
                    m_IldapDao.addOrderBy(Order.Asc("Login"));
                }
                else if (field == "Email")
                {
                    m_IldapDao.addOrderBy(Order.Asc("Email"));
                }
                else
                {
                    m_IldapDao.addOrderBy(Order.Asc("LastName"));
                    m_IldapDao.addOrderBy(Order.Asc("FirstName"));
                }

                userList = m_IldapDao.GetByCriteria(criterionList);
                m_IldapDao.clearOrderBy();
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        #endregion

        #region GetUserTemplateList
        public bool GetUserTemplateList(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userID = 0;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                //Spanish
                vrmUser user = m_IuserDao.GetByUserId(userID);

                if (user == null)
                {
                    myvrmEx = new myVRMException(207);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes


                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("companyId", organizationID));

                List<vrmUserTemplate> vTlist = m_ItempDao.GetByCriteria(criterionList);
                obj.outXml += "<UserTemplates>";
                if (vTlist.Count > 0)
                {
                    foreach (vrmUserTemplate Temp in vTlist)
                    {
                        if (Temp.deleted == 0)
                        {
                            obj.outXml += "<Template>";
                            obj.outXml += "<ID>" + Temp.id.ToString() + "</ID>";
                            obj.outXml += "<Name>" + Temp.name + "</Name>";
                            obj.outXml += "<RoleID>" + Temp.roleId.ToString() + "</RoleID>";

                            vrmUserRoles rl = (m_IUserRolesDao.GetById(Temp.roleId));
                            obj.outXml += "<RoleName>" + m_UtilFactory.GetTranslatedText(user.Language, rl.roleName) + "</RoleName>"; //Spanish

                            obj.outXml += "</Template>";
                        }
                    }
                }
                obj.outXml += "</UserTemplates>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetUserTemplateDetails
        public bool GetUserTemplateDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                obj.outXml = string.Empty;
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();


                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes
                if (organizationID < 11)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //<!-- new means new template, if there is 
                //     any value then you need to return that information-->
                node = xd.SelectSingleNode("//login/userTemplateID");
                string userTemplateID = node.InnerXml.Trim();

                obj.outXml += "<UserTemplate>";

                vrmUserTemplate vT = new vrmUserTemplate();
                if (userTemplateID.ToLower().CompareTo("new") != 0)
                    if ((vT = m_ItempDao.GetById(int.Parse(userTemplateID))) == null)
                        return false;
                obj.outXml += "<templateID>" + vT.id.ToString() + "</templateID>";
                obj.outXml += "<templateName>" + vT.name + "</templateName>";
                obj.outXml += "<initialTime>" + vT.initialtime + "</initialTime>";
                obj.outXml += "<videoProtocol>" + vT.videoProtocol + "</videoProtocol>";
                obj.outXml += "<IPISDNAddress>" + vT.ipisdnaddress + "</IPISDNAddress>";
                obj.outXml += "<connectionType>" + vT.connectiontype + "</connectionType>";
                obj.outXml += "<roleID>" + vT.roleId.ToString() + "</roleID>";
                obj.outXml += "<location>" + vT.location + "</location>";
                obj.outXml += "<languageID>" + vT.languageId.ToString() + "</languageID>";
                obj.outXml += "<lineRateID>" + vT.lineRateId.ToString() + "</lineRateID>";
                obj.outXml += "<bridgeID>" + vT.bridgeId.ToString() + "</bridgeID>";
                obj.outXml += "<timezone>" + vT.timezone.ToString() + "</timezone>";
                obj.outXml += "<departmentID>" + vT.deptId + "</departmentID>";//ZD 102052
                obj.outXml += "<accountExpiry>" + vT.expirationDate.ToString("MM/dd/yyyy") + "</accountExpiry>";
                obj.outXml += "<licenseExpiry>" + sysSettings.ExpiryDate.ToString("MM/dd/yyyy") + "</licenseExpiry>";
                obj.outXml += "<EquipmentID>" + vT.equipmentId.ToString() + "</EquipmentID>";
                obj.outXml += "<OutsideNetwork>" + vT.outsideNetwork.ToString() + "</OutsideNetwork>";
                obj.outXml += "<EmailNotification>" + vT.emailNotification.ToString() + "</EmailNotification>";
                //ZD 102052 START
                obj.outXml += "<enableParticipants>" + vT.enableParticipants.ToString() + "</enableParticipants>";
                obj.outXml += "<enableAV>" + vT.enableAV.ToString() + "</enableAV>";
                obj.outXml += "<enableAdditionalOption>" + vT.enableAdditionalOption.ToString() + "</enableAdditionalOption>";
                obj.outXml += "<enableAVWorkOrder>" + vT.enableAVWO.ToString() + "</enableAVWorkOrder>";
                obj.outXml += "<enableCateringWO>" + vT.enableCateringWO.ToString() + "</enableCateringWO>";
                obj.outXml += "<enableFacilityWO>" + vT.enableFacilityWO.ToString() + "</enableFacilityWO>";
                obj.outXml += "<exchangeUser>" + vT.enableExchange.ToString() + "</exchangeUser>";
                obj.outXml += "<dominoUser>" + vT.enableDomino.ToString() + "</dominoUser>";
                obj.outXml += "<mobileUser>" + vT.enableMobile.ToString() + "</mobileUser>";
                //ZD 102052 END
                obj.outXml += "<dateFormat>" + vT.DateFormat.ToString() + "</dateFormat>";//ZD 103673
                obj.outXml += "<timeFormat>" + vT.TimeFormat.ToString() + "</timeFormat>";

                obj.outXml += "<locationList>";

                if (vT.location != "" || vT.location != null)//Code changed for room search
                    obj.outXml += "<selected>" + vT.location + "</selected>";
                else
                    obj.outXml += "<selected></selected>";

                obj.outXml += "<mode>0</mode>";

                obj.outXml += "</locationList>";
                //FB 2891 - Start
                //List<vrmUserRoles> roles = m_IUserRolesDao.GetAll();                
                obj.outXml += "<roles>";

                List<ICriterion> criterionList1 = new List<ICriterion>();
                List<ICriterion> criterionList2 = new List<ICriterion>();

                criterionList1.Add(Expression.Gt("DisplayOrder", 0));
                m_IUserRolesDao.addOrderBy(Order.Asc("DisplayOrder"));
                List<vrmUserRoles> usrRoles = m_IUserRolesDao.GetByCriteria(criterionList1);

                criterionList2.Add(Expression.Lt("DisplayOrder", 1));
                List<vrmUserRoles> usrRoles1 = m_IUserRolesDao.GetByCriteria(criterionList2);
                usrRoles.AddRange(usrRoles1);

                if (usrRoles.Count <= 0)
                {
                    m_log.Error("Error in getting role list");
                    obj.outXml = "";
                    return false;
                }

                foreach (vrmUserRoles uR in usrRoles)//FB 2891 - End
                {
                    obj.outXml += "<role>";
                    obj.outXml += "<ID>" + uR.roleID.ToString() + "</ID>";
                    obj.outXml += "<name>" + uR.roleName + "</name>";
                    obj.outXml += "<menuMask>" + uR.roleMenuMask + "</menuMask>";
                    obj.outXml += "<locked>" + uR.locked.ToString() + "</locked>";
                    obj.outXml += "<level>" + uR.level.ToString() + "</level>";
                    obj.outXml += "<active>1</active>";
                    obj.outXml += "</role>";
                }
                obj.outXml += "</roles>";

                IList languages = vrmGen.getLanguage();

                obj.outXml += "<languages>";
                foreach (vrmLanguage ln in languages)
                {
                    obj.outXml += "<language>";
                    obj.outXml += "<ID>" + ln.Id.ToString() + "</ID>";
                    obj.outXml += "<name>" + ln.Name + "</name>";
                    obj.outXml += "</language>";
                }
                obj.outXml += "</languages>";

                List<vrmLineRate> lineRate = vrmGen.getLineRate();

                obj.outXml += "<lineRate>";
                foreach (vrmLineRate lr in lineRate)
                {
                    obj.outXml += "<rate>";
                    obj.outXml += "<lineRateID>" + lr.Id.ToString() + "</lineRateID>";
                    obj.outXml += "<lineRateName>" + lr.LineRateType + "</lineRateName>";
                    obj.outXml += "</rate>";
                }
                obj.outXml += "</lineRate>";
                IMCUDao IMCUDao = new hardwareDAO(m_configPath, m_log).GetMCUDao();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                //criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmMCU> bridges = IMCUDao.GetByCriteria(criterionList);

                obj.outXml += "<bridges>";
                foreach (vrmMCU mcu in bridges)
                {
                    obj.outXml += "<bridge>";
                    obj.outXml += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    //FB 1920 starts
                    if (organizationID != 11)
                    {
                        if (mcu.isPublic.ToString() == "1")
                            mcu.BridgeName = mcu.BridgeName + "(*)";
                    }
                    // FB 1920 Ends
                    obj.outXml += "<name>" + mcu.BridgeName + "</name>";
                    obj.outXml += "</bridge>";
                }
                obj.outXml += "</bridges>";
                Hashtable timeZones = new Hashtable();

                if (!timeZone.GetAllTimeZone(ref timeZones))
                    return false;
                if (timeZones.Count <= 0)
                    return false;

                obj.outXml += "<timezones>";
                List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                foreach (timeZoneData time in timeZoneList)
                {
                    obj.outXml += "<timezone>";
                    obj.outXml += "<timezoneID>" + time.TimeZoneID.ToString() + "</timezoneID>";
                    obj.outXml += "<timezoneName>" + time.TimeZoneDiff.ToString() + " ";
                    obj.outXml += time.TimeZone + "</timezoneName>";
                    obj.outXml += "</timezone>";
                }
                obj.outXml += "</timezones>";

                obj.outXml += "</UserTemplate>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SetUserTemplate
        public bool SetUserTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                //ZD 102052
                int enableParticipants = 0, enableAV = 0, enableAdditionalOption = 0, enableAVWO = 0;
                int enableCateringWO = 0, enableFacilityWO = 0, enableExchange = 0, enableDomino = 0, enableMobile = 0;
                string dateformat = "", timeformat = "";//ZD 103673
                int initialTime = 0;
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                vrmUserTemplate template = new vrmUserTemplate();


                node = xd.SelectSingleNode("//SetUserTemplate/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                template.companyId = organizationID;


                node = xd.SelectSingleNode("//SetUserTemplate/login");
                string userID = node.InnerXml.Trim();



                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/UserTemplateID");
                string userTemplateID = node.InnerXml.Trim();
                if (userTemplateID.ToLower().Trim().CompareTo("new") == 0)
                    template.id = 0;
                else
                    template.id = int.Parse(userTemplateID);

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/TemplateName");
                template.name = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/InitialTime");
                if (node.InnerXml.Trim().Length > 0)                    
                    int.TryParse(node.InnerXml.Trim(), out initialTime);
                template.initialtime = initialTime;
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/VideoProtocol");
                if (node.InnerXml.Trim().Length > 0)
                    template.videoProtocol = Convert.ToInt16(node.InnerXml.Trim());
                template.ipisdnaddress = "";//ZD 102052
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/IPISDNAddress");
                if (node.InnerXml.Trim().Length > 0)
                    template.ipisdnaddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/ConnectionType");
                if (node.InnerXml.Trim().Length > 0)
                    template.connectiontype = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/Location");
                template.location = "";//ZD 102052
                if (node.InnerXml.Trim().Length > 0)
                    template.location = node.InnerXml.Trim();//int.Parse(node.InnerXml.Trim());code chaged for room search
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/LanguageID");
                if (node.InnerXml.Trim().Length > 0)
                    template.languageId = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/LineRateID");
                if (node.InnerXml.Trim().Length > 0)
                    template.lineRateId = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/BridgeID");
                if (node.InnerXml.Trim().Length > 0)
                    template.bridgeId = Convert.ToInt16(node.InnerXml.Trim());
                template.deptId = "";//ZD 102052
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/DepartmentID");
                if (node.InnerXml.Trim().Length > 0)
                    template.deptId = node.InnerXml.Trim();//ZD 102052
                int groupId = 0;
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/GroupID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out groupId);
                if (groupId == 0)
                    groupId = -1;
                template.groupId = groupId;
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/ExpirationDate");
                if (node.InnerXml.Trim().Length > 0)
                    template.expirationDate = DateTime.Parse(node.InnerXml.Trim());
                else
                    template.expirationDate = DateTime.Parse(sysSettings.ExpiryDate.ToString("MM/dd/yyyy"));//FB 1493

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/RoleID");
                if (node.InnerXml.Trim().Length > 0)
                    template.roleId = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/TimeZoneID");
                if (node.InnerXml.Trim().Length > 0)
                    template.timezone = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/EmailNotification");
                if (node.InnerXml.Trim().Length > 0)
                    template.emailNotification = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/OutsideNetwork");
                if (node.InnerXml.Trim().Length > 0)
                    template.outsideNetwork = Convert.ToInt16(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/EquipmentID");
                if (node.InnerXml.Trim().Length > 0)
                    template.equipmentId = Convert.ToInt16(node.InnerXml.Trim());

                //ZD 102052 START
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/enableParticipants");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableParticipants);
                template.enableParticipants = enableParticipants;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/enableAV");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAV);
                template.enableAV = enableAV;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/enableAdditionalOption");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAdditionalOption);
                template.enableAdditionalOption = enableAdditionalOption;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/enableAVWorkOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAVWO);
                template.enableAVWO = enableAVWO;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/enableCateringWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableCateringWO);
                template.enableCateringWO = enableCateringWO;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/enableFacilityWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableFacilityWO);
                template.enableFacilityWO = enableFacilityWO;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/exchangeUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableExchange);
                template.enableExchange = enableExchange;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/dominoUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableDomino);
                template.enableDomino = enableDomino;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/mobileUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableMobile);
                template.enableMobile = enableMobile;

                //ZD 102052 END
                //ZD 103673 - Start
                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                template.DateFormat = dateformat;

                node = xd.SelectSingleNode("//SetUserTemplate/UserTemplate/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                template.TimeFormat = timeformat;
                //ZD 103673 - End

                m_ItempDao.SaveOrUpdate(template);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region DeleteUserTemplate
        public bool DeleteUserTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                //ZD 102052 Starts
                int userID = 0, userTemplateID = 0;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                    organizationID = defaultOrgId;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                node = xd.SelectSingleNode("//login/userTemplateID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userTemplateID);

                vrmUserTemplate vT = m_ItempDao.GetById(userTemplateID);

                if (vT == null)
                    return false;

                List<vrmLDAPGroups> ldapGroupList = null;
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoleOrTemplateId", userTemplateID));
                criterionList.Add(Expression.Eq("Deleted", 0));
                criterionList.Add(Expression.Eq("Origin", 1));
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                ldapGroupList = m_ILDAPGroupsDAO.GetByCriteria(criterionList, true);

                if (ldapGroupList != null && ldapGroupList.Count > 0)
                {
                    myvrmEx = new myVRMException(748);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                else
                {
                    vT.deleted = 1;
                    m_ItempDao.SaveOrUpdate(vT);
                }
                //ZD 102052 End

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetUserDetails
        public bool GetUserDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                //ZD 102826 Starts
                int userID = 0;
                node = xd.SelectSingleNode("//login/UserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                int EntityType = 0;
                node = xd.SelectSingleNode("//login/EntityType");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EntityType);

                vrmBaseUser user = new vrmBaseUser();

                switch (EntityType)
                {
                    case vrmUserConstant.TYPE_GUEST:
                        user = (vrmBaseUser)m_IGuestUserDao.GetByUserId(userID);
                        break;
                    case vrmUserConstant.TYPE_INACTIVE:
                        user = (vrmBaseUser)m_IinactiveUserDao.GetByUserId(userID);
                        break;
                    default:
                        user = (vrmBaseUser)m_IuserDao.GetByUserId(userID);
                        break;
                }
                //ZD 102826 End

                obj.outXml = "<user>";
                obj.outXml += "<userID>" + user.userid + "</userID>";
                obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                obj.outXml += "<userEmail>" + user.Email + "</userEmail>";
                obj.outXml += "<userPhone>" + user.Telephone + "</userPhone>";
                obj.outXml += "<alternativeEmail>" + user.AlternativeEmail + "</alternativeEmail>";
                obj.outXml += "<sendBoth>" + user.DoubleEmail + "</sendBoth>";
                obj.outXml += "<timeZone>" + user.TimeZone + "</timeZone>";
                obj.outXml += "<languageID>" + user.Language + "</languageID>";
                obj.outXml += "<menuMask>" + user.MenuMask + "</menuMask>";
                obj.outXml += "<initialTime>" + user.initialTime + "</initialTime>";
                obj.outXml += "<status>";
                obj.outXml += "<level>" + user.newUser + "</level>";
                obj.outXml += "<deleted>" + user.Deleted + "</deleted>";
                obj.outXml += "<locked>" + user.LockStatus + "</locked>";
                obj.outXml += "</status>";
                // end point information
                //obj.outXml += "<lineRateID>" + user.EndPoint.linerateid.ToString() + "</lineRateID>";
                //obj.outXml += "<videoProtocol>" + user.EndPoint.protocol.ToString() + "</videoProtocol>";
                //obj.outXml += "<IPISDNAddress>" + user.EndPoint.address + "</IPISDNAddress>";
                //obj.outXml += "<connectionType>" + user.EndPoint.connectiontype.ToString() + "</connectionType>";
                //obj.outXml += "<videoEquipmentID>" + user.EndPoint.videoequipmentid.ToString() + "</videoEquipmentID>";
                // address book ????            
                obj.outXml += "<addressBook>";
                obj.outXml += "<type></type>";
                obj.outXml += "<loginName></loginName>";
                obj.outXml += "<loginPassword></loginPassword>";
                obj.outXml += "<loginPath></loginPath>";
                obj.outXml += "</addressBook>";
                obj.outXml += "<staticID>" + user.StaticID + "</staticID>";//ZD 102826

                obj.outXml += "</user>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region SetUserDetails
        public bool SetUserDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/UserID");
                string userID = node.InnerXml.Trim();

                int iEntityType = 0;
                node = xd.SelectSingleNode("//login/EntityType");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out iEntityType);


                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                int userid = 0; ;
                if (userID.ToLower() == "new")
                {
                    if (iEntityType != vrmUserConstant.TYPE_USER)
                    {
                        myvrmEx = new myVRMException(479);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                    }
                }
                else
                {
                    userid = Convert.ToInt16(userID);
                }

                node = xd.SelectSingleNode("//login/firstName");
                string firstName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/lastName");
                string lastName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/login");
                string login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/password");
                string password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/userEmail");
                string userEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/timeZone");
                string timeZone = node.InnerXml.Trim();
                //ZD 103469 - Start
                string location = "";
                node = xd.SelectSingleNode("//login/location");                
                if(node != null)
                    location = node.InnerXml.Trim();
                //if (location.Length == 0) location = "0"; 
                //ZD 103469 - End
                node = xd.SelectSingleNode("//login/group");
                string group = node.InnerXml.Trim();
                if (group.Length == 0) group = "0";
                node = xd.SelectSingleNode("//login/ccGroup");
                string ccGroup = node.InnerXml.Trim();
                if (ccGroup.Length == 0) ccGroup = "0";
                node = xd.SelectSingleNode("//login/altemail");
                string altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/sendBoth");
                string sendBoth = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/emailClient");
                string emailClient = node.InnerXml.Trim();
                if (emailClient.Length == 0) emailClient = "0";
                node = xd.SelectSingleNode("//login/roleID");
                string roleID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/languageID");
                string languageID = node.InnerXml.Trim();
                if (languageID.Length == 0) languageID = "1"; // default US English
                node = xd.SelectSingleNode("//login/lineRateID");
                string defaultLineRate = node.InnerXml.Trim();
                if (defaultLineRate.Length == 0) defaultLineRate = "0";
                node = xd.SelectSingleNode("//login/videoProtocol");
                string defaultVideoProtocol = node.InnerXml.Trim();
                if (defaultVideoProtocol == "IP")
                    defaultVideoProtocol = "1";
                else if (defaultVideoProtocol == "ISDN")
                    defaultVideoProtocol = "2";
                node = xd.SelectSingleNode("//login/IPISDNAddress");
                string IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/<connectionType");
                string connectionType = node.InnerXml.Trim();
                if (connectionType.Length == 0) connectionType = "-1"; // [None]
                node = xd.SelectSingleNode("//login/<videoEquipmentID");
                string defaultEquipment = node.InnerXml.Trim();
                if (defaultEquipment.Length == 0) defaultEquipment = "-1"; // [None]
                node = xd.SelectSingleNode("//login/<isOutside");
                string outsidenetwork = node.InnerXml.Trim();
                if (outsidenetwork.Length == 0) outsidenetwork = "0"; // inside by default
                node = xd.SelectSingleNode("//login/emailMask");
                string emailMask = node.InnerXml.Trim();
                // Email mask
                if (emailMask.Length == 0) emailMask = "0xFFFF"; // Allow all emails    
                // Address type
                node = xd.SelectSingleNode("//login/addressTypeID");
                string addressTypeID = node.InnerXml.Trim();
                if (addressTypeID.Length == 0) addressTypeID = "1"; // default 1 (IP Address)

                //// We removed <level> attribute because it is controlled by the <roleID>
                //// field now. We have to determine the value of superadmin from that
                //CString superadmin, admin;
                //stmt = "SELECT level FROM Usr_Roles_D WHERE roleid = " + roleID;
                //SEx.GenericCommand((CString) __FILE__, (int) __LINE__, cmd, (CString) stmt);
                //if ( cmd.FetchNext() )
                //    admin = cmd.Field("level").asString();
                //else
                //    admin = "0";
                //if ( admin.Compare(_T("2")) == 0 ) superadmin = "1";

                //CString deleted		=	strOps.extractData ("<deleted>","</deleted>",inputXML);
                //if(deleted.Compare(_T("1"))==0)
                //{
                //    if ( !canDeleteUser(userid) )
                //    {
                //        outputXML = globalErrHandle.FetchErrorMessage(310);
                //        globalErrHandle.WriteMessage(_ttoi(userid), 310, "Cannot delete user. ");
                //        return outputXML;
                //    }
                //    else
                //    {
                //        userDeleted = true;
                //    }
                //}
                node = xd.SelectSingleNode("//login/locked");
                string locked = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/accountexpiry");
                string accountexpiry = node.InnerXml.Trim();

                DateTime accountExpiry = new DateTime();
                //if ( accountexpiry.IsEmpty() )
                //{
                //    if(m_pDBConn)
                //        accountexpiry = m_pDBConn->GetExpiryDate(); // Expiry date of VRM license
                //}
                //else
                //{
                //    if(common.CheckExpiryDate(accountexpiry)>0){
                //        outputXML = globalErrHandle.FetchErrorMessage(307);
                //        globalErrHandle.WriteMessage(_ttoi(userid), 307, "Cannot save user");
                //        return outputXML;
                //    }
                //}
                node = xd.SelectSingleNode("//login/initialTime");
                string initialTime = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/creditCard");
                string creditCard = node.InnerXml.Trim();
                if (creditCard.Length == 0) creditCard = "0";
                node = xd.SelectSingleNode("//login/bridgeID");
                string bridgeID = node.InnerXml.Trim();
                if (bridgeID.Length == 0) bridgeID = "1"; // [None]

                //try
                //{
                //    if(mode == CREATENEW)  // now the email and alternativeEmail are same. We need to change it later.
                //    {


                //        // KM 09/17/04
                //        // Check if current active user count has exceeded the max permissible.
                //        VRMContainer	vrmContainer(m_pDBConn);
                //        bool maxLimitReached = vrmContainer.IsMaxEntityLimitReached(1);
                //        if (maxLimitReached)
                //        {
                //            outputXML = globalErrHandle.FetchErrorMessage(252);
                //            goto quit;
                //        }		


                //        CString errMsg;	
                //        long maxusrid = common.FetchMaxUserID(err, errMsg);
                //        if(err < 0) return errMsg;
                //        userid = strCon.longToCString(maxusrid+1);


                //        outputXML = CheckUser(userid,firstname,lastname,email,login,password,0 ,err);
                //        if(err<0)
                //            return outputXML;


                vrmBaseUser user = new vrmBaseUser();
                if (userid == 0)
                {
                    user.id = 0;
                    user.userid = m_userDAO.GetID();
                }
                else
                {
                    if (iEntityType == vrmUserConstant.TYPE_GUEST)
                    {
                        user = (vrmBaseUser)m_IGuestUserDao.GetByUserId(userid);
                        user.LastLogin = DateTime.Today;
                    }
                    else
                    {
                        user = (vrmBaseUser)m_IuserDao.GetByUserId(userid);
                    }
                }
                if (!checkUser(user.userid, login, userEmail, user.companyId,""))
                {
                    myvrmEx = new myVRMException(478);
                    obj.outXml = myvrmEx.FetchErrorMsg();

                }
                user.companyId = organizationID;
                user.FirstName = firstName;
                user.LastName = lastName;
                user.Login = login;
                // MUST BE ENCRYPTED !!!
                user.Password = password;
                user.Email = userEmail;
                user.TimeZone = int.Parse(timeZone);
                user.PreferedRoom = location; //int.Parse(location); Code changed for Room Search
                user.PreferedGroup = int.Parse(group);
                user.CCGroup = int.Parse(ccGroup);
                user.AlternativeEmail = altemail;
                user.DoubleEmail = int.Parse(sendBoth);
                user.EmailClient = int.Parse(emailClient);
                // have to lookup user roles 
                //node = xd.SelectSingleNode("//login/roleID");
                //string roleID = node.InnerXml.Trim();
                //node = xd.SelectSingleNode("//login/languageID");

                // load endpoint information
                user.Language = int.Parse(languageID);
                user.EndPoint.linerateid = int.Parse(defaultLineRate);
                user.EndPoint.protocol = int.Parse(defaultVideoProtocol);
                user.EndPoint.address = IPISDNAddress;
                user.EndPoint.addresstype = int.Parse(addressTypeID);
                user.EndPoint.videoequipmentid = int.Parse(defaultEquipment);
                user.EndPoint.outsidenetwork = int.Parse(outsidenetwork);
                user.EndPoint.connectiontype = int.Parse(connectionType);
                user.EndPoint.bridgeid = int.Parse(bridgeID);
                user.emailmask = int.Parse(emailMask);

                user.LockStatus = int.Parse(locked);
                user.accountexpiry = accountExpiry;
                user.initialTime = int.Parse(initialTime);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetUserList
        public bool GetUserList(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userId = 0; //FB 3041
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();
                if (userID != "") //FB 3041
                    int.TryParse(userID, out userId);

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                string alphabet = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                string pageno = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/sortBy");
                string sortBy = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/EntityType");
                string EntityType = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();

                criterionList.Add(Expression.Eq("companyId", organizationID));//Organization Module Fixes

                ICriterion criterium = null;

                string field = "LastName";
                switch (int.Parse(sortBy))
                {
                    case 1:
                        field = "FirstName";
                        break;
                    case 3:
                        field = "Login";
                        break;
                    case 4:
                        field = "Email";
                        break;
                }
                if (alphabet != "all")
                {
                    if (alphabet == "a")
                    {
                        criterionList.Add(Expression.Le(field, "aZ"));
                    }
                    else
                    {
                        criterionList.Add(Expression.Like(field, alphabet + "%%"));
                    }
                }
                if (multiDepts == 1)    //Organization Module Fixes
                {
                    vrmUser user = m_IuserDao.GetByUserId(userId);//FB 3041
                    if (getDeptLevelCriterion(user, ref criterium))
                    {
                        criterionList.Add(criterium);
                    }
                }
                int iPageNo = int.Parse(pageno);
                if (iPageNo == 0)
                    iPageNo = 1;
                long ttlRecords = 0;

                IList userList = new List<vrmBaseUser>();
                switch (int.Parse(EntityType))
                {
                    case vrmUserConstant.TYPE_GUEST:
                        m_IGuestUserDao.pageSize(m_iMaxRecords);
                        m_IGuestUserDao.pageNo(iPageNo);
                        if (userId > 0) //FB 3041
                            criterionList.Add(Expression.Eq("userid", userId));

                        ttlRecords =
                            m_IGuestUserDao.CountByCriteria(criterionList);
                        m_IGuestUserDao.addOrderBy(Order.Asc(field));
                        userList = m_IGuestUserDao.GetByCriteria(criterionList);
                        break;
                    case vrmUserConstant.TYPE_INACTIVE:
                        m_IinactiveUserDao.pageSize(m_iMaxRecords);
                        m_IinactiveUserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IinactiveUserDao.CountByCriteria(criterionList);
                        m_IinactiveUserDao.addOrderBy(Order.Asc(field));
                        userList = m_IinactiveUserDao.GetByCriteria(criterionList);
                        break;
                    default:
                        m_IuserDao.pageSize(m_iMaxRecords);
                        m_IuserDao.pageNo(iPageNo);
                        ttlRecords =
                            m_IuserDao.CountByCriteria(criterionList);
                        m_IuserDao.addOrderBy(Order.Asc(field));
                        userList = m_IuserDao.GetByCriteria(criterionList);
                        break;
                }
                int ttlPages = (int)(ttlRecords / m_iMaxRecords);

                // and modulo remainder...
                if (ttlRecords % m_iMaxRecords > 0)
                    ttlPages++;

                obj.outXml = "<users>";
                foreach (vrmBaseUser user in userList)
                {
                    obj.outXml += "<user>";
                    obj.outXml += "<userID>" + user.userid + "</userID>";
                    obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                    obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                    obj.outXml += "<email>" + user.Email + "</email>";
                    //FB 3041 Start
                    vrmEndPoint ept = m_IeptDao.GetByEptId(user.endpointId);
                    if (ept != null)
                    {
                        obj.outXml += "<address>" + ept.address + "</address>";
                        obj.outXml += "<addresstype>" + ept.addresstype + "</addresstype>";
                        obj.outXml += "<outsidenetwork>" + ept.outsidenetwork + "</outsidenetwork>";
                        obj.outXml += "<protocol>1</protocol>";
                        obj.outXml += "<connectiontype>" + ept.connectiontype + "</connectiontype>";
                    }
                    else
                    {
                        obj.outXml += "<address></address>";
                        obj.outXml += "<addresstype>-1</addresstype>";
                        obj.outXml += "<outsidenetwork>0</outsidenetwork>";
                        obj.outXml += "<protocol>1</protocol>";
                        obj.outXml += "<connectiontype>1</connectiontype>";
                    }
                    //FB 3041 End
                    obj.outXml += "<status>";
                    obj.outXml += "<level>" + user.Admin.ToString() + "</level>";
                    obj.outXml += "<deleted>" + user.Deleted.ToString() + "</deleted>";
                    obj.outXml += "<locked>" + user.LockStatus.ToString() + "</locked>";
                    obj.outXml += "</status>";
                    obj.outXml += "</user>";
                }

                obj.outXml += "<pageNo>" + pageno.ToString() + "</pageNo>";
                obj.outXml += "<totalPages>" + ttlPages.ToString() + "</totalPages>";
                obj.outXml += "<sortBy>" + sortBy + "</sortBy>";
                obj.outXml += "<alphabet>" + alphabet + "</alphabet>";
                obj.outXml += "<totalNumber>" + ttlRecords.ToString() + "</totalNumber>";

                obj.outXml += "</users>";
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region ConvertToGMT
        public bool ConvertToGMT(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ConvertToGMT/DateTime");
                string dateTime = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//ConvertToGMT/TimeZone");
                string timeZoneID = node.InnerXml.Trim();

                DateTime dt = DateTime.Parse(dateTime);
                if (timeZone.changeToGMTTime(int.Parse(timeZoneID), ref dt))
                {
                    obj.outXml += "<ConvertToGMT>";
                    obj.outXml += "<DateTime>" + dt.ToString("d") + " " + dt.ToString("t") + "</DateTime>";
                    obj.outXml += "</ConvertToGMT>";
                    return true;
                }
                myvrmEx = new myVRMException(480);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region ConvertFromGMT
        public bool ConvertFromGMT(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ConvertFromGMT/DateTime");
                string dateTime = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//ConvertFromGMT/TimeZone");
                string timeZoneID = node.InnerXml.Trim();

                DateTime dt = DateTime.Parse(dateTime);
                if (timeZone.userPreferedTime(int.Parse(timeZoneID), ref dt))
                {
                    obj.outXml += "<ConvertFromGMT>";
                    obj.outXml += "<DateTime>" + dt.ToString("d") + " " + dt.ToString("t") + "</DateTime>";
                    obj.outXml += "</ConvertFromGMT>";
                    return true;
                }
                myvrmEx = new myVRMException(480);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region getDeptLevelCriterion
        private bool getDeptLevelCriterion(vrmUser user, ref ICriterion criterium)
        {
            try
            {
                // superadmin, nothing to do!
                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                    return false;

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> In = new List<int>();

                criterionList.Add(Expression.Eq("userId", user.userid));
                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                foreach (vrmUserDepartment dept in deptList)
                {
                    In.Add(dept.departmentId);
                }
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("departmentId", In));

                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                In = new List<int>();
                foreach (vrmUserDepartment dept in deptList)
                {
                    In.Add(dept.userId);
                }

                criterium = Expression.In("userid", In);
                criterium = Expression.Or(criterium, Expression.Eq("userid", 11));


                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region ChangeUserStatus
        public bool ChangeUserStatus(ref vrmDataObject obj)
        {
            string ExternalUserId = ""; //ZD 100890
            int orgid = 0; //ZD 100890
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int userID = int.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/user/userID");
                int actionID = int.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/user/action");
                int action = int.Parse(node.InnerXml.Trim());

                vrmUser user = new vrmUser();
                vrmInactiveUser userIn = new vrmInactiveUser();
                vrmGuestUser guest = new vrmGuestUser();
                vrmUserRoles roles = new vrmUserRoles();//ZD 101860
                List<ICriterion> criterionList1 = new List<ICriterion>();
                List<ICriterion> criterionList2 = new List<ICriterion>();
                switch (action)
                {
                    case vrmUserAction.USER_STATUS_ACTIVE:
                        userIn = m_IinactiveUserDao.GetByUserId(actionID);
                        user = new vrmUser(userIn);
                        user.id = 0;
                        user.LockStatus = vrmUserStatus.USER_ACTIVE;
                        m_IinactiveUserDao.Delete(userIn);
                        m_IuserDao.Save(user);
                        break;
                    case vrmUserAction.USER_STATUS_DELETE:
                        user = m_IuserDao.GetByUserId(actionID);

                        ExternalUserId = user.ExternalUserId;//ZD 100890
                        orgid = user.companyId;//ZD 100890

                        int errid = 310;//FB 1497

                        //ZD 100263
                        vrmUser LoginUser = m_IuserDao.GetByUserId(userID);
                        if (!CheckUserRights(LoginUser, user) || (LoginUser.userid == user.userid))
                        {
                            myvrmEx = new myVRMException(229);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        if (!canDeleteUser(user, ref errid))//FB 1497
                        {
                            myVRMException e = new myVRMException(errid);//FB 1497
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();//FB 1881
                            return false;
                        }
                        user.Email = user.Email + "_D"; //FB 2346
                        userIn = new vrmInactiveUser(user);
                        userIn.id = user.id;
                        userIn.Deleted = vrmUserStatus.USER_INACTIVE;
                        m_IuserDao.Delete(user);
                        userIn.searchId = -1; //FB 2617
                        m_IinactiveUserDao.Save(userIn);
                        //ZD 103878
                        if (InsertDeletedUserDetails(user))
                        {
                            UpdateConferenceUserIds(user, userID);
                        }
                        //ZD 101860                                                
                        roles = m_IUserRolesDao.GetById(userIn.roleID);
                        criterionList1.Add(Expression.Eq("createType", 2));
                        List<vrmUserRoles> rolelist = m_IUserRolesDao.GetByCriteria(criterionList1);
                        for (int r = 0; r < rolelist.Count; r++)
                        {
                            criterionList2 = new List<ICriterion>(); //ZD 103987
                            criterionList2.Add(Expression.Eq("roleID", rolelist[r].roleID));
                            List<vrmUser> Inusr = m_IuserDao.GetByCriteria(criterionList2);
                            if (Inusr.Count == 0)
                            {
                                rolelist[r].locked = 0;
                                m_IUserRolesDao.Update(rolelist[r]);
                            }
                        }
                        break;
                    case vrmUserAction.USER_STATUS_LOCK:
                        user = m_IuserDao.GetByUserId(actionID);
                        //user.lockCntTrns = vrmUserStatus.MAN_LOCKED; // Code Commented for FB 1401
                        //user.LockStatus = 1; // Code Commented for FB 1401
                        user.lockCntTrns = 1;   //FB 1401
                        user.LockStatus = vrmUserStatus.MAN_LOCKED; //FB 1401
                        m_IuserDao.Update(user);
                        break;
                    case vrmUserAction.USER_STATUS_UNLOCK:
                        user = m_IuserDao.GetByUserId(actionID);
                        //user.lockCntTrns = vrmUserStatus.USER_ACTIVE; // Code Commented for FB 1401
                        //user.LockStatus = 0;  // Code Commented for FB 1401
                        if (user.lockCntTrns == 10) //ZD  103459  Starts
                        {
                            myvrmEx = new myVRMException(425);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        else
                        {
                            user.lockCntTrns = 0;   //FB 1401
                            user.LockStatus = vrmUserStatus.USER_ACTIVE;    //FB 1401
                            m_IuserDao.Update(user);
                        } //ZD 103459  Ends
                        break;
                    case vrmUserAction.USER_GUEST_DELETE:
                        guest = m_IGuestUserDao.GetByUserId(actionID);
                        guest.Deleted = 1;
                        guest.accountexpiry = DateTime.Parse("01/01/1970");
                        m_IGuestUserDao.Update(guest);
                        break;

                    case vrmUserAction.USER_GUEST_UNDELETE:
                        guest = m_IGuestUserDao.GetByUserId(actionID);
                        guest.Deleted = 0;
                        guest.accountexpiry = DateTime.Parse("01/01/1970");
                        m_IGuestUserDao.Update(guest);
                        break;
                }
                obj.outXml = "<DeleteUser><UserExternalID>" + ExternalUserId + "</UserExternalID><OrganizationId>" + orgid + "</OrganizationId></DeleteUser>"; //ZD 100890
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        //ZD 103878
        #region InsertDeletedUserDetails
        private Boolean InsertDeletedUserDetails(vrmUser user)
        {
            try
            {
                vrmUserDeleteList usrDelList = new vrmUserDeleteList();
                user.Email = user.Email.Replace("_D", "");
                usrDelList = new vrmUserDeleteList(user);
                usrDelList.id = user.id;
                usrDelList.CreatedDate = DateTime.Now;

                m_UserDeleteListDAO.SaveOrUpdate(usrDelList);

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region UpdateConferenceUserIds

        private void UpdateConferenceUserIds(vrmUser user, int loginUsrID)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            String stmt = "";
            m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);
            int rowCnt = 0;
            int strExec = 0;
            try
            {
                DateTime curDate = DateTime.Now;
                m_rptLayer.OpenConnection();

                //Check Conference Host having this ID               
                stmt = "select COUNT(*) from Conf_Conference_D where confdate < dbo.userToGMTTime(" + loginUsrID + ", GETDATE()) and owner =" + user.userid; ;
                object retHVal = m_rptLayer.ExecuteScalar(stmt);
                if (retHVal != null)
                    rowCnt = (int)retHVal;
                //IF Yes
                if (rowCnt > 0)
                {
                    stmt = "update Conf_Conference_D set owner = " + user.id + " , UserIDRefText =  ISNULL(UserIDRefText,'')+'H'";//, HostName='" + user.FirstName + ' ' + user.LastName  + "'";
                    stmt += " where confnumname in (select confnumname from Conf_Conference_D where confdate < dbo.userToGMTTime(" + loginUsrID + ", GETDATE()) and owner =" + user.userid + ")";

                    strExec = m_rptLayer.ExecuteNonQuery(stmt);                
                }

                //Check Conference Requestor having this ID
                stmt = "select COUNT(*) from Conf_Conference_D where confdate < dbo.userToGMTTime(" + loginUsrID + ", GETDATE()) and userid =" + user.userid;
                object retRVal = m_rptLayer.ExecuteScalar(stmt);
                if (retRVal != null)
                    rowCnt = (int)retRVal;
                //IF Yes
                if (rowCnt > 0)
                {
                    stmt = "update Conf_Conference_D set userid = " + user.id + " , UserIDRefText =  ISNULL(UserIDRefText,'')+'R' ";
                    stmt += " where confnumname in (select confnumname from Conf_Conference_D where confdate < dbo.userToGMTTime(" + loginUsrID + ", GETDATE()) and userid =" + user.userid + ")";

                    strExec = m_rptLayer.ExecuteNonQuery(stmt);
                }
                //Check Conference Login User having this ID
                stmt = "select COUNT(*) from Conf_Conference_D where confdate < dbo.userToGMTTime(" + loginUsrID + ", GETDATE()) and loginUser =" + user.userid;

                object retLVal = m_rptLayer.ExecuteScalar(stmt);
                if (retLVal != null)
                    rowCnt = (int)retLVal;
                //IF Yes
                if (rowCnt > 0)
                {
                    stmt = "update Conf_Conference_D set loginUser = " + user.id + " , UserIDRefText =  ISNULL(UserIDRefText,'')+'L' ";
                    stmt += " where confnumname in (select confnumname from Conf_Conference_D where confdate < dbo.userToGMTTime(" + loginUsrID + ", GETDATE()) and loginUser =" + user.userid + ")";

                    strExec = m_rptLayer.ExecuteNonQuery(stmt);
                }

                m_rptLayer.CloseConnection();

            }
            catch (Exception e)
            {
                if (m_rptLayer != null)
                    m_rptLayer.CloseConnection();

                m_log.Error("sytemException", e);
            }
        }

        #endregion

        #region GetManageUser
        public bool GetManageUser(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                //FB 2027 Starts
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1;//FB 2269
                int SortingOrder = 0; //ZD 103405
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes
                //FB 2027 Ends
                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sortBy);
                //FB 2027 Ends

                //ZD 103405 start
                node = xd.SelectSingleNode("//login/SortingOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SortingOrder);
                //ZD 103405 End

                node = xd.SelectSingleNode("//login/audioaddon"); //FB 2023
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        int.TryParse(node.InnerText.Trim(), out isAudioAddOn);

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 2, ref outXml, organizationID, 0, deptUsers, BulkUser, 0))//FB 2027 //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetUserList
        public bool GetUserList(int userID, int type, int pageno, ref int totpages, string alphabet, int sortBy, int SortingOrder, int mode, ref string outXml, string BulkUser)//ZD 100263 //ZD 103405
        {

            try
            {
                int numrows = 0;
                int thresholdmax = 200; // Not more than 200 users at a time
                bool thresholdpassed = false;

                // this routing must work for LDAP also (not implemented yet)
                // when getting LDAP count newuser = 1

                int admin = 0;

                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;    //Organization Module

                vrmUser user = m_IuserDao.GetByUserId(userID);

                outXml = "<users>";

                // Spoof admin if this is an LDAP lookup (this is only visible to admmins) [AG 5-19-08]
                if (type == m_isUser)
                {
                    if (multiDepts == 1)    //Organization Module
                        admin = user.Admin;
                    else
                        admin = vrmUserConstant.SUPER_ADMIN;
                }
                else
                {
                    admin = vrmUserConstant.SUPER_ADMIN;
                }
                string checkAlpha = alphabet.ToLower();

                List<ICriterion> criteriumList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                List<int> userIn = new List<int>();
                //
                // if multi departments is enabled apply roles based security. 
                // if not then anyone can see all depts 
                //
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN) //Organization Module
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.In("departmentId", deptIn));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        userIn.Add(dept.userId);
                    }

                    criteriumList.Add(Expression.In("userid", userIn));
                }

                // LDAP_BlankUser_D
                // here : AND newuser = 1

                // first get the number of records WITHOUT criteria applied
                m_IuserDao.addProjection(Projections.RowCount());

                IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                numrows = int.Parse(list[0].ToString());
                totpages = numrows / m_PAGE_SIZE;
                if ((numrows % m_PAGE_SIZE) > 0)
                    totpages++;

                list = m_IuserDao.GetObjectByCriteria(criteriumList);
                int numrecs = int.Parse(list[0].ToString());
                m_IuserDao.clearProjection();

                string a_Order = null;

                switch (sortBy)
                {
                    case 1:
                        a_Order = "FirstName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("FirstName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("FirstName", alphabet + "%%"));
                        }
                        break;
                    case 3:
                        a_Order = "Login";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Login", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Login", alphabet + "%%"));
                        }
                        break;
                    case 4:
                        a_Order = "Email";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    default:
                        a_Order = "LastName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("LastName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("LastName", alphabet + "%%"));
                        }
                        break;

                }
                if (numrows > thresholdmax)
                {
                    thresholdpassed = true;
                }

                // add order by condition
                m_IuserDao.addOrderBy(Order.Asc(a_Order));

                m_IuserDao.pageSize(m_PAGE_SIZE);
                if (pageno == 0)
                    pageno = 1;

                m_IuserDao.pageNo(pageno);

                // always true criteria 
                if (criteriumList.Count == 0)
                    criteriumList.Add(Expression.Ge("Deleted", 0));

                List<vrmUser> userList = m_IuserDao.GetByCriteria(criteriumList);

                foreach (vrmUser vUser in userList)
                {
                    outXml += "<user>";
                    outXml += "<userID>" + vUser.userid.ToString() + "</userID>";
                    outXml += "<firstName>" + vUser.FirstName + "</firstName>";
                    outXml += "<lastName>" + vUser.LastName + "</lastName>";
                    outXml += "<login>" + vUser.Login + "</login>";
                    outXml += "<email>" + vUser.Email + "</email>";
                    outXml += "<telephone>" + vUser.Telephone + "</telephone>";

                    if (type != m_isLDAP)  //FB 2027
                    {
                        if (vUser.Password != null)
                            outXml += "<password>" + vUser.Password + "</password>";
                        else
                            outXml += "<password></password>";
                    }

                    if (mode == 1)
                    {
                        IUserAccountDao IuserAccountDAO = m_userDAO.GetUserAccountDao();
                        vrmAccount uAccount = IuserAccountDAO.GetByUserId(vUser.userid); //FB 2027

                        outXml += "<report>";
                        outXml += "<total>" + uAccount.TotalTime.ToString() + "</total>";

                        //ZD 102043 starts
                        //long timeUsed = uAccount.TotalTime - uAccount.TimeRemaining;
                        //outXml +="<used>" + timeUsed.ToString() + "</used>";

                        outXml += "<used>" + uAccount.UsedMinutes.ToString() + "</used>";
                        //ZD 102043 ends
                        // used = totalAmount- left
                        outXml += "<left>" + uAccount.TimeRemaining.ToString() + "</left>";
                        outXml += "</report>";
                    }
                    else if (mode == 2)
                    {
                        string lockstatus = "0";
                        string deleted = "0";

                        if (vUser.LockStatus != 0)
                            lockstatus = "1";

                        if (vUser.Deleted != 0) // Deleted
                            deleted = "1";

                        outXml += "<status>";
                        outXml += "<level>" + vUser.Admin.ToString() + "</level>";
                        outXml += "<deleted>" + deleted + "</deleted>";
                        outXml += "<locked>" + lockstatus + "</locked>";
                        outXml += "</status>";
                    }
                    outXml += "</user>";
                }
                outXml += "<pageNo>" + pageno.ToString() + "</pageNo>";
                outXml += "<totalPages>" + totpages.ToString() + "</totalPages>";
                outXml += "<sortBy>" + sortBy.ToString() + "</sortBy>";
                outXml += "<alphabet>" + alphabet + "</alphabet>";
                if (thresholdpassed)
                    outXml += "<canAll>0</canAll>";
                else
                    outXml += "<canAll>1</canAll>";

                outXml += "<totalNumber>" + numrecs.ToString() + "</totalNumber>";
                if (type != m_isLDAP)
                {
                    int usersRemain = sysSettings.UserLimit - numrows;
                    outXml += "<licensesRemain>" + usersRemain.ToString() + "</licensesRemain>";
                }
                outXml += "</users>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region canDeleteUser
        //
        // check if deleting this user valid
        //
        bool canDeleteUser(vrmUser user, ref int errid)//FB 1497
        {

            try
            {
                IConferenceDAO a_IconfDAO;
                conferenceDAO a_confDAO = new conferenceDAO(m_configPath, m_log);
                a_IconfDAO = a_confDAO.GetConferenceDao();

                //
                // check if this user is room assistant, MCU Approver, Dept approver, Room approver, 
                // work oreder admin or inventory admin. If not cannot delete. 
                //if (m_userDAO.checkUser(user))
                if (checkUser(user, ref errid)) //FB 1497
                {
                    //
                    // now check if user has any active conferences
                    //
                    DateTime checkDate = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref checkDate);
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Ge("confEnd", checkDate));
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("userid", user.userid));
                    List<vrmConference> confList = a_IconfDAO.GetByCriteria(criterionList);

                    // now check for ongoing conferences
                    if (confList.Count > 0)
                    {
                        return false;
                    }
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region checkUser
        public bool checkUser(int userid, string login, string email, int testOrgId, string UserDomain)//ZD 103784
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium;

                organizationID = testOrgId;
                //FB 1623, 1624
                //criterium = Expression.Eq("companyId", organizationID); //Organization fixes
                //criterionList.Add(criterium);

                //criterium = Expression.Eq("Login", login.ToUpper()).IgnoreCase(); //FB 1623, 1624
                //if (email.Trim().Length > 0)
                //{
                //    criterium = Expression.Or(criterium, Expression.Like("Email", email.Trim().ToUpper()).IgnoreCase());
                //}
                //FB 1921 Start
                if (login != "")
                {
                    criterium = Expression.Eq("Login", login.ToUpper()).IgnoreCase();
                    if (UserDomain != "")
                    {
                        criterium = Expression.And(criterium, Expression.Eq("UserDomain", UserDomain));//103784
                    }
                    criterium = Expression.Or(criterium, Expression.Eq("Email", email.Trim().ToUpper()).IgnoreCase());
                   
                }
                else
                {
                    criterium = Expression.Eq("Email", email.Trim().ToUpper()).IgnoreCase(); //FB 1623, 1624
                }
                //FB 1921 End

                criterionList.Add(criterium);
                if (userid > 0)
                {
                    criterium = Expression.And(criterium, Expression.Not(Expression.Eq("userid", userid)));
                }

                //ZD 103784 starts
                
                criterionList.Add(criterium);
                //ZD 103784 Ends

                List<vrmUser> results = m_IuserDao.GetByCriteria(criterionList);
                int errCount = results.Count;
                if (errCount > 0)
                    return false;
                IGuestUserDao guest = m_userDAO.GetGuestUserDao();

                //FB 2119 Start
                List<vrmGuestUser> guestResults = guest.GetByCriteria(criterionList, true);
                if (m_userType == m_isLDAP)
                {
                    for (int i = 0; i < guestResults.Count; i++)
                        guest.Delete(guestResults[i]);
                }
                else
                {
                    errCount = guestResults.Count;
                    if (errCount > 0)
                        return false;
                }
                //FB 2119 End

                IInactiveUserDao inactive = m_userDAO.GetInactiveUserDao();
                List<vrmInactiveUser> inactiveResults = inactive.GetByCriteria(criterionList);
                errCount = inactiveResults.Count;
                if (errCount > 0)
                    return false;

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        //FB 1497
        private bool checkUser(vrmUser user, ref int errid)
        {
            try
            {
                string hql;
                DataSet ds = null;

                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);
				//ALLDEV-498 - Start
                hql = " SELECT orgid FROM Org_Settings_D R WHERE ConfAdministrator=" + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 767;
                    return false;
                }
				//ALLDEV-498 - End

                hql = " Select la.AssistantId, * from Loc_Assistant_D la, Loc_Room_D loc where loc.Disabled =0  and la.RoomId = loc.RoomID and la.AssistantId = " + user.userid.ToString(); //ALDEV-807
                //hql = " SELECT  R.RoomID, R.Disabled FROM Loc_Room_D R WHERE Disabled = 0 and assistant = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 520;
                    return false;
                }
                hql = " Select ma.approverid, * from MCU_Approver_D ma, Mcu_List_D mc where mc.deleted =0  and ma.mcuid = mc.BridgeID and ma.approverid =  " + user.userid.ToString(); //ZD 102390
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT D.approverid FROM Dept_Approver_D D WHERE approverid = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " Select la.approverid, * from Loc_Approver_D la, Loc_Room_D loc where loc.Disabled =0  and la.roomid = loc.RoomID and la.approverid = " + user.userid.ToString(); //ZD 102390
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT W.AdminID FROM Inv_WorkOrder_D W WHERE AdminID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }
                hql = " SELECT C.AdminID FROM Inv_Category_D C WHERE AdminID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 520;
                    return false;
                }

                hql = " SELECT M.Admin FROM MCU_List_D M WHERE Deleted = 0 and Admin = " + user.userid.ToString();//ZD 102390
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 520;
                    return false;
                }

                hql = " SELECT A.approverid FROM  Sys_Approver_D A WHERE approverid = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 519;
                    return false;
                }

                hql = " SELECT A.UserID FROM  Grp_Participant_D A WHERE UserID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }

                //FB 2027
                hql = " SELECT A.Owner FROM  Grp_Detail_D A WHERE Owner = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }

                hql = " SELECT A.TmpOwner FROM  Tmp_List_D A WHERE TmpOwner = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }

                hql = " SELECT A.UserID FROM  Tmp_Participant_D A WHERE UserID = " + user.userid.ToString();
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 521;
                    return false;
                }
                //FB 2027

                //FB 3004 Start

                DateTime confEnd = DateTime.Now;
                DateTime confFrom = DateTime.Now;

                if (m_Search.getSearchDateRange(1, ref confEnd, ref confFrom, 0))
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                }

                //Check whether participant is scheduler
                hql = "Select c.userid from Conf_Conference_D c WHERE c.deleted=0 and c.userid='" + user.userid.ToString() + "'";
                hql += " and c.status in (0,1,5,6) and ((c.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') ";
                hql += " or c.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 518;
                    return false;
                }
                //Check whether participant is host
                hql = "Select c.confid from Conf_Conference_D c " +
                             " WHERE c.deleted=0 and c.owner='" + user.userid.ToString() + "'" +
                             " and c.status in (0,1,5,6) and ((c.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') " +
                             " or c.confdate >= '" + confFrom.ToString() + "')";

                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 518;
                    return false;
                }

                hql = "SELECT DISTINCT cu.confid FROM Conf_Conference_D vc, conf_user_D cu " +
                              " WHERE vc.deleted=0 and vc.status in (0,1,5,6)" +
                              " and vc.confid=cu.confid and cu.userid ='" + user.userid.ToString() + "' and ((vc.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,vc.duration,vc.confdate) >= '" + confFrom.ToString() + "') " + //ZD 104867
                              " or vc.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 518;
                    return false;
                }
                //FB 1497 end
                // FB 2670 Starts
                hql = "SELECT DISTINCT cv.confid FROM Conf_Conference_D vc, Conf_VNOCOperator_D cv " +
                            " WHERE vc.deleted=0 and vc.status in (0,1,5,6)" +
                            " and vc.confid=cv.confid and cv.vnocId ='" + user.userid.ToString() + "' and ((vc.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,vc.duration,vc.confdate) >= '" + confFrom.ToString() + "') " + ///ZD 104867
                            " or vc.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 697;
                    return false;
                }

                hql = "SELECT DISTINCT cv.confid FROM Conf_Conference_D vc, Conf_VNOCOperator_D cv " +
                         " WHERE vc.deleted=0 and vc.status in (0,1,5,6)" +
                         " and vc.confid=cv.confid and cv.vnocAssignAdminId ='" + user.userid.ToString() + "' and ((vc.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,duration,confdate) >= '" + confFrom.ToString() + "') ";
                hql += " or vc.confdate >= '" + confFrom.ToString() + "')";
                if (hql != "")
                    ds = m_rptLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 697;
                    return false;
                }
                //FB 2670 Ends
                //FB 3004 End
                return true;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SetBulkUserUpdate

        public bool SetBulkUserUpdate(ref vrmDataObject obj)
        {
            bool bRet = true;
            int AuditPCCount = 0;//ZD 101026
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            string sWhere = " Where", sWhereOrg ="", swhereDelimiter = ",", sUpdate = " Update Usr_List_D set", sValues = "", sQuery = "", newValue = "", susersID = "", susersDeptID = "", 
            sLastModified = " LastModifiedDate = '" + DateTime.UtcNow + "' ", UserDomain = "" ;//ZD 104850
            int iNewValue = 0, userID = 0, type = 0, minutes = 0, bridgeID = 0, langid = 0, roleid = 0, timeZoneID = 27;// ZD 104204
            XmlNodeList deptid = null;            
            ns_SqlHelper.SqlHelper sqlCon = null; // ZD 104204
            DateTime expiryDate = DateTime.MinValue;
            int strExec = -1;
            // ZD 101362 Ends
            try
            {
                
                int typeLimit = 0, errorID = 200, usrType = 0; //FB 1599
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int.TryParse(node.InnerXml.Trim(), out userID); // ZD 104204
                
                node = xd.SelectSingleNode("//login/type");
                int.TryParse(node.InnerXml.Trim(), out type); // ZD 104204
                // ZD 104204
                node = xd.SelectSingleNode("//login/newValue");
                if (node != null)
                {
                    newValue = node.InnerXml.Trim();
                    int.TryParse(newValue, out iNewValue);
                }

                node = xd.SelectSingleNode("//login/minutes");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out minutes);

                deptid = xd.SelectNodes(@"/login/departments/departmentID");
                if (deptid != null && deptid.Count > 0)
                    susersDeptID = " departmentId in (" + deptid.Cast<XmlNode>().Select(nde => nde.InnerXml).ToList().Aggregate((i, j) => i + swhereDelimiter + j) + ") ";

                node = xd.SelectSingleNode("//login/expiryDate");
                if (node != null && node.InnerText.Trim() != "") 
                {
                    DateTime.TryParse(node.InnerText.Trim(), out expiryDate);
                    if (expiryDate <= DateTime.Today.Subtract(new TimeSpan(24, 0, 0)))
                    {
                        myvrmEx = new myVRMException(203);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
 
                    }
                }

                node = xd.SelectSingleNode("//login/bridgeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID <= 0)
                    {
                        myvrmEx = new myVRMException(531);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                node = xd.SelectSingleNode("//login/languageID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out langid);
                    if (langid <= 0)
                    {
                        myvrmEx = new myVRMException(422);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                node = xd.SelectSingleNode("//login/roleID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out roleid);
                    if (roleid <= 0)
                    {
                        myvrmEx = new myVRMException(477);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                node = xd.SelectSingleNode("//login/timeZoneID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out timeZoneID);
                    if (timeZoneID <= 0)
                    {
                        myvrmEx = new myVRMException(422);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                // ZD 104204
                //FB 1599 start
                if (xd.SelectSingleNode("//login/organizationID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/organizationID").InnerText.Trim(), out organizationID);
                if (organizationID < 11)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                
                //FB 1599 end
                // ZD 101362 Starts
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);
                userids = new List<int>();//ZD 102232

                //ZD 104850 start
                node = xd.SelectSingleNode("//login/UserDomain");
                if (node != null)
                    UserDomain = node.InnerXml.Trim();
                //ZD 104850 End

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    susersID = uid.Cast<XmlNode>().Select(nde => nde.InnerXml).ToList().Aggregate((i, j) => i + swhereDelimiter + j);
                    sWhere += " userID in ( " + susersID + " )";

                }
                else
                    sWhere += " companyId = " + organizationID + " and UserID <> 11";

                sValues = " " + ((bulkoperations)type).ToString() + " = '" + newValue.Trim() + "' , " + sLastModified;
                
                    switch (type)
                    {
                        case 0:
                            sWhereOrg = sWhere.Replace("companyId", "orgID");
                            sQuery = "Update Acc_Balance_D set TotalTime =TotalTime + " + minutes.ToString() + " , TimeRemaining = TotalTime - UsedMinutes " + sWhereOrg;
                            break;
                        case 1:
                            
                            if(userall == 0)
                                 sQuery = " Delete from Usr_Dept_D " + sWhere + " and " + susersDeptID;
                            else
                                sQuery = " Delete from Usr_Dept_D where userID in (Select USerID from Usr_List_D "+ sWhere +" ) and " + susersDeptID;
                            for (int j = 0; j < deptid.Count; j++)
                            {
                                node = deptid[j];
                                sQuery += "; INSERT INTO Usr_Dept_D SELECT " + node.InnerXml.Trim() + ",UserID FROM Usr_List_D " + sWhere;
                            }
                            break;
                        case 2:
                            sValues = " accountexpiry = '" + expiryDate + "' , " + sLastModified;
                            sQuery = sUpdate + sValues + sWhere;
                            break;
                        case 3:
                            sValues = " BridgeID = " + bridgeID + " , " + sLastModified;;
                            sQuery = sUpdate + sValues + sWhere;
                            sQuery += " ; Update Ept_List_D set bridgeid = " + bridgeID + " where endpointId in (select endpointId from Usr_List_D " + sWhere + ")";
                            break;
                        case 4:
                            sValues = " Language = " + langid + " , " + sLastModified;
                            sQuery = sUpdate + sValues + sWhere;
                            break;
                        case 5:
                            sQuery = sUpdate +" roleID = x.roleID , MenuMask = x.roleMenuMask, Admin = x.level, UserRolename = x.roleName , "+ sLastModified +" From (select * from Usr_Roles_D where roleID = " + roleid + ") x " + sWhere + " ; update Usr_Roles_D set locked = 1 where createType = 2 and roleID = " + roleid;
                            break;
                        case 6:
                            sValues = " TimeZone = " + timeZoneID + " , " + sLastModified;
                            sQuery = sUpdate + sValues + sWhere;
                            break;
                        case 8:
                            sValues = " lockCntTrns = 1 , LockStatus = " + vrmUserStatus.MAN_LOCKED + " , " + sLastModified;
                            sQuery = sUpdate + sValues + sWhere;
                            break;
                        case 9:                            
                        case 10:                           
                        case 11:                            
                        case 12:
                        case 17:                            
                            sQuery = sUpdate + sValues + sWhere;
                            break;
                            
                        case 13:                           
                        case 14:                           
                        case 15: 
                        case 16: 
                        //ZD 101093                            
                            if (iNewValue > 0)
                            {
                                if(userall == 0)
                                    sQuery = "declare @decision int set @decision = 0 SELECT @decision = Y.finalcnt + z.existingcnt - " + uid.Count + " FROM (select x.limit - x.usrCnt as finalcnt from (select COUNT(*) as usrCnt, (select " + ((bulkoperationslicnese)type).ToString() + " from org_settings_d  where orgid = " + organizationID.ToString() + " ) as limit from Usr_List_D where companyId = " + organizationID.ToString() + " and " + ((bulkoperations)type).ToString() + " = 1 and UserID <> 11) x ) Y , (select COUNT(*) as existingcnt from Usr_List_D " + sWhere + " and " + ((bulkoperations)type).ToString() +" = 1) Z if (@decision >= 0) begin update Usr_List_D set " + sValues + " " + sWhere + " end";
                                else
                                    sQuery = "declare @decision int set @decision = 0 SELECT @decision = x.limit - x.usrCnt from (select COUNT(*) as usrCnt, (select  " + ((bulkoperationslicnese)type).ToString() + " from org_settings_d  where orgid = " + organizationID.ToString() + " ) as limit from Usr_List_D where companyId = " + organizationID.ToString() + " and UserID <> 11 ) x if (@decision >= 0) begin update Usr_List_D set " + sValues + " " + sWhere + " end";
                            
                            }
                            else
                                sQuery = sUpdate + sValues + sWhere;
                            break;

                        case 18:
                        case 19:
                        case 20:
                            sQuery = sUpdate + sValues + sWhere + " and Admin = 0";
                            break;
                        case 21://ZD 104850 start
                            sValues = " UserDomain = '" + UserDomain + "' , " + sLastModified;
                            sQuery = sUpdate + sValues + sWhere;
                            break;//ZD 104850  End.
                    }

                    sqlCon = new ns_SqlHelper.SqlHelper();
                    sqlCon.OpenConnection();
                    strExec = sqlCon.ExecuteNonQuery(sQuery);
                    if (strExec <= 0)
                    {
                        
                        switch (type)
                        {

                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            //ALLBUGS-40
                            case 18:
                            case 19:
                            case 20:
                            {
                                if (type == 19 || type == 20)   //Same error code sent to type = 19 and type = 20
                                    type = 18;
                                Int32.TryParse(((bulkoperationLicenseserror)type).ToString().Replace("errcode", ""), out errorID);
                                break;
                            }
                            //ALLBUGS-40
                        }
                        if (errorID <= 0)
                            errorID = 200;

                        myvrmEx = new myVRMException(errorID);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;

                    }

                   
               
                bRet = true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                bRet = false;
                throw ex;
            }
            return bRet;
        }
        #endregion

        //Code Added for FB Issue 826 -- Start
        #region GetAllManageUser
        public bool GetAllManageUser(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                int userID = int.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                string alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                int pageNo = int.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/sortBy");
                int sortBy = int.Parse(node.InnerXml.Trim());

                int totPages = 0;
                string outXml = string.Empty;
                if (GetAllUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, 2, ref outXml))
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region GetAllUserList
        public bool GetAllUserList(int userID, int type, int pageno, ref int totpages, string alphabet, int sortBy, int mode, ref string outXml)
        {

            try
            {
                int numrows = 0;
                int thresholdmax = 200; // Not more than 200 users at a time
                bool thresholdpassed = false;

                // this routing must work for LDAP also (not implemented yet)
                // when getting LDAP count newuser = 1
                int admin = 0;

                if (organizationID < 11)    //Organization Module
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;    //Organization Module

                vrmUser user = m_IuserDao.GetByUserId(userID);

                outXml = "<users>";

                // Spoof admin if this is an LDAP lookup (this is only visible to admmins) [AG 5-19-08]
                if (type == m_isUser)
                {
                    if (multiDepts == 1)
                        admin = user.Admin;
                    else
                        admin = vrmUserConstant.SUPER_ADMIN;
                }
                else
                {
                    admin = vrmUserConstant.SUPER_ADMIN;
                }
                string checkAlpha = alphabet.ToLower();

                List<ICriterion> criteriumList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                List<int> userIn = new List<int>();
                //
                // if multi departments is enabled apply roles based security. 
                // if not then anyone can see all depts 
                //
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN)
                {

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.In("departmentId", deptIn));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        userIn.Add(dept.userId);
                    }

                    criteriumList.Add(Expression.In("userid", userIn));
                }

                // LDAP_BlankUser_D
                // here : AND newuser = 1

                // first get the number of records WITHOUT criteria applied
                m_IuserDao.addProjection(Projections.RowCount());

                IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                numrows = int.Parse(list[0].ToString());
                totpages = numrows / m_PAGE_SIZE;
                if ((numrows % m_PAGE_SIZE) > 0)
                    totpages++;

                list = m_IuserDao.GetObjectByCriteria(criteriumList);
                int numrecs = int.Parse(list[0].ToString());
                m_IuserDao.clearProjection();

                string a_Order = null;

                switch (sortBy)
                {
                    case 1:
                        a_Order = "FirstName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("FirstName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("FirstName", alphabet + "%%"));
                        }
                        break;
                    case 3:
                        a_Order = "Login";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Login", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Login", alphabet + "%%"));
                        }
                        break;
                    case 4:
                        a_Order = "Email";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    default:
                        a_Order = "LastName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("LastName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("LastName", alphabet + "%%"));
                        }
                        break;

                }
                if (numrows > thresholdmax)
                {
                    thresholdpassed = true;
                }

                // add order by condition
                m_IuserDao.addOrderBy(Order.Asc(a_Order));

                m_IuserDao.pageSize(numrows);
                if (pageno == 0)
                    pageno = 1;

                m_IuserDao.pageNo(pageno);

                // always true criteria 
                if (criteriumList.Count == 0)
                    criteriumList.Add(Expression.Ge("Deleted", 0));

                List<vrmUser> userList = m_IuserDao.GetByCriteria(criteriumList);

                foreach (vrmUser vUser in userList)
                {
                    outXml += "<user>";
                    outXml += "<userID>" + vUser.userid.ToString() + "</userID>";
                    outXml += "<firstName>" + vUser.FirstName + "</firstName>";
                    outXml += "<lastName>" + vUser.LastName + "</lastName>";
                    outXml += "<login>" + vUser.Login + "</login>";
                    outXml += "<email>" + vUser.Email + "</email>";
                    outXml += "<telephone>" + vUser.Telephone + "</telephone>";

                    if (type != m_isLDAP)
                    {
                        if (vUser.Password != null)
                            outXml += "<password>" + vUser.Password + "</password>";
                        else
                            outXml += "<password></password>";
                    }

                    if (mode == 1)
                    {
                        IUserAccountDao IuserAccountDAO = m_userDAO.GetUserAccountDao();
                        vrmAccount uAccount = IuserAccountDAO.GetByUserId(vUser.userid);//FB 2027

                        outXml += "<report>";
                        outXml += "<total>" + uAccount.TotalTime.ToString() + "</total>";
                        long timeUsed = uAccount.TotalTime - uAccount.TimeRemaining;

                        outXml += "<used>" + timeUsed.ToString() + "</used>";
                        // used = totalAmount- left
                        outXml += "<left>" + uAccount.TimeRemaining.ToString() + "</left>";
                        outXml += "</report>";
                    }
                    else if (mode == 2)
                    {
                        string lockstatus = "0";
                        string deleted = "0";

                        if (vUser.LockStatus != 0)
                            lockstatus = "1";

                        if (vUser.Deleted != 0) // Deleted
                            deleted = "1";

                        outXml += "<status>";
                        outXml += "<level>" + vUser.Admin.ToString() + "</level>";
                        outXml += "<deleted>" + deleted + "</deleted>";
                        outXml += "<locked>" + lockstatus + "</locked>";
                        outXml += "</status>";
                    }
                    outXml += "</user>";
                }
                outXml += "<pageNo>" + pageno.ToString() + "</pageNo>";
                outXml += "<totalPages>" + totpages.ToString() + "</totalPages>";
                outXml += "<sortBy>" + sortBy.ToString() + "</sortBy>";
                outXml += "<alphabet>" + alphabet + "</alphabet>";
                if (thresholdpassed)
                    outXml += "<canAll>0</canAll>";
                else
                    outXml += "<canAll>1</canAll>";

                outXml += "<totalNumber>" + numrecs.ToString() + "</totalNumber>";
                if (type != m_isLDAP)
                {
                    int usersRemain = sysSettings.UserLimit - numrows;
                    outXml += "<licensesRemain>" + usersRemain.ToString() + "</licensesRemain>";
                }
                outXml += "</users>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        //Code added for FB Issue 826 -- End

        //Code Added for Webservice START
        #region ChkUserValidation
        public bool ChkUserValidation(ref vrmDataObject obj)
        {
            string sLogin = "";
            string sPwd = "";
            string sEnPwd = "";
            string sAuthenticated = "NO";//SSo Mode
            string UserDomain = "";//zd 103784
            int errID = 0, sClient = 0;//ZD 102052
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                
                node = xd.SelectSingleNode("//myVRM/login");
                if (node.InnerText != "")
                    sLogin = node.InnerXml.Trim();
                else
                {
                    myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                //ZD 102052 Starts
                node = xd.SelectSingleNode("//myVRM/client");
                if (node.InnerText != "")
                    int.TryParse(node.InnerXml.Trim(), out sClient);
                //ZD 102052 End
                //ZD 103784 Starts
                node = xd.SelectSingleNode("//myVRM/userDomain");
                if (node != null)
                   if (node.InnerText != "")
                       UserDomain = node.InnerXml.Trim();
                //ZD 103784 End
                node = xd.SelectSingleNode("//myVRM/IsAuthenticated");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {

                    node = xd.SelectSingleNode("//myVRM/password");
                    if (node.InnerText != "")
                        sPwd = node.InnerXml.Trim();
                    else
                    {//102684 start
                        node = xd.SelectSingleNode("//myVRM/importtdb");
                        if (node != null && node.InnerText == "1")
                        {
                            sPwd = "";
                        }
                        else
                        {
                            myvrmEx = new myVRMException(530); //FB 2054
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //102684 End
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Login", sLogin));
                if(!String.IsNullOrEmpty(UserDomain))
                    criterionList.Add(Expression.Eq("UserDomain", UserDomain)); //ZD 103784
                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList);

                //FB 2054 Starts
                if ((vrmUsr.Count <= 0))
                {
                    myvrmEx = new myVRMException(207); //FB 2054 //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;

                }

                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                if (vrmUsr[0].accountexpiry <= DateTime.Now)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(263);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (vrmUsr[0].LockStatus == 2 || vrmUsr[0].LockStatus == 3)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(254);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                else if (vrmUsr[0].lockCntTrns == 10)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(425);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2054 End
                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                if (usrOrg == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (usrOrg.EnableAPI == 1)
                {
                    //ZD 102052 Starts

                    if ((sClient == (int)clientFrom.OutlookYork && vrmUsr[0].enableExchange == 1) || (sClient == (int)clientFrom.OutlookGeneric && vrmUsr[0].enableExchange == 1) || (sClient == (int)clientFrom.LotusYork && vrmUsr[0].enableDomino == 1) || (sClient == (int)clientFrom.LotusGeneric && vrmUsr[0].enableDomino == 1)
                        || ((sClient == (int)clientFrom.AppleDevices && vrmUsr[0].enableMobile == 1)) || (sClient == (int)clientFrom.Exchange) || (sClient == (int)clientFrom.AndroidDevices)) //ZD 103346
                    {
                        obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID></success>";
                    }
                    else
                    {
                        if ((sClient == (int)clientFrom.OutlookYork && vrmUsr[0].enableExchange == 0) || (sClient == (int)clientFrom.OutlookGeneric && vrmUsr[0].enableExchange == 0))
                            errID = 523;
                        else if ((sClient == (int)clientFrom.LotusYork && vrmUsr[0].enableDomino == 0) || (sClient == (int)clientFrom.LotusGeneric && vrmUsr[0].enableDomino == 0))
                            errID = 524;
                        else if ((sClient == (int)clientFrom.AppleDevices && vrmUsr[0].enableMobile == 0))//ZD 103346
                            errID = 525;
                        else
                            errID = 522;

                        myvrmEx = new myVRMException(errID);
                        obj.outXml = myvrmEx.FetchErrorMsg();

                        return false;
                    }
                    //ZD 102052 Starts

                    //ZD 103239 starts
                    int EnablePIMServiceType = usrOrg.EnablePIMServiceType; ;
                    if (sClient == (int)clientFrom.AppleDevices)
                        EnablePIMServiceType = 1;

                    obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID><PIMServiceType>" + EnablePIMServiceType + "</PIMServiceType><Login>" + sLogin + "</Login><IsAuthenticated>" + sAuthenticated + "</IsAuthenticated></success>";//FB 2038//ZD 101380
                    //ZD 103239 End
                    return true;
                }
                myvrmEx = new myVRMException(252);
                obj.outXml = myvrmEx.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region CheckUserCredentials
        /// <summary>
        /// CheckUserCredentials
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CheckUserCredentials(ref vrmDataObject obj)
        {
            string sLogin = "";
            string sPwd = "";
            string sEnPwd = "";
            int sClient = 0;//FB 1899,1900
            string sAuthenticated = "NO";
            int errID;
            bool tdbuserpwd = false; //102684 
            bool isValid = false; //ZD 102826
            string UserDomain = "";//ZD 103784
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//myVRM/login");
                if (node.InnerText != "")
                    sLogin = node.InnerXml.Trim();
                else
                {
                    myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//myVRM/IsAuthenticated");
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//myVRM/userDomain");// 103784 starts
                if (node != null)
                    if (node.InnerText != "")
                        UserDomain = node.InnerXml.Trim();
                //ZD 102826 Starts
                node = xd.SelectSingleNode("//roomUID");
                if (node != null && node.InnerText != "")
                {
                    node = xd.SelectSingleNode("//myVRM/commandname");
                    if (node != null && node.InnerText.Trim().ToLower() == "gethome")
                        isValid = false;
                    else
                        isValid = true;
                }
                //ZD 102826 End

                List<ICriterion> criterionList = new List<ICriterion>();
                //102684 start
                node = xd.SelectSingleNode("//myVRM/importtdb");
                if (node != null && node.InnerText == "1")
                {
                    sPwd = "";
                    tdbuserpwd = true;
                }
                //102684 End
                if (sAuthenticated.ToUpper() == "NO")
                {

                    node = xd.SelectSingleNode("//myVRM/password");
                    if (node.InnerText != "")
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        if (!tdbuserpwd)//102684 
                        {
                            myvrmEx = new myVRMException(530); //FB 2054
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 1899,1900 Start
                    node = xd.SelectSingleNode("//myVRM/client");
                    if (node.InnerText != "")
                        int.TryParse(node.InnerXml.Trim(), out sClient);

                    //cFrom = GetClientName(sClient);
                    //FB 1899,1900 End
                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);

                    criterionList.Add(Expression.Eq("Email", sLogin));
                }
                else
                {
                    criterionList.Add(Expression.Eq("Login", sLogin));
                    criterionList.Add(Expression.Eq("UserDomain", UserDomain));//103784 ZD
                }


                //criterionList.Add(Expression.Eq("Password", sEnPwd));//FB 2054

                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList);



                if (vrmUsr.Count <= 0)
                {
                    //ZD 101865 starts
                    int usrid = 0;
                    int orgid1 = defaultOrgId;//ZD 102052
                    string fname = ""; string lname = "";
                    int role = 7;
                    node = xd.SelectSingleNode("//myVRM/importtdb");
                    if (node != null)
                    {
                        if (node.InnerText != "")
                        {
                            XmlNode Node1;
                            Node1 = xd.SelectSingleNode("//myVRM/userfname");
                            if (Node1 != null)
                            {
                                if (Node1.InnerText != "")
                                    fname = Node1.InnerXml.Trim();
                            }
                            Node1 = xd.SelectSingleNode("//myVRM/userlname");
                            if (Node1 != null)
                            {
                                if (Node1.InnerText != "")
                                    lname = Node1.InnerXml.Trim();
                            }
                            int usrOrigin = Convert.ToInt16(userOrigin.Outlook);
                            bool createUserbyTemplate = false;
                            //if (SetDynamicHost(sLogin, ref orgid1, ref usrid, lname, fname, lname, role, 2))//ZD 101865
                            string Domain = "";
                            if (SetDynamicHost(null, sLogin, fname, lname, sLogin, role, usrOrigin, createUserbyTemplate, ref orgid1, ref usrid, Domain)) //ZD 102863
                            {
                                obj.outXml = "<success><organizationID>" + defaultOrgId.ToString() + "</organizationID><LoginUserId>" + usrid + "</LoginUserId></success>";//FB 2038 //FB 2724
                                return true;
                            }
                            else
                            {
                                
                                myvrmEx = new myVRMException(207); //FB 2054 //FB 3055
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    else
                    {
                       
                        myvrmEx = new myVRMException(207); //FB 2054 //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }//ZD 101865 ends
                }
                OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                if (usrOrg == null)
                {
                  
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                ////ZD 101890 Starts
                if (usrOrg.EnableAPI == 1)
                {
                    //FB 1899,1900 Start
                    //ZD 102826
                    if ((sClient == (int)clientFrom.OutlookYork && vrmUsr[0].enableExchange == 1) || (sClient == (int)clientFrom.OutlookGeneric && vrmUsr[0].enableExchange == 1) || (sClient == (int)clientFrom.LotusYork && vrmUsr[0].enableDomino == 1) || (sClient == (int)clientFrom.LotusGeneric && vrmUsr[0].enableDomino == 1)
                        || ((sClient == (int)clientFrom.AppleDevices && vrmUsr[0].enableMobile == 1 || isValid)) || (sClient == (int)clientFrom.Exchange) || (sClient == (int)clientFrom.AndroidDevices)) //ZD 103346
                    {
                        obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID></success>";
                        //return true;
                    }
                    else
                    {
                        if ((sClient == (int)clientFrom.OutlookYork && vrmUsr[0].enableExchange == 0) || (sClient == (int)clientFrom.OutlookGeneric && vrmUsr[0].enableExchange == 0))
                            errID = 523;
                        else if ((sClient == (int)clientFrom.LotusYork && vrmUsr[0].enableDomino == 0) || (sClient == (int)clientFrom.LotusGeneric && vrmUsr[0].enableDomino == 0))
                            errID = 524;
                        else if ((sClient == (int)clientFrom.AppleDevices && vrmUsr[0].enableMobile == 0))//ZD 103346
                            errID = 525;
                        else
                            errID = 522;

                        myvrmEx = new myVRMException(errID);
                        obj.outXml = myvrmEx.FetchErrorMsg();

                        return false;
                    }
                    //FB 1899,1900 End
                }
                //if (vrmUsr[0].enableExchange != 1)
                //{
                //    myvrmEx = new myVRMException(252);
                //    obj.outXml = myvrmEx.FetchErrorMsg();
                //    return false;
                //}
                ////ZD 101890 Ends

                //FB 2054 Start
                if (sAuthenticated.ToUpper() == "NO")
                {
                    if (!tdbuserpwd) //102684 
                    {
                        if (vrmUsr[0].Password != sEnPwd)
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }
                if (vrmUsr[0].accountexpiry <= DateTime.Now)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(263);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (vrmUsr[0].LockStatus == 2 || vrmUsr[0].LockStatus == 3)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(254);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                else if (vrmUsr[0].lockCntTrns == 10)
                {
                    if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                        myvrmEx = new myVRMException(207);
                    else
                        myvrmEx = new myVRMException(425);

                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2054 End
                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (usrOrg.EnableAPI == 1)
                {
                    //ZD 103239 starts
                    int EnablePIMServiceType = usrOrg.EnablePIMServiceType; ;
                    if (sClient == (int)clientFrom.AppleDevices)
                        EnablePIMServiceType = 1;

                    obj.outXml = "<success><organizationID>" + vrmUsr[0].companyId + "</organizationID><PIMServiceType>" + EnablePIMServiceType + "</PIMServiceType><LoginUserId>" + vrmUsr[0].userid + "</LoginUserId></success>";//FB 2038 //FB 2724
                    //ZD 103239 End
                    return true;
                }
                else
                {
                    myvrmEx = new myVRMException(252);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
            }
            //}
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                obj.outXml = "";//FB 1881
                return false;
            }
        }
        #endregion
        //Code Added for Webservice END

        /* Organization Module - start */
        #region ManageApproverCounters
        /// <summary>
        /// Every user has a field 'approver' in the USER table which is actually 
        /// a counter. For every instance that a user is made an approver, this counter	
        /// is incremented. And if a user is removed from the approver list of any 
        /// entity, her approver counter is decremented. The flag signifies increment
        /// or decrement
        /// </summary>
        /// <param name="approverLevel">Entity Level (Room/MCU/Dept/System)</param>
        /// <param name="operatingFlag">Specifies whether increment(1) or decrement (2) by 1</param>
        /// <param name="approversList">List of Entity Approvers ID</param>
        /// <returns></returns>
        internal bool ManageApproverCounters(int approverLevel, int operatingFlag, List<int> approversList)
        {
            int COUNTER_INCREMENT = 1;
            int COUNTER_DECREMENT = 2;
            try
            {
                if (approverLevel == (int)LevelEntity.DEPT)
                    return true;

                if (approversList != null)
                {
                    vrmUser approver = null;
                    foreach (int approverID in approversList)
                    {
                        approver = m_IuserDao.GetByUserId(approverID);
                        if (approver != null)
                        {
                            int appCount = approver.approverCount;

                            if (operatingFlag == COUNTER_INCREMENT)
                                approver.approverCount = appCount + 1;
                            else if (operatingFlag == COUNTER_DECREMENT)
                                approver.approverCount = appCount - 1;

                            // In case the approver count goes below zero, set it back to zero
                            // This is just a fault-tolerance mechanism
                            if (approver.approverCount < 0)
                                approver.approverCount = 0;

                            m_IuserDao.SaveOrUpdate(approver);
                        }
                        approver = null;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                return false;
            }
        }
        #endregion
        /* Organization Module - end */

        #region BlockAllUsers
        public bool BlockAllUsers(ref vrmDataObject obj)
        {
            int strExec = -1;
            ns_SqlHelper.SqlHelper sqlCon = null;
            string updtqry = "";
            string succs = "0";
            try
            {
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);

                sqlCon.OpenConnection();
                updtqry = "Update USR_LIST_D set LockStatus = 3, LockcntTrns = 10 where UserID <> 11 and LockStatus <> 3"; //FB 1606
                strExec = sqlCon.ExecuteNonQuery(updtqry);

                if (strExec > 0)
                    succs = "1";
                obj.outXml = "<success>" + succs + "</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally  //FB 1606
            {
                if (sqlCon != null)
                {
                    sqlCon.CloseConnection();
                    sqlCon = null;
                }
            }
        }
        #endregion

        #region UNBlockAllUsers
        public bool UnBlockAllUsers(ref vrmDataObject obj)
        {
            int strExec = -1;
            ns_SqlHelper.SqlHelper sqlCon = null;
            string updtqry = "";
            string succs = "0";
            try
            {
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);

                sqlCon.OpenConnection();

                updtqry = "Update USR_LIST_D set LockStatus=0, LockcntTrns=0 where UserID <> 11 and LockcntTrns=10 and LockStatus=3";

                strExec = sqlCon.ExecuteNonQuery(updtqry);

                if (strExec > 0)
                    succs = "1";
                obj.outXml = "<success>" + succs + "</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally  //FB 1606
            {
                if (sqlCon != null)
                {
                    sqlCon.CloseConnection();
                    sqlCon = null;
                }
            }
        }
        #endregion

        //ZD 104524 start
        #region GetAudioUserList_old
        public bool GetAudioUserList_old(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userID = 0;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                //FB 2274 Starts
                int mutliOrgID = 0;
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;
                //FB 2274 Ends

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                List<ICriterion> criterionList = new List<ICriterion>();
                ICriterion criterium = null;
                criterionList.Add(Expression.Eq("companyId", organizationID));
                criterionList.Add(Expression.Eq("Audioaddon", "1"));
                criterionList.Add(Expression.Eq("LockStatus", 0));
                if (multiDepts == 1)
                {
                    vrmUser user = m_IuserDao.GetByUserId(userID);
                    if (getDeptLevelCriterionforAudiousers(user, ref criterium))
                    {
                        criterionList.Add(criterium);
                    }
                }

                IList userList = m_IuserDao.GetByCriteria(criterionList);
                outXML.Append("<users>");
                vrmBaseUser userDetail = null;
                //foreach (vrmBaseUser userDetail in userList)
                for (int i = 0; i < userList.Count; i++)
                {
                    userDetail = (vrmBaseUser)userList[i];
                    outXML.Append("<user>");
                    outXML.Append("<userID>" + userDetail.userid + "</userID>");
                    outXML.Append("<firstName>" + userDetail.FirstName + "</firstName>");
                    outXML.Append("<lastName>" + userDetail.LastName + "</lastName>");
                    outXML.Append("<userEmail>" + userDetail.Email + "</userEmail>");
                    outXML.Append("<login>" + userDetail.Login + "</login>");
                    outXML.Append("<audioaddon>" + userDetail.Audioaddon + "</audioaddon>");

                    //FB 2341 - Start
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("endpointid", userDetail.endpointId));
                    criterionList.Add(Expression.Eq("deleted", 0));
                    List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);
                    if (epList != null && epList.Count > 0)
                    {
                        outXML.Append("<audioDialIn>" + epList[0].address + "</audioDialIn>");
                        outXML.Append("<confCode>" + epList[0].ConferenceCode + "</confCode>");
                        outXML.Append("<leaderPin>" + epList[0].LeaderPin + "</leaderPin>");
                        outXML.Append("<AddressType>" + epList[0].addresstype + "</AddressType>");  //ZD 103574
                        outXML.Append("<Protocol>" + epList[0].protocol + "</Protocol>");  //ZD 103574
                    }
                    else
                    {
                        outXML.Append("<audioDialIn></audioDialIn>");
                        outXML.Append("<confCode></confCode>");
                        outXML.Append("<leaderPin></leaderPin>");
                        outXML.Append("<AddressType></AddressType>");  //ZD 103574
                        outXML.Append("<Protocol></Protocol>");  //ZD 103574
                    }
                    //outXML.Append("<audioDialIn>" +  userDetail.IPISDNAddress + "</audioDialIn>");
                    //outXML.Append("<confCode>" + userDetail.ConferenceCode + "</confCode>");
                    //outXML.Append("<leaderPin>" + userDetail.LeaderPin + "</leaderPin>");
                    //FB 2341 - End

                    //FB 2227 Starts
                    outXML.Append("<IntVideoNum>" + userDetail.InternalVideoNumber + "</IntVideoNum>");
                    outXML.Append("<ExtVideoNum>" + userDetail.ExternalVideoNumber + "</ExtVideoNum>");
                    outXML.Append("<PartyCode>" + userDetail.ParticipantCode + "</PartyCode>");  //ZD 101446
                    //FB 2227 Ends
                    outXML.Append("</user>");
                }
                outXML.Append("</users>");

                obj.outXml = outXML.ToString();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetAudioUserList
        public bool GetAudioUserListOld(ref vrmDataObject obj)//ALLDEV-814
        {
            int confid = 0, instanceid = 0, userid = 0, tzone = 0, mutliOrgID = 0;
            string cid = "",confdate ="", startTime = "", duration = "00", immediate = "", endTime = "", stmt = "";;
            double dur = 0;
            List<ICriterion> criterionLst = new List<ICriterion>();
            List<int> ConflictAudbridgeList = new List<int>();
            StringBuilder outXML = new StringBuilder();
            DateTime confdatetime = DateTime.Now;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode xNode;

                xNode = xd.SelectSingleNode("//login/userID");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out userid);

                xNode = xd.SelectSingleNode("//login/confID");
                if (xNode != null)
                    cid = xNode.InnerText.Trim();

                if (cid.ToLower() == "new" || cid == "")
                {
                    confid = 0;
                    instanceid = 0;
                }
                else
                {
                    CConfID cconf = new CConfID(cid);
                    confid = cconf.ID;
                    instanceid = cconf.instance;
                }

                xNode = xd.SelectSingleNode("//login/timeZone");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out tzone);
                else
                    tzone = sysSettings.TimeZone;

                xNode = xd.SelectSingleNode("//login/durationMin");
                if (xNode != null)
                    duration = (xNode.InnerText.Trim() == "") ? "00" : xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("//login/organizationID");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out organizationID);

                xNode = xd.SelectSingleNode("//login/multisiloOrganizationID");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out mutliOrgID);

                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;

                xNode = xd.SelectSingleNode("//login/immediate");
                if (xNode != null)
                    immediate = xNode.InnerText.Trim();

                if (immediate == "1")
                    tzone = sysSettings.TimeZone;

                xNode = xd.SelectSingleNode("login/startDate");
                if (xNode != null)
                    confdate = xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("login/startHour");
                if (xNode != null)
                    startTime += " " + xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("login/startMin");
                if (xNode != null)
                    startTime += ":" + xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("login/startSet");
                if (xNode != null)
                    startTime += " " + xNode.InnerText.Trim();

                if (!string.IsNullOrEmpty(confdate) && !string.IsNullOrEmpty(startTime))
                {
                    confdatetime = Convert.ToDateTime(confdate + " " + startTime);

                    double.TryParse(duration, out dur);

                    timeZone.changeToGMTTime(tzone, ref confdatetime);

                    DateTime endDate = confdatetime.AddMinutes(dur);

                    //ZD 104877 - Start
                    confdatetime = confdatetime.AddSeconds(50);
                    endDate = endDate.AddSeconds(30);
                    //ZD 104877 - End

                    startTime = confdatetime.ToString();
                    endTime = endDate.ToString();
                }

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime) && orgInfo.EnableAudbridgefreebusy == 1) // ZD 104854-Disney
                {
                    stmt = " select cf.userid,cf.confid,cf.instanceid,l.FirstName,l.LastName from myVRM.DataLayer.vrmConfUser cf, myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmUser l ";
                    stmt += " where ( ('" + startTime + "' >= cf.ConfStartDate and '" + startTime + "'  < dateadd(minute, cf.Duration,cf.ConfStartDate)) ";
                    stmt += " or (cf.ConfStartDate >= '" + startTime + "'  and cf.ConfStartDate < '" + endTime + "') ) and c.confid = cf.confid and c.instanceid=cf.instanceid ";
                    stmt += "  and c.orgId ='" + organizationID.ToString() + "'  and c.deleted = 0   AND ( NOT (cf.confid = '" + confid.ToString() + "')) and l.userid = cf.userid ";


                    IList conferences = m_IConferenceDAO.execQuery(stmt);
                    if (conferences != null && conferences.Count > 0)
                    {
                        int bridgeid = 0;
                        foreach (object[] ob in conferences)
                        {
                            int.TryParse(ob[0].ToString(), out bridgeid);
                            ConflictAudbridgeList.Add(bridgeid);
                        }
                    }
                }

                criterionLst.Add(Expression.Eq("companyId", organizationID));
                criterionLst.Add(Expression.Eq("Audioaddon", "1"));
                criterionLst.Add(Expression.Eq("LockStatus", 0));
                if (multiDepts == 1)
                {
                    ICriterion criterium = null;
                    vrmUser user = m_IuserDao.GetByUserId(userid);
                    if (getDeptLevelCriterionforAudiousers(user, ref criterium))
                    {
                        criterionLst.Add(criterium);
                    }
                }
                if (ConflictAudbridgeList.Count > 0)
                    criterionLst.Add(Expression.Not(Expression.In("userid", ConflictAudbridgeList)));

                IList userList = m_IuserDao.GetByCriteria(criterionLst);

                outXML.Append("<users>");
                vrmBaseUser userDetail = null;
                List<ICriterion> criterionList = null;
                for (int i = 0; i < userList.Count; i++)
                {
                    userDetail = (vrmBaseUser)userList[i];
                    outXML.Append("<user>");
                    outXML.Append("<userID>" + userDetail.userid + "</userID>");
                    outXML.Append("<firstName>" + userDetail.FirstName + "</firstName>");
                    outXML.Append("<lastName>" + userDetail.LastName + "</lastName>");
                    outXML.Append("<userEmail>" + userDetail.Email + "</userEmail>");
                    outXML.Append("<login>" + userDetail.Login + "</login>");
                    outXML.Append("<audioaddon>" + userDetail.Audioaddon + "</audioaddon>");

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("endpointid", userDetail.endpointId));
                    criterionList.Add(Expression.Eq("deleted", 0));
                    List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);
                    if (epList != null && epList.Count > 0)
                    {
                        outXML.Append("<audioDialIn>" + epList[0].address + "</audioDialIn>");
                        outXML.Append("<confCode>" + epList[0].ConferenceCode + "</confCode>");
                        outXML.Append("<leaderPin>" + epList[0].LeaderPin + "</leaderPin>");
                        outXML.Append("<AddressType>" + epList[0].addresstype + "</AddressType>");
                        outXML.Append("<Protocol>" + epList[0].protocol + "</Protocol>");
                    }
                    else
                    {
                        outXML.Append("<audioDialIn></audioDialIn>");
                        outXML.Append("<confCode></confCode>");
                        outXML.Append("<leaderPin></leaderPin>");
                        outXML.Append("<AddressType></AddressType>");
                        outXML.Append("<Protocol></Protocol>");
                    }
                    outXML.Append("<IntVideoNum>" + userDetail.InternalVideoNumber + "</IntVideoNum>");
                    outXML.Append("<ExtVideoNum>" + userDetail.ExternalVideoNumber + "</ExtVideoNum>");
                    outXML.Append("<PartyCode>" + userDetail.ParticipantCode + "</PartyCode>");

                    outXML.Append("</user>");
                }
                outXML.Append("</users>");

                obj.outXml = outXML.ToString();
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetAvailableRoom :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAvailableRoom :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //ALLDEV-814
        #region GetAudioUserList
        public bool GetAudioUserList(ref vrmDataObject obj)
        {
            int confid = 0, instanceid = 0, userid = 0, tzone = 0, mutliOrgID = 0;
            string cid = "", confdate = "", startTime = "", duration = "00", immediate = "", endTime = "", stmt = "", fetchFrom="";
            double dur = 0;
            List<ICriterion> criterionLst = new List<ICriterion>();
            List<int> ConflictAudbridgeList = new List<int>();
            StringBuilder outXML = new StringBuilder();
            DateTime confdatetime = DateTime.Now;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode xNode;

                xNode = xd.SelectSingleNode("//login/userID");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out userid);

                xNode = xd.SelectSingleNode("//login/confID");
                if (xNode != null)
                    cid = xNode.InnerText.Trim();

                if (cid.ToLower() == "new" || cid == "")
                {
                    confid = 0;
                    instanceid = 0;
                }
                else
                {
                    CConfID cconf = new CConfID(cid);
                    confid = cconf.ID;
                    instanceid = cconf.instance;
                }

                xNode = xd.SelectSingleNode("//login/timeZone");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out tzone);
                else
                    tzone = sysSettings.TimeZone;
                
                
                xNode = xd.SelectSingleNode("//login/durationMin");
                if (xNode != null)
                    duration = (xNode.InnerText.Trim() == "") ? "00" : xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("//login/organizationID");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out organizationID);

                xNode = xd.SelectSingleNode("//login/multisiloOrganizationID");
                if (xNode != null)
                    int.TryParse(xNode.InnerText.Trim(), out mutliOrgID);

                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;

                xNode = xd.SelectSingleNode("//login/immediate");
                if (xNode != null)
                    immediate = xNode.InnerText.Trim();

                if (immediate == "1")
                    tzone = sysSettings.TimeZone;

                xNode = xd.SelectSingleNode("login/startDate");
                if (xNode != null)
                    confdate = xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("login/startHour");
                if (xNode != null)
                    startTime += " " + xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("login/startMin");
                if (xNode != null)
                    startTime += ":" + xNode.InnerText.Trim();

                xNode = xd.SelectSingleNode("login/startSet");
                if (xNode != null)
                    startTime += " " + xNode.InnerText.Trim();

                if (!string.IsNullOrEmpty(confdate) && !string.IsNullOrEmpty(startTime))
                {
                    confdatetime = Convert.ToDateTime(confdate + " " + startTime);

                    double.TryParse(duration, out dur);

                    timeZone.changeToGMTTime(tzone, ref confdatetime);

                    DateTime endDate = confdatetime.AddMinutes(dur);

                    //ZD 104877 - Start
                    confdatetime = confdatetime.AddSeconds(45); //ALLDEV-814
                    endDate = endDate.AddSeconds(45);//ALLDEV-814
                    //ZD 104877 - End

                    startTime = confdatetime.ToString();
                    endTime = endDate.ToString();
                }

                //ALLDEV-814
                xNode = xd.SelectSingleNode("//login/FetchFrom");
                if (xNode != null)
                    fetchFrom = xNode.InnerText.Trim();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                DataSet ds = new DataSet();

                m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);
                m_rptLayer.OpenConnection();

                stmt = "select * from ( select * ";
                stmt += ",row_number() over (partition by FirstName order by profileId) as AudBgeAvail ";
                stmt += " from (select cast(a.UserID as varchar(10)) +'-'+ cast(b.profileId as varchar(10)) as AudionAddonRefID, userid, lastname, FirstName ,Email,Login,Audioaddon,";
                stmt += " InternalVideoNumber,ExternalVideoNumber,b.ParticipantCode,b.address, b.ConferenceCode,b.LeaderPin, b.addresstype, b.protocol,profileId, uId,b.IsUserAudio ";
                
                stmt += " from usr_list_d a, Ept_List_D b where a.endpointId = b.endpointId and Audioaddon = 1 and LockStatus = 0 and companyId='" + organizationID.ToString() + "') as x where 1=1 ";

                if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime) && orgInfo.EnableAudbridgefreebusy == 1) // ZD 104854-Disney
                {
                    stmt += " and x.AudionAddonRefID not in ( ";
                    stmt += " select cast(cf.UserID as varchar(10)) +'-'+ cast(l.profileId as varchar(10))  from Conf_User_D cf, Conf_Conference_D c ";
                    stmt += " , (select l.UserID, l.firstName, l.LastName,ep.profileId, ep.IsUserAudio from Usr_List_D l, Ept_List_D ep where l.endpointId = ep.endpointId and l.Audioaddon=1) l ";
                    stmt += " where c.ConfDate between '" + startTime + "' and '" + endTime + "'";
                    stmt += "  and c.confid = cf.confid and c.instanceid=cf.instanceid and l.IsUserAudio = 0 ";
                    stmt += "  and c.orgId ='" + organizationID.ToString() + "'  and c.deleted = 0   AND ( NOT (cf.confid = '" + confid.ToString() + "')) and l.userid = cf.userid and l.profileId = cf.profileID )";
                }
                stmt += " ) as Y ";

                if (fetchFrom.ToLower() == "express")
                    stmt += " where AudBgeAvail = 1 ";

                stmt += " order by FirstName ";

                if (stmt != "")
                    ds = m_rptLayer.ExecuteDataSet(stmt);

                //Doubt
                //if (multiDepts == 1)
                //{
                //    ICriterion criterium = null;
                //    vrmUser user = m_IuserDao.GetByUserId(userid);
                //    if (getDeptLevelCriterionforAudiousers(user, ref criterium))
                //    {
                //        criterionLst.Add(criterium);
                //    }
                //}

                outXML.Append("<users>");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];

                        outXML.Append("<user>");
                        outXML.Append("<userID>" + dr["userid"].ToString() + "</userID>");
                        outXML.Append("<firstName>" + dr["FirstName"].ToString() + "</firstName>");
                        outXML.Append("<lastName>" + dr["LastName"].ToString() + "</lastName>");
                        outXML.Append("<userEmail>" + dr["Email"].ToString() + "</userEmail>");
                        outXML.Append("<login>" + dr["Login"].ToString() + "</login>");
                        outXML.Append("<audioaddon>" + dr["Audioaddon"].ToString() + "</audioaddon>");

                        outXML.Append("<audioDialIn>" + dr["address"].ToString() + "</audioDialIn>");
                        outXML.Append("<confCode>" + dr["ConferenceCode"].ToString() + "</confCode>");
                        outXML.Append("<leaderPin>" + dr["LeaderPin"].ToString() + "</leaderPin>");
                        outXML.Append("<AddressType>" + dr["addresstype"].ToString() + "</AddressType>");
                        outXML.Append("<Protocol>" + dr["protocol"].ToString() + "</Protocol>");
                        //outXML.Append("<uId>" + dr["uId"].ToString() + "</uId>");
                        outXML.Append("<profileId>" + dr["profileId"].ToString() + "</profileId>");
                        outXML.Append("<IsUserAudio>" + dr["IsUserAudio"].ToString() + "</IsUserAudio>");

                        outXML.Append("<IntVideoNum>" + dr["InternalVideoNumber"].ToString() + "</IntVideoNum>");
                        outXML.Append("<ExtVideoNum>" + dr["ExternalVideoNumber"].ToString() + "</ExtVideoNum>");
                        outXML.Append("<PartyCode>" + dr["ParticipantCode"].ToString() + "</PartyCode>");

                        outXML.Append("</user>");
                    }
                }
                outXML.Append("</users>");

                obj.outXml = outXML.ToString();
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetAudioUserList :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAudioUserList :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //ZD 104524 End

        #region getDeptLevelCriterion
        private bool getDeptLevelCriterionforAudiousers(vrmUser user, ref ICriterion criterium)
        {
            string deptdID = "";
            try
            {
                // superadmin, nothing to do!
                if (user.Admin == vrmUserConstant.SUPER_ADMIN)
                    return false;

                List<ICriterion> criterionList = new List<ICriterion>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> In = new List<int>();

                criterionList.Add(Expression.Eq("userId", user.userid));
                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                foreach (vrmUserDepartment dept in deptList)
                {
                    if (deptdID == "")
                        deptdID = dept.departmentId.ToString();
                    else
                        deptdID += "," + dept.departmentId.ToString();
                }
                string stmt = "SELECT DISTINCT c.userId FROM myVRM.DataLayer.vrmUserDepartment c WHERE c.departmentId in ( " + deptdID + ")";
                IList result = m_IuserDeptDAO.execQuery(stmt);
                List<int> userList = new List<int>();
                if (result.Count > 0)
                {
                    foreach (object obj in result)
                    {
                        if (!userList.Contains((int)obj))
                            userList.Add((int)obj);
                    }
                }

                criterium = Expression.In("userid", userList);
                criterium = Expression.Or(criterium, Expression.Eq("userid", 11));


                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //Method changes for ZD 100263 FB 1830 - start
        #region RequestPassword
        public bool RequestPassword(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = null;
            List<vrmUser> requestor = null;
            emailFactory m_Email = null;
            string RequestID = "";//ZD 100263
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                string userEmail = "Its not there.";

                node = xd.SelectSingleNode("//login/emailLoginInfo/email");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        userEmail = node.InnerXml.Trim();
                }

                obj.outXml = "<error>-1</error>";

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Email", userEmail));

                requestor = m_IuserDao.GetByCriteria(criterionList);

                if (requestor != null)
                {
                    if (requestor.Count > 0)
                    {
                        m_Email = new emailFactory(ref obj);
                        //ZD 100263
                        Guid messageId = System.Guid.NewGuid();
                        RequestID = messageId + requestor[0].userid.ToString();
                        requestor[0].RequestID = RequestID;
                        m_IuserDao.SaveOrUpdate(requestor[0]);
                        m_Email.PasswordRequest(requestor[0], ref obj);
                    }
                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region RequestVRMAccount
        /// <summary>
        /// RequestVRMAccount
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RequestVRMAccount(ref vrmDataObject obj)
        {
            emailFactory m_Email = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                string fname = "", lname = "", email = "", loginname = "", addinfo = "", orgid = "11";

                node = xd.SelectSingleNode("//requestVRMAccount/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/firstName");
                if (node != null)
                    fname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/lastName");
                if (node != null)
                    lname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/email");
                if (node != null)
                    email = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/login");
                if (node != null)
                    loginname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//requestVRMAccount/additionalInfo");
                if (node != null)
                    addinfo = node.InnerText.Trim();

                if (fname == "" || email == "" || lname == "")
                {
                    obj.outXml = "Input xml error";
                    return false;
                }
                StringDictionary reqVal = new StringDictionary();
                reqVal.Add("fname", fname);
                reqVal.Add("lname", lname);
                reqVal.Add("email", email);
                reqVal.Add("login", loginname);
                reqVal.Add("orgid", orgid.ToString());
                reqVal.Add("addinfo", addinfo);

                m_Email = new emailFactory(ref obj);
                m_Email.RequestVRMAccount(ref reqVal, ref obj);
            }
            catch (Exception e)
            {
                m_log.Error("RequestVRMAccount", e);
                obj.outXml = "Input xml error";
                return false;
            }
            return true;
        }
        #endregion
        //Method addeded for FB 1830 - end

        //ZD 100263 Start
        #region ChangePasswordRequest
        public bool ChangePasswordRequest(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmUser requestor = null;
            string encryppassword = "", userEmail = "", password = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/emailLoginInfo/email");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        userEmail = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/emailLoginInfo/password");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        password = node.InnerXml.Trim();
                }

                if (userEmail == "" || password == "")
                    return false;

                requestor = m_IuserDao.GetByUserEmail(userEmail);

                if (requestor != null)
                {
                    cryptography.Crypto crypto = new cryptography.Crypto();
                    encryppassword = crypto.encrypt(password);
                    requestor.Password = encryppassword;
                    requestor.RequestID = "";
                    m_IuserDao.SaveOrUpdate(requestor);
                }
                else
                {
                    obj.outXml = "<error>-1</error>";
                    return false;
                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetRequestIDUser
        public bool GetRequestIDUser(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmUser requestor = null;
            string RequestID = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/emailLoginInfo/RequestID");
                if (node != null)
                {
                    if (node.InnerXml.Trim() != "")
                        RequestID = node.InnerXml.Trim();
                }

                if (RequestID == "")
                    return false;

                requestor = m_IuserDao.GetByUserRequestID(RequestID);

                if (requestor != null)
                {
                    obj.outXml = "<emailLoginInfo>";
                    obj.outXml += "<email>" + requestor.Email + "</email>";
                    obj.outXml += "</emailLoginInfo>";
                    requestor.RequestID = "";
                    m_IuserDao.SaveOrUpdate(requestor);
                }
                else
                {
                    obj.outXml = "<error>-1</error>";
                    return false;
                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //ZD 100263 End

        //Method addeded for FB 1860

        #region Get User Emails
        public bool GetUserEmails(ref vrmDataObject obj)
        {
            bool bRet = true;
            int usrID = 0;
            string userID = "";
            try
            {


                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    userID = node.InnerXml.Trim();

                int.TryParse(userID, out usrID);

                if (usrID <= 0)
                {
                    throw new Exception("Invalid User");
                }

                vrmUser Usr = m_IuserDao.GetByUserId(usrID);


                m_emailFactory = new emailFactory(ref obj);

                m_emailFactory.GetBlockedEmails(ref Usr, ref obj);


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region Set User Emails BLock Status
        public bool SetUserEmailsBlockStatus(ref vrmDataObject obj)
        {
            bool bRet = true;
            int usrID = 0;
            int blockStatus = 0;
            string userID = "";
            string blckStatus = "0";
            DateTime blockDate = DateTime.Now;
            try
            {


                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    userID = node.InnerXml.Trim();

                int.TryParse(userID, out usrID);

                node = xd.SelectSingleNode("//login/MailBlocked");
                if (node != null)
                    blckStatus = node.InnerXml.Trim();

                int.TryParse(blckStatus, out blockStatus);

                if (usrID <= 0)
                {
                    throw new Exception("Invalid User");
                }

                vrmUser Usr = m_IuserDao.GetByUserId(usrID);

                if (Usr.MailBlocked != blockStatus)
                {
                    Usr.MailBlocked = blockStatus;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref blockDate);
                    Usr.MailBlockedDate = blockDate;
                    m_IuserDao.SaveOrUpdate(Usr);
                }

                obj.outXml = "<success>1</success>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //Method addeded for FB 1860

        // FB 1899 & 1900
        //#region GetClientName
        //private clientFrom GetClientName(int sClient)
        //{
        //    clientFrom cFrom = clientFrom.None;
        //    try
        //    {
        //        //01 - Outlook York
        //        //02 - Outlook Generic
        //        //03 - Lotus York 
        //        //04 - Lotus Generic
        //        //05 - Ipad
        //        //06 - Other mobile devices

        //        if (sClient == 0)
        //            cFrom = clientFrom.None;
        //        else if (sClient == 01 || sClient == 02)
        //            cFrom = clientFrom.Exchange;
        //        else if (sClient == 03 || sClient == 04)
        //            cFrom = clientFrom.Lotus;
        //        else if (sClient == 05)
        //            cFrom = clientFrom.AppleDevices;
        //        else if (sClient == 06)
        //            cFrom = clientFrom.AndroidDevices;
        //    }
        //    catch (Exception e)
        //    {
        //        m_log.Error("sytemException", e);
        //    }

        //    return cFrom;
        //}
        //#endregion

        //FB 1959 - Start
        #region SetPreferedRoom
        public bool SetPreferedRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userid = 0;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);

                string location = "";
                node = xd.SelectSingleNode("//login/location");
                if (node != null)
                    location = node.InnerXml.Trim();

                vrmUser user = m_IuserDao.GetByUserId(userid);
                if (user != null)
                {
                    user.PreferedRoom = location;
                    user.LastLogin = DateTime.Today;
                    m_IuserDao.SaveOrUpdate(user);
                }
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }

        #endregion

        #region GetPreferedRoom

        public bool GetPreferedRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                StringBuilder strOutXML = new StringBuilder();
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userid = 0;
                node = xd.SelectSingleNode("//login/userID");
                int.TryParse(node.InnerXml.Trim(), out userid);

                strOutXML.Append("<GetPreferedRoom>");
                strOutXML.Append("<userID>" + userid + "</userID>");

                vrmUser user = new vrmUser();
                user = (vrmUser)m_IuserDao.GetByUserId(userid);
                string location = user.PreferedRoom;

                if (location.Trim() != "" && location.Trim() != "0") //FB 2124
                {
                    string[] strAry = null;
                    if (location.IndexOf(',') > 0)
                        strAry = location.Split(',');
                    else if (location != "0")
                    {
                        strAry = new string[1];//WhygoDoubt-2594
                        strAry[0] = location;
                    }

                    List<vrmRoom> locRooms = null;
                    vrmUser userInfo = null;

                    strOutXML.Append("<Rooms>");
                    for (int r = 0; r < strAry.Length; r++)
                    {
                        if (Convert.ToInt32(strAry[r].ToString()) == 0)
                            continue;

                        List<ICriterion> criterionListorg = new List<ICriterion>();
                        if (Convert.ToInt32(strAry[r].ToString()) > 0)
                            criterionListorg.Add(Expression.Eq("RoomID", Convert.ToInt32(strAry[r].ToString())));

                        locRooms = m_IRoomDAO.GetByCriteria(criterionListorg);

                        foreach (vrmRoom locRoom in locRooms)
                        {
                            if (locRoom.Name != "Phantom Room")
                            {
                                strOutXML.Append("<Room>");
                                strOutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");
                                strOutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                                strOutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");
                                strOutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                                strOutXML.Append("<MaximumConcurrentPhoneCalls>" + locRoom.MaxPhoneCall.ToString() + "</MaximumConcurrentPhoneCalls>");
                                //strOutXML.Append("<SetupTime>" + locRoom.SetupTime.ToString() + "</SetupTime>");//ZD 101563
                                //strOutXML.Append("<TeardownTime>" + locRoom.TeardownTime.ToString() + "</TeardownTime>");
                                //ZD 100619 Starts
                                
                                //ALLDEV-807 Starts
                                strOutXML.Append("<Assistants>");
                                //int i = 1;
                                foreach (vrmLocAssistant locRoomAssistant in locRoom.locationAssistant)
                                {
                                    strOutXML.Append("<Assistant>");
                                    userInfo = m_IuserDao.GetByUserId(locRoomAssistant.AssistantId);
                                    strOutXML.Append("<AssistantInchargeID>" + locRoomAssistant.AssistantId.ToString() + "</AssistantInchargeID>");
                                    if (userInfo != null)
                                    {
                                        strOutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                                        strOutXML.Append("<AssistantInchargeEmail>" + userInfo.Email + "  </AssistantInchargeEmail>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<AssistantInchargeName>" + locRoomAssistant.AssitantName + "  </AssistantInchargeName>");
                                        strOutXML.Append("<AssistantInchargeEmail>" + locRoomAssistant.EmailId + "  </AssistantInchargeEmail>");
                                    }
                                    strOutXML.Append("</Assistant>");
                                }
                                strOutXML.Append("</Assistants>");
                                strOutXML.Append("<AssistantInchargeID>" + locRoom.locationAssistant[0].AssistantId + "</AssistantInchargeID>");
                                strOutXML.Append("<AssistantInchargeName>" + locRoom.locationAssistant[0].AssitantName + "</AssistantInchargeName>");
                                strOutXML.Append("<AssistantInchargeEmail>" + locRoom.locationAssistant[0].EmailId + "</AssistantInchargeEmail>");
                                //ALLDEV-807 Ends
                                strOutXML.Append("<GuestContactPhone>" + locRoom.GuestContactPhone + "</GuestContactPhone>");
                                //ZD 100619 Ends
                                strOutXML.Append("<MultipleAssistantEmails>" + locRoom.notifyemails + "</MultipleAssistantEmails>");
                                if (locRoom.tier2 != null)
                                {
                                    if (locRoom.tier2.L3LocationId > 0)
                                    {
                                        strOutXML.Append("<Tier1ID>" + locRoom.tier2.L3LocationId + "</Tier1ID>");
                                        strOutXML.Append("<Tier1Name>" + locRoom.tier2.TopTierName + "</Tier1Name>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<Tier1ID></Tier1ID>");
                                        strOutXML.Append("<Tier1Name></Tier1Name>");
                                    }
                                    strOutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                                    strOutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                                }
                                else
                                {
                                    strOutXML.Append("<Tier1ID></Tier1ID>");
                                    strOutXML.Append("<Tier1Name></Tier1Name>");
                                    strOutXML.Append("<Tier2ID></Tier2ID>");
                                    strOutXML.Append("<Tier2Name></Tier2Name>");
                                }
                                strOutXML.Append("<CatererFacility>" + locRoom.Caterer.ToString() + "</CatererFacility>");
                                strOutXML.Append("<DynamicRoomLayout>" + locRoom.DynamicRoomLayout.ToString() + "</DynamicRoomLayout>");
                                strOutXML.Append("<ProjectorDefault>" + locRoom.ProjectorAvailable.ToString() + "</ProjectorDefault>");
                                strOutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                                strOutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                                strOutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                                strOutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                                strOutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                                strOutXML.Append("<City>" + locRoom.City + "</City>");
                                strOutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                                strOutXML.Append("<Disabled>" + locRoom.disabled.ToString() + "</Disabled>");
                                strOutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                                strOutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                                strOutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                                strOutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                                strOutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                                strOutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");
                                strOutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");

                                int appCnt = 0, j = 1;
                                if (locRoom.locationApprover.Count > 0)
                                    appCnt = 1;

                                strOutXML.Append("<Approval>" + appCnt + "</Approval>");
                                strOutXML.Append("<Approvers>");
                                for (j = 1; j < locRoom.locationApprover.Count; j++)
                                {
                                    vrmLocApprover locRoomApprov = locRoom.locationApprover[j];
                                    userInfo = m_IuserDao.GetByUserId(locRoomApprov.approverid);
                                    strOutXML.Append("<Approver" + j.ToString() + "ID>" + locRoomApprov.approverid.ToString() + "</Approver" + j.ToString() + "ID>");
                                    strOutXML.Append("<Approver" + j.ToString() + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "  </Approver" + j.ToString() + "Name>");
                                }
                                if (j == 1)
                                {
                                    strOutXML.Append("<Approver1ID></Approver1ID>");
                                    strOutXML.Append("<Approver1Name></Approver1Name>");
                                    strOutXML.Append("<Approver2ID></Approver2ID>");
                                    strOutXML.Append("<Approver2Name></Approver2Name>");
                                    strOutXML.Append("<Approver3ID></Approver3ID>");
                                    strOutXML.Append("<Approver3Name></Approver3Name>");
                                    strOutXML.Append("<ApprovalReq>No</ApprovalReq>");
                                }
                                else if (j == 2)
                                {
                                    strOutXML.Append("<Approver2ID></Approver2ID>");
                                    strOutXML.Append("<Approver2Name></Approver2Name>");
                                    strOutXML.Append("<Approver3ID></Approver3ID>");
                                    strOutXML.Append("<Approver3Name></Approver3Name>");
                                    strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                                }
                                else if (j == 3)
                                {
                                    strOutXML.Append("<Approver3ID></Approver3ID>");
                                    strOutXML.Append("<Approver3Name></Approver3Name>");
                                    strOutXML.Append("<ApprovalReq>Yes</ApprovalReq>");
                                }
                                strOutXML.Append("</Approvers>");
                                strOutXML.Append("</Room>");
                            }
                        }
                    }

                    strOutXML.Append("</Rooms>");
                }
                strOutXML.Append("</GetPreferedRoom>");

                obj.outXml = strOutXML.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }

        #endregion
        //FB 1959 - End

        //FB 2027 - Start
        #region SetUserRoles
        /// <summary>
        /// SetUserRoles (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUserRoles(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            ArrayList activeRoles = new ArrayList();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//userRole/roles/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                XmlElement root = xd.DocumentElement;
                XmlNodeList rlist = root.SelectNodes(@"/userRole/roles/role");
                for (int i = 0; i < rlist.Count; i++)
                {
                    string id, name, menumask;
                    int custadminrights = 0;// ZD 101233
                    node = rlist[i].SelectSingleNode("ID");
                    id = node.InnerXml.Trim();
                    node = rlist[i].SelectSingleNode("name");
                    name = node.InnerXml.Trim();
                    node = rlist[i].SelectSingleNode("menuMask");
                    menumask = node.InnerXml.Trim();
                    node = rlist[i].SelectSingleNode("custAdminRights"); // ZD 101233
                    if (node != null)
                        int.TryParse(node.InnerXml.Trim(), out custadminrights);

                    if (id.ToLower() == "new")
                    {
                        vrmUserRoles role = new vrmUserRoles();
                        role.roleID = getMaxRoleID();
                        role.roleName = name;
                        role.roleMenuMask = menumask;
                        role.level = custadminrights; // ZD 101233
                        role.crossaccess = 0;
                        role.createType = 2;
                        role.DisplayOrder = 0;//FB 2891
                        role.orgID = organizationID; //ZD 100923
                        m_IUserRolesDao.Save(role);
                        activeRoles.Add(role.roleID);
                    }
                    else
                    {
                        int rid;
                        int.TryParse(id, out rid);
                        vrmUserRoles rle = m_IUserRolesDao.GetById(rid);
                        //Latest Spanish
                        if (rle.createType != 1)
                        {
                            if (rle.createType == 2) // ZD 101233
                                rle.level = custadminrights;
                            if (rle != null)
                            {
                                rle.roleName = name;
                                rle.roleMenuMask = menumask;
                                m_IUserRolesDao.SaveOrUpdate(rle);
                                activeRoles.Add(rle.roleID);

                                criterionList1 = new List<ICriterion>();
                                criterionList1.Add(Expression.Eq("roleID", rid));
                                List<vrmUser> usrRole = m_IuserDao.GetByCriteria(criterionList1);
                                for (int l = 0; l < usrRole.Count; l++)
                                {
                                    usrRole[l].MenuMask = menumask;
                                    m_IuserDao.SaveOrUpdate(usrRole[l]);
                                }
                                List<vrmInactiveUser> inactiveURole = m_IinactiveUserDao.GetByCriteria(criterionList1);
                                for (int k = 0; k < inactiveURole.Count; k++)
                                {
                                    inactiveURole[k].MenuMask = menumask;
                                    m_IinactiveUserDao.SaveOrUpdate(inactiveURole[k]);
                                }
                            }
                        }
                    }
                }
                List<vrmUserRoles> roles = m_IUserRolesDao.GetAll();
                for (int i = 0; i < roles.Count; i++)
                {
                    if (!activeRoles.Contains(roles[i].roleID))
                    {
                        if (roles[i].locked == 0 && roles[i].orgID == organizationID) //ZD 103987
                        {
                            m_IUserRolesDao.Delete(roles[i]);

                            //ZD 101860
                            List<ICriterion> criterionList3 = new List<ICriterion>();
                            criterionList3.Add(Expression.Eq("roleID", roles[i].roleID));
                            List<vrmInactiveUser> Inusr = m_IinactiveUserDao.GetByCriteria(criterionList3);

                            List<ICriterion> criterionList4 = new List<ICriterion>();
                            criterionList4.Add(Expression.Eq("DisplayOrder", 8));
                            List<vrmUserRoles> rolelist = m_IUserRolesDao.GetByCriteria(criterionList4);

                            if (Inusr != null)
                            {
                                for (int j = 0; j < Inusr.Count; j++)
                                {
                                    Inusr[j].roleID = rolelist[0].roleID;
                                    Inusr[j].MenuMask = rolelist[0].roleMenuMask;
                                    m_IinactiveUserDao.Update(Inusr[j]);
                                }
                            }
                            roles.Remove(roles[i]);
                        }
                    }
                }
                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region getMaxRoleID
        /// <summary>
        /// getMaxRoleID (COM to .Net conversion)
        /// </summary>
        /// <returns></returns>
        private int getMaxRoleID()
        {
            try
            {
                string qString = "select max(rle.roleID) from myVRM.DataLayer.vrmUserRoles rle";

                IList list = m_IUserRolesDao.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = int.Parse(sMax);
                id += 1;
                return id;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SearchUserOrGuest
        /// <summary>
        /// SearchUserOrGuest (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        public bool SearchUserOrGuest(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, superadmin = 0, type = 0;
            string orgid = "", firstName = "", lastName = "", email = "";
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/user/firstName");
                if (node != null)
                    firstName = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/user/lastName");
                if (node != null)
                    lastName = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/user/email");
                if (node != null)
                    email = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/user/type");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out type);

                vrmUser user = m_IuserDao.GetByUserId(userid);
                superadmin = user.Admin;

                if (superadmin == 2)
                    superadmin = 1;
                else
                    superadmin = 0;
                outXML.Append("<users>");
                outXML.Append("<superAdmin>" + superadmin + "</superAdmin>");
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("companyId", organizationID));
                if (firstName.Length > 0)
                    criterionList.Add(Expression.Like("FirstName", "%%" + firstName.ToLower() + "%%").IgnoreCase());
                if (lastName.Length > 0)
                    criterionList.Add(Expression.Like("LastName", "%%" + lastName.ToLower() + "%%").IgnoreCase());
                if (email.Length > 0)
                    criterionList.Add(Expression.Like("Email", "%%" + email.ToLower() + "%%").IgnoreCase());

                IList usrFilt = new List<vrmBaseUser>();
                if (type == 2)
                    usrFilt = m_IGuestUserDao.GetByCriteria(criterionList);
                else
                {   //FB 2704
                    criterionList.Add(Expression.Eq("Audioaddon", "0"));//FB 2673
                    usrFilt = m_IuserDao.GetByCriteria(criterionList);
                }
                crypto = new cryptography.Crypto();
                foreach (vrmBaseUser uf in usrFilt)
                {
                    outXML.Append("<user>");
                    outXML.Append("<userID>" + uf.userid + "</userID>");
                    outXML.Append("<firstName>" + uf.FirstName + "</firstName>");
                    outXML.Append("<lastName>" + uf.LastName + "</lastName>");
                    outXML.Append("<login>" + uf.Login + "</login>");
                    if (uf.Password != null && uf.Password != "") //FB 2318
                        outXML.Append("<password>" + crypto.decrypt(uf.Password) + "</password>");
                    else
                        outXML.Append("<password></password>");

                    outXML.Append("<ErrorMsg>0</ErrorMsg>");//ALLDEV-498

                    outXML.Append("<email>" + uf.Email + "</email>");
                    //ZD 103415 Starts
                    vrmUserRoles role = null;
                    if (uf.roleID > 0)
                    {
                        role = m_IUserRolesDao.GetById(uf.roleID);
                        outXML.Append("<RoleName>" + m_UtilFactory.GetTranslatedText(user.Language, role.roleName) + "</RoleName>");
                    }
                    else
                    {
                        outXML.Append("<RoleName></RoleName>");
                    }
                    //ZD 103415 End 
                    outXML.Append("<status>");
                    outXML.Append("<level>" + uf.Admin + "</level>");
                    outXML.Append("<deleted>" + uf.Deleted + "</deleted>");
                    //ZD 101175 Starts
                    if (uf.LockStatus == 3)
                        outXML.Append("<locked>1</locked>");
                    else
                        outXML.Append("<locked>0</locked>");
                    //ZD 101175 Ends
                    if (type == 2)
                        outXML.Append("<crossaccess>0</crossaccess>");
                    else
                    {
                        outXML.Append("<crossaccess>" + role.crossaccess + "</crossaccess>");
                    }
                    outXML.Append("</status>");
                    outXML.Append("</user>");
                }
                outXML.Append("</users>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion


        // mode = 0: Normal mode returns only basic user information
        // mode = 1: In addition to basic information, returns account
        //			usage report. The table should be "Usr_List_D" (active VRM
        //			user) for this mode
        // mode = 2: In addition to basic information, returns status
        //			information. The table should be "Usr_List_D" (active VRM
        //			user) or Guest for this mode
        //
        // dept = 0 Return user list based on roles (ie super admin sess all
        //          admin sees at dept(s) level and user sees only their entries
        // dept = 1 For GetManageUser (conf create) user can see all in dept
        //

        #region GetUserList
        private bool GetUserList(int userID, int type, int pageno, ref int totpages, string alphabet, int sortBy, int SortingOrder, int mode, ref string outXml, int orgId, int alphabetmode, int deptUsers, string BulkUser, int isconfadmin) //FB 2269 //ZD 103405 //ALLDEV-498

        {
            bool bRet = true;
            try
            {

                int numrows = 0, numrecs = 0, audiobridgerec = 0; //FB 2023
                int isDeptUser = 1, adminaccess = 0, admin = 0; //ZD 100263; //FB 2269
                int level = 0, usersRemain = 0, Roleid = 0, iPageNo = pageno;
                long ttlRecords = 0;
                bool thresholdpassed = false;
                StringBuilder outUser = new StringBuilder();//FB 2027
                string a_Order = null, checkAlpha = alphabet.ToLower(), crossaccess = "0", lockstatus = "0", deleted = "0";

                //ZD 1012005 Starts
                List<ICriterion> deptCriterionList = new List<ICriterion>();
                List<ICriterion> criteriumList = new List<ICriterion>();
                List<ICriterion> citeriaOrg = new List<ICriterion>();
                List<ICriterion> AudiobridgescritList = new List<ICriterion>(); //FB 2023
                List<ICriterion> criti = new List<ICriterion>();
                List<ICriterion> criterionList = new List<ICriterion>();

                List<int> notuserIn = new List<int>();
                List<int> totlaDpt = new List<int>();
                List<int> confIn = new List<int>();
                List<int> deptIn = new List<int>();
                List<int> userIn = new List<int>();

                IList usrFilt = new List<vrmBaseUser>();
                IList userList = new List<vrmBaseUser>();
                List<vrmDept> selectedDept = new List<vrmDept>();
                List<vrmConfUser> confuserList = new List<vrmConfUser>();
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<vrmConference> confList = new List<vrmConference>();

                vrmBaseUser uf;
                vrmBaseUser userL = null;
                vrmConference ConfUserID = null;
                vrmConfUser Conf = null;
                vrmUser user;
                vrmUserRoles roles;
                OrgData orgdt;
                IUserAccountDao IuserAccountDAO;
                vrmAccount uAccount;

                // this routing must work for LDAP also (not implemented yet)
                // when getting LDAP count newuser = 1


                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                //FB 2269
                if (deptUsers == 0)
                    isDeptUser = orgInfo.isDeptUser;

                user = m_IuserDao.GetByUserId(userID);

                //ZD 100263
                if (user.roleID != 0)
                {
                    roles = new vrmUserRoles();
                    roles = m_IUserRolesDao.GetById((int)user.roleID);
                    adminaccess = roles.crossaccess;
                }
                //ZD 100263

                // Spoof admin if this is an LDAP lookup (this is only visible to admmins) [AG 5-19-08]
                if (type == m_isUser)
                {
                    //ZD 102005 Starts
                    deptCriterionList.Add(Expression.Eq("deleted", 0));
                    deptCriterionList.Add(Expression.Eq("orgId", organizationID.ToString()));
                    selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);
                    if (selectedDept.Count <= 0)
                        multiDepts = 0;
                    //ZD 102005 Ends

                    if (multiDepts == 1)
                        admin = user.Admin;
                    else
                        admin = vrmUserConstant.SUPER_ADMIN;
                }
                else
                {
                    admin = vrmUserConstant.SUPER_ADMIN;
                }

                criteriumList = new List<ICriterion>();
                citeriaOrg = new List<ICriterion>();
                AudiobridgescritList = new List<ICriterion>();

                criteriumList.Add(Expression.Eq("companyId", organizationID));
                citeriaOrg.Add(Expression.Eq("companyId", organizationID));
                AudiobridgescritList.Add(Expression.Eq("companyId", organizationID)); //FB 2023

                a_Order = null;
                checkAlpha = alphabet.ToLower();
                switch (sortBy)
                {
                    case 1:
                        a_Order = "FirstName";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("FirstName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("FirstName", alphabet + "%%"));
                        }
                        break;
                    case 3:
                        a_Order = "Login";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Login", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Login", alphabet + "%%"));
                        }
                        break;
                    case 4:
                        a_Order = "Email";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    case 5: //ZD 103405
                        a_Order = "UserRolename";
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("UserRolename", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("UserRolename", alphabet + "%%"));
                        }
                        break;
                    case 6: //ZD 103837
                        //a_Order = "lockCntTrns";
                        a_Order = "LockStatus"; //ZD 104171
                        if (checkAlpha != "all")
                        {
                            if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("Email", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("Email", alphabet + "%%"));
                        }
                        break;
                    
                    default:
                        a_Order = "LastName";
                        if (checkAlpha != "all")
                        {
                            if (alphabetmode == 1 && checkAlpha == "a")
                                criteriumList.Add(Expression.Sql("(SUBSTRING(LastName, 1, 1) <= 'a')"));
                            else if (checkAlpha == "a" && alphabet == "a" && alphabetmode == 0)
                                criteriumList.Add(Expression.Sql("(SUBSTRING(LastName, 1, 1) <= 'a')"));
                            else if (checkAlpha == "a")
                                criteriumList.Add(Expression.Like("LastName", "A%%"));
                            else
                                criteriumList.Add(Expression.Like("LastName", alphabet + "%%"));
                        }
                        break;

                }

                if (alphabetmode == 1)
                {
                    criteriumList.Add(Expression.Eq("Deleted", 0));
                    //ZD 104864 Starts  ALLDEV-497
                    criteriumList.Add(Expression.Eq("LockStatus", 0));
                    //ZD 104864 Ends ALLDEV-497
                    citeriaOrg.Add(Expression.Eq("Deleted", 0));
                    AudiobridgescritList.Add(Expression.Eq("Deleted", 0)); //FB 2023
                }

                if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                {
                    criteriumList.Add(Expression.Eq("Audioaddon", isAudioAddOn.ToString()));
                    AudiobridgescritList.Add(Expression.Eq("Audioaddon", isAudioAddOn.ToString()));
                }

                //ALLDEV-498 Starts
                if (isconfadmin == 1)
                {
                    criteriumList.Add(Expression.In("Admin", new int[] { 2, 3 }));
                }
                //ALLDEV-498 Ends

                if (isDeptUser == 1)
                {
                    if (type == m_isUser)
                    {
                        if (admin == 1)
                        {
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("userId", user.userid));
                            deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                            for (int x = 0; x < deptList.Count; x++)
                            {
                                if (!deptIn.Contains(deptList[x].departmentId))
                                    deptIn.Add(deptList[x].departmentId);
                            }
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.In("departmentId", deptIn));
                            deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                            for (int x = 0; x < deptList.Count; x++)
                            {
                                if (!userIn.Contains(deptList[x].userId))
                                    userIn.Add(deptList[x].userId);
                            }
                        }
                        if (admin == 0)
                        {
                            criterionList = new List<ICriterion>();
                            criti = new List<ICriterion>();
                            confList = new List<vrmConference>();
                            confIn = new List<int>();


                            criterionList.Add(Expression.Eq("owner", userID));
                            criterionList.Add(Expression.Eq("orgId", organizationID));
                            criterionList.Add(Expression.Ge("confdate", DateTime.UtcNow));
                            criterionList.Add(Expression.Eq("deleted", 0));
                            confList = m_IConferenceDAO.GetByCriteria(criterionList);

                            for (int i = 0; i < confList.Count; i++)
                            {
                                ConfUserID = confList[i];
                                confIn.Add(ConfUserID.confid);
                            }

                            criti.Add(Expression.In("confid", confIn));
                            confuserList = m_IConfUserDAO.GetByCriteria(criti);

                            for (int i = 0; i < confuserList.Count; i++)
                            {
                                Conf = confuserList[i];
                                if (!userIn.Contains(Conf.userid))
                                    userIn.Add(Conf.userid);
                            }
                            userIn.Add(11);
                            userIn.Add(userID);

                            if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN)
                            {
                                criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.Eq("userId", user.userid));
                                deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                                //ZD 102005 Starts
                                if (deptList.Count > 0)
                                {
                                    for (int x = 0; x < deptList.Count; x++)
                                    {
                                        if (!deptIn.Contains(deptList[x].departmentId))
                                            deptIn.Add(deptList[x].departmentId);
                                    }
                                    criterionList = new List<ICriterion>();
                                    criterionList.Add(Expression.In("departmentId", deptIn));
                                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                                    for (int x = 0; x < deptList.Count; x++)
                                    {
                                        if (!userIn.Contains(deptList[x].userId))
                                            userIn.Add(deptList[x].userId);
                                    }
                                }
                                else
                                {
                                    selectedDept = new List<vrmDept>();
                                    criterionList = new List<ICriterion>();
                                    notuserIn = new List<int>();
                                    totlaDpt = new List<int>();

                                    criterionList.Add(Expression.Eq("deleted", 0));
                                    criterionList.Add(Expression.Eq("orgId", organizationID.ToString()));
                                    selectedDept = m_IdeptDAO.GetByCriteria(criterionList);

                                    for (int cnt = 0; cnt < selectedDept.Count; cnt++)
                                    {
                                        if (!totlaDpt.Contains(selectedDept[cnt].departmentId))
                                            totlaDpt.Add(selectedDept[cnt].departmentId);
                                    }

                                    criterionList = new List<ICriterion>();
                                    criterionList.Add(Expression.In("departmentId", totlaDpt));
                                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);

                                    for (int cnt = 0; cnt < deptList.Count; cnt++)
                                    {
                                        if (!notuserIn.Contains(deptList[cnt].userId))
                                            notuserIn.Add(deptList[cnt].userId);
                                    }


                                    criterionList = new List<ICriterion>();
                                    criterionList.Add(Expression.Eq("Deleted", 0));
                                    criterionList.Add(Expression.Eq("Audioaddon", isAudioAddOn.ToString()));
                                    criterionList.Add(Expression.Not(Expression.In("userid", notuserIn)));
                                    criterionList.Add(Expression.Eq("companyId", organizationID));
                                    usrFilt = m_IuserDao.GetByCriteria(criterionList);


                                    for (int usrcnt = 0; usrcnt < usrFilt.Count; usrcnt++)
                                    {
                                        uf = new vrmBaseUser();
                                        uf = (vrmBaseUser)usrFilt[usrcnt];
                                        if (!userIn.Contains(uf.userid))
                                            userIn.Add(uf.userid);
                                    }
                                }
                                //ZD 102005 Ends
                            }
                            if (BulkUser != "" && BulkUser == "0")
                            {
                                criteriumList.Add(Expression.In("userid", userIn));//ZD 100263
                                criteriumList.Add(Expression.Not(Expression.Eq("Admin", 2)));
                            }
                            else
                            {
                                criteriumList.Add(Expression.Or(Expression.Eq("Admin", 2), Expression.In("userid", userIn)));
                            }
                            citeriaOrg.Add(Expression.Or(Expression.Eq("Admin", 2), Expression.In("userid", userIn)));
                            AudiobridgescritList.Add(Expression.Or(Expression.Eq("Admin", 2), Expression.In("userid", userIn))); //FB 2023
                        }
                    }
                }

                iPageNo = pageno;
                //if (iPageNo == 0) //ZD 100799 - To get All Users details
                //    iPageNo = 1;
                ttlRecords = 0;

                userList = new List<vrmBaseUser>();
                switch (type)
                {
                    case 1:
                        m_IuserDao.addProjection(Projections.RowCount());
                        IList list = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                        int.TryParse((list[0].ToString()), out numrows);
                        list = m_IuserDao.GetObjectByCriteria(citeriaOrg);
                        int.TryParse((list[0].ToString()), out numrecs);

                        if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                        {
                            list = m_IuserDao.GetObjectByCriteria(AudiobridgescritList);
                            int.TryParse((list[0].ToString()), out audiobridgerec);
                        }

                        m_IuserDao.clearProjection();
                        if (iPageNo > 0) //ZD 100799 - To get All Users details
                        {
                            m_IuserDao.pageSize(m_PAGE_SIZE);
                            m_IuserDao.pageNo(iPageNo);
                            ttlRecords =
                                m_IuserDao.CountByCriteria(criteriumList);
                        }

                        //confList = confList.Skip(m_iMaxRecords * (PageNo - 1)).Take(m_iMaxRecords).ToList(); 
                        //ZD 103405 start
                        if (SortingOrder == 0 && a_Order != "LockStatus")//ZD 103837 //ZD 104171
                          m_IuserDao.addOrderBy(Order.Asc(a_Order));
                        else
                          m_IuserDao.addOrderBy(Order.Desc(a_Order));

                        //a_Order = "lockCntTrns";
                        //    m_IuserDao.addOrderBy(Order.Desc(a_Order));
                        //ZD 103405 End 

                        userList = m_IuserDao.GetByCriteria(criteriumList);
                        break;
                    case 2:
                        m_IldapDao.addProjection(Projections.RowCount());
                        IList listL = m_IldapDao.GetObjectByCriteria(new List<ICriterion>());
                        int.TryParse((listL[0].ToString()), out numrows);
                        listL = m_IldapDao.GetObjectByCriteria(citeriaOrg);
                        int.TryParse((listL[0].ToString()), out numrecs);
                        m_IldapDao.clearProjection();
                        if (iPageNo > 0) //ZD 100799 - To get All Users details
                        {
                            m_IldapDao.pageSize(m_PAGE_SIZE);
                            m_IldapDao.pageNo(iPageNo);
                            ttlRecords =
                                m_IldapDao.CountByCriteria(criteriumList);
                        }

                        //ZD 103405 start
                        if (SortingOrder == 0)
                            m_IldapDao.addOrderBy(Order.Asc(a_Order));
                        else
                            m_IldapDao.addOrderBy(Order.Desc(a_Order));
                        //ZD 103405 End

                        userList = m_IldapDao.GetByCriteria(criteriumList);
                        break;
                    case 3:
                        m_IGuestUserDao.addProjection(Projections.RowCount());
                        IList listG = m_IGuestUserDao.GetObjectByCriteria(new List<ICriterion>());
                        int.TryParse((listG[0].ToString()), out numrows);
                        listG = m_IGuestUserDao.GetObjectByCriteria(citeriaOrg);
                        int.TryParse((listG[0].ToString()), out numrecs);
                        m_IGuestUserDao.clearProjection();
                        if (iPageNo > 0) //ZD 100799 - To get All Users details
                        {
                            m_IGuestUserDao.pageSize(m_PAGE_SIZE);
                            m_IGuestUserDao.pageNo(iPageNo);
                            ttlRecords =
                                m_IGuestUserDao.CountByCriteria(criteriumList);
                        }

                        //ZD 103405 start
                        if (SortingOrder == 0)
                            m_IGuestUserDao.addOrderBy(Order.Asc(a_Order));
                        else
                            m_IGuestUserDao.addOrderBy(Order.Desc(a_Order));
                        //ZD 103405 End

                        userList = m_IGuestUserDao.GetByCriteria(criteriumList);
                        break;
                    case 4:
                        m_IinactiveUserDao.addProjection(Projections.RowCount());
                        IList listI = m_IinactiveUserDao.GetObjectByCriteria(new List<ICriterion>());
                        int.TryParse((listI[0].ToString()), out numrows);

                        listI = m_IinactiveUserDao.GetObjectByCriteria(citeriaOrg);
                        int.TryParse((listI[0].ToString()), out numrecs);

                        if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                        {
                            listI = m_IuserDao.GetObjectByCriteria(AudiobridgescritList);
                            int.TryParse((listI[0].ToString()), out audiobridgerec);
                        }

                        m_IinactiveUserDao.clearProjection();
                        if (iPageNo > 0) //ZD 100799 - To get All Users details
                        {
                            m_IinactiveUserDao.pageSize(m_PAGE_SIZE);
                            m_IinactiveUserDao.pageNo(iPageNo);
                            ttlRecords =
                                m_IinactiveUserDao.CountByCriteria(criteriumList);
                        }

                        //ZD 103405 start
                        if (SortingOrder == 0)
                            m_IinactiveUserDao.addOrderBy(Order.Asc(a_Order));
                        else
                            m_IinactiveUserDao.addOrderBy(Order.Desc(a_Order));
                        //ZD 103405 End

                          
                        userList = m_IinactiveUserDao.GetByCriteria(criteriumList);
                        break;
                }

                totpages = numrows / m_PAGE_SIZE;
                if ((numrows % m_PAGE_SIZE) > 0)
                    totpages++;

                int ttlPages = (int)(ttlRecords / m_PAGE_SIZE);

                // and modulo remainder...
                if (ttlRecords % m_PAGE_SIZE > 0)
                    ttlPages++;

                outUser.Append("<users>");
                if (userList != null || userList.Count < 0)
                {
                    userL = null;
                    for (int i = 0; i < userList.Count; i++)
                    {
                        userL = (vrmBaseUser)userList[i];

                        //ZD 100263 Start
                        crossaccess = "0";
                        level = 0;
                        if (userL.roleID != 0)
                        {
                            roles = new vrmUserRoles();
                            roles = m_IUserRolesDao.GetById((int)userL.roleID);
                            crossaccess = roles.crossaccess.ToString();
                            level = roles.level;
                        }

                        if (adminaccess != 1 && BulkUser != "" && BulkUser == "0")
                            if (crossaccess == "1" && level == 2)
                                continue;

                        //ZD 100263 End

                        outUser.Append("<user>");
                        outUser.Append("<userID>" + userL.userid + "</userID>");
                        outUser.Append("<firstName>" + userL.FirstName + "</firstName>");
                        outUser.Append("<lastName>" + userL.LastName + "</lastName>");
                        outUser.Append("<login>" + userL.Login + "</login>");

                        if (type == m_isInActiveUser && userL.Audioaddon.Trim() == "1") //FB 2023
                            outUser.Append("<email></email>");
                        else
                            outUser.Append("<email>" + userL.Email + "</email>");

                        outUser.Append("<telephone>" + userL.Telephone + "</telephone>");
                        if (userL.Password != null)
                            outUser.Append("<password>" + userL.Password + "</password>");
                        else
                            outUser.Append("<password></password>");
                        outUser.Append("<externalBridgeNumber>" + userL.ExternalVideoNumber + "</externalBridgeNumber>"); //FB 2376
                        outUser.Append("<internalBridgeNumber>" + userL.InternalVideoNumber + "</internalBridgeNumber>"); //FB 2376
                        outUser.Append("<enableWebEx>" + userL.enableWebexUser + "</enableWebEx>"); //ZD 100221
                        //ZD 104289 - Start
                        outUser.Append("<ConferenceCode>" + userL.ConferenceCode + "</ConferenceCode>");
                        outUser.Append("<LeaderPin>" + userL.LeaderPin + "</LeaderPin>");
                        outUser.Append("<ParticipantCode>" + userL.ParticipantCode + "</ParticipantCode>");
                        //ZD 104289 - End
                        //ALLDEV-498 Starts
                        if (type == 1)
                        {
                            vrmUser usr;
                            usr = m_IuserDao.GetByUserId(userL.userid);
                            int errid = 0;

                            if (checkUser(usr, ref errid))
                            {
                                outUser.Append("<ErrorMsg>0</ErrorMsg>"); //Not Linked with any
                            }
                            else
                            {
                                if (errid == 767)
                                    outUser.Append("<ErrorMsg>2</ErrorMsg>"); //Assinged as Conf Adminitrator
                                else
                                    outUser.Append("<ErrorMsg>1</ErrorMsg>"); //Linked with some.

                            }
                        }
                        else
                        {
                            outUser.Append("<ErrorMsg>0</ErrorMsg>");
                        }
                        //ALLDEV-498 Ends

                        if (mode == 1)
                        {
                            IuserAccountDAO = m_userDAO.GetUserAccountDao();
                            uAccount = IuserAccountDAO.GetByUserId(userL.userid); //FB 2027

                            outUser.Append("<report>");
                            outUser.Append("<total>" + uAccount.TotalTime.ToString() + "</total>");

                            //ZD 102043 starts
                            //long timeUsed = uAccount.TotalTime - uAccount.TimeRemaining;
                            //outUser.Append("<used>" + timeUsed.ToString() + "</used>");

                            outUser.Append("<used>" + uAccount.UsedMinutes.ToString() + "</used>");
                            //ZD 102043 ends

                            // used = totalAmount- left
                            outUser.Append("<left>" + uAccount.TimeRemaining.ToString() + "</left>");
                            outUser.Append("</report>");
                        }
                        else if (mode == 2)
                        {
                            lockstatus = "0"; deleted = "0"; Roleid = 0;

                            if (userL.LockStatus != 0)
                                lockstatus = "1";

                            if (userL.Deleted != 0) // Deleted
                                deleted = "1";

                            if (userL.roleID.ToString() != null)
                                Roleid = userL.roleID;

                            if (Roleid > 0)//ZD 103415
                            {
                                roles = new vrmUserRoles();
                                roles = m_IUserRolesDao.GetById((int)Roleid);
                                crossaccess = roles.crossaccess.ToString();
                                outUser.Append("<RoleName>" + m_UtilFactory.GetTranslatedText(user.Language, roles.roleName.ToString()) + "</RoleName>");//ZD 103415
                            }
                            else
                                outUser.Append("<RoleName></RoleName>");//ZD 103415
                            outUser.Append("<Survey>" + userL.SendSurveyEmail.ToString() + "</Survey>");//FB 2348
                            outUser.Append("<status>");
                            outUser.Append("<level>" + userL.Admin.ToString() + "</level>");
                            outUser.Append("<deleted>" + deleted + "</deleted>");
                            outUser.Append("<locked>" + lockstatus + "</locked>");
                            outUser.Append("<crossaccess>" + crossaccess + "</crossaccess>");
                            outUser.Append("</status>");
                        }
                        outUser.Append("</user>");
                    }
                    outUser.Append("<pageNo>" + pageno.ToString() + "</pageNo>");
                    outUser.Append("<totalPages>" + ttlPages.ToString() + "</totalPages>");
                    outUser.Append("<sortBy>" + sortBy + "</sortBy>");
                    outUser.Append("<alphabet>" + alphabet + "</alphabet>");

                    if (isAudioAddOn < 2 && isAudioAddOn >= 0) //FB 2023
                        outUser.Append("<totalNumber>" + audiobridgerec + "</totalNumber>");
                    else
                        outUser.Append("<totalNumber>" + numrecs + "</totalNumber>");

                    if (thresholdpassed)
                        outUser.Append("<canAll>0</canAll>");
                    else
                        outUser.Append("<canAll>1</canAll>");

                    if (type != m_isLDAP)
                    {
                        orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                        usersRemain = orgdt.UserLimit - numrecs;
                        outUser.Append("<licensesRemain>" + usersRemain.ToString() + "</licensesRemain>");
                    }
                }
                outUser.Append("</users>");
                outXml = outUser.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region RetrieveUsers
        /// <summary>
        /// GetGuestList - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RetrieveUsers(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                string firstname = "";
                string lastname = "";
                string login = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                int SortingOrder = 0; //ZD 103405
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/name/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/name/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("/login/name/userName");
                if (node != null)
                    login = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);

                if (lastname != "")
                {
                    sortBy = 2;
                    alphabet = lastname;
                }
                else if (firstname != "")
                {
                    sortBy = 1;
                    alphabet = firstname;
                }
                else
                {
                    sortBy = 3;
                    alphabet = login;
                }

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 2, ref outXml, organizationID, 1, deptUsers, BulkUser, 0)) //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region RetrieveGuest
        /// <summary>
        /// RetrieveGuest - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool RetrieveGuest(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                string firstname = "";
                string lastname = "";
                string login = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                myVRMException myVRMEx = null;
                int SortingOrder = 0; //ZD 103405

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/name/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/name/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("/login/name/userName");
                if (node != null)
                    login = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);

                if (lastname != "")
                {
                    sortBy = 2;
                    alphabet = lastname;
                }
                else if (firstname != "")
                {
                    sortBy = 1;
                    alphabet = firstname;
                }
                else
                {
                    sortBy = 3;
                    alphabet = login;
                }

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isGustUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 2, ref outXml, organizationID, 1, deptUsers, BulkUser, 0)) //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetManageGuest
        /// <summary>
        /// GetManageGuest - COM to .NET Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetManageGuest(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                int SortingOrder = 0; //ZD 103405
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sortBy);
                //ZD 103405 start
                node = xd.SelectSingleNode("//login/SortingOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SortingOrder);
                //ZD 103405 End

                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isGustUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 2, ref outXml, organizationID, 0, deptUsers, BulkUser, 0))//FB 2269 //ZD 103405 //ALLDEV-498
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetUsers
        /// <summary>
        /// GetUsers - COM to .NET Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetUsers(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                int SortingOrder = 0; //ZD 103405
                string alphabet = "";
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sortBy);
                //ZD 103405 start
                node = xd.SelectSingleNode("//login/SortingOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SortingOrder);
                //ZD 103405 End 
                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isInActiveUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 0, ref outXml, organizationID, 0, deptUsers, BulkUser, 0)) //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    string Out = "<userInfo>" + outXml + "</userInfo>";
                    obj.outXml = Out;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SetOldUser
        /// <summary>
        /// SetOldUser (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetOldUser(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            string outxml = ""; //ZD 100152
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userid");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                vrmUser user = m_IuserDao.GetByUserId(userid);
                user.newUser = 0;
                m_IuserDao.Update(user);

                //ZD 100152 Starts
                if (sysSettings.PushtoGoogle == 1)
                {
                    outxml = "<ExporttoExternal>";
                    outxml += "<pushtogoogle>1</pushtogoogle>";
                    outxml += "</ExporttoExternal>";
                }
                obj.outXml = outxml;
                //ZD 100152 Ends
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetUserPreference
        /// <summary>
        /// GetUserPreference (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetUserPreference(ref vrmDataObject obj)
        {
            string lang = "";
            StringBuilder addr = new StringBuilder();
            string video = "";
            string group = "";
            string lot = "";
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            List<ICriterion> criterionList3 = new List<ICriterion>();
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                outXml.Append("<userPreference>");
                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList);
                outXml.Append("<userName>");
                outXml.Append("<firstName>" + userList[0].FirstName.ToString() + "</firstName>");
                outXml.Append("<lastName>" + userList[0].LastName.ToString() + "</lastName>");
                outXml.Append("</userName>");
                outXml.Append("<login>" + userList[0].Login.ToString() + "</login>");
                crypto = new cryptography.Crypto();
                if (userList[0].Password != null)
                    outXml.Append("<password>" + crypto.decrypt(userList[0].Password.ToString()) + "</password>");
                else
                    outXml.Append("<password></password>");
                outXml.Append("<userEmail>" + userList[0].Email.ToString() + "</userEmail>");
                outXml.Append("<alternativeEmail>" + userList[0].AlternativeEmail.ToString() + "</alternativeEmail>");
                outXml.Append("<sendBoth>" + userList[0].DoubleEmail.ToString() + "</sendBoth>");
                outXml.Append("<emailClient>" + userList[0].EmailClient.ToString() + "</emailClient>"); //0/1/2; 0: none, 1: Outlook, 2: Lotus
                outXml.Append("<timeZone>" + userList[0].TimeZone.ToString() + "</timeZone>");
                outXml.Append("<languageID>" + userList[0].Language.ToString() + "</languageID>");
                if (OldFetchLanguageList(obj, ref lang))
                    outXml.Append(lang);
                outXml.Append("<timezones>");
                int tzsid;
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);
                tzsid = orgInfo.tzsystemid;
                if (tzsid == 0)
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        outXml.Append("<timezone>");
                        outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                        outXml.Append("<timezoneName>" + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                        outXml.Append("</timezone>");
                    }
                }
                else
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        if (orgInfo.tzsystemid == timeZoneList[t].systemID)
                        {
                            outXml.Append("<timezone>");
                            outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                            outXml.Append("<timezoneName>" + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                            outXml.Append("</timezone>");
                        }
                    }
                }
                outXml.Append("</timezones>");
                outXml.Append("<locationList><selected><level1ID>" + userList[0].PreferedRoom.ToString() + "</level1ID></selected></locationList>");
                outXml.Append("<lineRateID>" + userList[0].DefLineRate.ToString() + "</lineRateID>");
                outXml.Append("<lineRate>");
                List<vrmLineRate> lineRate = vrmGen.getLineRate();
                for (int l = 0; l < lineRate.Count; l++)
                {
                    if (userList[0].userid == userid)
                    {
                        outXml.Append("<rate>");
                        outXml.Append("<lineRateID>" + lineRate[l].Id.ToString() + "</lineRateID>");
                        outXml.Append("<lineRateName>" + lineRate[l].LineRateType.ToString() + "</lineRateName>");
                        outXml.Append("</rate>");
                    }
                }
                outXml.Append("</lineRate>");
                outXml.Append("<videoProtocol>" + GetVideoPro(userList[0].DefVideoProtocol, obj) + "</videoProtocol>");
                if (FetchAddressTypeList(obj, ref addr))
                    outXml.Append(addr);
                outXml.Append("<IPISDNAddress>" + userList[0].IPISDNAddress.ToString() + "</IPISDNAddress>");
                outXml.Append("<connectionType>" + userList[0].connectionType.ToString() + "</connectionType>");  //0: Dial_Out, 1: Dial_in
                outXml.Append("<isOutside>" + userList[0].outsidenetwork.ToString() + "</isOutside>");
                outXml.Append("<emailMask>" + userList[0].emailmask.ToString() + "</emailMask>");
                outXml.Append("<addressTypeID>" + userList[0].addresstype.ToString() + "</addressTypeID>");
                outXml.Append("<videoEquipmentID>" + userList[0].DefaultEquipmentId.ToString() + "</videoEquipmentID>");
                if (FetchVideoEquipment2(userList[0].DefaultEquipmentId.ToString(), userid, 1, obj, ref video))
                    outXml.Append(video);
                criterionList1.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userid)));
                criterionList1.Add(Expression.Eq("GroupID", userList[0].PreferedGroup));
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList1);
                for (int g = 0; g < Groups.Count; g++)
                {
                    outXml.Append("<defaultAGroups>");
                    outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                    outXml.Append("</defaultAGroups>");
                }
                criterionList2.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userid)));
                if (FetchGroups(criterionList2, 0, obj, ref group))
                    outXml.Append(group);
                outXml.Append("<multiDepartment>" + orgInfo.MultipleDepartments + "</multiDepartment>");
                outXml.Append("<departments>");
                criterionList3.Add(Expression.Eq("deleted", 0));
                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                for (int d = 0; d < deptList.Count; d++)
                    criterionList3.Add(Expression.Eq("departmentId", deptList[d].departmentId));
                List<vrmDept> dept = m_IdeptDAO.GetByCriteria(criterionList3);
                for (int j = 0; j < dept.Count; j++)
                {
                    outXml.Append("<department>");
                    outXml.Append("<id>" + dept[j].departmentId.ToString() + "</id>");
                    outXml.Append("<name>" + dept[j].departmentName + "</name>");
                    outXml.Append("</department>");
                }
                outXml.Append("</departments>");
                outXml.Append("<expiryDate>" + userList[0].accountexpiry.ToString("MM/dd/yyyy") + "</expiryDate>");
                List<ICriterion> criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("BridgeID", userList[0].BridgeID));
                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionLst);
                for (int m = 0; m < MCUList.Count; m++)
                {
                    if (MCUList[m].BridgeName == "")
                    {
                        outXml.Append("<bridgeName> Unknown </bridgeName>");
                    }
                    else
                    {
                        outXml.Append("<bridgeName>" + MCUList[m].BridgeName + "</bridgeName>");
                    }
                }
                if (FetchLotusInfo(userid, obj, ref lot))
                    outXml.Append(lot);
                outXml.Append("</userPreference>");
                obj.outXml = outXml.ToString();
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region OldFetchLanguageList
        /// <summary>
        /// OldFetchLanguageList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchLanguageList(vrmDataObject obj, ref string langout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<languages>");
                List<vrmLanguage> lang = m_langDAO.GetAll();
                for (int l = 0; l < lang.Count; l++)
                {
                    outXml.Append("<language>");
                    outXml.Append("<ID>" + lang[l].Id.ToString() + "</ID>");
                    outXml.Append("<name>" + lang[l].Name.ToString() + "</name>");
                    outXml.Append("</language>");
                }
                outXml.Append("</languages>");
                obj.outXml = outXml.ToString();
                langout = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region GetVideoPro
        /// <summary>
        /// GetVideoPro (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public string GetVideoPro(int DefVideoProtocol, vrmDataObject obj)
        {
            string videoPro = "0";
            try
            {
                if (DefVideoProtocol == 0)
                    return videoPro;
                List<vrmVideoProtocol> vp_List = vrmGen.getVideoProtocols();
                for (int v = 0; v < vp_List.Count; v++)
                {
                    if (vp_List[v].Id == DefVideoProtocol)
                    {
                        videoPro = vp_List[v].VideoProtocolType;
                        return videoPro;
                    }
                    else
                    {
                        videoPro = "";
                    }
                }
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return obj.outXml;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return obj.outXml;
            }
            return videoPro;
        }
        #endregion

        #region FetchAddressTypeList
        /// <summary>
        /// FetchAddressTypeList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchAddressTypeList(vrmDataObject obj, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            try
            {
                outXml.Append("<addressType>");
                List<vrmAddressType> AddressTypeList = vrmGen.getAddressType();
                for (int a = 0; a < AddressTypeList.Count; a++)
                {
                    outXml.Append("<type>");
                    outXml.Append("<ID>" + AddressTypeList[a].Id.ToString() + "</ID>");
                    outXml.Append("<name>" + AddressTypeList[a].name.ToString() + "</name>");
                    outXml.Append("</type>");
                }
                outXml.Append("</addressType>");
                obj.outXml = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region FetchVideoEquipment2
        /// <summary>
        /// FetchVideoEquipment2 (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchVideoEquipment2(string selected, int userid, int mode, vrmDataObject obj, ref string videoout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<videoEquipment>");
                if (mode == 1)
                    outXml.Append("<selectedEquipment>" + selected + "</selectedEquipment>");
                List<vrmVideoEquipment> videoEqList = vrmGen.getVideoEquipment();
                for (int v = 0; v < videoEqList.Count; v++)
                {
                    outXml.Append("<equipment>");
                    outXml.Append("<videoEquipmentID>" + videoEqList[v].Id.ToString() + "</videoEquipmentID>");
                    outXml.Append("<videoEquipmentName>" + videoEqList[v].VEName + "</videoEquipmentName>");
                    outXml.Append("</equipment>");
                }
                outXml.Append("</videoEquipment>");
                obj.outXml = outXml.ToString();
                videoout = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region FetchGroups
        /// <summary>
        /// FetchGroups (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchGroups(List<ICriterion> criterionList2, int mode, vrmDataObject obj, ref string grpout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<groups>");
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList2);
                for (int g = 0; g < Groups.Count; g++)
                {
                    outXml.Append("<group>");
                    if (mode == 1)
                    {
                        outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                        outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                    }
                    else if (mode == 2)
                    {
                        outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                        outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                    }
                    else if (mode == 0)
                    {
                        outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                        outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                        List<ICriterion> criterionListG = new List<ICriterion>();
                        criterionListG.Add(Expression.Eq("GroupID", Groups[g].GroupID));
                        List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionListG);
                        //Count no of users in group				
                        outXml.Append("<numParticipants>" + partList.Count.ToString() + "</numParticipants>");
                        for (int p = 0; p < partList.Count; p++)
                        {
                            vrmUser partUser = m_IuserDao.GetByUserId(partList[p].UserID);
                            outXml.Append("<user>");
                            outXml.Append("<userID>" + partList[p].UserID.ToString() + "</userID>");
                            outXml.Append("<userFirstName>" + partUser.FirstName + "</userFirstName>");
                            outXml.Append("<userLastName>" + partUser.LastName + "</userLastName>");
                            outXml.Append("<userEmail>" + partUser.Email + "</userEmail>");
                            outXml.Append("<CC>" + partList[p].CC.ToString() + "</CC>");
                            outXml.Append("</user>");
                        }
                    }
                    outXml.Append("</group>");
                }
                outXml.Append("</groups>");
                obj.outXml = outXml.ToString();
                grpout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region FetchLotusInfo
        /// <summary>
        /// FetchLotusInfo (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchLotusInfo(int userid, vrmDataObject obj, ref string lotout)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionLot = new List<ICriterion>();
            try
            {
                outXml.Append("<lotus>");
                criterionLot.Add(Expression.Eq("userid", userid));
                List<vrmUserLotusNotesPrefs> usrlot = m_userlotusDAO.GetByCriteria(criterionLot);
                for (int l = 0; l < usrlot.Count; l++)
                {
                    outXml.Append("<lnLoginName>" + usrlot[l].noteslogin + "</lnLoginName>");
                    outXml.Append("<lnLoginPwd>" + usrlot[l].notespwd + "</lnLoginPwd>");
                    outXml.Append("<lnDBPath>" + usrlot[l].notespath + "</lnDBPath>");
                }
                if (usrlot.Count == 0)
                {
                    outXml.Append("<lnLoginName></lnLoginName>");
                    outXml.Append("<lnLoginPwd></lnLoginPwd>");
                    outXml.Append("<lnDBPath></lnDBPath>");
                }
                outXml.Append("</lotus>");
                obj.outXml = outXml.ToString();
                lotout = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region DeleteAllGuests
        /// <summary>
        /// DeleteAllGuests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteAllGuests(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                int.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                else
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("companyId", organizationID));
                    criterionList.Add(Expression.Eq("Deleted", 0));
                    List<vrmGuestUser> guestlN = m_IGuestUserDao.GetByCriteria(criterionList);

                    for (int i = 0; i < guestlN.Count; i++)
                    {
                        guestlN[i].Deleted = 1;
                    }
                    m_IGuestUserDao.SaveOrUpdateList(guestlN);
                    if (guestlN.Count < 0)
                    {
                        obj.outXml = "<error>-1</error>";
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in Deleting Guest User", ex);
                return false;
            }

        }
        #endregion

        #region GetAllocation
        /// <summary>
        /// GetAllocation - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAllocation(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                myVRMException myVRMEx = null;
                string alphabet = "";
                int pageNo = 0;
                int sortBy = 0;
                int deptUsers = 1; //FB 2269
                int SortingOrder = 0; //ZD 103405
                StringBuilder Out = new StringBuilder();

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;


                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);

                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sortBy);
                //ZD 103405 start
                node = xd.SelectSingleNode("//login/SortingOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SortingOrder);
                //ZD 103405 End
                int totPages = 0;
                string BulkUser = "0";//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 0, ref outXml, organizationID, 0, deptUsers, BulkUser, 0)) //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    Out.Append("<getAllocation>" + outXml.ToString() + "</getAllocation>");
                    obj.outXml = Out.ToString();
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region SetGroup
        /// <summary>
        /// SetGroup (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmGroupDetails grpdetails = new vrmGroupDetails();
            vrmGroupParticipant grppartys = new vrmGroupParticipant();
            myVRMException myVRMEx = new myVRMException();
            string firstname = "", lastname = "", email = "", orgid = "";
            string partyid = "", groupid = "";
            int loginid = 0, ownerid = 0, CC = 0, gustid = 0, grpid = 0, GroupID = 0, PartyID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userInfo/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out loginid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groupInfo/group/ownerID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out ownerid);
                grpdetails.Owner = ownerid;

                if (loginid <= 0 || ownerid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);
                grpdetails.orgId = organizationID;

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groupInfo/group/groupID");
                if (node != null)
                    groupid = node.InnerText.Trim();
                if (groupid != "new")
                    int.TryParse(groupid, out GroupID);

                string groupname = "";
                node = xd.SelectSingleNode("//login/groupInfo/group/groupName");
                if (node != null)
                    groupname = node.InnerText.Trim();
                if (groupname.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                grpdetails.Name = groupname;

                string description = "";
                node = xd.SelectSingleNode("//login/groupInfo/group/description");
                if (node != null)
                    description = node.InnerText.Trim();
                grpdetails.Description = description;

                int privategrp = 0;
                node = xd.SelectSingleNode("//login/groupInfo/group/public");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out privategrp);
                if (privategrp == 1)
                    grpdetails.Private = 0;
                else
                    grpdetails.Private = 1;

                if (groupid.ToLower() == "new")
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("orgId", organizationID));
                    criterionList.Add(Expression.Eq("Name", groupname.ToUpper()));

                    if (groupid.ToLower() != "new")
                    {
                        criterionList.Add(Expression.Not(Expression.Eq("GroupID", GroupID)));
                    }
                    IList<vrmGroupDetails> GroupIds = m_IGrpDetailDao.GetByCriteria(criterionList);
                    if (GroupIds.Count > 0)
                    {
                        myVRMEx = new myVRMException(415); //Duplicate Group Name.
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    m_IGrpDetailDao.addProjection(Projections.Max("GroupID"));
                    IList maxId = m_IGrpDetailDao.GetObjectByCriteria(new List<ICriterion>());
                    if (maxId[0] != null)
                        grpid = ((int)maxId[0]) + 1;
                    else
                        grpid = 1;
                    grpdetails.GroupID = grpid;
                    grpdetails.CC = CC;
                    m_IGrpDetailDao.Save(grpdetails);
                }
                else
                {
                    grpdetails = m_IGrpDetailDao.GetById(GroupID);
                    grpdetails.Name = groupname;
                    if (privategrp == 1)
                        grpdetails.Private = 0;
                    else
                        grpdetails.Private = 1;
                    grpdetails.Description = description;
                    grpdetails.CC = CC;
                    m_IGrpDetailDao.Update(grpdetails);
                }

                if (groupid != "new")
                {
                    List<ICriterion> criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("GroupID", GroupID));
                    List<vrmGroupParticipant> delgrps = m_IGrpParticipantsDao.GetByCriteria(criterionList1);
                    for (int i = 0; i < delgrps.Count; i++)
                    {
                        m_IGrpParticipantsDao.Delete(delgrps[i]);
                    }
                    delgrps.Clear();
                }

                vrmGuestUser gustusr = new vrmGuestUser();
                XmlNodeList nodes = xd.SelectNodes("//login//groupInfo/group/user");

                if (nodes.Count > 0)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        node = nodes[i].SelectSingleNode("userID");
                        if (node != null)
                            partyid = node.InnerText.Trim();
                        if (partyid != "new")
                            int.TryParse(partyid, out PartyID);

                        if (partyid.ToLower() == "new")
                        {
                            m_IGuestUserDao.addProjection(Projections.Max("userid"));
                            IList maxId1 = m_IGuestUserDao.GetObjectByCriteria(new List<ICriterion>());
                            if (maxId1[0] != null)
                                gustid = ((int)maxId1[0]) + 1;
                            else
                                gustid = 1;
                            gustusr.userid = gustid;

                            node = nodes[i].SelectSingleNode("userFirstName");
                            if (node != null)
                                firstname = node.InnerText.Trim();
                            //ZD 102366
                            if (firstname.IndexOf("<") >= 0 || firstname.IndexOf(">") >= 0)
                                firstname = firstname.Replace('<', '(').Replace('>', ')');
                            gustusr.FirstName = firstname;

                            node = nodes[i].SelectSingleNode("userLastName");
                            if (nodes[i] != null)
                                lastname = node.InnerText.Trim();
                            //ZD 102366
                            if (lastname.IndexOf("<") >= 0 || lastname.IndexOf(">") >= 0)
                                lastname = lastname.Replace('<', '(').Replace('>', ')');
                            gustusr.LastName = lastname;

                            node = nodes[i].SelectSingleNode("userEmail");
                            if (node != null)
                                email = node.InnerText.Trim();
                            //ZD 102366
                            if (email.IndexOf("'") >= 0)
                                email = email.Replace("'", "");
                            gustusr.Email = email;
                            gustusr.companyId = organizationID;

                            vrmUser loginusr = m_IuserDao.GetByUserId(loginid);
                            gustusr.DefLineRate = loginusr.DefLineRate;
                            gustusr.DefVideoProtocol = loginusr.DefVideoProtocol;
                            gustusr.DefVideoSession = loginusr.DefVideoSession;
                            gustusr.DefAudio = loginusr.DefAudio;
                            gustusr.TimeZone = loginusr.TimeZone;

                            m_IGuestUserDao.Save(gustusr);
                        }

                        if (groupid == "new")
                            grppartys.GroupID = grpid;
                        else
                            grppartys.GroupID = GroupID;
                        if (partyid == "new")
                            grppartys.UserID = gustid;
                        else
                            grppartys.UserID = PartyID;

                        grppartys.CC = CC;
                        m_IGrpParticipantsDao.Save(grppartys);
                    }
                }
                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetGroup
        /// <summary>
        /// GetGroup (COM to .Net conversion)  
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool singleDeptMode = true;
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            int userid = 0, sortby = 0, mode = 1;
            string orgid = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0 || sortby < 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                int.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                int.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out sortby);

                // Check if the user is a superadmin, show all groups.
                // Check if the user is an Admin, show Private Groups of Superadmin and all other groups.
                // Check if the user is a General, show their Groups and public Groups.

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    user = m_IuserDao.GetByUserId(userid);
                }

                user = m_IuserDao.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(user.roleID); //crossaccess

                if (mode == 0) //If User is General User
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", user.userid)));
                }
                criterionList.Add(Expression.Eq("orgId", organizationID));  //If User is Site Admin

                if (singleDeptMode)
                {
                    if (!user.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid)));
                    }
                }
                else
                {
                    if (!(user.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (user.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_IuserDao.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                userId.Add(userList[i].userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userId)), Expression.Eq("Private", 0)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid))); //If its Gereral Users
                        }
                    }
                }
                outXML.Append("<groups>");
                outXML.Append("<sortBy>" + sortby + "</sortBy>");

                if (sortby == 1)
                    m_IGrpDetailDao.addOrderBy(Order.Asc("Name"));
                else if (sortby == 2)
                    m_IGrpDetailDao.addOrderBy(Order.Asc("Owner"));
                else if (sortby == 3)
                    m_IGrpDetailDao.addOrderBy(Order.Asc("Private"));

                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);

                for (int i = 0; i < Groups.Count; i++)
                {
                    if (Groups[i].Private == 1)
                        Groups[i].Private = 0;
                    else
                        Groups[i].Private = 1;

                    vrmUser owner = m_IuserDao.GetByUserId(Groups[i].Owner);
                    outXML.Append("<group>");
                    outXML.Append("<groupID>" + Groups[i].GroupID.ToString() + "</groupID>");
                    outXML.Append("<groupName>" + Groups[i].Name + "</groupName>");
                    if (owner != null)
                    {
                        outXML.Append("<ownerName>" + owner.FirstName + " " + owner.LastName + "</ownerName>");
                        outXML.Append("<ownerID>" + Groups[i].Owner + "</ownerID>");
                    }
                    else//FB 2318
                    {
                        outXML.Append("<ownerName></ownerName>");
                        outXML.Append("<ownerID></ownerID>");
                    }
                    outXML.Append("<description>" + Groups[i].Description + "</description>");
                    outXML.Append("<public>" + Groups[i].Private + "</public>");

                    List<ICriterion> criterionList2 = new List<ICriterion>();
                    criterionList2.Add(Expression.Eq("GroupID", Groups[i].GroupID));

                    List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList2);
                    for (int j = 0; j < partList.Count; j++)
                    {
                        vrmUser partUser = m_IuserDao.GetByUserId(partList[j].UserID);
                        if (partUser != null)
                        {
                            outXML.Append("<user>");
                            outXML.Append("<userID>" + partList[j].UserID.ToString() + "</userID>");
                            outXML.Append("<userFirstName>" + partUser.FirstName + "</userFirstName>");
                            outXML.Append("<userLastName>" + partUser.LastName + "</userLastName>");
                            outXML.Append("<userEmail>" + partUser.Email + "</userEmail>");
                            outXML.Append("</user>");
                        }
                        else
                        {
                            vrmGuestUser guestUser = m_IGuestUserDao.GetByUserId(partList[j].UserID);

                            if (guestUser != null) //FB 2318
                            {
                                outXML.Append("<user>");
                                outXML.Append("<userID>" + partList[j].UserID.ToString() + "</userID>");
                                outXML.Append("<userFirstName>" + guestUser.FirstName + "</userFirstName>");
                                outXML.Append("<userLastName>" + guestUser.LastName + "</userLastName>");
                                outXML.Append("<userEmail>" + guestUser.Email + "</userEmail>");
                                outXML.Append("</user>");
                            }
                        }
                    }
                    outXML.Append("</group>");
                }
                outXML.Append("</groups>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SearchGroup
        /// <summary>
        /// SearchGroup (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        public bool SearchGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool singleDeptMode = true;
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            int userid = 0, mode = 1;
            string orgid = "", groupName = "", party = "", description = "";
            List<int> UserIds = null;
            List<int> GrpIds = null;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groupName");
                if (node != null)
                    groupName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/includedParticipant");
                if (node != null)
                    party = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/descriptionIncludes");
                if (node != null)
                    description = node.InnerText.Trim();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    user = m_IuserDao.GetByUserId(userid);
                }

                user = m_IuserDao.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(user.roleID); //crossaccess

                if (mode == 0) //If User is General User
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", user.userid)));
                }
                criterionList.Add(Expression.Eq("orgId", organizationID));  //If User is Site Admin
                if (groupName.Length > 0)
                    criterionList.Add(Expression.Like("Name", "%%" + groupName.ToLower() + "%%").IgnoreCase());
                if (description.Length > 0)
                    criterionList.Add(Expression.Like("Description", "%%" + description.ToLower() + "%%").IgnoreCase());

                if (singleDeptMode)
                {
                    if (!user.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid)));
                    }
                }
                else
                {
                    if (!(user.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (user.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_IuserDao.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_IuserDao.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                userId.Add(userList[i].userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userId)), Expression.Eq("Private", 0)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", user.userid))); //If its Gereral Users
                        }
                    }
                }
                outXML.Append("<groups>");

                List<vrmGroupDetails> Groups1 = m_IGrpDetailDao.GetByCriteria(criterionList);
                if (party.Length > 0)
                {
                    List<ICriterion> criterionList4 = new List<ICriterion>();
                    criterionList4.Add(Expression.Or(Expression.Like("FirstName", "%%" + party.ToLower() + "%%").IgnoreCase(), Expression.Like("LastName", "%%" + party.ToLower() + "%%").IgnoreCase()));
                    List<vrmUser> partyNames = m_IuserDao.GetByCriteria(criterionList4);
                    List<vrmGuestUser> guestNames = m_IGuestUserDao.GetByCriteria(criterionList4);
                    if (guestNames.Count > 0)
                    {
                        for (int a = 0; a < guestNames.Count; a++)
                        {
                            if (UserIds == null)
                                UserIds = new List<int>();

                            UserIds.Add(guestNames[a].userid);
                        }
                    }
                    for (int j = 0; j < partyNames.Count; j++)
                    {
                        if (UserIds == null)
                            UserIds = new List<int>();

                        UserIds.Add(partyNames[j].userid);
                    }
                    if (UserIds != null)
                    {
                        if (UserIds.Count > 0)
                        {
                            List<ICriterion> criterionList3 = new List<ICriterion>();
                            criterionList3.Add(Expression.In("UserID", UserIds));
                            List<vrmGroupParticipant> groupid = m_IGrpParticipantsDao.GetByCriteria(criterionList3);

                            for (int i = 0; i < groupid.Count; i++)
                            {
                                if (GrpIds == null)
                                    GrpIds = new List<int>();

                                GrpIds.Add(groupid[i].GroupID);
                            }
                        }
                    }
                }
                if (!(party != "" && GrpIds == null))
                {
                    for (int i = 0; i < Groups1.Count; i++)
                    {
                        if (Groups1[i].Private == 1)
                            Groups1[i].Private = 0;
                        else
                            Groups1[i].Private = 1;

                        if (GrpIds != null)
                            if (GrpIds.Count > 0)
                                if (!GrpIds.Contains(Groups1[i].GroupID))
                                    continue;

                        vrmUser owner1 = m_IuserDao.GetByUserId(Groups1[i].Owner);
                        outXML.Append("<group>");
                        outXML.Append("<groupID>" + Groups1[i].GroupID + "</groupID>");
                        outXML.Append("<groupName>" + Groups1[i].Name + "</groupName>");
                        outXML.Append("<ownerID>" + Groups1[i].Owner + "</ownerID>");
                        outXML.Append("<ownerName>" + owner1.FirstName + " " + owner1.LastName + "</ownerName>");
                        outXML.Append("<description>" + Groups1[i].Description + "</description>");
                        outXML.Append("<public>" + Groups1[i].Private + "</public>");
                        outXML.Append("<users>");

                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("GroupID", Groups1[i].GroupID));
                        List<vrmGroupParticipant> partList1 = m_IGrpParticipantsDao.GetByCriteria(criterionList3);
                        for (int s = 0; s < partList1.Count; s++)
                        {
                            vrmUser partUser1 = m_IuserDao.GetByUserId(partList1[s].UserID);
                            if (partUser1 != null)
                            {
                                outXML.Append("<user>");
                                outXML.Append("<userID>" + partList1[s].UserID.ToString() + "</userID>");
                                outXML.Append("<userFirstName>" + partUser1.FirstName + "</userFirstName>");
                                outXML.Append("<userLastName>" + partUser1.LastName + "</userLastName>");
                                outXML.Append("<userEmail>" + partUser1.Email + "</userEmail>");
                                outXML.Append("</user>");
                            }
                            else
                            {
                                vrmGuestUser guestUser1 = m_IGuestUserDao.GetByUserId(partList1[s].UserID);
                                outXML.Append("<user>");
                                outXML.Append("<userID>" + partList1[s].UserID.ToString() + "</userID>");
                                outXML.Append("<userFirstName>" + guestUser1.FirstName + "</userFirstName>");
                                outXML.Append("<userLastName>" + guestUser1.LastName + "</userLastName>");
                                outXML.Append("<userEmail>" + guestUser1.Email + "</userEmail>");
                                outXML.Append("</user>");
                            }
                        }
                        outXML.Append("</users>");
                        outXML.Append("</group>");
                    }
                }
                outXML.Append("<sortBy>1</sortBy>");
                outXML.Append("</groups>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetUserStatus
        /// <summary>
        /// SetUserStatus 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUserStatus(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmUser userIDs = new vrmUser();
            vrmInactiveUser deluser = new vrmInactiveUser();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                int loginid = 0, userid = 0, mode = 0;
                string orgid = "";
                long newUser = 0, currUsrCount = 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out loginid);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/users/user/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                int.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/users/user/mode");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out mode);

                if (loginid <= 0 || userid <= 0 || mode <= 0)
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                userIDs = m_IuserDao.GetByUserId(userid);
                deluser = m_IinactiveUserDao.GetByUserId(userid);

                if (mode == 1) //Permanently Delete One User
                {
                    //ZD 101525
                    string enableSSO = "0";
                    List<ICriterion> cList = new List<ICriterion>();
                    cList.Add(Expression.Eq("OrgId", 11));
                    List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(cList, true);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                        enableSSO = ldapSettingsList[0].EnableSSOMode.ToString();

                    //Config config = new Config(m_configPath);
                    //bool ret = config.Initialize();
                    //if (config.ldapEnabled)
                    if (enableSSO == "1")
                    {
                        myvrmEx = new myVRMException(229);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    else
                    {
                        //FB 2164 Start
                        int oroleID = deluser.roleID;
                        List<ICriterion> del = new List<ICriterion>();
                        del.Add(Expression.Eq("roleID", oroleID));
                        List<vrmUser> Ausrlist = m_IuserDao.GetByCriteria(del);
                        List<vrmInactiveUser> usrlist = m_IinactiveUserDao.GetByCriteria(del);
                        int numrows = usrlist.Count;
                        int Act = Ausrlist.Count;
                        if (Act == 0)
                        {
                            if (numrows == 1)
                            {
                                List<ICriterion> critrole = new List<ICriterion>();
                                critrole.Add(Expression.Eq("roleID", oroleID));
                                critrole.Add(Expression.Eq("createType", 2));
                                List<vrmUserRoles> usrrle = m_IUserRolesDao.GetByCriteria(critrole);
                                for (int j = 0; j < usrrle.Count; j++)
                                {
                                    usrrle[j].locked = 0;
                                    m_IUserRolesDao.Update(usrrle[j]);
                                }
                            }
                        }
                        //FB 2164 End
                        if (userIDs != null)
                            m_IuserDao.Delete(userIDs);
                        m_IinactiveUserDao.Delete(deluser);

                        List<ICriterion> criterionListA = new List<ICriterion>();
                        criterionListA.Add(Expression.Eq("userid", userid));
                        List<vrmAccountGroupList> AccountGroup = m_IGroupAccountDAO.GetByCriteria(criterionListA);
                        for (int i = 0; i < AccountGroup.Count; i++)
                            m_IGroupAccountDAO.Delete(AccountGroup[i]);

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("approverid", userid));
                        List<vrmLocApprover> LocApprov = m_ILocApprovDAO.GetByCriteria(criterionList);
                        for (int a = 0; a < LocApprov.Count; a++)
                            m_ILocApprovDAO.Delete(LocApprov[a]);

                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        criterionList1.Add(Expression.Eq("approverid", userid));
                        List<vrmMCUApprover> MCUapprov = m_IMCUapprover.GetByCriteria(criterionList1);
                        for (int b = 0; b < MCUapprov.Count; b++)
                            m_IMCUapprover.Delete(MCUapprov[b]);

                        List<ICriterion> criterionList2 = new List<ICriterion>();
                        criterionList2.Add(Expression.Eq("approverid", userid));
                        List<sysApprover> sysApprover = m_ISysApproverDAO.GetByCriteria(criterionList2);
                        for (int c = 0; c < sysApprover.Count; c++)
                            m_ISysApproverDAO.Delete(sysApprover[c]);

                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("userid", userid));
                        List<vrmConfUser> ConfUser = m_IConfUserDAO.GetByCriteria(criterionList3);
                        for (int d = 0; d < ConfUser.Count; d++)
                            m_IConfUserDAO.Delete(ConfUser[d]);

                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("userId", userid));
                        List<vrmAccount> usrAccount = m_IuserAccountDAO.GetByCriteria(criterionList4);
                        for (int e = 0; e < usrAccount.Count; e++)
                            m_IuserAccountDAO.Delete(usrAccount[e]);

                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("UserID", userid));
                        List<vrmGroupParticipant> grpusr = m_IGrpParticipantsDao.GetByCriteria(criterionList5);
                        for (int f = 0; f < grpusr.Count; f++)
                            m_IGrpParticipantsDao.Delete(grpusr[f]);

                        List<ICriterion> criterionList6 = new List<ICriterion>();
                        criterionList6.Add(Expression.Eq("userid", userid));
                        List<vrmTempUser> tempusr = m_TempUserDAO.GetByCriteria(criterionList6);
                        for (int g = 0; g < tempusr.Count; g++)
                            m_TempUserDAO.Delete(tempusr[g]);

                        List<ICriterion> criterionList7 = new List<ICriterion>();
                        criterionList7.Add(Expression.Eq("userId", userid));
                        List<vrmUserDepartment> deptusr = m_IuserDeptDAO.GetByCriteria(criterionList7);
                        for (int h = 0; h < deptusr.Count; h++)
                            m_IuserDeptDAO.Delete(deptusr[h]);
                    }
                }
                else if (mode == 2) //Restore One User
                {
                    List<ICriterion> criterionList8 = new List<ICriterion>();
                    criterionList8.Add(Expression.Eq("companyId", deluser.companyId));
                    newUser = m_IuserDao.CountByCriteria(criterionList8);
                    newUser = newUser + 1;
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(deluser.companyId);
                    //ZD 101443
                    if (sysSettings.IsLDAP == 0 && newUser > orgInfo.UserLimit)
                    {
                        myvrmEx = new myVRMException(252);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2346 Starts
                    string userMail = deluser.Email.Substring(deluser.Email.IndexOf('@'));
                    string tempemail = userMail;
                    if (userMail.Contains("_D"))
                        userMail = userMail.Replace("_D", "");
                    deluser.Email = deluser.Email.Replace(tempemail, userMail);

                    List<ICriterion> criterionListUser = new List<ICriterion>();
                    criterionListUser.Add(Expression.Eq("Email", deluser.Email));
                    newUser = m_IuserDao.CountByCriteria(criterionListUser);
                    if (newUser > 0)
                    {
                        myvrmEx = new myVRMException(261);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2346 Ends
                    if (deluser.enableExchange == 1)
                    {
                        List<ICriterion> criterionList9 = new List<ICriterion>();
                        criterionList9.Add(Expression.Eq("enableExchange", 1));
                        criterionList9.Add(Expression.Eq("Deleted", 0));
                        criterionList9.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList9);

                        if (currUsrCount >= orgInfo.ExchangeUserLimit)
                        {
                            myvrmEx = new myVRMException(460);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (deluser.enableDomino == 1)
                    {
                        List<ICriterion> criterionList10 = new List<ICriterion>();
                        criterionList10.Add(Expression.Eq("enableDomino", 1));
                        criterionList10.Add(Expression.Eq("Deleted", 0));
                        criterionList10.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList10);

                        if (currUsrCount >= orgInfo.DominoUserLimit)
                        {
                            myvrmEx = new myVRMException(461);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (deluser.enableMobile == 1)
                    {
                        List<ICriterion> criterionList11 = new List<ICriterion>();
                        criterionList11.Add(Expression.Eq("enableMobile", 1));
                        criterionList11.Add(Expression.Eq("Deleted", 0));
                        criterionList11.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList11);

                        if (currUsrCount >= orgInfo.MobileUserLimit)
                        {
                            myvrmEx = new myVRMException(526);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (deluser.enablePCUser == 1)
                    {
                        List<ICriterion> criterionList11 = new List<ICriterion>();
                        criterionList11.Add(Expression.Eq("enablePCUser", 1));
                        criterionList11.Add(Expression.Eq("Deleted", 0));
                        criterionList11.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList11);

                        if (currUsrCount >= orgInfo.PCUserLimit)
                        {
                            myvrmEx = new myVRMException(682);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    //ZD 100221 Starts
                    currUsrCount = 0;
                    if (deluser.enableWebexUser == 1)
                    {
                        List<ICriterion> criterionList11 = new List<ICriterion>();
                        criterionList11.Add(Expression.Eq("enableWebexUser", 1));
                        criterionList11.Add(Expression.Eq("Deleted", 0));
                        criterionList11.Add(Expression.Eq("companyId", organizationID));
                        currUsrCount = m_IuserDao.CountByCriteria(criterionList11);

                        if (currUsrCount >= orgInfo.WebexUserLimit)
                        {
                            myvrmEx = new myVRMException(718);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 100221 Ends
                    vrmUser user = new vrmUser();
                    user = new vrmUser(deluser);
                    user.LockStatus = vrmUserStatus.USER_ACTIVE;
                    user.Deleted = 0;
                    user.newUser = 1;
                    user.TickerSpeed = 6;//FB 2027 - Starts
                    user.TickerSpeed1 = 6;
                    user.TickerStatus = 1;
                    user.TickerStatus1 = 1;//FB 2027 - End
                    m_IinactiveUserDao.Delete(deluser);
                    m_IuserDao.Save(user);

                    //ZD 101860
                    vrmUserRoles roles = new vrmUserRoles();
                    roles = m_IUserRolesDao.GetById(user.roleID);

                    List<ICriterion> critCustRole = new List<ICriterion>();
                    critCustRole.Add(Expression.Eq("createType", 2));
                    List<vrmUserRoles> rolelist = m_IUserRolesDao.GetByCriteria(critCustRole);
                    for (int r = 0; r < rolelist.Count; r++)
                    {
                        List<ICriterion> critUsr = new List<ICriterion>();
                        critUsr.Add(Expression.Eq("roleID", rolelist[r].roleID));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(critUsr);
                        if (usr != null)
                        {
                            if (usr.Count == 0)
                            {
                                rolelist[r].locked = 0;
                            }
                            else
                            {
                                rolelist[r].locked = 1;
                            }
                            m_IUserRolesDao.Update(rolelist[r]);
                        }
                    }
                }
                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GuestRegister
        /// <summary>
        /// GuestRegister (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GuestRegister(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            string fname = "";
            string lname = "";
            string email = "";
            string pwd = "";
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/userFirstName");
                if (node != null)
                    fname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/userLastName");
                if (node != null)
                    lname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/userPassword");
                if (node != null)
                    pwd = node.InnerXml.Trim();

                criterionList.Add(Expression.Eq("Email", email));
                List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                List<vrmGuestUser> Gusrlist = m_IGuestUserDao.GetByCriteria(criterionList);
                for (int u = 0; u < usrlist.Count; u++)
                {
                    obj.outXml = "<GuestRegister><userID>" + usrlist[u].userid.ToString() + "</userID></GuestRegister>";
                    return false;
                }
                for (int i = 0; i < Gusrlist.Count; i++)
                {
                    obj.outXml = "<GuestRegister><userID>" + Gusrlist[i].userid.ToString() + "</userID></GuestRegister>";
                    return false;
                }
                if (pwd.Length > 0)
                {
                    crypto = new cryptography.Crypto();
                    pwd = crypto.encrypt(pwd);
                }
                string uString = "SELECT MAX(userid + 1) as userid FROM myVRM.DataLayer.vrmUser";
                IList list = m_IuserDao.execQuery(uString);
                //Empty DB Issue FB 2164
                int uid = 0;
                if (list[0] != null)
                    int.TryParse(list[0].ToString(), out uid);

                string qString = "SELECT MAX(userid + 1) as userid FROM myVRM.DataLayer.vrmGuestUser";
                IList glist = m_IGuestUserDao.execQuery(qString);
                //Empty DB Issue FB 2164
                int gid = 0;
                if (glist[0] != null)
                    int.TryParse(glist[0].ToString(), out gid);

                if (uid > gid)
                {
                    userid = uid;
                }
                else
                {
                    userid = gid;
                }
                vrmGuestUser Gusr = new vrmGuestUser();
                Gusr.companyId = orgid;
                Gusr.userid = userid;
                //ZD 102366
                if (fname.IndexOf("<") >= 0 || fname.IndexOf(">") >= 0)
                    fname = fname.Replace('<', '(').Replace('>', ')');
                if (lname.IndexOf("<") >= 0 || lname.IndexOf(">") >= 0)
                    lname = lname.Replace('<', '(').Replace('>', ')');
                if (email.IndexOf("'") >= 0)
                    email = email.Replace("'", "");
                Gusr.FirstName = fname;
                Gusr.LastName = lname;
                Gusr.Login = email;
                Gusr.Email = email;
                Gusr.Password = pwd;
                Gusr.emailmask = 1; //FB 2550
                Gusr.TimeZone = 26;//FB 2550
                m_IGuestUserDao.Save(Gusr);
                obj.outXml = "<GuestRegister><userID>" + userid.ToString() + "</userID></GuestRegister>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetUserRoles
        /// <summary>
        /// GetUserRoles (COM TO .NetConversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetUserRoles(ref vrmDataObject obj)
        {
            try
            {
                List<int> ModuleList = new List<int>();
                StringBuilder outXML = new StringBuilder();
                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> criterionList1 = new List<ICriterion>();//FB 2891
                int userID = 11, isFoodEnabled = 1, isRoomEnabled = 1, isHkEnabled = 1;
                List<string> orgID = new List<string>(); //ZD 100923

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                //ZD 100923 Starts
                orgID.Add("0");
                orgID.Add(organizationID.ToString());
                //ZD 100923 Ends

                ModuleList.Add(0);
                node = xd.SelectSingleNode("//login/foodModuleEnable");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out isFoodEnabled);
                    if (isFoodEnabled == 0)
                        ModuleList.Add(4);
                }
                node = xd.SelectSingleNode("//login/roomModuleEnable");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out isRoomEnabled);
                    if (isRoomEnabled == 0)
                        ModuleList.Add(5);
                }
                node = xd.SelectSingleNode("//login/hkModuleEnable");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out isHkEnabled);
                    if (isHkEnabled == 0)
                        ModuleList.Add(6);
                }
                //FB 2891 - Start
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Not(Expression.In("roleID", ModuleList)));
                outXML.Append("<userRole><roles>");
                criterionList.Add(Expression.Gt("DisplayOrder", 0));
                m_IUserRolesDao.addOrderBy(Order.Asc("DisplayOrder"));

                criterionList.Add(Expression.In("orgID", orgID)); //ZD 100923
                //criterionList.Add(Expression.And(Expression.Eq("orgID", organizationID), Expression.Eq("orgID", 0))); // ZD 100923
                List<vrmUserRoles> usrRoles = m_IUserRolesDao.GetByCriteria(criterionList);

                criterionList1.Add(Expression.Not(Expression.In("roleID", ModuleList)));
                criterionList1.Add(Expression.Lt("DisplayOrder", 1));
                criterionList1.Add(Expression.In("orgID", orgID)); //ZD 100923
                List<vrmUserRoles> usrRoles1 = m_IUserRolesDao.GetByCriteria(criterionList1);
                usrRoles.AddRange(usrRoles1);
                //FB 2891 - End

                for (int i = 0; i < usrRoles.Count; i++)
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("roleId", usrRoles[i].roleID));
                    criterionList.Add(Expression.Eq("deleted", 0));//ZD 101860
                    List<vrmUserTemplate> usrTemplate = m_ItempDao.GetByCriteria(criterionList);

                    outXML.Append("<role>");
                    outXML.Append("<ID>" + usrRoles[i].roleID.ToString() + "</ID>");
                    outXML.Append("<name>" + usrRoles[i].roleName + "</name>");
                    outXML.Append("<menuMask>" + usrRoles[i].roleMenuMask + "</menuMask>");
                    outXML.Append("<locked>" + usrRoles[i].locked.ToString() + "</locked>");
                    outXML.Append("<level>" + usrRoles[i].level.ToString() + "</level>");


                    if (usrTemplate.Count > 0)
                        outXML.Append("<active>1</active>");
                    else
                        outXML.Append("<active>0</active>");

                    outXML.Append("<crossaccess>" + usrRoles[i].crossaccess.ToString() + "</crossaccess>");
                    outXML.Append("<createType>" + usrRoles[i].createType.ToString() + "</createType>");
                    outXML.Append("</role>");
                }
                outXML.Append("</roles></userRole>");
                obj.outXml = outXML.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetUserRoles" + ex.Message);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region DeleteGroup
        /// <summary>
        /// DeleteGroup
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteGroup(ref vrmDataObject obj)
        {
            try
            {
                myVRMException myVRMEx = null;
                List<ICriterion> criterionList = new List<ICriterion>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 11, groupID = 0, i = 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/groups/group/groupID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out groupID);

                criterionList.Add(Expression.Eq("GroupID", groupID));

                List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList);
                for (i = 0; i < partList.Count; i++)
                    m_IGrpParticipantsDao.Delete(partList[i]);

                List<vrmGroupDetails> GrpDetails = m_IGrpDetailDao.GetByCriteria(criterionList);
                for (i = 0; i < GrpDetails.Count; i++)
                    m_IGrpDetailDao.Delete(GrpDetails[i]);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("DeleteGroup" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetEmailList
        /// <summary>
        /// GetEmailList - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetEmailList(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 0; //FB 2269 
                int SortingOrder = 0; //ZD 103405
                int isconfadmin = 0;    //ALLDEV-498
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                int.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sortBy);

                node = xd.SelectSingleNode("//login/audioaddon"); //FB 2023
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        int.TryParse(node.InnerText.Trim(), out isAudioAddOn);
                //ZD 103405 start
                node = xd.SelectSingleNode("//login/SortingOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SortingOrder);
                //ZD 103405 End

                //ALLDEV-498 Starts
                node = xd.SelectSingleNode("//login/isconfadmin");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out isconfadmin);
                //ALLDEV-498 Ends
                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 2, ref outXml, organizationID, 1, deptUsers, BulkUser, isconfadmin)) //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region GetGuestList
        /// <summary>
        /// GetGuestList - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetGuestList(ref vrmDataObject obj)
        {

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                int pageNo = 0;
                int sortBy = 0;
                string alphabet = "";
                int totPages = 0;
                int deptUsers = 1; //FB 2269
                int SortingOrder = 0; //ZD 103405
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                int.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments; //Organization Module Fixes

                node = xd.SelectSingleNode("//login/alphabet");
                if (node != null)
                    alphabet = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/pageNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out pageNo);
                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out sortBy);
                //ZD 103405 start
                node = xd.SelectSingleNode("//login/SortingOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SortingOrder);
                //ZD 103405 End
                string BulkUser = string.Empty;//ZD 100263
                string outXml = string.Empty;
                if (GetUserList(userID, m_isGustUser, pageNo, ref totPages, alphabet, sortBy, SortingOrder, 2, ref outXml, organizationID, 1, deptUsers, BulkUser, 0)) //FB 2269 //ZD 103405 //ALLDEV-498
                {
                    obj.outXml = outXml;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //FB 2670 START
        #region GetVNOCUserList
        /// <summary>
        /// GetVNOCUserList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetVNOCUserList(ref vrmDataObject obj)
        {
            bool bRet = true;
            int i = 0, isCrossSilo = 0; //FB 2766
            vrmBaseUser user = null;
            vrmOrganization vrmOrg = null;
            StringBuilder OutXML = new StringBuilder();
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                int.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;


                node = xd.SelectSingleNode("//login/isCrossSilo"); //FB 2766
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out isCrossSilo);

                int.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                List<ICriterion> VNOCcriterionList = new List<ICriterion>();
                VNOCcriterionList.Add(Expression.Eq("level", 3));
                VNOCcriterionList.Add(Expression.Eq("crossaccess", 1));
                List<vrmUserRoles> usrrole = m_IUserRolesDao.GetByCriteria(VNOCcriterionList);

                List<ICriterion> criterionList = new List<ICriterion>();

                if (isCrossSilo == 0) //FB 2766
                {
                    if (sysSettings.ConciergeSupport == 1)
                        criterionList.Add(Expression.Or(Expression.Eq("companyId", organizationID), Expression.Eq("companyId", 11)));
                    else if (sysSettings.ConciergeSupport == 2)
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                }

                criterionList.Add(Expression.Eq("roleID", usrrole[0].roleID));

                IList userList = new List<vrmBaseUser>();

                userList = m_IuserDao.GetByCriteria(criterionList);
                OutXML.Append("<users>");

                for (i = 0; i < userList.Count; i++)
                {
                    user = (vrmBaseUser)userList[i];
                    vrmOrg = m_IOrgDAO.GetById(organizationID);
                    if (user.LockStatus == vrmUserStatus.USER_ACTIVE)
                    {
                        vrmOrg = m_IOrgDAO.GetById(user.companyId); //FB 2766
                        OutXML.Append("<user>");
                        OutXML.Append("<userID>" + user.userid + "</userID>");
                        if (sysSettings.ConciergeSupport == 1)
                        {
                            if ((organizationID != 11 && user.companyId == 11) || (organizationID == 11 && user.companyId == 11 && isCrossSilo == 1)) //FB 2820
                            {
                                OutXML.Append("<firstName>" + user.FirstName + " *</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");
                            }
                            else
                            {
                                OutXML.Append("<firstName>" + user.FirstName + "</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");
                            }
                        }
                        else
                        {
                            if (user.companyId == 11 && isCrossSilo == 1) //FB 2820
                            {
                                OutXML.Append("<firstName>" + user.FirstName + " *</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");
                            }
                            else
                            {
                                OutXML.Append("<firstName>" + user.FirstName + "</firstName>");
                                OutXML.Append("<lastName>" + user.LastName + "</lastName>");

                            }
                        }
                        OutXML.Append("<userEmail>" + user.Email + "</userEmail>");
                        OutXML.Append("<orgName>" + vrmOrg.orgname + "</orgName>"); //FB 2766
                        OutXML.Append("<login>" + user.Login + "</login>");
                        OutXML.Append("</user>");
                    }
                }

                OutXML.Append("</users>");
                obj.outXml = OutXML.ToString();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //FB 2670 END

        #region FetchGroups
        /// <summary>
        /// FetchGroups (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public void FetchGroups(List<ICriterion> criterionList, ref StringBuilder outXml, int mode, string ParentTag)
        {
            try
            {
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);
                vrmGuestUser gustUsr = null;
                vrmUser partUser = null;
                string partyFName = "", partyLName = "", partyEmail = "";

                for (int g = 0; g < Groups.Count; g++)
                {
                    if (ParentTag.Trim() != "")
                        outXml.Append("<" + ParentTag + ">");

                    outXml.Append("<groupID>" + Groups[g].GroupID.ToString() + "</groupID>");
                    outXml.Append("<groupName>" + Groups[g].Name + "</groupName>");
                    if (mode == 0)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("GroupID", Groups[g].GroupID));
                        List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList);
                        outXml.Append("<numParticipants>" + partList.Count + "</numParticipants>"); //FB 2027 goc
                        for (int p = 0; p < partList.Count; p++)
                        {
                            partUser = m_IuserDao.GetByUserId(partList[p].UserID);
                            if (partUser == null)
                            {
                                gustUsr = m_IGuestUserDao.GetByUserId(partList[p].UserID);
                                if (gustUsr != null) //FB 2318
                                {
                                    partyFName = gustUsr.FirstName;
                                    partyLName = gustUsr.LastName;
                                    partyEmail = gustUsr.Email;
                                }
                            }
                            else
                            {
                                partyFName = partUser.FirstName;
                                partyLName = partUser.LastName;
                                partyEmail = partUser.Email;
                            }
                            outXml.Append("<user>");
                            outXml.Append("<userID>" + partList[p].UserID.ToString() + "</userID>");
                            outXml.Append("<userFirstName>" + partyFName + "</userFirstName>");
                            outXml.Append("<userLastName>" + partyLName + "</userLastName>");
                            outXml.Append("<userEmail>" + partyEmail + "</userEmail>");
                            outXml.Append("<CC>" + partList[p].CC.ToString() + "</CC>");
                            outXml.Append("</user>");
                        }
                    }

                    if (ParentTag.Trim() != "")
                        outXml.Append("</" + ParentTag + ">");
                }
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("vrmException", myVRMEx);
                throw myVRMEx;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                throw ex;
            }
        }

        //FB 2639_GetOldConf
        public void FetchGroups(List<ICriterion> criterionList, ref XmlWriter outXml, int mode, string ParentTag)
        {
            try
            {
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);
                vrmGuestUser gustUsr = null;
                vrmUser partUser = null;
                string partyFName = "", partyLName = "", partyEmail = "";

                for (int g = 0; g < Groups.Count; g++)
                {
                    outXml.WriteStartElement(ParentTag);
                    outXml.WriteElementString("groupID", Groups[g].GroupID.ToString());
                    outXml.WriteElementString("groupName", Groups[g].Name);
                    if (mode == 0)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("GroupID", Groups[g].GroupID));
                        List<vrmGroupParticipant> partList = m_IGrpParticipantsDao.GetByCriteria(criterionList);
                        outXml.WriteElementString("numParticipants", partList.Count.ToString()); //FB 2027 goc
                        for (int p = 0; p < partList.Count; p++)
                        {
                            partUser = m_IuserDao.GetByUserId(partList[p].UserID);
                            if (partUser == null)
                            {
                                gustUsr = m_IGuestUserDao.GetByUserId(partList[p].UserID);
                                if (gustUsr != null) //FB 2318
                                {
                                    partyFName = gustUsr.FirstName;
                                    partyLName = gustUsr.LastName;
                                    partyEmail = gustUsr.Email;
                                }
                            }
                            else
                            {
                                partyFName = partUser.FirstName;
                                partyLName = partUser.LastName;
                                partyEmail = partUser.Email;
                            }
                            outXml.WriteStartElement("user");
                            outXml.WriteElementString("userID", partList[p].UserID.ToString());
                            outXml.WriteElementString("userFirstName", partyFName);
                            outXml.WriteElementString("userLastName", partyLName);
                            outXml.WriteElementString("userEmail", partyEmail);
                            outXml.WriteElementString("CC", partList[p].CC.ToString());
                            outXml.WriteFullEndElement(); //ZD 100116
                        }
                    }
                    outXml.WriteFullEndElement(); //ZD 100116
                }
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("vrmException", myVRMEx);
                throw myVRMEx;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                throw ex;
            }
        }
        //FB 2639_GetOldConf End
        #endregion

        #region GetManageBulkUsers
        /// <summary>
        /// GetManageBulkUsers (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetManageBulkUsers(ref vrmDataObject obj)
        {
            string lang = "";
            string time = "";
            string role = "";
            string bridge = "";
            string deptout = "";
            int err = 0;
            bool bRet = true;

            List<ICriterion> criterionList3 = new List<ICriterion>();
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                outXml.Append("<getManageBulkUsers>");
                List<sysData> sysDatas = new List<sysData>();
                sysDatas = m_IsystemDAO.GetAll();
                int tzid = sysDatas[0].TimeZone;
                outXml.Append("<timezoneID>" + tzid.ToString() + "</timezoneID>");
                outXml.Append("<timezones>");
                if (FetchAllTimeZones(obj, err, orgid, ref time))
                    outXml.Append(time);
                outXml.Append("</timezones>");
                if (OldFetchUserRoleList(obj, ref role))
                    outXml.Append(role);
                if (OldFetchLanguageList(obj, ref lang))
                    outXml.Append(lang);
                if (FetchBridges(obj, err, orgid, ref bridge))
                    outXml.Append(bridge);
                if (GetManageDepartment(obj, userid, orgid, ref deptout))
                    outXml.Append(deptout);
                outXml.Append("</getManageBulkUsers>");
                obj.outXml = outXml.ToString();
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchAllTimeZones
        /// <summary>
        /// FetchAllTimeZones (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchAllTimeZones(vrmDataObject obj, int err, int orgid, ref string time)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                int tzsid;
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);
                tzsid = orgInfo.tzsystemid;
                if (tzsid == 0)
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        outXml.Append("<timezone>");
                        outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                        outXml.Append("<timezoneName>" + timeZoneList[t].TimeZoneDiff.ToString() + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                        outXml.Append("</timezone>");
                    }
                }
                else
                {
                    List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();
                    for (int t = 0; t < timeZoneList.Count; t++)
                    {
                        if (orgInfo.tzsystemid == timeZoneList[t].systemID)
                        {
                            outXml.Append("<timezone>");
                            outXml.Append("<timezoneID>" + timeZoneList[t].TimeZoneID.ToString() + "</timezoneID>");
                            outXml.Append("<timezoneName>" + timeZoneList[t].TimeZoneDiff.ToString() + timeZoneList[t].TimeZone.ToString() + "</timezoneName>");
                            outXml.Append("</timezone>");
                        }
                    }
                }
                obj.outXml = outXml.ToString();
                time = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region OldFetchUserRoleList
        /// <summary>
        /// OldFetchUserRoleList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchUserRoleList(vrmDataObject obj, ref string role)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<roles>");
                //FB 2891 - Start
                //List<vrmUserRoles> roles = m_IUserRolesDao.GetAll();
                List<ICriterion> criterionList1 = new List<ICriterion>();
                List<ICriterion> criterionList2 = new List<ICriterion>();

                criterionList1.Add(Expression.Gt("DisplayOrder", 0));
                m_IUserRolesDao.addOrderBy(Order.Asc("DisplayOrder"));
                List<vrmUserRoles> usrRoles = m_IUserRolesDao.GetByCriteria(criterionList1);

                criterionList2.Add(Expression.Lt("DisplayOrder", 1));
                List<vrmUserRoles> usrRoles1 = m_IUserRolesDao.GetByCriteria(criterionList2);

                usrRoles.AddRange(usrRoles1);

                if (usrRoles.Count <= 0)
                {
                    m_log.Error("Error in getting role list");
                    outXml.Append("");
                    return false;
                }
                foreach (vrmUserRoles uR in usrRoles)//FB 2891 - End
                {
                    outXml.Append("<role>");
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("roleId", uR.roleID));
                    List<vrmUserTemplate> tmplist = m_ItempDao.GetByCriteria(criterionList);
                    outXml.Append("<ID>" + uR.roleID.ToString() + "</ID>");
                    outXml.Append("<name>" + uR.roleName + "</name>");
                    outXml.Append("<menuMask>" + uR.roleMenuMask + "</menuMask>");
                    outXml.Append("<locked>" + uR.locked.ToString() + "</locked>");
                    outXml.Append("<level>" + uR.level.ToString() + "</level>");
                    if (tmplist.Count > 0)
                    {
                        outXml.Append("<active>1</active>");
                    }
                    else
                    {
                        outXml.Append("<active>0</active>");
                    }
                    outXml.Append("<createType>" + uR.createType.ToString() + "</createType>");
                    outXml.Append("</role>");
                }
                outXml.Append("</roles>");
                obj.outXml = outXml.ToString();
                role = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region FetchBridges
        /// <summary>
        /// FetchBridges (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchBridges(vrmDataObject obj, int err, int orgid, ref string bridge)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                outXml.Append("<bridges>");
                List<ICriterion> criterionList2 = new List<ICriterion>();
                criterionList2.Add(Expression.Eq("deleted", 0));
                criterionList2.Add(Expression.Or(Expression.Eq("orgId", orgid), Expression.Eq("isPublic", 1)));
                List<vrmMCU> mculist = m_ImcuDao.GetByCriteria(criterionList2);
                foreach (vrmMCU mcu in mculist)
                {
                    outXml.Append("<bridge>");
                    outXml.Append("<ID>" + mcu.BridgeID.ToString() + "</ID>");
                    string PublicMCU = mcu.isPublic.ToString();
                    if (orgid != 11 && PublicMCU != "0")
                    {
                        outXml.Append("<name>" + mcu.BridgeName + "(*)</name>");
                    }
                    else
                    {
                        outXml.Append("<name>" + mcu.BridgeName + "</name>");
                    }
                    outXml.Append("<interfaceType>" + mcu.MCUType.id.ToString().ToString() + "</interfaceType>");
                    List<ICriterion> criterionList3 = new List<ICriterion>();
                    criterionList3.Add(Expression.Eq("userid", mcu.Admin));
                    List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList3);
                    if (usrlist.Count > 0)
                    {
                        outXml.Append("<administrator>" + usrlist[0].FirstName + usrlist[0].LastName + "</administrator>");
                    }
                    else
                    {
                        outXml.Append("<administrator> Unknown </administrator>");
                    }
                    string exit, virtualBridge;
                    virtualBridge = mcu.VirtualBridge.ToString();
                    if (virtualBridge == "0")
                    {
                        exit = "1";
                    }
                    else
                    {
                        exit = "0";
                    }
                    outXml.Append("<administrator>" + exit + "</administrator>");
                    outXml.Append("<status>" + mcu.Status.ToString() + "</status>");
                    outXml.Append("</bridge>");
                }
                outXml.Append("</bridges>");
                obj.outXml = outXml.ToString();
                bridge = outXml.ToString();
                return true;
            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region GetManageDepartment
        /// <summary>
        /// GetManageDepartment (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetManageDepartment(vrmDataObject obj, int userid, int orgid, ref string deptout)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                if (orgid < 11)
                    orgid = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = m_IuserDao.GetByUserId(userid);

                List<ICriterion> deptCriterionList = new List<ICriterion>();
                deptCriterionList.Add(Expression.Eq("deleted", 0));
                string org = Convert.ToString(orgid);
                deptCriterionList.Add(Expression.Eq("orgId", org));

                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<int> deptIn = new List<int>();
                if (multiDepts == 1 && user.Admin != vrmUserConstant.SUPER_ADMIN)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", user.userid));
                    deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                    foreach (vrmUserDepartment dept in deptList)
                    {
                        deptIn.Add(dept.departmentId);
                    }
                    deptCriterionList.Add(Expression.In("departmentId", deptIn));
                }
                List<vrmDept> selectedDept = m_IdeptDAO.GetByCriteria(deptCriterionList);

                outXml.Append("<multiDepartment>" + multiDepts.ToString() + "</multiDepartment>");
                outXml.Append("<departments>");

                foreach (vrmDept dept in selectedDept)
                {
                    outXml.Append("<department>");
                    outXml.Append("<id>" + dept.departmentId.ToString() + "</id>");
                    outXml.Append("<name>" + dept.departmentName + "</name>");
                    outXml.Append("<securityKey>" + dept.securityKey + "</securityKey>");
                    outXml.Append("</department>");
                }
                outXml.Append("</departments>");
                obj.outXml = outXml.ToString();
                deptout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        //FB 2027
        #region FetchGlobalInfo
        /// <summary>
        /// FetchGlobalInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public void FetchGlobalInfo(int organizationID, int userID, ref StringBuilder outXml, ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            crypto = new cryptography.Crypto(); //ZD 100263
            try
            {
                outXml.Append("<globalInfo>");
                vrmOrganization vrmOrg = m_IOrgDAO.GetById(organizationID);
                outXml.Append("<organizationName>" + vrmOrg.orgname + "</organizationName>");
                outXml.Append("<siteBJNUserLimit>" + sysSettings.MaxBlueJeansUsers + "</siteBJNUserLimit>"); //ZD 104116

                vrmUser usr = new vrmUser();
                usr = m_IuserDao.GetByUserId(userID);

                outXml.Append("<userID>" + userID.ToString() + "</userID>");
                outXml.Append("<userName>");
                outXml.Append("<firstName>" + usr.FirstName + "</firstName>");
                outXml.Append("<lastName>" + usr.LastName + "</lastName>");
                outXml.Append("</userName>");
                outXml.Append("<userEmail>" + usr.Email + "</userEmail>");
                outXml.Append("<menuMask>" + usr.MenuMask + "</menuMask>");

                //FB 2659 - Starts
                string[] mary = usr.MenuMask.ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int usrmenu = Convert.ToInt32(ccary[1]);
                bool isExpuser = Convert.ToBoolean((((usrmenu & 1) == 1) && ((usrmenu & 2) == 2)) || ((((usrmenu & 2) == 2) && ((usrmenu & 64) == 64))));

                if (isExpuser)
                    outXml.Append("<poweruser>0</poweruser>");
                else
                    outXml.Append("<poweruser>1</poweruser>");
                //FB 2659 - End


                if (usr.Admin == 2 || usr.approverCount > 0)
                    outXml.Append("<approver>" + 1 + "</approver>");
                else
                    outXml.Append("<approver>" + 0 + "</approver>");

                outXml.Append("<admin>" + usr.Admin + "</admin>");
                outXml.Append("<level>3</level>");// hardcode level as 3 (that's what it used to be)
                outXml.Append("<enableAV>" + usr.enableAV + "</enableAV>");
                outXml.Append("<enableParticipants>" + usr.enableParticipants + "</enableParticipants>");
                //ZD 101122
                outXml.Append("<enableAVWorkOrder>" + usr.EnableAVWO + "</enableAVWorkOrder>");
                outXml.Append("<enableCateringWO>" + usr.EnableCateringWO + "</enableCateringWO>");
                outXml.Append("<enableFacilityWO>" + usr.EnableFacilityWO + "</enableFacilityWO>");

                outXml.Append("<enableAdditionalOption>" + usr.enableAdditionalOption + "</enableAdditionalOption>");//ZD 101093

                vrmLanguage hostLang = m_langDAO.GetLanguageById(usr.Language);
                outXml.Append("<language>" + hostLang.LanguageFolder + "</language>");
                outXml.Append("<languageid>" + hostLang.Id + "</languageid>");

                vrmUserRoles usrRole = m_IUserRolesDao.GetById(usr.roleID);
                outXml.Append("<crossaccess>" + usrRole.crossaccess.ToString() + "</crossaccess>");
                outXml.Append("<level>" + usrRole.level.ToString() + "</level>");
                outXml.Append("<rolename>" + usrRole.roleName + "</rolename>");
                outXml.Append("<createType>" + usrRole.createType + "</createType>"); // ZD 101388

                outXml.Append("<timeFormat>" + usr.TimeFormat + "</timeFormat>");
                outXml.Append("<timeZoneDisplay>" + usr.Timezonedisplay + "</timeZoneDisplay>");
                outXml.Append("<organizationID>" + organizationID.ToString() + "</organizationID>");
                outXml.Append("<defaultConfTemplate>" + usr.DefaultTemplate.ToString() + "</defaultConfTemplate>");
                outXml.Append("<userWorkNo>" + usr.WorkPhone + "</userWorkNo>");
                outXml.Append("<userCellNo>" + usr.CellPhone + "</userCellNo>");
                outXml.Append("<tickerStatus>" + usr.TickerStatus.ToString() + "</tickerStatus>");
                outXml.Append("<tickerSpeed>" + usr.TickerSpeed.ToString() + "</tickerSpeed>");
                outXml.Append("<tickerPosition>" + usr.TickerPosition.ToString() + "</tickerPosition>");
                outXml.Append("<tickerDisplay>" + usr.TickerDisplay.ToString() + "</tickerDisplay>");
                outXml.Append("<tickerBackground>" + usr.TickerBackground + "</tickerBackground>");
                outXml.Append("<rssFeedLink>" + usr.RSSFeedLink + "</rssFeedLink>");
                outXml.Append("<tickerStatus1>" + usr.TickerStatus1.ToString() + "</tickerStatus1>");
                outXml.Append("<tickerSpeed1>" + usr.TickerSpeed1.ToString() + "</tickerSpeed1>");
                outXml.Append("<tickerPosition1>" + usr.TickerPosition1.ToString() + "</tickerPosition1>");
                outXml.Append("<tickerDisplay1>" + usr.TickerDisplay1.ToString() + "</tickerDisplay1>");
                outXml.Append("<tickerBackground1>" + usr.TickerBackground1 + "</tickerBackground1>");
                outXml.Append("<rssFeedLink1>" + usr.RSSFeedLink1 + "</rssFeedLink1>");

                OrganizationFactory orgFactory = new OrganizationFactory(ref obj);
                orgFactory.AppendOrgDetails(ref outXml, organizationID);

                vrmSystemFactory m_systemFactory = new vrmSystemFactory(ref obj);

                m_systemFactory.AppendEmailDetails(ref outXml);

                string outStrXML = "";
                FetchLotusInfo(userID, obj, ref outStrXML);
                outXml.Append(outStrXML);

                sysTechData sysTechDta = m_ISysTechDAO.GetTechByOrgId(organizationID);
                if (sysTechDta != null)
                {
                    outXml.Append("<contactDetails>");
                    outXml.Append("<name>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.name) + "</name>"); //ZD 104391
                    outXml.Append("<email>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.email) + "</email>"); //ZD 104391
                    outXml.Append("<phone>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.phone) + "</phone>");//ZD 104391
                    outXml.Append("<additionInfo>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.info) + "</additionInfo>");//ZD 104391
                    outXml.Append("<feedback>" + sysTechDta.feedback + "</feedback>");
                    outXml.Append("</contactDetails>");
                }

                if (usr.BridgeID > 0)
                {
                    vrmMCU mcu = new vrmMCU();
                    mcu = m_ImcuDao.GetById(usr.BridgeID);

                    List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                    foreach (vrmMCUVendor vendor in vendorList)
                    {
                        if (vendor.id == mcu.BridgeTypeId)
                        {
                            outXml.Append("<primaryBridge>" + vendor.BridgeInterfaceId + "</primaryBridge>");
                            break;
                        }
                    }
                }
                else
                    outXml.Append("<primaryBridge/>");
                //ZD 100263 Starts 
                outXml.Append("<enEmailID>" + crypto.encrypt(usr.Email) + "</enEmailID>");
                outXml.Append("<enUserPWD>" + usr.Password + "</enUserPWD>");
                if (macID != "")
                    outXml.Append("<enMacID>" + crypto.encrypt(macID) + "</enMacID>");
                else
                    outXml.Append("<enMacID></enMacID>");
                //ZD 100263 Ends
                //ZD 103496 Starts
                string UserDeptList = "";
                List<int> DeptListIds = new List<int>();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userId", userID));
                List<vrmUserDepartment> deptList = m_IuserDeptDAO.GetByCriteria(criterionList);
                if (deptList != null && deptList.Count > 0)
                {
                    DeptListIds = deptList.Select(tempDeptUsers => tempDeptUsers.departmentId).ToList();
                    UserDeptList = string.Join(",", DeptListIds);
                }
                outXml.Append("<UserDeptList>" + UserDeptList + "</UserDeptList>");
                outXml.Append("<UserPreferedRoom>" + usr.PreferedRoom + "</UserPreferedRoom>");
                //ZD 103496 End
                outXml.Append("</globalInfo>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchGlobalInfo" + ex.Message);
            }
        }

        #endregion

        #region GetSettingsSelect
        /// <summary>
        /// GetSettingsSelect
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSettingsSelect(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userID = 0;
                myVRMException myVRMEx = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                outXml.Append("<user>");

                Conference confFact = new Conference(ref obj);
                confFact.FetchMostConfs(organizationID, userID, ref outXml);

                UserFactory usrFact = new UserFactory(ref obj);
                usrFact.FetchGlobalInfo(organizationID, userID, ref outXml, ref obj);

                outXml.Append("</user>");

                obj.outXml = outXml.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
                return false;
            }
        }

        #endregion
        //FB 2027 - End

        //FB 2027(Bulk Tools) Start

        #region SetBulkUserAddMinutes
        /// <summary>
        /// SetBulkUserAddMinutes (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserAddMinutes(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int minutes = 0;
            int AuditPCCunt = 0; //ZD 101026
            List<ICriterion> criterionList = new List<ICriterion>();
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                vrmUser user = new vrmUser();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/minutes");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out minutes);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //criterionList.Add(Expression.Eq("userId", uid));
                    //node = uid[i];
                    vrmAccount acc = m_IuserAccountDAO.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    acc.TotalTime = acc.TotalTime + minutes;
                    acc.TimeRemaining = acc.TotalTime - acc.UsedMinutes; //ZD 102043
                    m_IuserAccountDAO.Update(acc);
                    obj.outXml = "<success> Operation succesful </success>";

                    SetAuditUser(MODIFYOLD, acc.userId, DateTime.UtcNow, userid, ref AuditPCCunt); //ZD 101026
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserBridge
        /// <summary>
        /// SetBulkUserBridge (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserBridge(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int bridgeID = 0;
            int AuditPCCount = 0;//ZD 101026
            List<ICriterion> criterionList = new List<ICriterion>();
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/bridgeID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out bridgeID);
                else
                {
                    myVRMEx = new myVRMException(531);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //criterionList.Add(Expression.Eq("userId", uid));
                    //node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    user.BridgeID = bridgeID;
                    user.LastModifiedDate = DateTime.UtcNow;
                    m_IuserDao.Update(user);
                    int eptid = user.endpointId;
                    List<ICriterion> criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("endpointid", eptid));
                    List<vrmEndPoint> ept = m_IeptDao.GetByCriteria(criterionList1);
                    for (int e = 0; e < ept.Count; e++)
                    {
                        ept[e].bridgeid = bridgeID;
                        ept[e].Lastmodifieddate = user.LastModifiedDate;
                        m_IeptDao.Update(ept[e]);
                    }
                    obj.outXml = "<success> Operation succesful </success>";

                    //ZD 101026
                    if (m_HardwareFactory != null)
                        m_HardwareFactory.SetAuditEndpoint(false, false, ept);
                    SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);

                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserDelete
        /// <summary>
        /// SetBulkUserDelete (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserDelete(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int errid = 0;
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                vrmUser user = new vrmUser();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //node = uid[i];
                    //user = m_IuserDao.GetByUserId(int.Parse(node.InnerXml.Trim()));
                    user = m_IuserDao.GetByUserId(vuser[i].userid);
                    m_IuserDao.clearFetch();
                    if (!canDeleteUser(user, ref errid))
                    {
                        myVRMException e = new myVRMException(errid);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                        return false;
                    }
                }
                for (int i = 0; i < vuser.Count; i++)
                {
                    //node = uid[i];
                    //user = m_IuserDao.GetByUserId(int.Parse(node.InnerXml.Trim()));
                    user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    m_IuserDao.clearFetch();
                    vrmInactiveUser Inusr = new vrmInactiveUser(user);
                    Inusr.id = user.id;
                    Inusr.Deleted = vrmUserStatus.USER_INACTIVE;
                    m_IuserDao.Delete(user);
                    m_IinactiveUserDao.Save(Inusr);
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserDepartment
        /// <summary>
        /// SetBulkUserDepartment (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserDepartment(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int AuditPCCunt = 0;//ZD 101026
            List<ICriterion> criterionList = null;
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                XmlNodeList deptid = node.SelectNodes(@"/login/departments/departmentID");
                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    if (deptid.Count != 0)
                    {
                        for (int j = 0; j < deptid.Count; j++)
                        {
                            node = deptid[j];
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("departmentId", Convert.ToInt32(deptid[j].InnerText)));
                            criterionList.Add(Expression.Eq("userId", vuser[i].userid));
                            List<vrmUserDepartment> usrdpt = m_IuserDeptDAO.GetByCriteria(criterionList);
                            if (usrdpt.Count == 0)
                            {
                                vrmUserDepartment usrDpt = new vrmUserDepartment();
                                usrDpt.userId = Convert.ToInt32(vuser[i].userid);
                                usrDpt.departmentId = Convert.ToInt32(deptid[j].InnerText);
                                m_IuserDeptDAO.Save(usrDpt);

                                SetAuditUser(MODIFYOLD, usrDpt.userId, DateTime.UtcNow, userid, ref AuditPCCunt); //ZD 101026
                            }
                        }
                    }
                    else
                    {
                        myVRMEx = new myVRMException(533);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                obj.outXml = "<success>Operation successful</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserExpiryDate
        /// <summary>
        /// SetBulkUserExpiryDate (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserExpiryDate(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            string date;
            int AuditPCCount = 0;//ZD 101026
            List<ICriterion> criterionList = new List<ICriterion>();
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                date = xd.SelectSingleNode("//login/expiryDate").InnerText;
                DateTime expiryDate = Convert.ToDateTime(date);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //criterionList.Add(Expression.Eq("userId", uid));
                    //node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    user.accountexpiry = expiryDate;
                    user.LastModifiedDate = DateTime.UtcNow;
                    m_IuserDao.Update(user);
                    SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);//ZD 101026
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserLanguage
        /// <summary>
        /// SetBulkUserLanguage (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserLanguage(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int langid = 0;
            int AuditPCCount = 0;//ZD 101026
            List<ICriterion> criterionList = new List<ICriterion>();
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/languageID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out langid);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //criterionList.Add(Expression.Eq("userId", uid));
                    //node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    user.Language = langid;
                    user.LastModifiedDate = DateTime.UtcNow;
                    m_IuserDao.Update(user);
                    SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);//ZD 101026
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserLock
        /// <summary>
        /// SetBulkUserLock (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserLock(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int AuditPCCount = 0;//ZD 101026
            int UnBlockAllUsers = 0; //ZD 103459
            List<ICriterion> criterionList = new List<ICriterion>();
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            string stmt = ""; //ZD 104171
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                //ZD 103459 start Unblock User
                node = xd.SelectSingleNode("//login/UnBlockAlluser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out UnBlockAllUsers);
                //ZD 103459 End

                if (UnBlockAllUsers == 0)
                {
                    if (userall == 0)
                    {
                        root = xd.DocumentElement;
                        uid = root.SelectNodes(@"/login/users/userID");
                        userids = new List<int>();
                        for (int i = 0; i < uid.Count; i++)
                        {
                            userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                        }
                        criUsrList = new List<ICriterion>();
                        criUsrList.Add(Expression.In("userid", userids));
                        vuser = m_IuserDao.GetByCriteria(criUsrList);
                    }
                    else
                    {
                        criUsrList = new List<ICriterion>();
                        criUsrList.Add(Expression.Eq("companyId", orgid));
                        criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                        vuser = m_IuserDao.GetByCriteria(criUsrList);
                    }
                    m_IuserDao.clearFetch();
                    for (int i = 0; i < vuser.Count; i++)
                    {
                        //criterionList.Add(Expression.Eq("userId", uid));
                        //node = uid[i];
                        vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                        user.lockCntTrns = 1;
                        user.LockStatus = vrmUserStatus.MAN_LOCKED;
                        user.LastModifiedDate = DateTime.UtcNow;
                        m_IuserDao.Update(user);
                        SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);//ZD 101026
                        obj.outXml = "<success> Operation succesful </success>";
                    }
                }
                else //103459 Start
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    criUsrList.Add(Expression.Eq("lockCntTrns", 10));

                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                    m_IuserDao.clearFetch();
                    if (vuser.Count > 1)
                    {
                        myvrmEx = new myVRMException(425);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    else //ZD 104171 start
                    {
                        if (m_rptLayer == null)
                            m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);
                        m_rptLayer.OpenConnection();
                        stmt = "Update Usr_List_D set lockCntTrns = 0 , lockstatus = 0, LastModifiedDate = '" + DateTime.Now + "', LastModifiedUser = '" + userid + "'  where lockstatus > 1  or  lockCntTrns= 1 and UserID <> 11 ";
                        m_rptLayer.ExecuteNonQuery(stmt);
                        m_rptLayer.CloseConnection();
                        obj.outXml = "<success> Operation succesful </success>";
                        //criUsrList = new List<ICriterion>();
                        //criUsrList.Add(Expression.Eq("companyId", orgid));
                        //criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                        //criUsrList.Add(Expression.Eq("lockCntTrns", 1));
                        //vuser = m_IuserDao.GetByCriteria(criUsrList);
                        //m_IuserDao.clearFetch();
                        //for (int i = 0; i < vuser.Count; i++)
                        //{
                        //    vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                        //    user.lockCntTrns = 0;
                        //    user.LockStatus = vrmUserStatus.USER_ACTIVE;
                        //    user.LastModifiedDate = DateTime.UtcNow;
                        //    m_IuserDao.Update(user);
                        //    SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);//ZD 101026
                        //    obj.outXml = "<success> Operation succesful </success>";
                        //}
                    } //ZD 104171 End
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserRole
        /// <summary>
        /// SetBulkUserRole (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserRole(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0, orgid = 0, roleid = 0, level = 0;
            int AuditPCCount = 0;//ZD 101026
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            string menu = "";
            vrmUser user = null;// ZD 101362 Ends
            vrmUserRoles roles = new vrmUserRoles();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<vrmUserRoles> rolelist = null;
            List<ICriterion> criterionList2 = new List<ICriterion>();
            List<vrmUser> usr = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/roleID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roleid);
                else
                {
                    myVRMEx = new myVRMException(477);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
               
                for (int i = 0; i < vuser.Count; i++)
                {
                    //node = uid[i];
                    user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    roles = new vrmUserRoles();
                    roles = m_IUserRolesDao.GetById(roleid);
                    menu = roles.roleMenuMask;
                    level = roles.level;
                    user.roleID = roleid;
                    user.MenuMask = menu;
                    user.UserRolename = roles.roleName; //ZD 103405
                    user.Admin = level;
                    user.LastModifiedDate = DateTime.UtcNow;
                    m_IuserDao.Update(user);
                    SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);//ZD 101026
                    roles.locked = 1;
                    m_IUserRolesDao.Update(roles);
                    criterionList1 = new List<ICriterion>();
                    criterionList1.Add(Expression.Eq("createType", 2));
                    rolelist = m_IUserRolesDao.GetByCriteria(criterionList1);
                    for (int r = 0; r < rolelist.Count; r++)
                    {
                        criterionList2 = new List<ICriterion>();
                        criterionList2.Add(Expression.Eq("roleID", rolelist[r].roleID));
                        usr = m_IuserDao.GetByCriteria(criterionList2);
                        if (usr.Count == 0)
                        {
                            rolelist[r].locked = 0;
                            m_IUserRolesDao.Update(rolelist[r]);
                        }
                    }
                }
                obj.outXml = "<success> Operation succesful </success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBulkUserTimeZone
        /// <summary>
        /// SetBulkUserTimeZone (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool SetBulkUserTimeZone(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = null;
            int userid = 0;
            int orgid = 0;
            int time = 0;
            int AuditPCCount = 0;//ZD 101026
            List<ICriterion> criterionList = new List<ICriterion>();
            // ZD 101362 Starts
            int userall = 0;
            XmlElement root = null;
            XmlNodeList uid = null;
            List<vrmUser> vuser = null;
            List<int> userids = null;
            // ZD 101362 Ends
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/timeZoneID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out time);
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                // ZD 101362 Starts
                List<ICriterion> criUsrList = new List<ICriterion>();
                node = xd.SelectSingleNode("//login/allusers");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userall);

                if (userall == 0)
                {
                    root = xd.DocumentElement;
                    uid = root.SelectNodes(@"/login/users/userID");
                    userids = new List<int>();
                    for (int i = 0; i < uid.Count; i++)
                    {
                        userids.Add(Convert.ToInt16(uid[i].InnerXml.Trim()));
                    }
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.In("userid", userids));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                else
                {
                    criUsrList = new List<ICriterion>();
                    criUsrList.Add(Expression.Eq("companyId", orgid));
                    criUsrList.Add(Expression.Not(Expression.Eq("userid", 11)));
                    vuser = m_IuserDao.GetByCriteria(criUsrList);
                }
                m_IuserDao.clearFetch();
                for (int i = 0; i < vuser.Count; i++)
                {
                    //criterionList.Add(Expression.Eq("userId", uid));
                    //node = uid[i];
                    vrmUser user = m_IuserDao.GetByUserId(vuser[i].userid);// ZD 101362 Ends
                    user.TimeZone = time;
                    user.LastModifiedDate = DateTime.UtcNow;
                    m_IuserDao.Update(user);
                    SetAuditUser(MODIFYOLD, user.userid, user.LastModifiedDate, userid, ref AuditPCCount);//ZD 101026
                    obj.outXml = "<success> Operation succesful </success>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2027(Bulk Tools) Ends

        #region GetOldUser

        public bool GetOldUser(ref vrmDataObject obj)
        {
            try
            {
                string lot = "";
                StringBuilder addr = new StringBuilder();
                string video = "";
                string lang = "";
                string bridge = "";
                string dept = "";
                string blckStatus = "0";
                DateTime blockDate = DateTime.Now;
                int loginID = 0;
                int userID = 0;
                int orgid = 0;
                int err = 0;
                StringBuilder outXml = new StringBuilder();
                StringBuilder PCoutXml = new StringBuilder(); //FB 2693
                XmlDocument xd = new XmlDocument();
                myVRMException myVRMEx = null;
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);

                node = xd.SelectSingleNode("//login/user/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out orgid);

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                int.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = mutliOrgID;
                //FB 2274 Ends

                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> criterionList2 = new List<ICriterion>();
                List<ICriterion> criterionList3 = new List<ICriterion>();
                outXml.Append("<oldUser>");

                vrmUser user = m_IuserDao.GetByUserId(userID);
                if (orgid == 0)
                {
                    orgid = user.companyId;
                }

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                hardwareDAO a_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);
                a_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);

                outXml.Append("<userID>" + user.userid + "</userID>");
                outXml.Append("<userName>");
                outXml.Append("<firstName>" + user.FirstName + "</firstName>");
                outXml.Append("<lastName>" + user.LastName + "</lastName>");
                outXml.Append("</userName>");
                outXml.Append("<userOrgID>" + user.companyId + "</userOrgID>"); //FB 2865
                outXml.Append("<login>" + user.Login + "</login>");
                outXml.Append("<UserDomain>" +user.UserDomain +"</UserDomain>"); //ZD 104850
                if (user.Password != null)
                {
                    //string password = string.Empty; //FB 3054 Starts
                    //if (user.Password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    password = crypto.decrypt(user.Password);
                    //}
                    outXml.Append("<password>" + user.Password + "</password>"); //FB 3054 Ends
                }
                else
                    outXml.Append("<password></password>");
                outXml.Append("<userEmail>" + user.Email + "</userEmail>");
                outXml.Append("<workPhone>" + user.WorkPhone + "</workPhone>");
                outXml.Append("<cellPhone>" + user.CellPhone + "</cellPhone>");
                outXml.Append("<emailClient>" + user.EmailClient + "</emailClient>");
                outXml.Append("<SavedSearch>" + user.searchId + "</SavedSearch>");
                outXml.Append("<timeZone>" + user.TimeZone + "</timeZone>");
                outXml.Append("<dateFormat>" + user.DateFormat + "</dateFormat>");
                outXml.Append("<enableAV>" + user.enableAV + "</enableAV>");
                outXml.Append("<enableParticipants>" + user.enableParticipants + "</enableParticipants>");
                //ZD 101122
                outXml.Append("<enableAVWorkOrder>" + user.EnableAVWO + "</enableAVWorkOrder>");
                outXml.Append("<enableCateringWO>" + user.EnableCateringWO + "</enableCateringWO>");
                outXml.Append("<enableFacilityWO>" + user.EnableFacilityWO + "</enableFacilityWO>");

                outXml.Append("<enableAdditionalOption>" + user.enableAdditionalOption + "</enableAdditionalOption>");//ZD 101093
                outXml.Append("<timeFormat>" + user.TimeFormat + "</timeFormat>");
                outXml.Append("<timezoneDisplay>" + user.Timezonedisplay + "</timezoneDisplay>");
                outXml.Append("<exchangeUser>" + user.enableExchange + "</exchangeUser>");
                outXml.Append("<dominoUser>" + user.enableDomino + "</dominoUser>");
                outXml.Append("<mobileUser>" + user.enableMobile + "</mobileUser>");
                outXml.Append("<PIMNotification>" + user.PluginConfirmations + "</PIMNotification>");//FB 2141
                if (user.TickerSpeed == 0)
                {
                    outXml.Append("<tickerStatus></tickerStatus>");
                    outXml.Append("<tickerSpeed></tickerSpeed>");
                }
                else
                {
                    outXml.Append("<tickerStatus>" + user.TickerStatus + "</tickerStatus>");
                    outXml.Append("<tickerSpeed>" + user.TickerSpeed + "</tickerSpeed>");
                }
                outXml.Append("<tickerPosition>" + user.TickerPosition + "</tickerPosition>");
                outXml.Append("<tickerDisplay>" + user.TickerDisplay + "</tickerDisplay>");
                outXml.Append("<tickerBackground>" + user.TickerBackground + "</tickerBackground>");
                outXml.Append("<rssFeedLink>" + user.RSSFeedLink + "</rssFeedLink>");
                if (user.TickerSpeed1 == 0)
                {
                    outXml.Append("<tickerStatus1></tickerStatus1>");
                    outXml.Append("<tickerSpeed1></tickerSpeed1>");
                }
                else
                {
                    outXml.Append("<tickerStatus1>" + user.TickerStatus1 + "</tickerStatus1>");
                    outXml.Append("<tickerSpeed1>" + user.TickerSpeed1 + "</tickerSpeed1>");
                }
                outXml.Append("<tickerPosition1>" + user.TickerPosition1 + "</tickerPosition1>");
                outXml.Append("<tickerDisplay1>" + user.TickerDisplay1 + "</tickerDisplay1>");
                outXml.Append("<tickerBackground1>" + user.TickerBackground1 + "</tickerBackground1>");
                outXml.Append("<rssFeedLink1>" + user.RSSFeedLink1 + "</rssFeedLink1>");
                if (user.Audioaddon == null)//FB 2027
                    user.Audioaddon = "0";
                outXml.Append("<Audioaddon>" + user.Audioaddon + "</Audioaddon>");

                List<ICriterion> criterionEmail = new List<ICriterion>();
                criterionEmail.Add(Expression.Eq("EmailLangId", user.EmailLangId));
                List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterionEmail);
                if (emailLangs.Count != 0)
                {
                    for (int e = 0; e < emailLangs.Count; e++)
                    {
                        outXml.Append("<EmailLang>" + emailLangs[e].EmailLangId + "</EmailLang>");
                        outXml.Append("<EmailLangName>" + emailLangs[e].EmailLanguage + "</EmailLangName>");
                    }
                }
                else
                {
                    outXml.Append("<EmailLang></EmailLang>");
                    outXml.Append("<EmailLangName></EmailLangName>");
                }

                outXml.Append("<systemExpiryDate>" + sysSettings.ExpiryDate.ToString("d") + "</systemExpiryDate>");
                StringBuilder tzOutXml = new StringBuilder();
                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);
                m_OrgFactory.organizationID = orgid;
                m_OrgFactory.timeZonesToXML(ref tzOutXml);
                outXml.Append(tzOutXml);

                outXml.Append("<locationList>");
                outXml.Append("<selected>");
                outXml.Append("<level1ID>" + user.PreferedRoom + "</level1ID>");//ZD 102052
                outXml.Append("</selected>");
                outXml.Append("</locationList>");

                criterionList = new List<ICriterion>();
                if (user.Admin != 2)//ZD 101883
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", user.userid)));
                }
                criterionList.Add(Expression.Eq("GroupID", user.PreferedGroup));
                criterionList.Add(Expression.Eq("orgId", orgid));
                List<vrmGroupDetails> Groups = m_IGrpDetailDao.GetByCriteria(criterionList);
                foreach (vrmGroupDetails gd in Groups)
                {
                    outXml.Append("<defaultAGroups>");
                    outXml.Append("<groupID>" + gd.GroupID + "</groupID>");
                    outXml.Append("</defaultAGroups>");
                }
                criterionList.Add(Expression.Eq("GroupID", user.CCGroup));
                List<vrmGroupDetails> ccGroups = m_IGrpDetailDao.GetByCriteria(criterionList);

                foreach (vrmGroupDetails gc in ccGroups)
                {
                    outXml.Append("<defaultCGroups>");
                    outXml.Append("<groupID>" + gc.GroupID + "</groupID>");
                    outXml.Append("</defaultCGroups>");
                }

                criterionList2.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userID)));
                criterionList2.Add(Expression.Eq("orgId", orgid));
                outXml.Append("<groups>");
                FetchGroups(criterionList2, ref outXml, 0, "group");
                outXml.Append("</groups>");

                outXml.Append("<alternativeEmail>" + user.AlternativeEmail + "</alternativeEmail>");
                outXml.Append("<sendBoth>" + user.DoubleEmail + "</sendBoth>");

                outXml.Append("<lineRateID>" + user.DefLineRate + "</lineRateID>");

                List<vrmLineRate> LineRate = vrmGen.getLineRate();
                outXml.Append("<lineRate>");

                foreach (vrmLineRate lr in LineRate)
                {
                    outXml.Append("<rate>");
                    outXml.Append("<lineRateID>" + lr.Id + "</lineRateID>");
                    outXml.Append("<lineRateName>" + lr.LineRateType + "</lineRateName>");
                    outXml.Append("</rate>");
                }
                outXml.Append("</lineRate>");

                IEptDao a_IeptDao;
                a_IeptDao = a_hardwareDAO.GetEptDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("endpointid", user.endpointId));
                List<vrmEndPoint> epList = a_IeptDao.GetByCriteria(criterionList);

                vrmEndPoint ep = new vrmEndPoint();
                if (epList.Count > 0)
                    ep = epList[0];

                outXml.Append("<videoProtocol>" + ep.protocol + "</videoProtocol>");
                outXml.Append("<conferenceCode>" + ep.ConferenceCode + "</conferenceCode>");
                outXml.Append("<leaderPin>" + ep.LeaderPin + "</leaderPin>");
                outXml.Append("<participantCode>" + user.ParticipantCode + "</participantCode>"); //ZD 101446
                outXml.Append("<IPISDNAddress>" + ep.address + "</IPISDNAddress>");
                outXml.Append("<connectionType>" + ep.connectiontype + "</connectionType>");
                outXml.Append("<isOutside>" + ep.outsidenetwork + "</isOutside>");
                outXml.Append("<emailMask>" + user.emailmask + "</emailMask>");
                outXml.Append("<addressTypeID>" + ep.addresstype + "</addressTypeID>");
                outXml.Append("<ExchangeID>" + ep.ExchangeID + "</ExchangeID>");
                outXml.Append("<APIPortNo>" + ep.ApiPortNo + "</APIPortNo>");
                outXml.Append("<URL>" + ep.endptURL + "</URL>");
                outXml.Append("<videoEquipmentID>" + ep.videoequipmentid + "</videoEquipmentID>");
                outXml.Append("<IsUserAudio>" + ep.IsUserAudio + "</IsUserAudio>"); //ALLDEV-814

                if (FetchVideoEquipment2(user.DefaultEquipmentId.ToString(), userID, 0, obj, ref video))
                    outXml.Append(video);

                outXml.Append("<multiDepartment>" + multiDepts + "</multiDepartment>");

                if (FetchDepartmentList(userID, obj, 1, err, orgid, ref dept))
                    outXml.Append(dept);

                criterionList3.Add(Expression.Eq("userId", user.userid));
                List<vrmAccount> uAccount = m_IuserAccountDAO.GetByCriteria(criterionList3);
                if (uAccount.Count != 0)
                {
                    //ZD 102043 starts                    
                    for (int i = 0; i < uAccount.Count; i++)
                    {
                        outXml.Append("<initialTime>" + uAccount[i].TotalTime + "</initialTime>");
                        outXml.Append("<timeRemaining>" + (uAccount[i].TotalTime - uAccount[i].UsedMinutes) + "</timeRemaining>");
                    }
                    //ZD 102043 End
                }
                else
                {
                    outXml.Append("<initialTime></initialTime>");
                }
                outXml.Append("<expiryDate>" + user.accountexpiry.ToString("d") + "</expiryDate>");
                outXml.Append("<bridgeID>" + user.BridgeID + "</bridgeID>");
                outXml.Append("<EndpointID>" + user.endpointId + "</EndpointID>");
                outXml.Append("<status>");
                outXml.Append("<level>" + user.Admin + "</level>");
                outXml.Append("<deleted>0</deleted>");

                string locked = "0";
                if (user.lockCntTrns > 0)
                    locked = "1";

                outXml.Append("<locked>" + locked + "</locked>");
                outXml.Append("</status>");
                outXml.Append("<creditCard></creditCard>");

                outXml.Append("<languageID>" + user.Language + "</languageID>");

                if (OldFetchLanguageList(obj, ref lang))
                    outXml.Append(lang);

                outXml.Append("<roleID>" + user.roleID + "</roleID>");
                IUserRolesDao a_IUserRolesDao = m_userDAO.GetUserRolesDao();
                List<vrmUserRoles> uRoleList = a_IUserRolesDao.GetAll();

                outXml.Append("<roles>");
                foreach (vrmUserRoles uRole in uRoleList)
                {
                    outXml.Append("<role>");
                    outXml.Append("<ID>" + uRole.roleID + "</ID>");
                    outXml.Append("<name>" + uRole.roleName + "</name>");
                    outXml.Append("<menuMask>" + uRole.roleMenuMask + "</menuMask>");
                    outXml.Append("<locked>" + uRole.locked + "</locked>");
                    outXml.Append("<level>" + uRole.level + "</level>");
                    IUserTemplateDao a_userTemplateDAO = m_userDAO.GetUserTemplateDao();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("roleId", user.roleID));
                    List<vrmUserTemplate> uTempList = a_userTemplateDAO.GetByCriteria(criterionList);
                    if (uTempList.Count > 0)
                        outXml.Append("<active>1</active>");
                    else
                        outXml.Append("<active>0</active>");
                    outXml.Append("<createType>" + uRole.createType + "</createType>");
                    outXml.Append("</role>");
                }
                outXml.Append("</roles>");

                if (FetchLotusInfo(userID, obj, ref lot))
                    outXml.Append(lot);

                if (OldFetchBridgeList(orgid, obj, ref bridge))
                    outXml.Append(bridge);

                if (FetchAddressTypeList(obj, ref addr))
                    outXml.Append(addr);

                if (user.MailBlocked == 1)
                {
                    blckStatus = "1";
                    blockDate = user.MailBlockedDate;
                    timeZone.GMTToUserPreferedTime(user.TimeZone, ref blockDate);
                }
                //FB 2227 Start
                outXml.Append("<IntVideoNum>" + user.InternalVideoNumber + "</IntVideoNum>");
                outXml.Append("<ExtVideoNum>" + user.ExternalVideoNumber + "</ExtVideoNum>");
                //FB 2227 End
                outXml.Append("<HelpRequsetorEmail></HelpRequsetorEmail>"); //FB 2268//ZD 100322
                outXml.Append("<HelpRequsetorPhone></HelpRequsetorPhone>"); //FB 2268//ZD 100322
                outXml.Append("<RFIDValue>" + user.RFIDValue + "</RFIDValue>"); //FB 2724
                outXml.Append("<SendSurveyEmail>" + user.SendSurveyEmail + "</SendSurveyEmail>"); //FB 2348
                outXml.Append("<EnableVNOCselection>" + user.EnableVNOCselection + "</EnableVNOCselection>"); //FB 2608
                outXml.Append("<PrivateVMR>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(user.PrivateVMR) + "</PrivateVMR>"); //FB 2481  //FB 2721
                //FB 2262 //FB 2599 code added starts
                outXml.Append("<VidyoURL>" + user.VidyoURL + "</VidyoURL>");
                outXml.Append("<Extension>" + user.Extension + "</Extension>");
                outXml.Append("<Pin>" + user.Pin + "</Pin>");
                //FB 2262 //FB 2599 code added ends
                //FB 2392-Whygo Starts
                outXml.Append("<ParentReseller>" + user.ParentReseller + "</ParentReseller>");
                outXml.Append("<CorpUser>" + user.CorpUser + "</CorpUser>");
                outXml.Append("<Region>" + user.Region + "</Region>");
                outXml.Append("<ESUserID>" + user.ESUserID + "</ESUserID>");
                //FB 2392-Whygo End
                outXml.Append("<BrokerRoomNum>" + user.BrokerRoomNum + "</BrokerRoomNum>"); //FB 3001
                //outXml.Append("<Secured>" + user.Secured + "</Secured>"); //FB 2595 
                outXml.Append("<login><emailBlockStatus>" + blckStatus + "</emailBlockStatus><emailBlockDate>" + blockDate + "</emailBlockDate></login>");
                //FB 2655 - DTMF Start 
                outXml.Append("<DTMF>");
                outXml.Append("<PreConfCode>" + user.PreConfCode + "</PreConfCode>");
                outXml.Append("<PreLeaderPin>" + user.PreLeaderPin + "</PreLeaderPin>");
                outXml.Append("<PostLeaderPin>" + user.PostLeaderPin + "</PostLeaderPin>");
                outXml.Append("<AudioDialInPrefix>" + user.AudioDialInPrefix + "</AudioDialInPrefix>");
                outXml.Append("</DTMF>");
                //FB 2655 - DTMF End
                //FB 2693 Starts
                outXml.Append("<EnablePCUser>" + user.enablePCUser + "</EnablePCUser>");
                //ZD 100168 inncrewin 23/12/2013
                outXml.Append("<IsStaticIDEnabled>" + user.IsStaticIDEnabled + "</IsStaticIDEnabled>");
                outXml.Append("<StaticID>" + user.StaticID + "</StaticID>");
                // ZD 100172 Starts
                outXml.Append("<MenuMask>" + user.MenuMask + "</MenuMask>");
                outXml.Append("<LandingPageTitle>" + user.LandingPageTitle + "</LandingPageTitle>");
                outXml.Append("<LandingPageLink>" + user.LandingPageLink + "</LandingPageLink>");
                // ZD 100172 Ends
                if (user.enablePCUser > 0)
                {
                    GetUserPCDetails(user.userid, 0, ref PCoutXml);
                    outXml.Append(PCoutXml);
                }
                //FB 2693 Ends
                //ZD 100221 Starts
                outXml.Append("<EnableWebexConf>" + user.enableWebexUser + "</EnableWebexConf>");
                outXml.Append("<WebexUserName>" + user.WebexUserName + "</WebexUserName>");
                outXml.Append("<WebexPassword>" + user.WebexPassword + "</WebexPassword>");
                //ZD 100221 End
                //ZD 104021 Start
                outXml.Append("<EnableBJNConf>" + user.EnableBJNConf + "</EnableBJNConf>");
                outXml.Append("<BJNUserName>" + user.BJNUserEmail + "</BJNUserName>");
				//ZD 104021 End
                //ZD 100664 Starts
                StringBuilder auditOutXML = null;
                vrmUser LoginUser = m_IuserDao.GetByUserId(loginID);
                outXml.Append("<LastModifiedDetails>");
                if (GetAuditUser(user.userid, LoginUser.TimeZone, ref auditOutXML) && !string.IsNullOrEmpty(auditOutXML.ToString()))
                    outXml.Append(auditOutXML);
                outXml.Append("</LastModifiedDetails>");
                //ZD 100664 End

                outXml.Append("</oldUser>");
                obj.outXml = outXml.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region OldFetchBridgeList
        /// <summary>
        /// OldFetchBridgeList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchBridgeList(int orgid, vrmDataObject obj, ref string bridgeout)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterion = new List<ICriterion>();
            try
            {
                outXml.Append("<bridges>");
                criterion.Add(Expression.Eq("VirtualBridge", 0));
                criterion.Add(Expression.Eq("deleted", 0));
                criterion.Add(Expression.Eq("orgId", orgid));
                List<vrmMCU> bridges = m_ImcuDao.GetByCriteria(criterion);

                for (int i = 0; i < bridges.Count; i++)
                {
                    outXml.Append("<bridge>");
                    outXml.Append("<ID>" + bridges[i].BridgeID.ToString() + "</ID>");
                    outXml.Append("<name>" + bridges[i].BridgeName + "</name>");
                    outXml.Append("</bridge>");
                }
                outXml.Append("</bridges>");
                obj.outXml = outXml.ToString();
                bridgeout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region FetchDepartmentList
        /// <summary>
        /// FetchDepartmentList (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchDepartmentList(int userid, vrmDataObject obj, int mode, int err, int orgid, ref string deptout)
        {
            string admin = "";
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterion = new List<ICriterion>();
            List<ICriterion> criterion2 = new List<ICriterion>();
            List<ICriterion> criterion3 = new List<ICriterion>();
            try
            {
                outXml.Append("<departments>");
                if (userid != 0)
                {
                    vrmUser user = m_IuserDao.GetByUserId(userid);
                    admin = Convert.ToString(user.Admin);
                    if (mode == 1)
                    {
                        criterion.Add(Expression.Eq("userId", userid));
                        List<vrmUserDepartment> deptlist = m_IuserDeptDAO.GetByCriteria(criterion);
                        for (int i = 0; i < deptlist.Count; i++)
                        {
                            outXml.Append("<selected>" + deptlist[i].departmentId.ToString() + "</selected>");
                        }
                        criterion2.Add(Expression.Eq("deleted", 0));
                        criterion2.Add(Expression.Eq("orgId", Convert.ToString(orgid)));
                        List<vrmDept> depart = m_IdeptDAO.GetByCriteria(criterion2);
                        for (int j = 0; j < depart.Count; j++)
                        {
                            outXml.Append("<department>");
                            outXml.Append("<id>" + depart[j].departmentId.ToString() + "</id>");
                            outXml.Append("<name>" + depart[j].departmentName + "</name>");
                            outXml.Append("<securityKey>" + depart[j].securityKey + "</securityKey>");
                            outXml.Append("</department>");
                        }
                    }
                    else if (mode == 0)
                    {
                        if (admin == "2")
                        {
                            List<vrmDept> depart = m_IdeptDAO.GetByCriteria(criterion2);
                            for (int j = 0; j < depart.Count; j++)
                            {
                                outXml.Append("<department>");
                                outXml.Append("<id>" + depart[j].departmentId.ToString() + "</id>");
                                outXml.Append("<name>" + depart[j].departmentName + "</name>");
                                outXml.Append("<securityKey>" + depart[j].securityKey + "</securityKey>");
                                outXml.Append("</department>");
                            }
                        }
                        else
                        {
                            vrmUserDepartment dept = m_IuserDeptDAO.GetById(userid); //FB 2027
                            criterion3.Add(Expression.Eq("departmentId", dept.departmentId));
                            List<vrmDept> dept1 = m_IdeptDAO.GetByCriteria(criterion3);
                            for (int k = 0; k < dept1.Count; k++)
                            {
                                outXml.Append("<department>");
                                outXml.Append("<id>" + dept1[k].departmentId.ToString() + "</id>");
                                outXml.Append("<name>" + dept1[k].departmentName + "</name>");
                                outXml.Append("<securityKey>" + dept1[k].securityKey + "</securityKey>");
                                outXml.Append("</department>");
                            }
                        }
                    }
                }
                outXml.Append("</departments>");
                obj.outXml = outXml.ToString();
                deptout = outXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2027 SetUser Start

        #region SetUser
        /// <summary>
        /// SetUser
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUser(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false;
                int loginID = 0, userid = 0, deleted = 0, roleID = 0, initialTime = 0, err = 0, IsStaticIDEnabled = 0;//ZD 100969
                string userID = "", lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", outputXML = "";
                string firstname = "", lastname = "", password = "", email = "", altemail = "", login = "", StaticID = "";//ZD 100969
                string OldStaticId = "";//ZD 100890
                int AuditPCCount = 0;//ZD 101026
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();

                //ZD 100890 Start
                if (userID != "new")
                {
                    int usr = 0;
                    int.TryParse(userID, out usr);
                    vuser = m_IuserDao.GetByUserId(usr);
                    OldStaticId = vuser.StaticID;
                }
                //ZD 100890 End

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }

                //ZD 100456
                string editFrom = "";
                node = xd.SelectSingleNode("//saveUser/EditFrom");
                if (node != null)
                    editFrom = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/audioaddon"); //FB 2023
                if (node != null)
                    if (node.InnerText.Trim() != "")
                        int.TryParse(node.InnerText.Trim(), out isAudioAddOn);

                //ZD 100969 
                node = xd.SelectSingleNode("//saveUser/IsStaticIDEnabled");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out IsStaticIDEnabled);
                node = xd.SelectSingleNode("//saveUser/StaticID");
                if (node != null)
                    StaticID = node.InnerText.Trim();
                //ZD 100969 
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID); //ZD 104862 

                if (userID == "new")
                {
                    vrmUser requestor = null;
                    if (orgInfo.EnableBlockUserDI == 1 && (editFrom == "D" || editFrom == "ND")) //ZD 104862 start
                    {
                        string UsrOutput = CheckUser(userid, firstname, lastname, email, altemail, login, password, 1, ref err, loginID, obj, IsStaticIDEnabled, StaticID, vuser.UserDomain);
                        if (err < 0)
                        {
                            requestor = m_IuserDao.GetByUserEmail(email);                            
                            mode = MODIFYOLD;
                            userid = requestor.userid;
                        }
                        else
                        {
                            mode = CREATENEW;
                        }
                    }
                    else
                        mode = CREATENEW;
                }
                else
                {
                    mode = MODIFYOLD;
                    int.TryParse(userID, out userid);
                }
                
                //ZD 104862 End 

                if (mode == CREATENEW)
                {
                    try
                    {
                        if (userDeleted == false)
                        {
                            vrmUser user = new vrmUser();
                            if (!ActiveUser(ref obj, ref user, err, ref AuditPCCount))//ZD 101026
                            {
                                outputXML = obj.outXml;
                                return false;
                            }
                            else
                            {
                                userid = user.userid;
                            }
                        }
                        else
                        {
                            vrmInactiveUser Inuser = new vrmInactiveUser();
                            if (!InActiveUser(ref obj, ref Inuser, err, ref AuditPCCount))//ZD 101026
                            {
                                outputXML = obj.outXml;
                                return false;
                            }
                            else
                            {
                                userid = Inuser.userid;
                            }
                        }

                        List<ICriterion> criterionLot = new List<ICriterion>();
                        criterionLot.Add(Expression.Eq("userid", userid));
                        List<vrmUserLotusNotesPrefs> usrlot = m_userlotusDAO.GetByCriteria(criterionLot);
                        foreach (vrmUserLotusNotesPrefs usrlt in usrlot)
                            if (usrlt != null)
                                m_userlotusDAO.Delete(usrlt);
                        if (lotus != "")
                        {
                            vrmUserLotusNotesPrefs usrlotus = new vrmUserLotusNotesPrefs();
                            usrlotus.userid = userid;
                            usrlotus.noteslogin = lnLoginName;
                            usrlotus.notespwd = lnLoginPwd;
                            usrlotus.notespath = lnDBPath;
                            m_userlotusDAO.Save(usrlotus);
                        }
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    try
                    {
                        List<ICriterion> criterionList6 = new List<ICriterion>();
                        int insertTime = 0;
                        if (initialTime == 0)
                        {
                            insertTime = TOTALTIMEFORNEWUSER;
                        }
                        else
                        {
                            insertTime = initialTime;
                        }
                        vrmAccount usracc = new vrmAccount();
                        usracc.userId = userid;
                        usracc.TotalTime = insertTime;
                        usracc.TimeRemaining = insertTime;
                        usracc.UsedMinutes = 0; //ZD 102043
                        usracc.ScheduledMinutes = 0; //ZD 102043
                        usracc.InitTime = DateTime.Now;
                        usracc.ExpirationTime = DateTime.Today.AddYears(10);
                        m_IuserAccountDAO.Save(usracc);
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    List<ICriterion> criterionList7 = new List<ICriterion>();
                    criterionList7.Add(Expression.Eq("roleID", roleID));
                    List<vrmUserRoles> usrrole = m_IUserRolesDao.GetByCriteria(criterionList7);
                    for (int i = 0; i < usrrole.Count; i++)
                    {
                        usrrole[i].locked = 1;
                        m_IUserRolesDao.Update(usrrole[i]);
                    }
                }
                else
                {
                    //ZD 101525
                    string enableSSO = "0";
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", 11));
                    List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList, true);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                        enableSSO = ldapSettingsList[0].EnableSSOMode.ToString();

                    //Config config = new Config(m_configPath);
                    //bool ret = config.Initialize();
                    //if (!config.ldapEnabled)
                    if (enableSSO == "0")
                    {
                        string output = "";
                        if (editFrom == "") //ZD 104862
                        {
                            vuser = m_IuserDao.GetByUserId(userid);//ZD 103784
                            output = CheckUser(userid, firstname, lastname, email, altemail, login, password, 1, ref err, loginID, obj, IsStaticIDEnabled, StaticID, vuser.UserDomain); //ZD 100969 //ZD 103784
                            if (err < 0)
                            {
                                obj.outXml = output;
                                return false;
                            }
                            //ALLDEV-498
                            if (orgInfo.ConfAdministrator.Trim() == userid.ToString())
                            {
                                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                                Int32 admin = userrole.level;
                                if (vuser.Admin > 1 && admin <= 1)
                                {
                                    myVRMException e = new myVRMException(768);
                                    m_log.Error(e);
                                    obj.outXml = e.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                    }
                    try
                    {
                        vrmInactiveUser LdInusr = m_IinactiveUserDao.GetByUserId(userid);
                        int numrows = 0, oroleID = 0;
                        vrmUser Ldusr = m_IuserDao.GetByUserId(userid);
                        oroleID = Ldusr.roleID;
                        List<ICriterion> criterionList9 = new List<ICriterion>();
                        criterionList9.Add(Expression.Eq("roleID", oroleID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList9);
                        numrows = usrlist.Count;
                        if (numrows == 1)
                        {
                            //FB 2164 Start
                            criterionList9.Add(Expression.Eq("roleID", oroleID));
                            List<vrmInactiveUser> Inusrlist = m_IinactiveUserDao.GetByCriteria(criterionList9);
                            if (Inusrlist.Count == 0)
                            {
                                List<ICriterion> critrole = new List<ICriterion>();
                                critrole.Add(Expression.Eq("roleID", oroleID));
                                critrole.Add(Expression.Eq("createType", 2));
                                List<vrmUserRoles> usrrle = m_IUserRolesDao.GetByCriteria(critrole);
                                for (int j = 0; j < usrrle.Count; j++)
                                {
                                    usrrle[j].locked = 0;
                                    m_IUserRolesDao.Update(usrrle[j]);
                                }
                            }
                            //FB 2164 End
                        }
                        if (userDeleted == false)
                        {
                            if (!LDAPActiveUser(ref obj, userid, ref AuditPCCount))//ZD 101026
                                return false;
                        }
                        else
                        {
                            if (!LDAPInActiveUser(ref obj, userid, ref AuditPCCount))//ZD 101026
                                return false;
                        }
                        if (userDeleted == false)
                        {
                            List<ICriterion> critrol = new List<ICriterion>();
                            critrol.Add(Expression.Eq("roleID", roleID));
                            List<vrmUserRoles> usrle = m_IUserRolesDao.GetByCriteria(critrol);
                            for (int k = 0; k < usrle.Count; k++)
                            {
                                usrle[k].locked = 1;
                                m_IUserRolesDao.Update(usrle[k]);
                            }
                        }
                        List<ICriterion> criterionLot = new List<ICriterion>();
                        criterionLot.Add(Expression.Eq("userid", userid));
                        List<vrmUserLotusNotesPrefs> usrlot = m_userlotusDAO.GetByCriteria(criterionLot);
                        foreach (vrmUserLotusNotesPrefs usrlt in usrlot)
                            if (usrlt != null)
                                m_userlotusDAO.Delete(usrlt);
                        if (lotus != "")
                        {
                            vrmUserLotusNotesPrefs usrlotus = new vrmUserLotusNotesPrefs();
                            usrlotus.userid = userid;
                            usrlotus.noteslogin = lnLoginName;
                            usrlotus.notespwd = lnLoginPwd;
                            usrlotus.notespath = lnDBPath;
                            m_userlotusDAO.Save(usrlotus);
                        }

                        if (editFrom == "" || editFrom == "ND") //ZD 104862
                        {
                            if (userDeleted == false)
                            {
                                List<ICriterion> criterionList6 = new List<ICriterion>();
                                criterionList6.Add(Expression.Eq("userId", userid));
                                List<vrmAccount> uAccount = m_IuserAccountDAO.GetByCriteria(criterionList6);
                                for (int c = 0; c < uAccount.Count; c++)
                                {
                                    //ZD 100263 Starts //NATOFIX
                                    vrmUser LoginUser = m_IuserDao.GetByUserId(loginID);
                                    vrmUserRoles LoginUserRole = m_IUserRolesDao.GetById(LoginUser.roleID);//ZD 100263
                                    if (!CheckUserRights(LoginUser, Ldusr))
                                    {
                                        if (LoginUser.userid != Ldusr.userid)
                                        {
                                            myvrmEx = new myVRMException(229);
                                            obj.outXml = myvrmEx.FetchErrorMsg();
                                            return false;
                                        }
                                    }
                                    else if (LoginUser.userid != Ldusr.userid)
                                    {
                                        //ZD 102043 Starts
                                        //totalTime = uAccount[c].TotalTime;
                                        //if (initialTime > totalTime)
                                        //    totalTime = initialTime;

                                        uAccount[c].TotalTime = initialTime;
                                        //uAccount[c].TimeRemaining = initialTime;
                                        uAccount[c].TimeRemaining = initialTime - uAccount[c].UsedMinutes;
                                        m_IuserAccountDAO.Update(uAccount[c]);
                                        //ZD 102043 End
                                    }
                                    //ZD 100263 Ends
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    bool userExists = false;
                    try
                    {

                        List<ICriterion> criterionL = new List<ICriterion>();
                        criterionL.Add(Expression.Eq("userId", userid));
                        List<vrmAccount> usracc1 = m_IuserAccountDAO.GetByCriteria(criterionL);
                        if (usracc1.Count != 0)
                            userExists = true;
                    }
                    catch (Exception e)
                    {
                        m_log.Error("sytemException", e);
                        throw e;
                    }
                    if (!userExists)
                    {
                        vrmAccount usracc1 = m_IuserAccountDAO.GetByUserId(userid);
                        usracc1.userId = userid;
                        usracc1.TotalTime = TOTALTIMEFORNEWUSER;
                        usracc1.TimeRemaining = TOTALTIMEFORNEWUSER;
                        usracc1.UsedMinutes = 0; //ZD 102043
                        usracc1.ScheduledMinutes = 0; //ZD 102043
                        usracc1.InitTime = DateTime.Now;
                        usracc1.ExpirationTime = DateTime.Today.AddYears(10);
                        m_IuserAccountDAO.Save(usracc1);
                    }
                }
                List<ICriterion> criterionD = new List<ICriterion>();
                criterionD.Add(Expression.Eq("userId", userid));
                List<vrmUserDepartment> depart = m_IuserDeptDAO.GetByCriteria(criterionD);
                for (int d = 0; d < depart.Count; d++)
                {
                    m_IuserDeptDAO.Delete(depart[d]);
                }
                XmlNodeList deplist = xd.SelectNodes("//saveUser/user/departments/id");

                if (deplist.Count > 0)
                {
                    for (int i = 0; i < deplist.Count; i++)
                    {
                        int id = 0;
                        int.TryParse(deplist[i].InnerText.Trim(), out id);
                        vrmUserDepartment usrdept = new vrmUserDepartment();
                        usrdept.userId = userid;
                        usrdept.departmentId = id;
                        m_IuserDeptDAO.Save(usrdept);
                    }
                }
                outputXML = "<SetUser><userID>" + userid + "</userID><organizationID>" + organizationID + "</organizationID><OldStaticId>" + OldStaticId + "</OldStaticId></SetUser>"; //ZD 100890
                //FB 2158 start
                if (mode == CREATENEW)
                    WelcomeNewUser(ref obj);
                //FB 2158 end
                //ZD 100664
                DateTime LastModifiedDate = DateTime.UtcNow;

                SetAuditUser(mode, userid, LastModifiedDate, loginID, ref AuditPCCount);

                obj.outXml = outputXML;
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }


        #endregion

        #region FetchMaxUserID
        public bool FetchMaxUserID(int err, string errmsg, vrmDataObject obj, ref int maxuserid)
        {
            int maxID = 0, maxIDInactive = 0, maxIDUser = 0, maxIDGuest = 0;
            try
            {
                vrmUser user = new vrmUser();
                vrmInactiveUser userIn = new vrmInactiveUser();
                vrmGuestUser guest = new vrmGuestUser();
                m_IuserDao.addProjection(Projections.Max("userid"));
                IList maxId1 = m_IuserDao.GetObjectByCriteria(new List<ICriterion>());
                if (maxId1[0] != null)
                    maxIDUser = ((int)maxId1[0]);

                m_IinactiveUserDao.addProjection(Projections.Max("userid"));
                IList maxId2 = m_IinactiveUserDao.GetObjectByCriteria(new List<ICriterion>());
                if (maxId2[0] != null)
                    maxIDInactive = ((int)maxId2[0]);

                m_IGuestUserDao.addProjection(Projections.Max("userid"));
                IList maxId3 = m_IGuestUserDao.GetObjectByCriteria(new List<ICriterion>());
                if (maxId3[0] != null)
                    maxIDGuest = ((int)maxId3[0]);
                if (err < 0)
                    return false;
                if (maxIDUser > maxIDInactive)
                {
                    if (maxIDUser > maxIDGuest)
                        maxID = maxIDUser;
                    else
                        maxID = maxIDGuest;
                }
                else
                {
                    if (maxIDInactive > maxIDGuest)
                        maxID = maxIDInactive;
                    else
                        maxID = maxIDGuest;
                }
                maxuserid = maxID;
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region CheckUser
        /// <summary>
        /// CheckUser
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="emailAddress"></param>
        /// <param name="altemail"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="mode"></param>
        /// <param name="err"></param>
        /// <param name="loginID"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string CheckUser(int userid, string firstname, string lastname, string emailAddress, string altemail, string login, string password, int mode, ref int err, int loginID, vrmDataObject obj, int isEnabledStaticId, string StaticId, string UserDomain) //ZD 100969 //ZD 103784
        {
            string email = ""; //ZD 100263_SQL
            try
            {
                //ZD 100263_SQL Starts
                //firstname - used only in nHibernate
                //lastname  - Not used anywhere in this command
                email = emailAddress.Replace("'", "''"); // Used in both SQL and nHibernate. So changed variable name
                altemail = altemail.Replace("'", "''"); // Used only in SQL 
                login = login.Replace("'", "''"); //Used in SQL and nHibernate( but no use in nHibernate).
                //ZD 100263_SQL End

                string stmt;
                DataSet ds = null;
                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);

                vrmUser actuser = new vrmUser();
                if (isAudioAddOn == 1) //FB 2023
                {
                    if (firstname == "" || email == "")
                    {
                        err = -1;
                        obj.outXml = "Blank Fields";
                    }
                }
                else
                {
                    if (firstname == "" || lastname == "" || email == "")
                    {
                        err = -1;
                        obj.outXml = "Blank Fields";
                    }
                }
                //ZD 100969 Start
                if (isEnabledStaticId == 1 && StaticId != "")
                {
                    stmt = "Select * from Usr_List_D where StaticId = '" + StaticId.Trim() + "' and userid != " + userid;
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(722);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                        return obj.outXml;
                    }
                }
                //ZD 100969 End
                if (isAudioAddOn == 1) //FB 2023
                {
                    stmt = "Select * from Usr_List_D where Audioaddon = 1 and Deleted = 0 and email = '"
                          + email.Trim() + "' and userid != " + userid;
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(335);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                        return obj.outXml;
                    }
                }
                if (mode != 2)
                {
                    if (mode == 0)
                    {
                        stmt = "SELECT firstname, lastname from Usr_List_D WHERE ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " userid=" + userid + " or email='" + email + "'";
                      
                        if (login.Length > 0)
                        {
                            stmt += "  or ( login = '" + login + "'";
                            stmt += "  and UserDomain = '" + UserDomain + "')";// ZD 103784
                        }
                        
                        
                    }
                    else if (mode == 3)
                    {
                        stmt = "SELECT firstname, lastname FROM Usr_List_D WHERE ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += "email = '" + email + "' AND userid != " + userid;
                    }
                    else
                    {
                        stmt = " select firstname, lastname from Usr_List_D WHERE (";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or ( login = '" + login + "'";
                            stmt += "  and UserDomain = '" + UserDomain + "')";// ZD 103784
                        }
                        stmt += " ) and userid!=" + userid;
                       
                    }
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(261);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }

                    if (mode == 0)
                    {
                        stmt = "select firstname, lastname from Usr_Inactive_D where ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " userid=" + userid + " or email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or ( login = '" + login + "'";
                            stmt += "  and UserDomain = '" + UserDomain + "')";// ZD 103784
                        }
                       
                    }
                    else if (mode == 3)
                    {
                        stmt = "SELECT firstname, lastname FROM Usr_Inactive_D WHERE ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += "email = '" + email + "' AND userid != " + userid;
                    }
                    else
                    {
                        stmt = " select firstname, lastname from Usr_Inactive_D where ( ";
                        if (altemail.Length > 0)
                        {
                            stmt += " AlternativeEmail = '" + altemail + "' or ";
                            stmt += "email = '" + altemail + "' or ";
                        }
                        stmt += " AlternativeEmail = '" + email + "' or ";
                        stmt += " email='" + email + "'";
                        if (login.Length > 0)
                        {
                            stmt += "  or ( login = '" + login + "'";
                            stmt += "  and UserDomain = '" + UserDomain + "')";// ZD 103784
                        }
                        stmt += " ) and userid!=" + userid;
                      
                    }
                    if (stmt != "")
                        ds = m_rptLayer.ExecuteDataSet(stmt);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        err = -2;
                        myVRMException e = new myVRMException(426);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                }
                else
                {
                    List<ICriterion> Email = new List<ICriterion>();
                    Email.Add(Expression.Eq("Email", emailAddress)); //ZD 100263_SQL
                    //Email.Add(Expression.Eq("Email", email));
                    List<vrmUser> mail = m_IuserDao.GetByCriteria(Email);
                    if (mail.Count > 0)
                    {
                        for (int i = 0; i < mail.Count; i++)
                        {
                            if (mail[i].FirstName == "" || mail[i].LastName == "" || mail[i].Login == "")
                            {
                                err = 1;
                                userid = mail[i].userid;
                                obj.outXml = "";
                            }
                            else
                            {
                                err = -3;
                                myVRMException e = new myVRMException(261);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                            }
                        }
                    }
                    else
                    {
                        List<ICriterion> fname = new List<ICriterion>();
                        fname.Add(Expression.Eq("FirstName", firstname));
                        List<vrmUser> first = m_IuserDao.GetByCriteria(fname);
                        if (first.Count > 0)
                        {
                            login = firstname + userid;
                            password = login;
                        }
                        else
                        {
                            login = firstname;
                            password = login;
                        }
                    }
                }
                return obj.outXml;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region ActiveUser
        private bool ActiveUser(ref vrmDataObject obj, ref vrmUser user, int err, ref int AuditPCCount)//ZD 101026
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false, savePw = false; //ZD 100221
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, superadmin;
                int enableAVWO = 0, enableCatWO = 0, enableFacWO = 0; //ZD 101122
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, endpointId = 0, userid = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0, tickerStatus1 = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0, tickerStatus = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, enablePIMnotification = 0, SendSurveyEmail = 0, EnableVNOCselection = 0;//FB 2141 FB 2348 FB 2608
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0; //FB 2392-Whygo 
                string BrokerRoomNum = ""; //FB 3001
                //int Secured = 0;//FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "", participantCode = ""; //ZD 101446
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", timezonedisplay = "", tickerBackground = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone = "";//FB 2227//FB 2268
                string PrivateVMR = "", userDomain = ""; //ZD 104850//FB 2481
                string VidyoURL = "", Extension = "", Pin = "", LandingPageTitle = "", LandingPageLink = ""; //FB 2262 //FB 2599 // ZD 100172
                string PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 FB 2724
                int enablePCUser = 0, enableWebexUser = 0, isPwdChngd = 0, EnableBJNConf = 0; //FB 2693 //ZD 100221 //ZD 100781 //ZD 104021
                string WebexUserName = "", WebexPassword = "", bjnUserName = "";//ZD 100221 //ZD 104021
                DateTime accountexpiry = new DateTime();
                string StaticID = null;
                int IsStaticIDEnabled = 0;
                int RoomViewType = 1;//ZD 100621
                int enableAdditionalOption = 0;//ZD 101093
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/UserDomain");//ZD 104850 start
                if (node != null)
                    userDomain = node.InnerXml.Trim();//ZD 104850 End
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    //if (location.Length == 0) //ZD 103469
                        //location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out prefergroup);
                //ZD 102052
                if (prefergroup == 0)
                    prefergroup = -1;
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/participantCode"); //ZD 101446
                if (node != null)
                    participantCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                int.TryParse(EMailLanguage, out EmailLanguage);
                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        int.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    //if (accountexpiry == DateTime.MinValue)
                    //    accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                    //ZD 100456
                    if (accountexpiry == DateTime.MinValue)
                    {
                        cryptography.Crypto cryp = new cryptography.Crypto();
                        vrmOrganization vrmOrg = m_IOrgDAO.GetById(organizationID);
                        string orglicense = cryp.decrypt(vrmOrg.orgExpiryKey);
                        XmlDocument xd1 = new XmlDocument();
                        xd1.LoadXml(orglicense);
                        XmlNode Orgnode;

                        Orgnode = xd1.SelectSingleNode("//Organization/ExpiryDate");
                        string ExpirationDate = Orgnode.InnerXml.Trim();
                        DateTime.TryParse(ExpirationDate, out accountexpiry);
                    }

                }
                //ZD 100781 Starts
                node = xd.SelectSingleNode("//saveUser/user/isPwdChanged");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out isPwdChngd);
                //ZD 100781 Ends
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableParticipants);

                //ZD 101122 - Start
                node = xd.SelectSingleNode("//saveUser/user/enableAVWorkOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAVWO);

                node = xd.SelectSingleNode("//saveUser/user/enableCateringWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableCatWO);

                node = xd.SelectSingleNode("//saveUser/user/enableFacilityWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableFacWO);
                //ZD 101122 - End

                //ZD 101093 START
                node = xd.SelectSingleNode("//saveUser/user/enableAdditionalOption");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAdditionalOption);
                //ZD 101093 END

                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableMob);
                node = xd.SelectSingleNode("//saveUser/PIMNotification");//FB 2141
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enablePIMnotification);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail");//FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone");//FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail");//FB 2348
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail); //FB 2633

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection); //FB 2633

                node = xd.SelectSingleNode("//saveUser/PrivateVMR");//FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End

                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out Region);

                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End

                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();

                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    int.TryParse(node.InnerText.Trim(), out Secured);

                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End

                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                //ZD 100168 12/22/2013 inncrewin
                node = xd.SelectSingleNode("//saveUser/IsStaticIDEnabled");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out IsStaticIDEnabled);
                node = xd.SelectSingleNode("//saveUser/StaticID");
                if (node != null)
                    StaticID = node.InnerText.Trim();
                //ZD 100168 12/22/2013 inncrewin

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");

                //FB 2693 Ends
                //ZD 100221 Starts
                node = xd.SelectSingleNode("//saveUser/EnableWebexConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableWebexUser);

                node = xd.SelectSingleNode("//saveUser/WebexUserName");
                if (node != null)
                    WebexUserName = node.InnerText.Trim();
                savePw = false;
                node = xd.SelectSingleNode("//saveUser/WebexPassword");
                if (node != null)
                {
                    savePw = true;
                    WebexPassword = node.InnerText.Trim();
                }
                //ZD 100221 Starts

                // ZD 100172 Starts
                node = xd.SelectSingleNode("//saveUser/LandingPageTitle");
                if (node != null)
                    LandingPageTitle = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/LandingPageLink");
                if (node != null)
                    LandingPageLink = node.InnerText.Trim();
                // ZD 100172 Ends
                //ZD 104021 Start
                node = xd.SelectSingleNode("//saveUser/EnableBJNConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableBJNConf);

                node = xd.SelectSingleNode("//saveUser/BJNUserName");
                if (node != null)
                    bjnUserName = node.InnerText.Trim();
                //ZD 104021 End
                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcUsrLimit = 0, webexUsrLimit = 0, BJNUsrLimit = 0;//FB 2693 //ZD 100221 //ZD 104021
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1, xWebexUsr = -1, xBJNUsr = -1; //FB 2693 //ZD 100221 //ZD 104021
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcUsrLimit = orgInfo.PCUserLimit; //FB 2693
                    webexUsrLimit = orgInfo.WebexUserLimit; //ZD 100221
                    BJNUsrLimit = orgInfo.BlueJeansUserLimit;//ZD 104021
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        IList<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList); //ZD 101714
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        //ZD 101443
                        if (sysSettings.IsLDAP == 0 && newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        int.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                            xWebexUsr = usr[i].enableWebexUser; //ZD 100221
                            xBJNUsr = usr[i].EnableBJNConf;//ZD 104021
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))//ZD 101525
                        {
                            if (currUsrCount > exchangeUsrLimit)
                            {
                                myVRMException e = new myVRMException(460);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (sysSettings.IsLDAP == 0 && sysSettings.EnableCloudInstallation == 1 && currUsrCount > dominoUsrLimit)//ZD 101525
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > mobileUsrLimit) //ZD 101525
                            {
                                myVRMException e = new myVRMException(526);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcUsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    //ZD 100221 Starts
                    currUsrCount = 0;
                    if (enableWebexUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableWebexUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", enableWebexUser));
                        List<vrmUser> usrWebex = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrWebex.Count != 0)
                        {
                            currUsrCount = usrWebex.Count;
                        }
                        if (xWebexUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > webexUsrLimit)
                        {
                            myVRMException e = new myVRMException(718);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 100221 Ends
                    //ZD 104021 Starts
                    currUsrCount = 0;
                    if (EnableBJNConf == 1 && sysSettings.MaxBlueJeansUsers > 0) //ZD 104116
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("EnableBJNConf", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrBJN = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrBJN.Count != 0)
                        {
                            currUsrCount = usrBJN.Count;
                        }
                        if (xBJNUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > BJNUsrLimit)
                        {
                            myVRMException e = new myVRMException(761);//Blue Jeans user limit exceeds VRM license.
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 104021 Ends
                    if (sysSettings.MaxBlueJeansUsers == -2 && EnableBJNConf == 0) //ZD 104116
                    {
                        EnableBJNConf = 1;
                        bjnUserName = email;
                    }

                   
                    int maxusrid = 0; string errMsg = "", Output = "";
                    if (FetchMaxUserID(err, errMsg, obj, ref maxusrid))
                        if (err < 0)
                            obj.outXml = errMsg;
                    userid = maxusrid + 1;

                    //string userDomain = "";//ZD 103784
                    Output = CheckUser(userid, firstname, lastname, email, altemail, login, password, 0, ref err, loginID, obj, IsStaticIDEnabled, StaticID, userDomain); //ZD 100969 //ZD 103784
                    if (err < 0)
                    {
                        Output = obj.outXml;
                        return false;
                    }
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string menu = usr1rle.roleMenuMask;
                    user = new vrmUser();
                    user.userid = userid;
                    user.FirstName = firstname;
                    user.LastName = lastname;
                    string pass = string.Empty;
                    //if (password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    pass = crypto.encrypt(password);
                    //}
                    user.Password = password; //FB 3054
                    user.TimeZone = timezoneid;
                    user.PreferedRoom = location;
                    user.PreferedGroup = prefergroup;
                    user.CCGroup = preferCCgroup;
                    user.Email = email;
                    user.WorkPhone = workPhone;
                    user.CellPhone = cellPhone;
                    user.AlternativeEmail = altemail;
                    user.DoubleEmail = doubleemail;
                    user.DefLineRate = defaultLineRate;
                    user.DefVideoProtocol = defaultVideoProtocol;
                    user.ConferenceCode = conferenceCode;
                    user.LeaderPin = leaderPin;
                    user.ParticipantCode = participantCode; //ZD 101446
                    user.Admin = admin;
                    user.Deleted = deleted;
                    user.lockCntTrns = locked;
                    user.Login = login;
                    user.roleID = roleID;
                    user.UserRolename = usr1rle.roleName; //ZD 103405
                    user.EmailClient = emailClient;
                    user.newUser = 1;
                    user.IPISDNAddress = IPISDNAddress;
                    user.BridgeID = bridgeID;
                    user.DefaultEquipmentId = defaultEquipment;
                    user.connectionType = connectionType;
                    user.Language = languageID;
                    user.EmailLangId = EmailLanguage;
                    user.outsidenetwork = outsidenetwork;
                    user.emailmask = emailmask;
                    user.addresstype = addresstype;
                    user.accountexpiry = accountexpiry;
                    //ZD 100781 Starts
                    if (isPwdChngd == 1 || Audioaddon == "1")
                        user.PasswordTime = DateTime.UtcNow;
                    user.IsPasswordReset = 0;
                    //ZD 100781 Ends
                    user.searchId = SavedSearch;
                    user.endpointId = endpointId;
                    user.DateFormat = dateformat;
                    user.enableAV = enableAV;
                    user.enableParticipants = enableParticipants;
                    user.EnableAVWO = enableAVWO;//ZD 101122
                    user.EnableCateringWO = enableCatWO;
                    user.EnableFacilityWO = enableFacWO;
                    user.enableAdditionalOption = enableAdditionalOption;//ZD 101093
                    user.TimeFormat = timeformat;
                    user.Timezonedisplay = timezonedisplay;
                    user.enableExchange = enableExc;
                    user.enableDomino = enableDom;
                    user.enableMobile = enableMob;
                    user.TickerStatus = tickerStatus;
                    user.TickerPosition = tickerPosition;
                    user.TickerSpeed = tickerSpeed;
                    user.TickerBackground = tickerBackground;
                    user.TickerDisplay = tickerDisplay;
                    user.RSSFeedLink = rssFeedLink;
                    user.TickerStatus1 = tickerStatus1;
                    user.TickerPosition1 = tickerPosition1;
                    user.TickerSpeed1 = tickerSpeed1;
                    user.TickerBackground1 = tickerBackground1;
                    user.TickerDisplay1 = tickerDisplay1;
                    user.RSSFeedLink1 = rssFeedLink1;
                    user.Audioaddon = Audioaddon;
                    user.LockStatus = locked;
                    user.companyId = organizationID;
                    user.MenuMask = menu;
                    user.PluginConfirmations = enablePIMnotification;//FB 2141
                    //FB 2227 - Start
                    user.InternalVideoNumber = InternalVideoNumber;
                    user.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 - End
                    user.HelpReqEmailID = ""; //FB 2268//ZD 100322
                    user.HelpReqPhone = "";//FB 2268//ZD 100322
                    user.RFIDValue = RFIDValue;//FB 2724
                    user.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    user.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    user.PrivateVMR = PrivateVMR;//FB 2481
                    //FB 2599 Start
                    //FB 2262
                    user.VidyoURL = VidyoURL;
                    user.Extension = Extension;
                    user.Pin = Pin;
                    //FB 2599 end

                    //FB 2392-Whygo Starts
                    user.ParentReseller = ParentReseller;
                    user.CorpUser = CorpUser;
                    user.Region = Region;
                    user.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    user.BrokerRoomNum = BrokerRoomNum; //FB 3001
                    //user.Secured = Secured; //FB 2595
                    //FB 2655 Start
                    user.PreConfCode = PreConfCode;
                    user.PreLeaderPin = PreLeaderPin;
                    user.PostLeaderPin = PostLeaderPin;
                    user.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    //FB 2693 Starts
                    user.enablePCUser = enablePCUser;
                    user.LandingPageTitle = LandingPageTitle; // ZD 100172
                    user.LandingPageLink = LandingPageLink;
                    //ZD 100157 Starts
                    //ZD 101714
                    if (user.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM" || user.PerCalStartTime.ToString().IndexOf("0001") >= 0)
                        user.PerCalStartTime = DateTime.Now;
                    if (user.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM" || user.PerCalEndTime.ToString().IndexOf("0001") >= 0)
                        user.PerCalEndTime = DateTime.Now;
                    if (user.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM" || user.RoomCalStartTime.ToString().IndexOf("0001") >= 0)
                        user.RoomCalStartTime = DateTime.Now;
                    if (user.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM" || user.RoomCalEndime.ToString().IndexOf("0001") >= 0)
                        user.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    //ZD 100221 Starts
                    user.enableWebexUser = enableWebexUser;
                    user.WebexUserName = WebexUserName;
                    if (savePw)
                        user.WebexPassword = WebexPassword;
                    //ZD 104021 Start
                    user.EnableBJNConf = EnableBJNConf;
                    user.BJNUserEmail = bjnUserName;
					//ZD 104021 End
                    //ZD 100221 Ends //ZD 101026                    
                    if (!InsertUserPCDetails(PCNodes, userid, ref AuditPCCount))
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    //ZD 101175 Start
                    if (mode == CREATENEW)
                    {
                        user.RoomViewType = RoomViewType;//ZD 100621
                        user.RoomRecordsView = 20;
                    }
                    //ZD 101175 End
                    user.IsStaticIDEnabled = IsStaticIDEnabled;
                    user.StaticID = (StaticID != null && StaticID.Trim() != "") ? StaticID : "";
                    user.LastModifiedUser = loginID;//ZD 100664
                    user.LastModifiedDate = DateTime.UtcNow; //ZD 100664
                    user.UserOrgin = 1;//101865
                    user.UserDomain = userDomain;//ZD 103784
                    m_IuserDao.Save(user);

                    //ZD 104116 Starts
                    if (user.EnableBJNConf == 1 && !string.IsNullOrEmpty(user.BJNUserEmail))
                    {
                        if (RTCobj == null)//FB 2363 start
                            RTCobj = new VRMRTC.VRMRTC();
                        NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                        bool ret = RTCobj.LoadConfigParams(configPath, ref configParams);
                        if (ret)
                        {

                            BJNxml = "<GetUserDetails><UserID>" + user.userid.ToString() + "</UserID><organizationID>" + user.companyId.ToString() + "</organizationID><BJNUserEmail>" + user.BJNUserEmail + "</BJNUserEmail></GetUserDetails>";

                            NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);
                            ops.GetUserBJNDetails(BJNxml, ref outRTCXML, ref msg);
                            obj.outXml = msg;
                        }
                    }
                    //ZD 104116 Ends
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region InActiveUser
        private bool InActiveUser(ref vrmDataObject obj, ref vrmInactiveUser Inuser, int err, ref int AuditPCCount)//ZD 101026
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false, savePw = false; //ZD 100221
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, superadmin;
                int enableAVWO = 0, enableCatWO = 0, enableFacWO = 0; //ZD 101122
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, endpointId = 0, userid = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, tickerStatus = 0, tickerStatus1 = 0, SendSurveyEmail = 0, EnableVNOCselection = 0;//FB 2348 FB 2608
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0; //FB 2392-Whygo
                string BrokerRoomNum = ""; //FB 3001
                //int Secured = 0; //FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "", participantCode = ""; //ZD 101446
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", tickerBackground = "", timezonedisplay = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone = ""; //FB 2227//FB 2268
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = "", LandingPageTitle = "", LandingPageLink = ""; //FB 2262 //FB 2599 // ZD 100172
                string PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 //FB 2724
                DateTime accountexpiry = new DateTime();
                int enablePCUser = 0, enableWebexUser = 0, isPwdChngd = 0, EnableBJNConf = 0; //FB 2693 //ZD 100221 //ZD 100781 //ZD 104021
                string WebexUserName = "", WebexPassword = "", bjnUserName = "";//ZD 100221 //ZD 104021
                string StaticID = "", userDomain = ""; //ZD 104850 //ZD 100890
                int IsStaticIDEnabled = 0; //ZD 100890
                int RoomViewType = 1;//ZD 100621
                int enableAdditionalOption = 0;//ZD 101093

                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                //ZD 104850 Start
                node = xd.SelectSingleNode("//saveUser/user/UserDomain");
                if (node != null)
                    userDomain = node.InnerText.Trim();
                //ZD 104850 End
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    //if (location.Length == 0) //ZD 103469
                        //location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out prefergroup);
                //ZD 102052
                if (prefergroup == 0)
                    prefergroup = -1;
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/participantCode"); //ZD 101446
                if (node != null)
                    participantCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                int.TryParse(EMailLanguage, out EmailLanguage);

                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                //if (EmailLanguage == 0)
                //    EmailLanguage = languageID;
                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        int.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                //ZD 100781 Starts
                node = xd.SelectSingleNode("//saveUser/user/isPwdChanged");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out isPwdChngd);
                //ZD 100781 Ends
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableParticipants);

                //ZD 101122 - Start
                node = xd.SelectSingleNode("//saveUser/user/enableAVWorkOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAVWO);

                node = xd.SelectSingleNode("//saveUser/user/enableCateringWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableCatWO);

                node = xd.SelectSingleNode("//saveUser/user/enableFacilityWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableFacWO);
                //ZD 101122 - End

                //ZD 101093 START
                node = xd.SelectSingleNode("//saveUser/user/enableAdditionalOption");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAdditionalOption);
                //ZD 101093 END

                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableMob);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail"); //FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone"); //FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                //FB 2348
                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail); //FB 2633

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection);//FB 2633

                node = xd.SelectSingleNode("//saveUser/PrivateVMR"); //FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End
                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out Region);
                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End
                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();
                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    int.TryParse(node.InnerText.Trim(), out Secured);

                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End
                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");

                //FB 2693 Ends
                //ZD 100221 Starts
                node = xd.SelectSingleNode("//saveUser/EnableWebexConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableWebexUser);

                node = xd.SelectSingleNode("//saveUser/WebexUserName");
                if (node != null)
                    WebexUserName = node.InnerText.Trim();

                savePw = false;
                node = xd.SelectSingleNode("//saveUser/WebexPassword");
                if (node != null)
                {
                    savePw = true;
                    WebexPassword = node.InnerText.Trim();
                }
                //ZD 100221 Ends
                //ZD 104021 Start
                node = xd.SelectSingleNode("//saveUser/EnableBJNConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableBJNConf);

                node = xd.SelectSingleNode("//saveUser/BJNUserName");
                if (node != null)
                    bjnUserName = node.InnerText.Trim();
              	//ZD 104021 End
                // ZD 100172 Starts
                node = xd.SelectSingleNode("//saveUser/LandingPageTitle");
                if (node != null)
                    LandingPageTitle = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/LandingPageLink");
                if (node != null)
                    LandingPageLink = node.InnerText.Trim();
                // ZD 100172 Ends
                //ZD 100890 Start
                node = xd.SelectSingleNode("//saveUser/IsStaticIDEnabled");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out IsStaticIDEnabled);
                node = xd.SelectSingleNode("//saveUser/StaticID");
                if (node != null)
                    StaticID = node.InnerText.Trim();
                //ZD 100890 End
                //FB 2693 Ends

                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcLsrLimit = 0, webexUsrLimit = 0, BJNUsrLimit = 0;//FB 2693 //ZD 100221 //ZD 104021
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1, xWebexUsr = -1, xBJNUsr = -1; //FB 2693 //ZD 100221 //ZD 104021
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcLsrLimit = orgInfo.PCUserLimit; //FB 2693
                    webexUsrLimit = orgInfo.WebexUserLimit; //ZD 100221
                    BJNUsrLimit = orgInfo.BlueJeansUserLimit;//ZD 104021
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        //ZD 101443
                        if (sysSettings.IsLDAP == 0 && newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        int.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                            xWebexUsr = usr[i].enableWebexUser; //ZD 100221
                            xBJNUsr = usr[i].EnableBJNConf;//ZD 104021
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > exchangeUsrLimit)
                            {
                                myVRMException e = new myVRMException(460);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > mobileUsrLimit)
                            {
                                myVRMException e = new myVRMException(526);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcLsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    //ZD 100221 Starts
                    currUsrCount = 0;
                    if (enableWebexUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableWebexUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", enableWebexUser));
                        List<vrmUser> usrWebex = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrWebex.Count != 0)
                        {
                            currUsrCount = usrWebex.Count;
                        }
                        if (xWebexUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > webexUsrLimit)
                        {
                            myVRMException e = new myVRMException(718);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 100221 Ends
                    //ZD 104021 Starts
                    currUsrCount = 0;
                    if (EnableBJNConf == 1 && sysSettings.MaxBlueJeansUsers > 0) //ZD 104116
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("EnableBJNConf", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrBJN = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrBJN.Count != 0)
                        {
                            currUsrCount = usrBJN.Count;
                        }
                        if (xBJNUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > BJNUsrLimit)
                        {
                            myVRMException e = new myVRMException(761);//Blue Jeans user limit exceeds VRM license.
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 104021 Ends
                    if (sysSettings.MaxBlueJeansUsers == -2 && EnableBJNConf == 0) //ZD 104116
                    {
                        EnableBJNConf = 1;
                        bjnUserName = email;
                    }
                    int maxusrid = 0; string errMsg = "", Output = "";
                    if (FetchMaxUserID(err, errMsg, obj, ref maxusrid))
                        if (err < 0)
                            obj.outXml = errMsg;
                    userid = maxusrid + 1;
                    //string userDomain = "";//ZD 103784 //ZD 104850
                    Output = CheckUser(userid, firstname, lastname, email, altemail, login, password, 0, ref err, loginID, obj, IsStaticIDEnabled, StaticID, userDomain);//ZD 100969 //ZD 103784
                    if (err < 0)
                    {
                        Output = obj.outXml;
                        return false;
                    }
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string menu = usr1rle.roleMenuMask;
                    Inuser = new vrmInactiveUser();
                    Inuser.userid = userid;
                    Inuser.FirstName = firstname;
                    Inuser.LastName = lastname;
                    //string pass = string.Empty;
                    //if (password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    pass = crypto.encrypt(password);
                    //}
                    Inuser.Password = password; //FB 3054
                    Inuser.TimeZone = timezoneid;
                    Inuser.PreferedRoom = location;
                    Inuser.PreferedGroup = prefergroup;
                    Inuser.CCGroup = preferCCgroup;
                    Inuser.Email = email;
                    Inuser.WorkPhone = workPhone;
                    Inuser.CellPhone = cellPhone;
                    Inuser.AlternativeEmail = altemail;
                    Inuser.DoubleEmail = doubleemail;
                    Inuser.DefLineRate = defaultLineRate;
                    Inuser.DefVideoProtocol = defaultVideoProtocol;
                    Inuser.ConferenceCode = conferenceCode;
                    Inuser.LeaderPin = leaderPin;
                    Inuser.ParticipantCode = participantCode;//ZD 101446
                    Inuser.Admin = admin;
                    Inuser.Deleted = deleted;
                    Inuser.lockCntTrns = locked;
                    Inuser.Login = login;
                    Inuser.roleID = roleID;
                    Inuser.UserRolename = usr1rle.roleName; //ZD 103405
                    Inuser.EmailClient = emailClient;
                    Inuser.newUser = 1;
                    Inuser.IPISDNAddress = IPISDNAddress;
                    Inuser.BridgeID = bridgeID;
                    Inuser.DefaultEquipmentId = defaultEquipment;
                    Inuser.connectionType = connectionType;
                    Inuser.Language = languageID;
                    Inuser.EmailLangId = EmailLanguage;
                    Inuser.outsidenetwork = outsidenetwork;
                    Inuser.emailmask = emailmask;
                    Inuser.addresstype = addresstype;
                    Inuser.accountexpiry = accountexpiry;
                    //ZD 100781 Starts
                    if (isPwdChngd == 1 || Audioaddon == "1")
                        Inuser.PasswordTime = DateTime.UtcNow;
                    Inuser.IsPasswordReset = 0;
                    //ZD 100781 Ends
                    Inuser.searchId = SavedSearch;
                    Inuser.endpointId = endpointId;
                    Inuser.DateFormat = dateformat;
                    Inuser.enableAV = enableAV;
                    Inuser.enableParticipants = enableParticipants;
                    Inuser.EnableAVWO = enableAVWO;//ZD 101122
                    Inuser.EnableCateringWO = enableCatWO;
                    Inuser.EnableFacilityWO = enableFacWO;
                    Inuser.enableAdditionalOption = enableAdditionalOption;//ZD 101093
                    Inuser.TimeFormat = timeformat;
                    Inuser.Timezonedisplay = timezonedisplay;
                    Inuser.enableExchange = enableExc;
                    Inuser.enableDomino = enableDom;
                    Inuser.enableMobile = enableMob;
                    Inuser.TickerStatus = tickerStatus;
                    Inuser.TickerPosition = tickerPosition;
                    Inuser.TickerSpeed = tickerSpeed;
                    Inuser.TickerBackground = tickerBackground;
                    Inuser.TickerDisplay = tickerDisplay;
                    Inuser.RSSFeedLink = rssFeedLink;
                    Inuser.TickerStatus1 = tickerStatus1;
                    Inuser.TickerPosition1 = tickerPosition1;
                    Inuser.TickerSpeed1 = tickerSpeed1;
                    Inuser.TickerBackground1 = tickerBackground1;
                    Inuser.TickerDisplay1 = tickerDisplay1;
                    Inuser.RSSFeedLink1 = rssFeedLink1;
                    Inuser.Audioaddon = Audioaddon;
                    Inuser.LockStatus = locked;
                    Inuser.Deleted = deleted;
                    Inuser.companyId = organizationID;
                    Inuser.MenuMask = menu;
                    //FB 2227 - start
                    Inuser.InternalVideoNumber = InternalVideoNumber;
                    Inuser.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 end
                    Inuser.HelpReqEmailID = ""; //FB 2268//ZD 100322
                    Inuser.HelpReqPhone = "";//FB 2268//ZD100322
                    Inuser.RFIDValue = RFIDValue;//FB 2724
                    Inuser.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    Inuser.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    Inuser.PrivateVMR = PrivateVMR;//FB 2481
                    //FB 2599 Start
                    //FB 2262
                    Inuser.VidyoURL = VidyoURL;//FB 2481
                    Inuser.Extension = Extension;
                    Inuser.Pin = Pin;
                    //FB 2599 End
                    //FB 2392-Whygo Starts
                    Inuser.ParentReseller = ParentReseller;
                    Inuser.CorpUser = CorpUser;
                    Inuser.Region = Region;
                    Inuser.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    Inuser.BrokerRoomNum = BrokerRoomNum; //FB 3001
                    //Inuser.Secured = Secured; //FB 2595
                    //FB 2655 Start
                    Inuser.PreConfCode = PreConfCode;
                    Inuser.PreLeaderPin = PreLeaderPin;
                    Inuser.PostLeaderPin = PostLeaderPin;
                    Inuser.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    //FB 2693 Starts
                    Inuser.enablePCUser = enablePCUser;
                    Inuser.LandingPageTitle = LandingPageTitle; // ZD 100172
                    Inuser.LandingPageLink = LandingPageLink;
                    //ZD 100890 Start
                    Inuser.IsStaticIDEnabled = IsStaticIDEnabled;
                    Inuser.StaticID = (StaticID != null && StaticID.Trim() != "") ? StaticID : "";
                    //ZD 100890 End
                    //ZD 100157 Starts
                    if (Inuser.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.PerCalStartTime = DateTime.Now;
                    if (Inuser.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.PerCalEndTime = DateTime.Now;
                    if (Inuser.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.RoomCalStartTime = DateTime.Now;
                    if (Inuser.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                        Inuser.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    //ZD 100221 Starts
                    Inuser.enableWebexUser = enableWebexUser;
                    Inuser.WebexUserName = WebexUserName;
                    if (savePw)
                        Inuser.WebexPassword = WebexPassword;
                    //ZD 100221 Ends
                    //ZD 104021 Start
                    Inuser.EnableBJNConf = EnableBJNConf;
                    Inuser.BJNUserEmail = bjnUserName;
					//ZD 104021 End
                    //ZD 101175 Start
                    if (mode == CREATENEW)
                    {
                        Inuser.RoomViewType = RoomViewType;//ZD 100621
                        Inuser.RoomRecordsView = 20;
                    }
                    //ZD 101175 End

                    if (!InsertUserPCDetails(PCNodes, userid, ref AuditPCCount))//ZD 101026
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    Inuser.LastModifiedUser = loginID; //ZD 100664 
                    Inuser.LastModifiedDate = DateTime.UtcNow; //ZD 100664
                    Inuser.UserDomain = userDomain;//ZD 103784
                    m_IinactiveUserDao.Save(Inuser);

                    //ZD 104116 Starts
                    if (Inuser.EnableBJNConf == 1 && !string.IsNullOrEmpty(Inuser.BJNUserEmail))
                    {
                        if (RTCobj == null)//FB 2363 start
                            RTCobj = new VRMRTC.VRMRTC();

                        NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                        bool ret = RTCobj.LoadConfigParams(configPath, ref configParams);
                        if (ret)
                        {

                            BJNxml = "<GetUserDetails><UserID>" + Inuser.userid.ToString() + "</UserID><organizationID>" + Inuser.companyId.ToString() + "</organizationID><BJNUserEmail>" + Inuser.BJNUserEmail + "</BJNUserEmail></GetUserDetails>";

                            NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);
                            ops.GetUserBJNDetails(BJNxml, ref outRTCXML, ref msg);
                        }
                        obj.outXml = msg;
                    }
                    //ZD 104116 Ends
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region LDAPActiveUser
        private bool LDAPActiveUser(ref vrmDataObject obj, int userid, ref int AuditPCCount)//ZD 101026
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false, savePw = false; //ZD 100221
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, sLocked = 1, superadmin;
                int enableAVWO = 0, enableCatWO = 0, enableFacWO = 0; //ZD 101122
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, tickerStatus = 0, tickerStatus1 = 0, enablePIMnotification = 0, SendSurveyEmail = 0, EnableVNOCselection = 0;//FB 2141 //FB 2348 FB 2608
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0; //FB 2392-Whygo
                string BrokerRoomNum = "";//FB 3001
                //int Secured = 0; //FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "", participantCode = ""; //ZD 101446
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", timezonedisplay = "", tickerBackground = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone = "";//FB 2227 Fix//FB 2268
                DateTime accountexpiry = new DateTime();
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = "", LandingPageTitle = "", LandingPageLink = ""; //FB 2262 //FB 2599 // ZD 100172
                string PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 FB 2724
                int enablePCUser = 0, enableWebexUser = 0, isPwdChngd = 0, EnableBJNConf = 0; //FB 2693 //ZD 100221 //ZD 100781 //ZD 104021
                string WebexUserName = "", WebexPassword = "", bjnUserName = "";//ZD 100221 //ZD 104021 
                string StaticID = "",userDomain = "" ; //ZD 104850 //ZD 100890
                int IsStaticIDEnabled = 0; //ZD 100890
                int RoomViewType = 1;//ZD 100621
                int enableAdditionalOption = 0;//ZD 101093
                List<vrmUserLobby> usrLobbyIcons = new List<vrmUserLobby>();//ZD 103625
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //ZD 100456
                string editFrom = "";
                node = xd.SelectSingleNode("//saveUser/EditFrom");
                if (node != null)
                    editFrom = node.InnerText.Trim();

                //ZD 104862
                if (userid > 0 && editFrom == "ND") //this check for entering exiting user records with out id values in DataImport Excel D - Modify, ND - New Data
                {
                    editFrom = "D";
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                //ZD 104850 Start
                node = xd.SelectSingleNode("//saveUser/user/UserDomain");
                if (node != null)
                    userDomain = node.InnerText.Trim();
                //ZD 104850 End
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    //if (location.Length == 0) //ZD 103469
                        //location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out prefergroup);
                //ZD 102052
                if (prefergroup == 0)
                    prefergroup = -1;
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/participantCode"); //ZD 101446
                if (node != null)
                    participantCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                int.TryParse(EMailLanguage, out EmailLanguage);
                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        int.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                //ZD 100781 Starts
                node = xd.SelectSingleNode("//saveUser/user/isPwdChanged");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out isPwdChngd);
                //ZD 100781 Ends
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableParticipants);

                //ZD 101122 - Start
                node = xd.SelectSingleNode("//saveUser/user/enableAVWorkOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAVWO);

                node = xd.SelectSingleNode("//saveUser/user/enableCateringWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableCatWO);

                node = xd.SelectSingleNode("//saveUser/user/enableFacilityWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableFacWO);
                //ZD 101122 - End

                //ZD 101093 START
                node = xd.SelectSingleNode("//saveUser/user/enableAdditionalOption");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAdditionalOption);
                //ZD 101093 END

                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableMob);

                node = xd.SelectSingleNode("//saveUser/PIMNotification");//FB 2141
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enablePIMnotification);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail");//FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone"); //FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail"); //FB 2348
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail);//FB 2633

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection);//FB 2633

                node = xd.SelectSingleNode("//saveUser/PrivateVMR");//FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End

                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out Region);

                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End
                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();
                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    int.TryParse(node.InnerText.Trim(), out Secured);
                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End

                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");

                //FB 2693 Ends
                //ZD 100221 Starts
                node = xd.SelectSingleNode("//saveUser/EnableWebexConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableWebexUser);

                node = xd.SelectSingleNode("//saveUser/WebexUserName");
                if (node != null)
                    WebexUserName = node.InnerText.Trim();

                savePw = false;
                node = xd.SelectSingleNode("//saveUser/WebexPassword");
                if (node != null)
                {
                    savePw = true;
                    WebexPassword = node.InnerText.Trim();
                }
                //ZD 100221 Starts
                //ZD 104021 Start
                node = xd.SelectSingleNode("//saveUser/EnableBJNConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableBJNConf);

                node = xd.SelectSingleNode("//saveUser/BJNUserName");
                if (node != null)
                    bjnUserName = node.InnerText.Trim();
         		//ZD 104021 End
                // ZD 100172 Start
                node = xd.SelectSingleNode("//saveUser/LandingPageTitle");
                if (node != null)
                    LandingPageTitle = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/LandingPageLink");
                if (node != null)
                    LandingPageLink = node.InnerText.Trim();
                // ZD 100172 Ends

                //ZD 100890 Start
                node = xd.SelectSingleNode("//saveUser/IsStaticIDEnabled");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out IsStaticIDEnabled);
                node = xd.SelectSingleNode("//saveUser/StaticID");
                if (node != null)
                    StaticID = node.InnerText.Trim();
                //ZD 100890 End


                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcLsrLimit = 0, webexUsrLimit = 0, BJNUsrLimit = 0;//FB 2693 //ZD 100221 //ZD 104021
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1, xWebexUsr = -1, xBJNUsr = -1; //FB 2693 //ZD 100221 //ZD 104021
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcLsrLimit = orgInfo.PCUserLimit; //FB 2693
                    webexUsrLimit = orgInfo.WebexUserLimit; //ZD 100221
                    BJNUsrLimit = orgInfo.BlueJeansUserLimit;//ZD 104021
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        //ZD 101443
                        if (sysSettings.IsLDAP == 0 && newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        int.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                            xWebexUsr = usr[i].enableWebexUser; //ZD 100221
                            xBJNUsr = usr[i].EnableBJNConf;//ZD 104021
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > exchangeUsrLimit)
                            {
                                myVRMException e = new myVRMException(460);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > mobileUsrLimit)
                            {
                                myVRMException e = new myVRMException(526);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcLsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    //ZD 100221 Starts
                    currUsrCount = 0;
                    if (enableWebexUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableWebexUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrWebex = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrWebex.Count != 0)
                        {
                            currUsrCount = usrWebex.Count;
                        }
                        if (xWebexUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > webexUsrLimit)
                        {
                            myVRMException e = new myVRMException(718);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 100221 Ends
                    //ZD 104021 Starts
                    currUsrCount = 0;
                    if (EnableBJNConf == 1 && sysSettings.MaxBlueJeansUsers > 0) //ZD 104116
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("EnableBJNConf", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrBJN = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrBJN.Count != 0)
                        {
                            currUsrCount = usrBJN.Count;
                        }
                        if (xBJNUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > BJNUsrLimit)
                        {
                            myVRMException e = new myVRMException(761);//Blue Jeans user limit exceeds VRM license.
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 104021 Ends
                    if (sysSettings.MaxBlueJeansUsers == -2 && EnableBJNConf == 0) //ZD 104116
                    {
                        EnableBJNConf = 1;
                        bjnUserName = email;
                    }
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string mask = usr1rle.roleMenuMask;
                    vrmUser Ldusr = m_IuserDao.GetByUserId(userid);
                    if (locked == vrmUserStatus.USER_ACTIVE)
                        sLocked = 0;

                    //ZD 101525
                    string enableSSO = "0";
                    List<ICriterion> criList = new List<ICriterion>();
                    criList.Add(Expression.Eq("OrgId", 11));
                    List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criList, true);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                        enableSSO = ldapSettingsList[0].EnableSSOMode.ToString();

                    //Config config = new Config(m_configPath);
                    //bool ret = config.Initialize();
                    //if (!config.ldapEnabled)
                    if (enableSSO == "0")
                    {
                        //string pass = string.Empty;
                        //if (password.Length > 0)
                        //{
                        //    cryptography.Crypto crypto = new cryptography.Crypto();
                        //    pass = crypto.encrypt(password);
                        //}
                        Ldusr.FirstName = firstname;
                        Ldusr.LastName = lastname;
                        Ldusr.Password = password;
                        Ldusr.Login = login;
                    }

                    //ZD 100263-Starts
                    vrmUser LoginUser = m_IuserDao.GetByUserId(loginID);
                    vrmUserRoles LoginUserRole = m_IUserRolesDao.GetById(LoginUser.roleID);//ZD 100263
                    if (!CheckUserRights(LoginUser, Ldusr))
                    {
                        if (LoginUser.userid != Ldusr.userid)
                        {
                            myvrmEx = new myVRMException(229);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (LoginUser.userid != Ldusr.userid)
                    {
                        Ldusr.enableExchange = enableExc;
                        Ldusr.roleID = roleID;
                        Ldusr.MenuMask = mask;
                        Ldusr.Admin = admin;
                        Ldusr.UserRolename = usr1rle.roleName; //ZD 103405
                        if (editFrom == "" || editFrom == "ND") //ZD 104862 //ZD 100456
                        {
                            Ldusr.enableDomino = enableDom; //ZD 104862
                            Ldusr.accountexpiry = accountexpiry;
                            //ZD 100263 Phase 2 Starts
                            Ldusr.enableMobile = enableMob;
                            Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                            //ZD 100263 Phase 2 Ends
                        }
                    }
                    else if (LoginUserRole.level == 2 && LoginUserRole.crossaccess == 0)
                    {
                        Ldusr.enableExchange = enableExc;
                        if (editFrom == "" || editFrom == "ND") //ZD 104862
                        {
                            Ldusr.enableDomino = enableDom;
                            Ldusr.enableMobile = enableMob;
                            Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                            //ZD 100263 Phase 2 Ends
                        }
                    }
                    else if (LoginUserRole.crossaccess == 1) //ZD 100263
                    {
						//ZD 104862 - Start
                        Ldusr.enableExchange = enableExc;
                        if (editFrom == "" || editFrom == "ND") //ZD 104862
                        {
                            Ldusr.accountexpiry = accountexpiry;
                            //ZD 100263 Phase 2 Starts
                            Ldusr.enableDomino = enableDom;
                            Ldusr.enableMobile = enableMob;
                            Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                            //ZD 100263 Phase 2 Ends
                        }
						//ZD 104862 - ENd
                    }
                    //ZD 100263-End

                    Ldusr.Email = email;
                    Ldusr.TimeZone = timezoneid;
                    Ldusr.enableExchange = enableExc;
                    //ZD 103625 - Start
                    if (Ldusr.Language != languageID)
                    {
                        List<ICriterion> criterionList6 = new List<ICriterion>();
                        criterionList6.Add(Expression.Eq("UserID", userid));
                        usrLobbyIcons = m_IUserLobbyDao.GetByCriteria(criterionList6, true);
                        for (int i = 0; i < usrLobbyIcons.Count; i++)
                            m_IUserLobbyDao.Delete(usrLobbyIcons[i]);
                    }
                    //ZD 103625 - End
                    //ZD 100456
                    if (editFrom == "" || editFrom == "ND") //ZD 104862
                    {
                        Ldusr.enableDomino = enableDom; //ZD 104862
                        Ldusr.WorkPhone = workPhone;
                        Ldusr.CellPhone = cellPhone;
                        Ldusr.PreferedRoom = location;
                        Ldusr.PreferedGroup = prefergroup;
                        Ldusr.CCGroup = preferCCgroup;
                        Ldusr.AlternativeEmail = altemail;
                        Ldusr.DoubleEmail = doubleemail;
                        Ldusr.EmailClient = emailClient;
                        Ldusr.DefLineRate = defaultLineRate;
                        Ldusr.DefVideoProtocol = defaultVideoProtocol;
                        Ldusr.ConferenceCode = conferenceCode;
                        Ldusr.LeaderPin = leaderPin;
                        Ldusr.ParticipantCode = participantCode; //ZD 101446
                        //Ldusr.Admin = admin; //ZD 100263
                        Ldusr.Deleted = deleted;
                        Ldusr.lockCntTrns = locked;
                        Ldusr.IPISDNAddress = IPISDNAddress;
                        Ldusr.connectionType = connectionType;
                        //Ldusr.roleID = roleID;//ZD 100263
                        Ldusr.Language = languageID;
                        Ldusr.EmailLangId = EmailLanguage;
                        Ldusr.DefaultEquipmentId = defaultEquipment;
                        Ldusr.outsidenetwork = outsidenetwork;
                        Ldusr.emailmask = emailmask;
                        Ldusr.addresstype = addresstype;
                        //Ldusr.accountexpiry = accountexpiry; //ZD 100263
                        //ZD 100781 Starts
                        if (isPwdChngd == 1 || Audioaddon == "1")
                            Ldusr.PasswordTime = DateTime.UtcNow;
                        Ldusr.IsPasswordReset = 0;
                        //ZD 100781 Starts
                        Ldusr.LockStatus = sLocked;
                        Ldusr.BridgeID = bridgeID;
                        Ldusr.searchId = SavedSearch;
                        Ldusr.DateFormat = dateformat;
                        Ldusr.enableAV = enableAV;
                        Ldusr.enableParticipants = enableParticipants;
                        Ldusr.EnableAVWO = enableAVWO;//ZD 101122
                        Ldusr.EnableCateringWO = enableCatWO;
                        Ldusr.EnableFacilityWO = enableFacWO;
                        Ldusr.enableAdditionalOption = enableAdditionalOption;//ZD 101093
                        Ldusr.TimeFormat = timeformat;
                        Ldusr.Timezonedisplay = timezonedisplay;
                        Ldusr.enableMobile = enableMob;
                        Ldusr.TickerStatus = tickerStatus;
                        Ldusr.TickerPosition = tickerPosition;
                        Ldusr.TickerSpeed = tickerSpeed;
                        Ldusr.TickerBackground = tickerBackground;
                        Ldusr.TickerDisplay = tickerDisplay;
                        Ldusr.RSSFeedLink = rssFeedLink;
                        Ldusr.TickerStatus1 = tickerStatus1;
                        Ldusr.TickerPosition1 = tickerPosition1;
                        Ldusr.TickerSpeed1 = tickerSpeed1;
                        Ldusr.TickerBackground1 = tickerBackground1;
                        Ldusr.TickerDisplay1 = tickerDisplay1;
                        Ldusr.RSSFeedLink1 = rssFeedLink1;
                        Ldusr.Audioaddon = Audioaddon;
                        //Ldusr.MenuMask = mask;//ZD 100263
                        Ldusr.PluginConfirmations = enablePIMnotification;//FB 2141
                        //FB 2227 Start
                        Ldusr.InternalVideoNumber = InternalVideoNumber;
                        Ldusr.ExternalVideoNumber = ExternalVideoNumber;
                        //FB 2227 End
                        Ldusr.HelpReqEmailID = ""; //FB 2268//ZD 100322
                        Ldusr.HelpReqPhone = "";//FB 2268//ZD 100322
                        Ldusr.RFIDValue = RFIDValue;//FB 2724
                        Ldusr.SendSurveyEmail = SendSurveyEmail;//FB 2348
                        Ldusr.EnableVNOCselection = EnableVNOCselection;//FB 2608
                        Ldusr.PrivateVMR = PrivateVMR;//FB 2481
                        //FB 2599 Start
                        //FB 2262
                        Ldusr.VidyoURL = VidyoURL;
                        Ldusr.Extension = Extension;
                        Ldusr.Pin = Pin;
                        //FB 2599 End
                        //FB 2392-Whygo Starts
                        Ldusr.ParentReseller = ParentReseller;
                        Ldusr.CorpUser = CorpUser;
                        Ldusr.Region = Region;
                        Ldusr.ESUserID = ESUserID;
                        //FB 2392-Whygo End
                        Ldusr.BrokerRoomNum = BrokerRoomNum;//FB 3001
                        //Ldusr.Secured = Secured;//FB 2595
                        //FB 2655 Start
                        Ldusr.PreConfCode = PreConfCode;
                        Ldusr.PreLeaderPin = PreLeaderPin;
                        Ldusr.PostLeaderPin = PostLeaderPin;
                        Ldusr.AudioDialInPrefix = AudioDialInPrefix;
                        //FB 2655 End

                        //ZD 100890 Start
                        Ldusr.IsStaticIDEnabled = IsStaticIDEnabled;
                        Ldusr.StaticID = (StaticID != null && StaticID.Trim() != "") ? StaticID : "";
                        //ZD 100890 End

                        //FB 2693 Starts
                        Ldusr.enablePCUser = enablePCUser;

                        Ldusr.LandingPageTitle = LandingPageTitle; // ZD 100172
                        Ldusr.LandingPageLink = LandingPageLink;
                        Ldusr.LastModifiedUser = loginID;//ZD 100664
                        Ldusr.LastModifiedDate = DateTime.UtcNow; //ZD 100664
                        //ZD 101175 Start
                        if (mode == CREATENEW)
                        {
                            Ldusr.RoomViewType = RoomViewType;//ZD 100621
                            Ldusr.RoomRecordsView = 20;
                        }
                        //ZD 101175 End
                        //ZD 100157 Starts
                        if (Ldusr.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                            Ldusr.PerCalStartTime = DateTime.Now;
                        if (Ldusr.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                            Ldusr.PerCalEndTime = DateTime.Now;
                        if (Ldusr.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                            Ldusr.RoomCalStartTime = DateTime.Now;
                        if (Ldusr.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                            Ldusr.RoomCalEndime = DateTime.Now;
                        //ZD 100221 Starts
                        Ldusr.enableWebexUser = enableWebexUser;
                        Ldusr.WebexUserName = WebexUserName;
                        if (savePw)
                            Ldusr.WebexPassword = WebexPassword;
                        //ZD 100221 Ends
                    }
                    //ZD 104021 Start
                    Ldusr.EnableBJNConf = EnableBJNConf;
                    Ldusr.BJNUserEmail = bjnUserName;
					//ZD 104021 End
                    Ldusr.UserDomain = userDomain;//ZD 104850
                    //ZD 100157 Ends
                    if (!InsertUserPCDetails(PCNodes, userid, ref AuditPCCount))//ZD 101026
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    m_IuserDao.Update(Ldusr);

                    //ZD 104116 Starts
                    if (Ldusr.EnableBJNConf == 1 && !string.IsNullOrEmpty(Ldusr.BJNUserEmail))
                    {
                        if (RTCobj == null)//FB 2363 start
                            RTCobj = new VRMRTC.VRMRTC();

                        NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                        bool ret = RTCobj.LoadConfigParams(configPath, ref configParams);
                        if (ret)
                        {
                            BJNxml = "<GetUserDetails><UserID>" + Ldusr.userid.ToString() + "</UserID><organizationID>" + Ldusr.companyId.ToString() + "</organizationID><BJNUserEmail>" + Ldusr.BJNUserEmail + "</BJNUserEmail></GetUserDetails>";
                            NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);
                            ops.GetUserBJNDetails(BJNxml, ref outRTCXML, ref msg);

                            obj.outXml = msg;
                        }
                        
                    }
                    //ZD 104116 Ends
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region LDAPInActiveUser
        private bool LDAPInActiveUser(ref vrmDataObject obj, int userid, ref int AuditPCCount)//ZD 101026
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                myVRMException myVRMEx = null;
                vrmUser vuser = new vrmUser();
                int mode = CREATENEW;
                bool userDeleted = false, savePw = false; //ZD 100221
                int SavedSearch = 0, enableAV = 0, enableExc = 0, enableDom = 0, enableMob = 0, enableParticipants = 0, initialTime = 0, sLocked = 1, superadmin;
                int enableAVWO = 0, enableCatWO = 0, enableFacWO = 0; //ZD 101122
                int emailClient = 0, bridgeID = 0, defaultEquipment = 0, connectionType = 0, languageID = 0, outsidenetwork = 0, emailmask = 0, addresstype = 0;
                int loginID = 0, timezoneid = 0, prefergroup = 0, doubleemail = 0, locked = 0, deleted = 0;
                int roleID = 0, admin = 0, extUser = 0, newUser = 0, preferCCgroup = 0, defaultLineRate = 0, defaultVideoProtocol = 0;
                int tickerPosition1 = 0, tickerSpeed1 = 0, tickerPosition = 0, tickerSpeed = 0, tickerDisplay1 = 0, tickerDisplay = 0, tickerStatus = 0, tickerStatus1 = 0, SendSurveyEmail = 0, EnableVNOCselection = 0;//FB 2348
                int ParentReseller = 0, CorpUser = 0, Region = 0, ESUserID = 0;//FB 2392-Whygo
                string BrokerRoomNum = "";//FB 3001
                //int Secured = 0; //FB 2595
                string userID = "", firstname = "", lastname = "", password = "", email = "", location = "";
                string altemail = "", strdoubleemail = "", workPhone = "", cellPhone = "", conferenceCode = "", leaderPin = "", participantCode = "";//ZD 101446
                string Audioaddon = "", videoProtocol = "", EMailLanguage = "";
                string creditCard = "", dateformat = "", IPISDNAddress = "";
                string timeformat = "", timezonedisplay = "", tickerBackground = "";
                string rssFeedLink = "", tickerBackground1 = "", rssFeedLink1 = "";
                string lnLoginName = "", lnLoginPwd = "", lnDBPath = "", lotus = "", ExchangeID = "", login = "", userInterface = "";
                string InternalVideoNumber = "", ExternalVideoNumber = "", HelpReqEmail = "", HelpReqPhone = "";//FB 2227//FB 2268
                string PrivateVMR = "";//FB 2481
                string VidyoURL = "", Extension = "", Pin = "", LandingPageTitle = "", LandingPageLink = ""; //FB 2262 //FB 2599 // ZD 100172
                string PreConfCode = "", PreLeaderPin = "", PostLeaderPin = "", AudioDialInPrefix = "", RFIDValue = "";//FB 2655 FB 2724
                DateTime accountexpiry = new DateTime();
                int enablePCUser = 0, enableWebexUser = 0, isPwdChngd = 0, EnableBJNConf = 0; //FB 2693 //ZD 100221 //ZD 100781 //ZD 104021
                string WebexUserName = "", WebexPassword = "", bjnUserName = "";//ZD 100221 //ZD 104021
                string StaticID = "",userDomain = "";//ZD 104850 //ZD 100890
                int IsStaticIDEnabled = 0; //ZD 100890
                int RoomViewType = 1;//ZD 100621
                int enableAdditionalOption = 0;//ZD 101093
                node = xd.SelectSingleNode("//saveUser/login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//saveUser/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//saveUser/user/userID");
                if (node != null)
                {
                    userID = node.InnerXml.Trim();
                }
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (userID == "new")
                    mode = CREATENEW;
                else
                    mode = MODIFYOLD;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    firstname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lastname = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/login");
                if (node != null)
                    login = node.InnerXml.Trim();
                //ZD 104850 Start
                node = xd.SelectSingleNode("//saveUser/user/UserDomain");
                if (node != null)
                    userDomain = node.InnerText.Trim();
                //ZD 104850 End
                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                    password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/timeZone");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out timezoneid);
                node = xd.SelectSingleNode("//saveUser/user/location");
                if (node != null)
                {
                    location = node.InnerXml.Trim();
                    //if (location.Length == 0) //ZD 103469
                        //location = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/group");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out prefergroup);
                //ZD 102052
                if (prefergroup == 0)
                    prefergroup = -1;
                node = xd.SelectSingleNode("//saveUser/user/ccGroup");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out preferCCgroup);
                node = xd.SelectSingleNode("//saveUser/user/alternativeEmail");
                if (node != null)
                    altemail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/sendBoth");
                if (node != null)
                {
                    strdoubleemail = node.InnerXml.Trim();
                    if (strdoubleemail == "1")
                        doubleemail = 1;
                    else
                        doubleemail = 0;
                }
                node = xd.SelectSingleNode("//saveUser/user/workPhone");
                if (node != null)
                    workPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/cellPhone");
                if (node != null)
                    cellPhone = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/conferenceCode");
                if (node != null)
                    conferenceCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/leaderPin");
                if (node != null)
                    leaderPin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/participantCode"); //ZD 101446
                if (node != null)
                    participantCode = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/audioaddon");
                if (node != null)
                    Audioaddon = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/EmailLang");
                if (node != null)
                    EMailLanguage = node.InnerXml.Trim();
                int EmailLanguage = 0;
                int.TryParse(EMailLanguage, out EmailLanguage);
                node = xd.SelectSingleNode("//saveUser/user/emailClient");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out emailClient);
                node = xd.SelectSingleNode("//saveUser/user/roleID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roleID);
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out languageID);
                    if (languageID == 0)
                        languageID = 1; //default US English
                }
                //if (EmailLanguage == 0)
                //    EmailLanguage = languageID;

                node = xd.SelectSingleNode("//saveUser/user/lineRateID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out defaultLineRate);
                node = xd.SelectSingleNode("//saveUser/user/videoProtocol");
                if (node != null)
                {
                    videoProtocol = node.InnerXml.Trim();
                    if (videoProtocol == "IP")
                        defaultVideoProtocol = 1;
                    else if (videoProtocol == "ISDN")
                        defaultVideoProtocol = 2;
                }
                node = xd.SelectSingleNode("//saveUser/user/IPISDNAddress");
                if (node != null)
                    IPISDNAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/connectionType");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out connectionType);
                    if (connectionType == 0)
                        connectionType = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/videoEquipmentID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out defaultEquipment);
                    if (defaultEquipment == 0)
                        defaultEquipment = -1; //None
                }
                node = xd.SelectSingleNode("//saveUser/user/isOutside");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out outsidenetwork);
                node = xd.SelectSingleNode("//saveUser/emailMask");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out emailmask);
                }
                node = xd.SelectSingleNode("//saveUser/user/addressTypeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out addresstype);
                    if (addresstype == 0)
                        addresstype = 1;// default 1 (IP Address)
                }
                node = xd.SelectSingleNode("//saveUser/user/SavedSearch");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SavedSearch);

                vrmUserRoles userrole = m_IUserRolesDao.GetById(roleID);
                admin = userrole.level;
                if (admin == 0)
                    admin = 0;
                if (admin == 2)
                    superadmin = 1;

                node = xd.SelectSingleNode("//saveUser/user/status/deleted");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out deleted);
                    if (deleted == 1)
                    {
                        int usr = 0;
                        int.TryParse(userID, out usr);
                        vuser = m_IuserDao.GetByUserId(usr);
                        int errid = 310;
                        if (!canDeleteUser(vuser, ref errid))
                        {
                            myVRMException e = new myVRMException(310);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                        }
                        else
                            userDeleted = true;
                    }
                }
                node = xd.SelectSingleNode("//saveUser/user/status/locked");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out locked);
                    if (locked == 1)
                        locked = 3; // MAN_LOCKED
                    else
                        locked = 0; // USER_ACTIVE
                }
                node = xd.SelectSingleNode("//saveUser/user/expiryDate");
                if (node != null)
                {
                    DateTime.TryParse(node.InnerXml.Trim(), out accountexpiry);
                    if (accountexpiry == DateTime.MinValue)
                        accountexpiry = sysSettings.ExpiryDate; // Expiry date of VRM license

                }
                //ZD 100781 Starts
                node = xd.SelectSingleNode("//saveUser/user/isPwdChanged");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out isPwdChngd);
                //ZD 100781 Ends
                node = xd.SelectSingleNode("//saveUser/user/initialTime");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out initialTime);
                node = xd.SelectSingleNode("//saveUser/user/creditCard");
                if (node != null)
                {
                    creditCard = node.InnerXml.Trim();
                    if (creditCard.Length == 0)
                        creditCard = "0";
                }
                node = xd.SelectSingleNode("//saveUser/user/bridgeID");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out bridgeID);
                    if (bridgeID == 0)
                        bridgeID = 1;// [None]
                }
                node = xd.SelectSingleNode("//saveUser/user/dateFormat");
                if (node != null)
                {
                    dateformat = node.InnerXml.Trim();
                    if (dateformat.Length == 0)
                        dateformat = "MM/dd/yyyy";
                }
                node = xd.SelectSingleNode("//saveUser/user/enableAV");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAV);
                node = xd.SelectSingleNode("//saveUser/user/enableParticipants");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableParticipants);

                //ZD 101122 - Start
                node = xd.SelectSingleNode("//saveUser/user/enableAVWorkOrder");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAVWO);

                node = xd.SelectSingleNode("//saveUser/user/enableCateringWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableCatWO);

                node = xd.SelectSingleNode("//saveUser/user/enableFacilityWO");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableFacWO);
                //ZD 101122 - End

                //ZD 101093 START
                node = xd.SelectSingleNode("//saveUser/user/enableAdditionalOption");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableAdditionalOption);
                //ZD 101093 END

                node = xd.SelectSingleNode("//saveUser/user/timeFormat");
                if (node != null)
                {
                    timeformat = node.InnerXml.Trim();
                    if (timeformat.Length == 0)
                        timeformat = "1";
                }
                node = xd.SelectSingleNode("//saveUser/user/timezoneDisplay");
                if (node != null)
                {
                    timezonedisplay = node.InnerXml.Trim();
                    if (timezonedisplay.Length == 0)
                        timezonedisplay = "1";
                }
                node = xd.SelectSingleNode("//saveUser/exchangeUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableExc);
                node = xd.SelectSingleNode("//saveUser/dominoUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableDom);
                node = xd.SelectSingleNode("//saveUser/mobileUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableMob);
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed);
                    if (tickerSpeed == 0)
                        tickerSpeed = 6;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground");
                if (node != null)
                {
                    tickerBackground = node.InnerXml.Trim();
                    if (tickerBackground.Length == 0)
                        tickerBackground = "#3399ff";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink");
                if (node != null)
                    rssFeedLink = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/tickerStatus1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerStatus1);
                node = xd.SelectSingleNode("//saveUser/user/tickerPosition1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerPosition1);
                node = xd.SelectSingleNode("//saveUser/user/tickerSpeed1");
                if (node != null)
                {
                    int.TryParse(node.InnerXml.Trim(), out tickerSpeed1);
                    if (tickerSpeed1 == 0)
                        tickerSpeed1 = 3;
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerBackground1");
                if (node != null)
                {
                    tickerBackground1 = node.InnerXml.Trim();
                    if (tickerBackground1.Length == 0)
                        tickerBackground1 = "#ffff99";
                }
                node = xd.SelectSingleNode("//saveUser/user/tickerDisplay1");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out tickerDisplay1);
                node = xd.SelectSingleNode("//saveUser/user/rssFeedLink1");
                if (node != null)
                    rssFeedLink1 = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/userInterface");
                if (node != null)
                    userInterface = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExchangeID");
                if (node != null)
                    ExchangeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus");
                if (node != null)
                    lotus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginName");
                if (node != null)
                    lnLoginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnLoginPwd");
                if (node != null)
                    lnLoginPwd = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/lotus/lnDBPath");
                if (node != null)
                    lnDBPath = node.InnerXml.Trim();
                //FB 2227 - Start
                node = xd.SelectSingleNode("//saveUser/user/IntVideoNum");
                if (node != null)
                    InternalVideoNumber = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//saveUser/user/ExtVideoNum");
                if (node != null)
                    ExternalVideoNumber = node.InnerXml.Trim();
                //FB 2227 - End
                node = xd.SelectSingleNode("//saveUser/HelpRequsetorEmail");//FB 2268
                if (node != null)
                    HelpReqEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/HelpRequsetorPhone"); //FB 2268
                if (node != null)
                    HelpReqPhone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/RFIDValue");//FB 2724
                if (node != null)
                    RFIDValue = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/SendSurveyEmail"); //FB 2348
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SendSurveyEmail);

                node = xd.SelectSingleNode("//saveUser/EnableVNOCselection");//FB 2608
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableVNOCselection); //FB 2633


                node = xd.SelectSingleNode("//saveUser/PrivateVMR");//FB 2481
                if (node != null)
                    PrivateVMR = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim());//FB 2721

                //FB 2599 Start
                //FB 2262
                node = xd.SelectSingleNode("//saveUser/VidyoURL");
                if (node != null)
                    VidyoURL = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Pin");
                if (node != null)
                    Pin = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//saveUser/Extension");
                if (node != null)
                    Extension = node.InnerXml.Trim();
                //FB 2599 End
                //FB 2392-Whygo Starts
                node = xd.SelectSingleNode("//saveUser/ParentReseller");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ParentReseller);

                node = xd.SelectSingleNode("//saveUser/CorpUser");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out CorpUser);

                node = xd.SelectSingleNode("//saveUser/Region");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out Region);

                node = xd.SelectSingleNode("//saveUser/ESUserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ESUserID);
                //FB 2392-Whygo End
                node = xd.SelectSingleNode("//saveUser/BrokerRoomNum"); //FB 3001
                if (node != null)
                    BrokerRoomNum = node.InnerText.Trim();
                //node = xd.SelectSingleNode("//saveUser/Secured");//FB 2595
                //if (node != null)
                //    int.TryParse(node.InnerText.Trim(), out Secured);

                //FB 2655 - DTMF - Start 
                node = xd.SelectSingleNode("//saveUser/DTMF/PreConfCode");
                if (node != null)
                    PreConfCode = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PreLeaderPin");
                if (node != null)
                    PreLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/PostLeaderPin");
                if (node != null)
                    PostLeaderPin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/DTMF/AudioDialInPrefix");
                if (node != null)
                    AudioDialInPrefix = node.InnerText.Trim();
                //FB 2655 - DTMF - End

                //FB 2693 Starts
                node = xd.SelectSingleNode("//saveUser/EnablePCUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enablePCUser);

                XmlNodeList PCNodes = xd.SelectNodes("//saveUser/PCDetails/PCDetail");


                //FB 2693 Ends
                //ZD 100221 Starts
                node = xd.SelectSingleNode("//saveUser/EnableWebexConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableWebexUser);

                node = xd.SelectSingleNode("//saveUser/WebexUserName");
                if (node != null)
                    WebexUserName = node.InnerText.Trim();
                savePw = false;
                node = xd.SelectSingleNode("//saveUser/WebexPassword");
                if (node != null)
                {
                    savePw = true;
                    WebexPassword = node.InnerText.Trim();
                }
                //ZD 100221 Starts
                //ZD 104021 Start
                node = xd.SelectSingleNode("//saveUser/EnableBJNConf");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableBJNConf);

                node = xd.SelectSingleNode("//saveUser/BJNUserName");
                if (node != null)
                    bjnUserName = node.InnerText.Trim();
				//ZD 104021 End
                // ZD 100172 Starts
                node = xd.SelectSingleNode("//saveUser/LandingPageTitle");
                if (node != null)
                    LandingPageTitle = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/LandingPageLink");
                if (node != null)
                    LandingPageLink = node.InnerText.Trim();
                // ZD 100172 Ends

                //ZD 100890 Start
                node = xd.SelectSingleNode("//saveUser/IsStaticIDEnabled");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out IsStaticIDEnabled);
                node = xd.SelectSingleNode("//saveUser/StaticID");
                if (node != null)
                    StaticID = node.InnerText.Trim();
                //ZD 100890 End

                //FB 2693 Ends

                try
                {
                    string elang = "";
                    if (EmailLanguage != 0)
                    {
                        List<ICriterion> criterion1List = new List<ICriterion>();
                        criterion1List.Add(Expression.Eq("EmailLangId", EmailLanguage));
                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterion1List);
                        if (emailLangs.Count != 0)
                            elang = emailLangs[0].EmailLanguage;
                        else
                            EmailLanguage = languageID;
                    }
                    int exchangeUsrLimit = 0, dominoUsrLimit = 0, mobileUsrLimit = 0, pcLsrLimit = 0, webexUsrLimit = 0, BJNUsrLimit = 0;//FB 2693 //ZD 100221 //ZD 104021
                    int currUsrCount = 0, xDUsr = -1, xEUsr = -1, xMUsr = -1, xPCUsr = -1, xWebexUsr = -1, xBJNUsr = -1; //FB 2693 //ZD 100221 //ZD 104021
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                    extUser = orgInfo.UserLimit;
                    exchangeUsrLimit = orgInfo.ExchangeUserLimit;
                    dominoUsrLimit = orgInfo.DominoUserLimit;
                    mobileUsrLimit = orgInfo.MobileUserLimit;
                    pcLsrLimit = orgInfo.PCUserLimit; //FB 2693
                    webexUsrLimit = orgInfo.WebexUserLimit; //ZD 100221
                    BJNUsrLimit = orgInfo.BlueJeansUserLimit;//ZD 104021
                    if (mode == CREATENEW)
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrlist = m_IuserDao.GetByCriteria(criterionList);
                        if (usrlist.Count > 0)
                        {
                            newUser = usrlist.Count;
                        }
                        newUser = newUser + 1;
                        //ZD 101443
                        if (sysSettings.IsLDAP == 0 && newUser > extUser)
                        {
                            myVRMException e = new myVRMException(252);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        List<ICriterion> criterionList1 = new List<ICriterion>();
                        int.TryParse(userID, out userid);
                        criterionList1.Add(Expression.Eq("userid", userid));
                        List<vrmUser> usr = m_IuserDao.GetByCriteria(criterionList1);
                        for (int i = 0; i < usr.Count; i++)
                        {
                            xDUsr = usr[i].enableDomino;
                            xEUsr = usr[i].enableExchange;
                            xMUsr = usr[i].enableMobile;
                            xPCUsr = usr[i].enablePCUser; //FB 2693
                            xWebexUsr = usr[i].enableWebexUser; //ZD 100221
                            xBJNUsr = usr[i].EnableBJNConf;//ZD 104021
                        }
                    }

                    if (enableExc == 1)
                    {
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("enableExchange", 1));
                        criterionList3.Add(Expression.Eq("Deleted", 0));
                        criterionList3.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrexc = m_IuserDao.GetByCriteria(criterionList3);
                        if (usrexc.Count != 0)
                        {
                            currUsrCount = usrexc.Count;
                        }
                        if (xEUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > exchangeUsrLimit)
                            {
                                myVRMException e = new myVRMException(460);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    currUsrCount = 0;
                    if (enableDom == 1)
                    {
                        List<ICriterion> criterionList4 = new List<ICriterion>();
                        criterionList4.Add(Expression.Eq("enableDomino", 1));
                        criterionList4.Add(Expression.Eq("Deleted", 0));
                        criterionList4.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrDom = m_IuserDao.GetByCriteria(criterionList4);
                        if (usrDom.Count != 0)
                        {
                            currUsrCount = usrDom.Count;
                        }
                        if (xDUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > dominoUsrLimit)
                        {
                            myVRMException e = new myVRMException(461);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    currUsrCount = 0;
                    if (enableMob == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableMobile", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrMob = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrMob.Count != 0)
                        {
                            currUsrCount = usrMob.Count;
                        }
                        if (xMUsr <= 0)
                            currUsrCount = currUsrCount + 1;

                        if (!(sysSettings.IsLDAP == 1 && sysSettings.EnableCloudInstallation == 1))
                        {
                            if (currUsrCount > mobileUsrLimit)
                            {
                                myVRMException e = new myVRMException(526);
                                m_log.Error(e);
                                obj.outXml = e.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    //FB 2693 Starts
                    currUsrCount = 0;
                    if (enablePCUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enablePCUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrPC = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrPC.Count != 0)
                        {
                            currUsrCount = usrPC.Count;
                        }
                        if (xPCUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > pcLsrLimit)
                        {
                            myVRMException e = new myVRMException(682);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2693 Ends
                    //ZD 100221 Starts
                    currUsrCount = 0;
                    if (enableWebexUser == 1)
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("enableWebexUser", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", enableWebexUser));
                        List<vrmUser> usrWebex = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrWebex.Count != 0)
                        {
                            currUsrCount = usrWebex.Count;
                        }
                        if (xWebexUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > webexUsrLimit)
                        {
                            myVRMException e = new myVRMException(718);
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 100221 Ends
                    //ZD 104021 Starts
                    currUsrCount = 0;
                    if (EnableBJNConf == 1 && sysSettings.MaxBlueJeansUsers > 0) //ZD 104116
                    {
                        List<ICriterion> criterionList5 = new List<ICriterion>();
                        criterionList5.Add(Expression.Eq("EnableBJNConf", 1));
                        criterionList5.Add(Expression.Eq("Deleted", 0));
                        criterionList5.Add(Expression.Eq("companyId", organizationID));
                        List<vrmUser> usrBJN = m_IuserDao.GetByCriteria(criterionList5);
                        if (usrBJN.Count != 0)
                        {
                            currUsrCount = usrBJN.Count;
                        }
                        if (xBJNUsr <= 0)
                            currUsrCount = currUsrCount + 1;
                        if (currUsrCount > BJNUsrLimit)
                        {
                            myVRMException e = new myVRMException(761);//Blue Jeans user limit exceeds VRM license.
                            m_log.Error(e);
                            obj.outXml = e.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 104021 Ends
                    if (sysSettings.MaxBlueJeansUsers == -2 && EnableBJNConf == 0) //ZD 104116
                    {
                        EnableBJNConf = 1;
                        bjnUserName = email;
                    }
                    vrmUserRoles usr1rle = m_IUserRolesDao.GetById(roleID);
                    string mask = usr1rle.roleMenuMask;
                    int id = 0;
                    vrmUser Lusr = m_IuserDao.GetByUserId(userid);
                    id = Lusr.id;
                    vrmInactiveUser Ldinusr = m_IinactiveUserDao.GetById(id);
                    Ldinusr.id = id;
                    Ldinusr.userid = userid;
                    Ldinusr.FirstName = firstname;
                    Ldinusr.LastName = lastname;
                    string pass = string.Empty;
                    //if (password.Length > 0)
                    //{
                    //    cryptography.Crypto crypto = new cryptography.Crypto();
                    //    pass = crypto.encrypt(password);
                    //}
                    Ldinusr.Password = password; //FB 3054
                    Ldinusr.Email = email;
                    Ldinusr.Email = email;
                    Ldinusr.WorkPhone = workPhone;
                    Ldinusr.CellPhone = cellPhone;
                    Ldinusr.TimeZone = timezoneid;
                    Ldinusr.PreferedRoom = location;
                    Ldinusr.PreferedGroup = prefergroup;
                    Ldinusr.CCGroup = preferCCgroup;
                    Ldinusr.AlternativeEmail = altemail;
                    Ldinusr.DoubleEmail = doubleemail;
                    Ldinusr.EmailClient = emailClient;
                    Ldinusr.DefLineRate = defaultLineRate;
                    Ldinusr.DefVideoProtocol = defaultVideoProtocol;
                    Ldinusr.ConferenceCode = conferenceCode;
                    Ldinusr.LeaderPin = leaderPin;
                    Ldinusr.ParticipantCode = participantCode; //ZD 101446
                    Ldinusr.Admin = admin;
                    Ldinusr.Deleted = deleted;
                    Ldinusr.lockCntTrns = locked;
                    Ldinusr.Login = login;
                    //Ldinusr.newUser = 0;
                    Ldinusr.IPISDNAddress = IPISDNAddress;
                    Ldinusr.connectionType = connectionType;
                    Ldinusr.roleID = roleID;
                    Ldinusr.UserRolename = usr1rle.roleName; //ZD 103405
                    Ldinusr.Language = languageID;
                    Ldinusr.EmailLangId = EmailLanguage;
                    Ldinusr.DefaultEquipmentId = defaultEquipment;
                    Ldinusr.outsidenetwork = outsidenetwork;
                    Ldinusr.emailmask = emailmask;
                    Ldinusr.addresstype = addresstype;
                    Ldinusr.accountexpiry = accountexpiry;
                    //ZD 100781 Starts
                    if (isPwdChngd == 1 || Audioaddon == "1")
                        Ldinusr.PasswordTime = DateTime.UtcNow;
                    Ldinusr.IsPasswordReset = 0;
                    //ZD 100781 Ends
                    Ldinusr.LockStatus = sLocked;
                    Ldinusr.BridgeID = bridgeID;
                    Ldinusr.searchId = SavedSearch;
                    Ldinusr.DateFormat = dateformat;
                    Ldinusr.enableAV = enableAV;
                    Ldinusr.enableParticipants = enableParticipants;
                    Ldinusr.EnableAVWO = enableAVWO;//ZD 101122
                    Ldinusr.EnableCateringWO = enableCatWO;
                    Ldinusr.EnableFacilityWO = enableFacWO;
                    Ldinusr.enableAdditionalOption = enableAdditionalOption;//ZD 101093
                    Ldinusr.TimeFormat = timeformat;
                    Ldinusr.Timezonedisplay = timezonedisplay;
                    Ldinusr.enableExchange = enableExc;
                    Ldinusr.enableDomino = enableDom;
                    Ldinusr.enableMobile = enableMob;
                    Ldinusr.TickerStatus = tickerStatus;
                    Ldinusr.TickerPosition = tickerPosition;
                    Ldinusr.TickerSpeed = Ldinusr.TickerSpeed;
                    Ldinusr.TickerBackground = tickerBackground;
                    Ldinusr.TickerDisplay = tickerDisplay;
                    Ldinusr.RSSFeedLink = rssFeedLink;
                    Ldinusr.TickerStatus1 = tickerStatus1;
                    Ldinusr.TickerPosition1 = tickerPosition1;
                    Ldinusr.TickerSpeed1 = Ldinusr.TickerSpeed1;
                    Ldinusr.TickerBackground1 = tickerBackground1;
                    Ldinusr.TickerDisplay1 = tickerDisplay1;
                    Ldinusr.RSSFeedLink1 = rssFeedLink1;
                    Ldinusr.Audioaddon = Audioaddon;
                    Ldinusr.MenuMask = mask;
                    //FB 2227 Start
                    Ldinusr.InternalVideoNumber = InternalVideoNumber;
                    Ldinusr.ExternalVideoNumber = ExternalVideoNumber;
                    //FB 2227 End
                    Ldinusr.HelpReqEmailID = "";//FB 2268//ZD 100322
                    Ldinusr.HelpReqPhone = "";//FB 2268//ZD 100322
                    Ldinusr.RFIDValue = RFIDValue;//FB 2724
                    Ldinusr.SendSurveyEmail = SendSurveyEmail;//FB 2348
                    Ldinusr.EnableVNOCselection = EnableVNOCselection;//FB 2608
                    Ldinusr.PrivateVMR = PrivateVMR;//FB 2481
                    //FB 2599 Start
                    //FB 2262
                    Ldinusr.VidyoURL = VidyoURL;
                    Ldinusr.Extension = Extension;
                    Ldinusr.Pin = Pin;
                    //FB 2599 End
                    //FB 2392-Whygo Starts
                    Ldinusr.ParentReseller = ParentReseller;
                    Ldinusr.CorpUser = CorpUser;
                    Ldinusr.Region = Region;
                    Ldinusr.ESUserID = ESUserID;
                    //FB 2392-Whygo End
                    Ldinusr.BrokerRoomNum = BrokerRoomNum; //FB 3001
                    //FB 2655 Start
                    Ldinusr.PreConfCode = PreConfCode;
                    Ldinusr.PreLeaderPin = PreLeaderPin;
                    Ldinusr.PostLeaderPin = PostLeaderPin;
                    Ldinusr.AudioDialInPrefix = AudioDialInPrefix;
                    //FB 2655 End
                    //FB 2693 Starts
                    Ldinusr.enablePCUser = enablePCUser;
                    Ldinusr.LandingPageTitle = LandingPageTitle; // ZD 100172
                    Ldinusr.LandingPageLink = LandingPageLink;
                    //ZD 100890 Start
                    Ldinusr.IsStaticIDEnabled = IsStaticIDEnabled;
                    Ldinusr.StaticID = (StaticID != null && StaticID.Trim() != "") ? StaticID : "";
                    //ZD 100890 End
                    //ZD 100157 Starts
                    if (Ldinusr.PerCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.PerCalStartTime = DateTime.Now;
                    if (Ldinusr.PerCalEndTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.PerCalEndTime = DateTime.Now;
                    if (Ldinusr.RoomCalStartTime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.RoomCalStartTime = DateTime.Now;
                    if (Ldinusr.RoomCalEndime.ToString() == "1/1/0001 12:00:00 AM")
                        Ldinusr.RoomCalEndime = DateTime.Now;
                    //ZD 100157 Ends
                    if (!InsertUserPCDetails(PCNodes, userid, ref AuditPCCount))//ZD 101026
                    {
                        myVRMException e = new myVRMException(681);
                        m_log.Error(e);
                        obj.outXml = e.FetchErrorMsg();
                    }
                    //FB 2693 Ends
                    //ZD 101175 Start
                    if (mode == CREATENEW)
                    {
                        Ldinusr.RoomViewType = RoomViewType; //ZD 100621
                        Ldinusr.RoomRecordsView = 20;
                    }
                    //ZD 101175 End                  
                    //ZD 100221 Starts
                    Ldinusr.enableWebexUser = enableWebexUser;
                    Ldinusr.WebexUserName = WebexUserName;
                    if (savePw)
                        Ldinusr.WebexPassword = WebexPassword;
                    //ZD 100221 Ends
                    //ZD 104021 Start
                    Ldinusr.EnableBJNConf = EnableBJNConf;
                    Ldinusr.BJNUserEmail = bjnUserName;
             		//ZD 104021 End     
                    Ldinusr.UserDomain = userDomain;//ZD 104850
                    Ldinusr.LastModifiedUser = loginID;//ZD 100664
                    Ldinusr.LastModifiedDate = DateTime.UtcNow; //ZD 100664
                    m_IinactiveUserDao.Save(Ldinusr);
                    m_IuserDao.Delete(Lusr);

                    //ZD 104116 Starts
                    if (Lusr.EnableBJNConf == 1 && !string.IsNullOrEmpty(Lusr.BJNUserEmail))
                    {
                        if (RTCobj == null)//FB 2363 start
                            RTCobj = new VRMRTC.VRMRTC();

                        NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                        bool ret = RTCobj.LoadConfigParams(configPath, ref configParams);
                        if (ret)
                        {
                            BJNxml = "<GetUserDetails><UserID>" + Lusr.userid.ToString() + "</UserID><organizationID>" + Lusr.companyId.ToString() + "</organizationID><BJNUserEmail>" + Lusr.BJNUserEmail + "</BJNUserEmail></GetUserDetails>";
                            NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);
                            ops.GetUserBJNDetails(BJNxml, ref outRTCXML, ref msg);

                            obj.outXml = msg;
                        }
                    }
                    //ZD 104116 Ends
                    return true;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    throw e;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //FB 2027 SetUser End

        //ZD 100152 starts
        public bool FetchGoogleInfo(int _userID, ref StringBuilder _outXml)
        {
            Tuple<int, string> usertp = null; //ALLDEV-856
            try
            {

                usertp = new Tuple<int, string>(_userID, ""); //ALLDEV-856
                vrmUserToken usr = new vrmUserToken();
                usr = m_IUserTokenDAO.GetByUserId(usertp); //ALLDEV-856

                vrmUser usrG = new vrmUser();
                usrG = m_IuserDao.GetByUserId(_userID);
                sysSettings _sys = new sysSettings(m_configPath);
                sysMailData _MailData = new sysMailData();
                _sys.getSysMail(ref _MailData);



                _xSettings = new XmlWriterSettings();
                _xSettings.OmitXmlDeclaration = true;
                using (_xWriter = XmlWriter.Create(_outXml, _xSettings))
                {
                    _xWriter.WriteStartElement("GoogleInfo");
                    _xWriter.WriteElementString("GoogleClientID", sysSettings.GoogleClientID);
                    _xWriter.WriteElementString("GoogleSecretID", sysSettings.GoogleSecretID);
                    _xWriter.WriteElementString("GoogleAPIKey", sysSettings.GoogleAPIKey);
                    _xWriter.WriteElementString("PushtoGoogle", sysSettings.PushtoGoogle.ToString());
                    _xWriter.WriteElementString("PollCount", sysSettings.PollCount.ToString()); //ALLDEV-856
                    _xWriter.WriteElementString("GoogleIntegration", sysSettings.GoogleIntegration.ToString()); //ALLDEV-856
                    _xWriter.WriteElementString("AdminEmailaddress", sysSettings.AdminEmailaddress); //ALLDEV-856

                    if (usr != null)
                    {
                        _xWriter.WriteElementString("RefreshToken", usr.RefreshToken);
                        _xWriter.WriteElementString("AccessToken", usr.AccessToken);
                        _xWriter.WriteElementString("ChanelExpiration", usr.ChannelExpiration.ToString());

                    }
                    else
                    {
                        _xWriter.WriteElementString("RefreshToken", "");
                        _xWriter.WriteElementString("AccessToken", "");
                        _xWriter.WriteElementString("ChanelExpiration", DateTime.Now.AddMinutes(-2).ToString());
                    }

                    _xWriter.WriteElementString("OrgID", usrG.companyId.ToString());
                    _xWriter.WriteElementString("usertimezone", usrG.TimeZone.ToString());//ALLDEV-816
                    _xWriter.WriteElementString("ChannelAddress", _MailData.websiteURL);
                    _xWriter.WriteFullEndElement();

                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("FetchGoogleInfo", ex);
                return false;
            }
        }
        //ZD 100152 starts

        #region GetHome
        /// <summary>
        /// GetHome
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetHome(ref vrmDataObject obj)
        {
            string sLogin = "";
            string sEmail = "";
            string sPwd = "";
            string sEnPwd = "";
            string sHomeurl = "";
            string stmt = "", UserDateFormat = "", pwdate = "", UserDate = ""; ;//ZD 100995
            string sAuthenticated = "NO";//SSo Mode
            bool bRet = true;
            DateTime currentLoginDate = DateTime.Now;
            DateTime currentdate = new DateTime();
            int lockcnt = 0;
            int lockcntrns = 0;
            int errno = 0;
            StringBuilder outXMLSB = null;
            Conference fetchconf = null;
            ns_SqlHelper.SqlHelper sqlCon = null;
            vrmSystemFactory validateLicense = null;
            string errMsg = "", datetime = "";//FB 2678
            string enEmailID = "", enMacID = ""; //ZD 100263
            DateTime PwdDateTime = DateTime.UtcNow;//ZD 10078
            emailFactory m_Email = null; //ZD 100781
            string[] Date, PwdDate; //ZD 100995
            string strDomain = "";//101812
            //ZD 101525 - Start
            List<string> groupName = null;
            //ZD 102052
            string fName = "", lname = "", emailAddress = "";
            int orgid = 0;
            int userid = 0;
            int roleid = 0;
            List<vrmLDAPGroups> ldapGroupList = null;
            List<vrmUserRoles> roleList = null;
            try
            {

                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentLoginDate);
                currentdate = new DateTime(currentLoginDate.Year, currentLoginDate.Month, currentLoginDate.Day, 0, 0, 0);
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node, node1, nodetdb; //ZD 100263 //102684

                node = xd.SelectSingleNode("//login/homeURL");
                if (node != null)
                {
                    if (node.InnerText != "")
                        sHomeurl = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/userName"); //SSO Fix
                if (node != null)
                {
                    if (node.InnerText != "")
                        sLogin = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else
                {
                    node = xd.SelectSingleNode("//login/emailID");
                    node1 = xd.SelectSingleNode("//login/enEmailID"); //ZD 100263
                    if (node != null || node1 != null)
                    {
                        if (node.InnerText != "" || (node1.InnerText != ""))//ZD 100263
                            sEmail = node.InnerXml.Trim();
                        else
                        {
                            myvrmEx = new myVRMException(207); //FB 2054 //FB 3055 1st Pint
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;

                        }
                    }

                }

                node = xd.SelectSingleNode("//login/userAuthenticated");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {

                    node = xd.SelectSingleNode("//login/userPassword");
                    node1 = xd.SelectSingleNode("//login/enUserPWD"); //ZD 100263
                    if (node != null || node1 != null)
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        //102684 start
                        nodetdb = xd.SelectSingleNode("//myVRM/importtdb");
                        if (nodetdb != null && nodetdb.InnerText == "1" && node == null)
                        {
                            sPwd = "";
                        }//102684 End
                        else
                        {
                            myvrmEx = new myVRMException(530); //FB 2054
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                //ZD 100263 Starts
                node = xd.SelectSingleNode("//login/macID");
                if (node != null)
                    if (node.InnerText != "")
                        macID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/enEmailID");
                if (node != null)
                {
                    crypto = new cryptography.Crypto();
                    if (node.InnerText != "")
                    {
                        enEmailID = node.InnerXml.Trim();
                        sEmail = crypto.decrypt(enEmailID);


                        node = xd.SelectSingleNode("//login/enUserPWD");
                        if (node != null)
                            if (node.InnerText != "")
                                sEnPwd = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//login/enMacID");
                        if (node != null)
                            if (node.InnerText != "")
                            {
                                enMacID = node.InnerXml.Trim();
                                enMacID = crypto.decrypt(enMacID);
                            }
                        if (enMacID != macID)
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                //ZD 100263 Ends

                //ZD 101812
                node = xd.SelectSingleNode("//login/userDomain");
                if (node != null)
                    if (node.InnerText != "")
                        strDomain = node.InnerXml.Trim();


                List<ICriterion> criterionList = new List<ICriterion>();

                if (sLogin.Trim() != "")
                {
                    criterionList.Add(Expression.Eq("Login", sLogin));
                    criterionList.Add(Expression.Eq("UserDomain", strDomain));// ZD 103784
                }
                else if (sEmail.Trim() != "")
                    criterionList.Add(Expression.Eq("Email", sEmail));

                //ZD 103244 Multiple Domain STarts
                List<ICriterion> criterionListUL = new List<ICriterion>();
                vrmLDAPConfig ldapSettings = null;
                criterionListUL = new List<ICriterion>();
                criterionListUL.Add(Expression.Eq("OrgId", 11));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionListUL);
                m_log.Error("Into Ldap.1... ");
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapSettings = ldapSettingsList[0];
                m_log.Error("Into Ldap.123... ");
                string ssstrDomain = strDomain + "\\";

                m_log.Error("Domain of the user " + ssstrDomain + " : System Ldap domain " + ldapSettings.domainPrefix.ToLower().Trim());

                if (ldapSettings.domainPrefix.ToLower().Trim() != ssstrDomain.ToLower().Trim() && ldapSettings.EnableSSOMode == 1 && ldapSettings.TenantOption == 2)
                {
                    criterionListUL = new List<ICriterion>();
                    criterionListUL.Add(Expression.Eq("domainPrefix", ssstrDomain.Trim()));
                    ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionListUL);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    {
                        ldapSettings = ldapSettingsList[0];
                        organizationID = ldapSettings.OrgId;
                    }
                    else
                    {
                        m_log.Error("chk point 17..............");
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    criterionList.Add(Expression.Eq("companyId", ldapSettings.OrgId));
                }
                //ZD 103244 Multiple Domain Ends

                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList, true);
                bool CreateUserbyTemplate = false;//ZD 102052

                if (vrmUsr.Count <= 0)
                {
                    //ZD 101525 - Start
                    groupName = null;
                    fName = ""; lname = ""; orgid = 11; emailAddress = "";//ZD 102052
                    userid = 0;
                    roleid = 0;
                    //orgid = "11";//ZD 102052
                    if (CheckIsLDAPUser(ref groupName, ref lname, ref fName, ref emailAddress, ref sLogin, ref strDomain)) // 101812
                    {
                        //Create New User
                        List<vrmLDAPGroups> vrmLDAPGroups = null;
                        List<vrmUserTemplate> templateList = null;
                        vrmUserTemplate UsrTemplate = null;
                        bool isUserExists = false;

                        if (organizationID < defaultOrgId)//101812
                            organizationID = defaultOrgId;
                        orgid = organizationID;//101812 //ZD 102052


                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.In("GroupName", groupName));
                        criterionList.Add(Expression.Eq("OrgId", organizationID));
                        criterionList.Add(Expression.Gt("RoleOrTemplateId", 0));
                        ldapGroupList = m_ILDAPGroupsDAO.GetByCriteria(criterionList, true);

                        if (ldapGroupList != null && ldapGroupList.Count > 0)
                        {
                            List<int> TemplateIds = null;
                            List<int> RoleIds = null;
                            templateList = new List<vrmUserTemplate>();
                            isUserExists = true;
                            criterionList = new List<ICriterion>();
                            vrmLDAPGroups = ldapGroupList.Where(g => g.Origin == 1 && g.RoleOrTemplateId > 0).ToList();
                            if (vrmLDAPGroups != null && vrmLDAPGroups.Count > 0)
                                TemplateIds = vrmLDAPGroups.Select(ldgrp => ldgrp.RoleOrTemplateId).ToList();
                            List<vrmUserRoles> userRolesort = null;
                            if (TemplateIds != null && TemplateIds.Count > 0)
                            {
                                criterionList.Add(Expression.In("id", TemplateIds));
                                criterionList.Add(Expression.Eq("deleted", 0));
                                criterionList.Add(Expression.Eq("companyId", organizationID));
                                templateList = m_ItempDao.GetByCriteria(criterionList);
                                if (templateList != null && templateList.Count > 0)
                                    RoleIds = templateList.Select(t => t.roleId).ToList();

                                criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.In("roleID", RoleIds));
                                roleList = m_IUserRolesDao.GetByCriteria(criterionList, true);

                                userRolesort = new List<vrmUserRoles>();
                                if (roleList.Where(r => r.crossaccess == 1).Any())
                                    userRolesort = roleList.OrderByDescending(r => r.level).OrderByDescending(r => r.crossaccess).ThenBy(r => r.createType).ThenBy(r => r.roleName).ToList();
                                else
                                    userRolesort = roleList.OrderByDescending(r => r.level).ThenBy(r => r.createType).ThenBy(r => r.roleName).ToList();
                                roleid = userRolesort[0].roleID;

                                templateList = templateList.Where(t => t.roleId == roleid).ToList();
                                UsrTemplate = templateList.OrderByDescending(t => t.name).Last();
                                CreateUserbyTemplate = true;
                            }
                            else
                            {
                                RoleIds = ldapGroupList.Select(l => l.RoleOrTemplateId).ToList();
                                //criterionList.Add(Expression.In("roleID", ldapGroupList.Select(ldgrp => ldgrp.RoleOrTemplateId).ToList()));
                                criterionList.Add(Expression.In("roleID", RoleIds));
                                roleList = m_IUserRolesDao.GetByCriteria(criterionList, true);

                                userRolesort = new List<vrmUserRoles>();
                                if (roleList.Where(r => r.crossaccess == 1).Any())
                                    userRolesort = roleList.OrderByDescending(r => r.level).OrderByDescending(r => r.crossaccess).ThenBy(r => r.createType).ThenBy(r => r.roleName).ToList();
                                else
                                    userRolesort = roleList.OrderByDescending(r => r.level).ThenBy(r => r.createType).ThenBy(r => r.roleName).ToList();

                                roleid = userRolesort[0].roleID;
                                CreateUserbyTemplate = false;
                            }
                            //ZD 102052
                            int usrOrigin = Convert.ToInt16(userOrigin.Web);
                            if (SetDynamicHost(UsrTemplate, emailAddress, fName, lname, sLogin, roleid, usrOrigin, CreateUserbyTemplate, ref orgid, ref userid, strDomain))
                            {
                                m_IuserDao.clearFetch();
                                m_IuserDao.clearProjection();
                                vrmUsr = new List<vrmUser>();
                                criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.Eq("userid", userid));
                                vrmUsr = m_IuserDao.GetByCriteria(criterionList, true);
                            }
                            else
                                isUserExists = false;
                        }

                        if (!isUserExists)
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        myvrmEx = new myVRMException(207);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //ZD 101525 - End
                }

                if (vrmUsr[0] == null || string.IsNullOrEmpty(vrmUsr[0].Email)) //ZD 103635
                {
                    myvrmEx = new myVRMException(207); //FB 3055
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                lockcntrns = vrmUsr[0].lockCntTrns;

                if (vrmUsr[0].userid != 11)
                {
                    if (validateLicense == null)
                        validateLicense = new vrmSystemFactory(ref obj);

                    if (!validateLicense.ValidateLicense(sysSettings.license, ref errno, ref datetime)) //FB 2678
                    {
                        if (errno == 515)
                        {
                            vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                            m_IuserDao.Update(vrmUsr[0]);
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                errno = 207;
                            else
                                errno = 263;
                        }
                        myvrmEx = new myVRMException(errno);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    if (!(currentLoginDate <= sysSettings.ExpiryDate && vrmUsr[0].accountexpiry >= currentLoginDate))
                    {
                        vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                        m_IuserDao.Update(vrmUsr[0]);
                        if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                            myvrmEx = new myVRMException(207);
                        else
                            myvrmEx = new myVRMException(263);

                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    #region  Password Expiration Chacking Module
                    if (usrOrg.EnablePasswordExp == 1) //ZD 100781 Starts
                    {
                        if (vrmUsr[0].IsPasswordReset == 1)
                        {
                            PwdDateTime = vrmUsr[0].PasswordTime.AddHours(24);
                            if (PwdDateTime < DateTime.UtcNow)
                            {
                                vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                                m_IuserDao.Update(vrmUsr[0]);

                                if (sysSettings.roomCascadingControl == 1)
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(725);

                                obj.outXml = myvrmEx.FetchErrorMsg(usrOrg.EnablePasswordRule); // ZD 103954

                                //m_Email = new emailFactory(ref obj);
                                //m_Email.ChangePasswordRequest(vrmUsr[0]); // comment for passwordreset new method

                                return false;
                            }

                        }
                        else
                        {
                            PwdDateTime = vrmUsr[0].PasswordTime.AddMonths(usrOrg.PasswordMonths);
                            if (PwdDateTime.Date.AddDays(-1) < DateTime.UtcNow.Date)
                            {
                                vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                                m_IuserDao.Update(vrmUsr[0]);


                                if (sysSettings.roomCascadingControl == 1)
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(725);

                                obj.outXml = myvrmEx.FetchErrorMsg(usrOrg.EnablePasswordRule); // ZD 103954

                                //m_Email = new emailFactory(ref obj);
                                //m_Email.ChangePasswordRequest(vrmUsr[0]); // comment for passwordreset new method
                                 
                                return false;
                            }
                        }
                    }
                    # endregion
                    // ALLDEV-497
                    if (vrmUsr[0].LockStatus == vrmUserStatus.MAN_LOCKED)
                    {
                        if (vrmUsr[0].lockCntTrns == 10)
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(425);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        else
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(254);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (vrmUsr[0].LockStatus == vrmUserStatus.AUTO_LOCKED)
                    {
                        if (currentLoginDate.Subtract(vrmUsr[0].LastLogin).Minutes < 1)
                        {
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
					else if (vrmUsr[0].LockStatus == vrmUserStatus.DATAIMPORT_LOCKED) //ZD 104862
                    {
                        if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                            myvrmEx = new myVRMException(207);
                        else
                            myvrmEx = new myVRMException(254);

                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100781 Ends
				//ALLDEV-497
               

                if (sAuthenticated.ToUpper() == "NO")
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        lockcntrns++;
                        if (lockcntrns >= 3 && vrmUsr[0].userid != 11)//FB 2027T
                        {
                            lockcnt = vrmUserStatus.AUTO_LOCKED;
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        else
                        {
                            myvrmEx = new myVRMException(207);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        bRet = false;
                    }
                }

                if (bRet)
                {
                    if ((vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days) + 1 < 15)
                    {
                        if (vrmUsr[0].userid != 11)
                        {
                            myvrmEx = new myVRMException(264);
                            //errMsg = myvrmEx.FetchErrorMsg().Replace("%s", vrmUsr[0].accountexpiry.ToString(vrmUsr[0].DateFormat)).Replace("{0}", (vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days + 1).ToString()); 
                            //ZD 100288 Starts
                            errMsg = myvrmEx.FetchErrorMsg();
                            errMsg = m_UtilFactory.GetTranslatedText(vrmUsr[0].Language, errMsg);
                            //ZD 100995
                            UserDateFormat = vrmUsr[0].DateFormat;
                            UserDate = vrmUsr[0].accountexpiry.ToString(vrmUsr[0].DateFormat);
                            if (usrOrg.EmailDateFormat == 1)
                            {
                                UserDateFormat = "dd MMM yyyy";
                                Date = vrmUsr[0].accountexpiry.ToString(UserDateFormat).Split(' ');
                                UserDate = Date[0] + " " + m_UtilFactory.GetTranslatedText(vrmUsr[0].Language, Date[1]) + " " + Date[2];

                            }
                            errMsg = errMsg.Replace("%s", UserDate).Replace("{0}", (vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days + 1).ToString());
                            //ZD 100288 Ends
                        }
                    }
                    //ZD 100781 Starts
                    if (usrOrg.EnablePasswordExp == 1)
                    {
                        PwdDateTime = vrmUsr[0].PasswordTime.AddMonths(usrOrg.PasswordMonths);
                        if ((PwdDateTime.Subtract(currentLoginDate).Days) < 4)
                        {
                            if (vrmUsr[0].userid != 11)
                            {
                                myvrmEx = new myVRMException(724);
                                errMsg = myvrmEx.FetchErrorMsg();
                                errMsg = m_UtilFactory.GetTranslatedText(vrmUsr[0].Language, errMsg);
                                //ZD 100995
                                UserDateFormat = vrmUsr[0].DateFormat;
                                pwdate = PwdDateTime.ToString(UserDateFormat);
                                if (usrOrg.EmailDateFormat == 1)
                                {
                                    UserDateFormat = "dd MMM yyyy";
                                    PwdDate = PwdDateTime.ToString(UserDateFormat).Split(' ');
                                    pwdate = PwdDate[0] + " " + m_UtilFactory.GetTranslatedText(vrmUsr[0].Language, PwdDate[1]) + " " + PwdDate[2];
                                }
                                errMsg = errMsg.Replace("%s", pwdate).Replace("{0}", (PwdDateTime.Subtract(currentLoginDate).Days + 1).ToString());
                                //ZD 100995
                            }
                        }
                    }
                    //ZD 100781 Ends
                    outXMLSB = new StringBuilder();

                    outXMLSB.Append("<user>");

                    if (fetchconf == null)
                        fetchconf = new Conference(ref obj);

                    fetchconf.FetchMostConfs(vrmUsr[0].companyId, vrmUsr[0].userid, ref outXMLSB);
                    FetchGlobalInfo(vrmUsr[0].companyId, vrmUsr[0].userid, ref outXMLSB, ref obj);

                    if (sysSettings.PushtoGoogle == 1)//ZD 100152
                        FetchGoogleInfo(vrmUsr[0].userid, ref outXMLSB);

                    outXMLSB.Append(errMsg);
                    outXMLSB.Append("</user>");

                    obj.outXml = outXMLSB.ToString();

                    lockcnt = vrmUserStatus.USER_ACTIVE;
                    lockcntrns = 0;


                    if (usrOrg == null)
                    {
                        myvrmEx = new myVRMException(207); //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                vrmUsr[0].LastLogin = currentLoginDate;
                vrmUsr[0].LockStatus = lockcnt;
                vrmUsr[0].lockCntTrns = lockcntrns;
                vrmUsr[0].RequestID = "";//ZD 100263
                m_IuserDao.SaveOrUpdateList(vrmUsr);

                stmt = "INSERT INTO Sys_LoginAudit_D (UserID,LoginDate) Values(" + vrmUsr[0].userid.ToString();
                stmt += ",'" + currentLoginDate.ToString() + "')";

                if (sqlCon == null)
                    sqlCon = new ns_SqlHelper.SqlHelper(obj.ConfigPath);

                sqlCon.OpenConnection();

                sqlCon.ExecuteNonQuery(stmt);

                sqlCon.CloseConnection();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //FB 2558 WhyGo - Starts
        #region UserAuthentication
        /// <summary>
        /// UserAuthentication
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UserAuthentication(ref vrmDataObject obj)
        {
            string sLogin = "", sEmail = "", sPwd = "", sEnPwd = "", sHomeurl = "", UserDateFormat = "", UserDate = "", pwdate = "";//ZD 100995
            string errMsg = "", sAuthenticated = "NO";//SSo Mode
            int lockcnt = 0, lockcntrns = 0, errno = 0;
            bool bRet = true;
            DateTime currentLoginDate = DateTime.Now;
            DateTime currentdate = new DateTime();
            StringBuilder outXMLSB = null;
            vrmSystemFactory validateLicense = null;
            string datetime = ""; //FB 2678
            DateTime PwdDateTime = DateTime.UtcNow; //ZD 100781
            emailFactory m_Email = null; //ZD 100781
            string[] Date, PwdDate; //ZD 100995
            //ZD 101456 Starts
            List<vrmUserPC> usrExtVMRs = null;
            List<ICriterion> PCcriterionList = new List<ICriterion>();
            //ZD 101456 ENds
            bool TDbpassword = false; //102684
            string UserDomain = "";//ZD 103784
            try
            {

                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentLoginDate);
                currentdate = new DateTime(currentLoginDate.Year, currentLoginDate.Month, currentLoginDate.Day, 0, 0, 0);
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node, nodetdb; //102684

                node = xd.SelectSingleNode("//login/homeURL");
                if (node != null)
                {
                    if (node.InnerText != "")
                        sHomeurl = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/userName"); //SSO Fix
                if (node != null)
                {
                    if (node.InnerText != "")
                        sLogin = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(207); //FB 3055 1st Pint
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else
                {
                    node = xd.SelectSingleNode("//login/emailID");
                    if (node != null)
                    {
                        if (node.InnerText != "")
                            sEmail = node.InnerXml.Trim();
                        else
                        {
                            myvrmEx = new myVRMException(207); //FB 3055 1st Pint
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                node = xd.SelectSingleNode("//login/userAuthenticated");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                //ZD 103784 starts
                node = xd.SelectSingleNode("//login/userDomain");//SSo Mode
                if (node != null)
                    if (node.InnerText != "")
                        UserDomain = node.InnerXml.Trim();
                //UserDomain
                //ZD 103784 Ends
                if (sAuthenticated.ToUpper() == "NO")//SSo Mode
                {
                    node = xd.SelectSingleNode("//login/userPassword");
                    if (node != null && node.InnerText != "") //ZD  102684
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        nodetdb = xd.SelectSingleNode("//login/TDBUserPassword");
                        if (nodetdb != null && node == null && nodetdb.InnerText == "1")
                        {
                            List<ICriterion> TdbcriterionList = new List<ICriterion>();

                            if (sLogin.Trim() != "")
                                TdbcriterionList.Add(Expression.Eq("Login", sLogin));
                            else if (sEmail.Trim() != "")
                                TdbcriterionList.Add(Expression.Eq("Email", sEmail));

                            List<vrmUser> vrmUsrTDB = m_IuserDao.GetByCriteria(TdbcriterionList, true);
                            string TDBpassword = "";
                            int TDBUserid = 0;
                            if (vrmUsrTDB.Count == 0)
                            {
                                TDBpassword = "pwd";
                                obj.outXml = "<UserTDBpassword><Userpassword>" + TDBpassword + "</Userpassword></UserTDBpassword>";
                                return false;
                            }
                            else
                            {
                                TDBpassword = vrmUsrTDB[0].Password.ToString();
                                crypto = new cryptography.Crypto();
                                if (TDBpassword != "")
                                    TDBpassword = crypto.decrypt(TDBpassword);
                                TDBUserid = vrmUsrTDB[0].userid;
                                obj.outXml = "<UserTDBpassword><userID>" + TDBUserid + "</userID><Userpassword>" + TDBpassword + "</Userpassword></UserTDBpassword>";
                                return false;
                            }
                            //sPwd = "";
                            //TDbpassword = true;

                        }
                        else
                        {
                            myvrmEx = new myVRMException(530);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                List<ICriterion> criterionList = new List<ICriterion>();

                if (sLogin.Trim() != "")
                {
                    //ZD 103784 starts
                    //criterionList.Add(Expression.Eq("Login", sLogin));
                    m_log.Error("UserName is empty.............................");
                    criterionList.Add(Expression.And(Expression.Eq("Login", sLogin), Expression.Eq("UserDomain", UserDomain)));
                    //ZD 103784 Ends
                }
                else if (sEmail.Trim() != "")
                    criterionList.Add(Expression.Eq("Email", sEmail));

                List<vrmUser> vrmUsr = m_IuserDao.GetByCriteria(criterionList, true);

                if (vrmUsr.Count <= 0)
                {
                    //102684
                    if (!TDbpassword)
                    {
                        myvrmEx = new myVRMException(207); //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                if (vrmUsr[0] == null)
                {
                    //102684
                    if (!TDbpassword)
                    {
                        myvrmEx = new myVRMException(207); //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                OrgData usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmUsr[0].companyId);
                lockcntrns = vrmUsr[0].lockCntTrns;

                if (vrmUsr[0].userid != 11)
                {
                    if (validateLicense == null)
                        validateLicense = new vrmSystemFactory(ref obj);

                    if (!validateLicense.ValidateLicense(sysSettings.license, ref errno, ref datetime)) //FB 2678
                    {
                        if (errno == 515)
                        {
                            vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                            m_IuserDao.Update(vrmUsr[0]);
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                errno = 207;
                            else
                                errno = 263;
                        }
                        myvrmEx = new myVRMException(errno);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (!(currentLoginDate <= sysSettings.ExpiryDate && vrmUsr[0].accountexpiry >= currentLoginDate))
                    {
                        vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                        m_IuserDao.Update(vrmUsr[0]);
                        if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                            myvrmEx = new myVRMException(207);
                        else
                            myvrmEx = new myVRMException(263);

                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    #region  Password Expiration Checking Module
                    if (usrOrg.EnablePasswordExp == 1) //ZD 100781 Starts
                    {
                        if (vrmUsr[0].IsPasswordReset == 1)
                        {
                            PwdDateTime = vrmUsr[0].PasswordTime.AddHours(24);
                            if (PwdDateTime < DateTime.UtcNow)
                            {
                                vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                                m_IuserDao.Update(vrmUsr[0]);

                                if (sysSettings.roomCascadingControl == 1)
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(725);

                                obj.outXml = myvrmEx.FetchErrorMsg(usrOrg.EnablePasswordRule); // ZD 103954

                                //m_Email = new emailFactory(ref obj);
                                //m_Email.ChangePasswordRequest(vrmUsr[0]); // comment for passwordreset new method

                                return false;
                            }

                        }
                        else
                        {
                            PwdDateTime = vrmUsr[0].PasswordTime.AddMonths(usrOrg.PasswordMonths);
                            if (PwdDateTime.Date.AddDays(-1) < DateTime.UtcNow.Date)
                            {
                                vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                                m_IuserDao.Update(vrmUsr[0]);


                                if (sysSettings.roomCascadingControl == 1)
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(725);

                                obj.outXml = myvrmEx.FetchErrorMsg(usrOrg.EnablePasswordRule); // ZD 103954

                                //m_Email = new emailFactory(ref obj);
                                //m_Email.ChangePasswordRequest(vrmUsr[0]); // comment for passwordreset new method

                                return false;
                            }
                        }
                    }
                    # endregion
                    else
                    {
                        if (vrmUsr[0].LockStatus == vrmUserStatus.MAN_LOCKED)
                        {
                            if (vrmUsr[0].lockCntTrns == 10)
                            {
                                if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(425);

                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            else
                            {
                                if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(254);

                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        else if (vrmUsr[0].LockStatus == vrmUserStatus.AUTO_LOCKED)
                        {
                            if (currentLoginDate.Subtract(vrmUsr[0].LastLogin).Minutes < 1)
                            {
                                if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                    myvrmEx = new myVRMException(207);
                                else
                                    myvrmEx = new myVRMException(208);

                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    } //ZD 100781 Ends
                }

                if (sAuthenticated.ToUpper() == "NO")
                {
                    if (vrmUsr[0].Password != sEnPwd)
                    {
                        lockcntrns++;
                        if (lockcntrns >= 3 && vrmUsr[0].userid != 11)
                        {
                            lockcnt = vrmUserStatus.AUTO_LOCKED;
                            if (sysSettings.roomCascadingControl == 1) //ZD 100263 - Error Message Change
                                myvrmEx = new myVRMException(207);
                            else
                                myvrmEx = new myVRMException(208);

                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        else
                        {
                            //102684
                            if (!TDbpassword)
                            {
                                myvrmEx = new myVRMException(207);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                            }
                        }
                        bRet = false;
                    }
                }

                if (bRet)
                {
                    if ((vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days) + 1 < 15)
                    {
                        if (vrmUsr[0].userid != 11)
                        {
                            myvrmEx = new myVRMException(264);
                            //ZD 100995
                            UserDateFormat = vrmUsr[0].DateFormat;
                            UserDate = vrmUsr[0].accountexpiry.ToString(vrmUsr[0].DateFormat);
                            if (usrOrg.EmailDateFormat == 1)
                            {
                                UserDateFormat = "dd MMM yyyy";
                                Date = vrmUsr[0].accountexpiry.ToString(UserDateFormat).Split(' ');
                                UserDate = Date[0] + " " + m_UtilFactory.GetTranslatedText(vrmUsr[0].Language, Date[1]) + " " + Date[2];

                            }
                            errMsg = errMsg.Replace("%s", UserDate).Replace("{0}", (vrmUsr[0].accountexpiry.Subtract(currentLoginDate).Days + 1).ToString());
                            //ZD 100288 Ends
                        }
                    }
                    //ZD 100781 Starts
                    if (usrOrg.EnablePasswordExp == 1)
                    {
                        PwdDateTime = vrmUsr[0].PasswordTime.AddMonths(usrOrg.PasswordMonths);
                        if ((PwdDateTime.Subtract(currentLoginDate).Days) < 4)
                        {
                            if (vrmUsr[0].userid != 11)
                            {
                                myvrmEx = new myVRMException(724);
                                errMsg = myvrmEx.FetchErrorMsg();
                                //ZD 100995
                                UserDateFormat = vrmUsr[0].DateFormat;
                                pwdate = PwdDateTime.ToString(UserDateFormat);
                                if (usrOrg.EmailDateFormat == 1)
                                {
                                    UserDateFormat = "dd MMM yyyy";
                                    PwdDate = PwdDateTime.ToString(UserDateFormat).Split(' ');
                                    pwdate = PwdDate[0] + " " + m_UtilFactory.GetTranslatedText(vrmUsr[0].Language, PwdDate[1]) + " " + PwdDate[2];
                                }
                                errMsg = errMsg.Replace("%s", pwdate).Replace("{0}", (PwdDateTime.Subtract(currentLoginDate).Days + 1).ToString());
                                //ZD 100995
                            }
                        }
                    }
                    //ZD 100781 Ends
                    //FB 2659 - Starts
                    string[] mary = vrmUsr[0].MenuMask.ToString().Split('-');
                    string[] mmary = mary[1].Split('+');
                    string[] ccary = mary[0].Split('*');
                    int usrmenu = Convert.ToInt32(ccary[1]);
                    bool isExpuser = Convert.ToBoolean((((usrmenu & 1) == 1) && ((usrmenu & 2) == 2)) || ((((usrmenu & 2) == 2) && ((usrmenu & 64) == 64))));

                    outXMLSB = new StringBuilder();

                    outXMLSB.Append("<user>");

                    outXMLSB.Append("<globalInfo>");
                    outXMLSB.Append("<userID>" + vrmUsr[0].userid.ToString() + "</userID>");
                    //ZD 101838 start
                    if (vrmUsr[0].enableAV == 1)
                        outXMLSB.Append("<enableAV>1</enableAV>");
                    else
                        outXMLSB.Append("<enableAV>0</enableAV>");
                    if (vrmUsr[0].enableAdditionalOption == 1)
                        outXMLSB.Append("<enableAdditionalOption>1</enableAdditionalOption>");
                    else
                        outXMLSB.Append("<enableAdditionalOption>0</enableAdditionalOption>");
                    if (vrmUsr[0].EnableAVWO == 1)
                        outXMLSB.Append("<enableAVWorkOrder>1</enableAVWorkOrder>");
                    else
                        outXMLSB.Append("<enableAVWorkOrder>0</enableAVWorkOrder>");
                    if (vrmUsr[0].EnableCateringWO == 1)
                        outXMLSB.Append("<enableCateringWO>1</enableCateringWO>");
                    else
                        outXMLSB.Append("<enableCateringWO>0</enableCateringWO>");
                    if (vrmUsr[0].EnableFacilityWO == 1)
                        outXMLSB.Append("<enableFacilityWO>1</enableFacilityWO>");
                    else
                        outXMLSB.Append("<enableFacilityWO>0</enableFacilityWO>");
                    //ZD 101838 End
                    //ZD 101456 Starts
                    if (usrOrg.PCUserLimit > 0 && vrmUsr[0].enablePCUser > 0)
                        outXMLSB.Append("<enableDesktopVideo>1</enableDesktopVideo>");
                    else
                        outXMLSB.Append("<enableDesktopVideo>0</enableDesktopVideo>");
                    PCcriterionList = new List<ICriterion>();
                    usrExtVMRs = new List<vrmUserPC>();
                    PCcriterionList.Add(Expression.Eq("userid", vrmUsr[0].userid));
                    PCcriterionList.Add(Expression.Eq("PCId", vrmPCVendor.BlueJeans));

                    usrExtVMRs = m_IUserPCDAO.GetByCriteria(PCcriterionList);

                    if (usrOrg.EnableBlueJeans > 0 && usrExtVMRs.Count > 0)
                        outXMLSB.Append("<enableBlueJeans>1</enableBlueJeans>");
                    else
                        outXMLSB.Append("<enableBlueJeans>0</enableBlueJeans>");

                    outXMLSB.Append("<StartMode> " + sysSettings.StartMode + "</StartMode>"); // 102638
                    PCcriterionList = new List<ICriterion>();
                    usrExtVMRs = new List<vrmUserPC>();
                    PCcriterionList.Add(Expression.Eq("userid", vrmUsr[0].userid));
                    PCcriterionList.Add(Expression.Eq("PCId", vrmPCVendor.Jabber));
                    usrExtVMRs = m_IUserPCDAO.GetByCriteria(PCcriterionList);

                    if (usrOrg.EnableJabber > 0 && usrExtVMRs.Count > 0)
                        outXMLSB.Append("<enableJabber>1</enableJabber>");
                    else
                        outXMLSB.Append("<enableJabber>0</enableJabber>");

                    PCcriterionList = new List<ICriterion>();
                    usrExtVMRs = new List<vrmUserPC>();
                    PCcriterionList.Add(Expression.Eq("userid", vrmUsr[0].userid));
                    PCcriterionList.Add(Expression.Eq("PCId", vrmPCVendor.Lync));
                    usrExtVMRs = m_IUserPCDAO.GetByCriteria(PCcriterionList);

                    if (usrOrg.EnableLync > 0 && usrExtVMRs.Count > 0)
                        outXMLSB.Append("<enableLync>1</enableLync>");
                    else
                        outXMLSB.Append("<enableLync>0</enableLync>");

                    PCcriterionList = new List<ICriterion>();
                    usrExtVMRs = new List<vrmUserPC>();
                    PCcriterionList.Add(Expression.Eq("userid", vrmUsr[0].userid));
                    PCcriterionList.Add(Expression.Eq("PCId", vrmPCVendor.Vidtel));
                    usrExtVMRs = m_IUserPCDAO.GetByCriteria(PCcriterionList);

                    if (usrOrg.EnableVidtel > 0 && usrExtVMRs.Count > 0)
                        outXMLSB.Append("<enableVidtel>1</enableVidtel>");
                    else
                        outXMLSB.Append("<enableVidtel>0</enableVidtel>");
                    //ZD 101456 Ends

                    if (isExpuser) //FB 2659
                        outXMLSB.Append("<poweruser>0</poweruser>");
                    else
                        outXMLSB.Append("<poweruser>1</poweruser>");

                    outXMLSB.Append("<organizationID>" + vrmUsr[0].companyId + "</organizationID>");
                    outXMLSB.Append("<EnableNetworkFeatures>" + sysSettings.EnableNetworkFeatures + "</EnableNetworkFeatures>");
                    outXMLSB.Append("<roomCascadingControl>" + sysSettings.roomCascadingControl + "</roomCascadingControl>"); //ZD 100263
                    outXMLSB.Append("</globalInfo>");

                    outXMLSB.Append("</user>");
                    obj.outXml = outXMLSB.ToString();

                    lockcnt = vrmUserStatus.USER_ACTIVE;
                    lockcntrns = 0;


                    if (usrOrg == null)
                    {
                        myvrmEx = new myVRMException(207); //FB 3055
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                vrmUsr[0].LastLogin = currentLoginDate;
                vrmUsr[0].LockStatus = lockcnt;
                vrmUsr[0].lockCntTrns = lockcntrns;
                m_IuserDao.SaveOrUpdateList(vrmUsr);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion
        //FB 2558 WhyGo - End
        //FB 2027 Ends

        //FB 1830 - Tranlsation
        #region Get Language Texts
        public bool GetLanguageTexts(ref vrmDataObject obj)
        {
            bool bRet = true;
            int usrID = 0;                        
            StringBuilder m_Translatetext = new StringBuilder();            
            int BrowserLanguageid = 0; //ZD 103531
            List<vrmLanguageText> languagetexts = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                //ZD 103531 - Start
                node = xd.SelectSingleNode("//login/browserlangid");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out BrowserLanguageid);

                if (BrowserLanguageid > 0)
                {
                    languagetexts = m_ILanguagetext.GetByLanguageId(BrowserLanguageid);
                }
                else
                {
                    node = xd.SelectSingleNode("//login/userID");
                    if (node != null)
                        int.TryParse(node.InnerXml.Trim(), out usrID);

                    if (usrID <= 0)
                    {
                        throw new Exception("Invalid User");
                    }

                    vrmUser Usr = m_IuserDao.GetByUserId(usrID);
                    languagetexts = m_ILanguagetext.GetByLanguageId(Usr.Language);
                }
                //ZD 103531 - End

                if (languagetexts != null && languagetexts.Count > 0)
                {
                    m_Translatetext.Append("<Translations>");
                    for (int languagecnt = 0; languagecnt < languagetexts.Count; languagecnt++)
                    {
                        m_Translatetext.Append("<Translation>");
                        m_Translatetext.Append("<uid>" + languagetexts[languagecnt].uId.ToString() + "</uid>");
                        m_Translatetext.Append("<LanguageID>" + languagetexts[languagecnt].LanguageID.ToString() + "</LanguageID>");
                        m_Translatetext.Append("<Text>" + languagetexts[languagecnt].Text.Replace("\r", "").Replace("\n", "").ToString() + "</Text>");//FB 2272
                        m_Translatetext.Append("<TranslatedText>" + languagetexts[languagecnt].TranslatedText.Replace("\r", "").Replace("\n", "").ToString() + "</TranslatedText>");
                        m_Translatetext.Append("</Translation>");
                    }
                    m_Translatetext.Append("</Translations>");
                }
                obj.outXml = m_Translatetext.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //NewLobby Start
        #region GetUserLobbyIcons
        /// <summary>
        /// GetUserLobbyIcons
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <GetUserLobbyIcons><OrganisationID></OrganisationID><userid></userid></GetUserLobbyIcons>
        public bool GetUserLobbyIcons(ref vrmDataObject obj)
        {
            int userID = 0;
            StringBuilder outXML = new StringBuilder();
            try
            {
                vrmIconsRef iconRef = null;
                List<ICriterion> critList = new List<ICriterion>();

                critList.Add(Expression.Eq("UserID", userID));
                List<vrmUserLobby> usrLobbyIcons = m_IUserLobbyDao.GetByCriteria(critList);

                outXML.Append("<GetUserLobbyIcons>");
                if (usrLobbyIcons != null)
                {
                    outXML.Append("<LobbyIcons>");
                    for (int i = 0; i < usrLobbyIcons.Count; i++)
                    {
                        iconRef = m_IIconsRefDAO.GetById(usrLobbyIcons[i].IconID);

                        outXML.Append("<Icon>");
                        outXML.Append("<IconID>" + usrLobbyIcons[i].IconID + "</IconID>");
                        outXML.Append("<Label>" + usrLobbyIcons[i].Label + "</Label>");
                        outXML.Append("<GridPosition>" + usrLobbyIcons[i].GridPosition + "</GridPosition>");

                        if (iconRef != null)
                        {
                            outXML.Append("<IconUri>" + iconRef.IconUri + "</IconUri>");
                            outXML.Append("<IconTarget>" + iconRef.IconTarget + "</IconTarget>");
                        }
                        else
                        {
                            outXML.Append("<IconUri></IconUri>");
                            outXML.Append("<IconTarget></IconTarget>");
                        }
                        outXML.Append("</Icon>");
                    }
                    outXML.Append("</LobbyIcons>");
                }
                outXML.Append("</GetUserLobbyIcons>");

                obj.outXml = outXML.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetUserLobbyIcons: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region SetUserLobbyIcons
        /// <summary>
        /// SetUserLobbyIcons
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML: <SetUserLobbyIcons>
        //<OrganisationID></OrganisationID>
        //<userid></userid>
        //<LobbyIcons>
        //<Icon>  (Node for each icon - start)
        //<GridPosition></GridPosition>
        //<Label></Label>
        //<IconID></IconID>
        //</Icon>  (Node for each icon - start)
        //</LobbyIcons>
        //</SetUserLobbyIcons>

        public bool SetUserLobbyIcons(ref vrmDataObject obj)
        {
            try
            {
                int gridPos = 0, IconId = 0, UserID = 11;
                string selectedColor = "", selectedImage = ""; // ZD 101006
                List<vrmUserLobby> usrLobbyIcons = new List<vrmUserLobby>();
                vrmUserLobby userlobby = null;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNodeList nodes;

                nodes = xd.SelectNodes("//SetIconReference/LobbyIcons/Icon/userID");
                int.TryParse(nodes[0].SelectSingleNode("//SetIconReference/LobbyIcons/Icon/userID").InnerText, out UserID);
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("UserID", UserID));
                usrLobbyIcons = m_IUserLobbyDao.GetByCriteria(criterionList, true);
                for (int i = 0; i < usrLobbyIcons.Count; i++)
                    m_IUserLobbyDao.Delete(usrLobbyIcons[i]);


                nodes = xd.SelectNodes("//SetIconReference/LobbyIcons/Icon");

                if (nodes != null)
                {
                    usrLobbyIcons = new List<vrmUserLobby>();
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        userlobby = new vrmUserLobby();

                        if (nodes[i].SelectSingleNode("userID") != null)
                            int.TryParse(nodes[i].SelectSingleNode("userID").InnerText, out UserID);
                        userlobby.UserID = UserID;

                        if (nodes[i].SelectSingleNode("iconUri") != null)
                        {
                            int.TryParse(nodes[i].SelectSingleNode("iconUri").InnerText, out gridPos);
                            userlobby.GridPosition = gridPos;
                        }

                        if (nodes[i].SelectSingleNode("defaultLabel") != null)
                            userlobby.Label = nodes[i].SelectSingleNode("defaultLabel").InnerText;

                        if (nodes[i].SelectSingleNode("iconID") != null)
                        {
                            int.TryParse(nodes[i].SelectSingleNode("iconID").InnerText, out IconId);
                            userlobby.IconID = IconId;
                        }

                        // ZD 101006 Starts
                        if (nodes[i].SelectSingleNode("SelectedColor") != null)
                            selectedColor = nodes[i].SelectSingleNode("SelectedColor").InnerText;

                        if (nodes[i].SelectSingleNode("SelectedImage") != null)
                            selectedImage = nodes[i].SelectSingleNode("SelectedImage").InnerText;

                        userlobby.SelectedColor = selectedColor;
                        userlobby.SelectedImage = selectedImage;
                        // ZD 101006 Ends

                        usrLobbyIcons.Add(userlobby);
                    }

                    if (usrLobbyIcons.Count > 0)
                        m_IUserLobbyDao.SaveOrUpdateList(usrLobbyIcons);
                }
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SetUserLobbyIcons: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //NewLobby End

        //FB 2158 start
        #region WelcomeNewUser
        /// <summary>
        /// Account email
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool WelcomeNewUser(ref vrmDataObject obj)
        {
            emailFactory m_Email = null;
            try
            {
                string fname = "", lname = "", email = "", password = "";
                int usrlang = 1; //ZD 100580
                StringDictionary newUsr = new StringDictionary();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//saveUser/user/userName/firstName");
                if (node != null)
                    fname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/user/userName/lastName");
                if (node != null)
                    lname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//saveUser/user/password");
                if (node != null)
                {
                    password = node.InnerXml.Trim();
                    crypto = new cryptography.Crypto();
                    if (password != "")
                        password = crypto.decrypt(password);
                }

                //ZD 100580 start
                node = xd.SelectSingleNode("//saveUser/user/languageID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out usrlang);

                if (usrlang <= 0)
                    usrlang = 1;
                //ZD 100580 end

                node = xd.SelectSingleNode("//saveUser/user/userEmail");
                if (node != null)
                    email = node.InnerXml.Trim();

                if (fname == "" || lname == "" || email == "" || password == "")
                {
                    obj.outXml = "User Detail Missed";
                    return false;
                }
                newUsr.Add("fname", fname);
                newUsr.Add("lname", lname);
                newUsr.Add("email", email);
                newUsr.Add("password", password);
                newUsr.Add("orgid", organizationID.ToString());
                newUsr.Add("language", usrlang.ToString()); //ZD 100580

                m_Email = new emailFactory(ref obj);
                m_Email.SendMailtoNewUser(ref newUsr, ref obj);
            }
            catch (Exception e)
            {
                m_log.Error("RequestVRMAccount", e);
                obj.outXml = ""; ;
                return false;
            }
            return true;
        }
        #endregion
        //FB 2158 end

        //FB 2227
        #region SetBridgenumbers
        /// <summary>
        /// Set the dial in number ie video bridge numbers from notes
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool SetBridgenumbers(ref vrmDataObject obj)
        {
            XmlDocument xd = null;
            XmlNode node = null;
            int usrID = 0;
            string externalVideoNumber = "";
            string internalVideoNumber = "";
            vrmUser usr = null;
            try
            {

                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//SetBridgenumbers/userID");

                if (node != null)
                    int.TryParse(node.InnerText, out usrID);

                try
                {
                    usr = m_IuserDao.GetByUserId(usrID);
                }
                catch (Exception ex)
                {
                    obj.outXml = "<error><message>User does not exist</message></error>";
                    return false;
                }

                node = xd.SelectSingleNode("//SetBridgenumbers/Bridgenumber/External");
                if (node != null)
                    externalVideoNumber = node.InnerText;

                node = xd.SelectSingleNode("//SetBridgenumbers/Bridgenumber/Internal");
                if (node != null)
                    internalVideoNumber = node.InnerText;

                usr.InternalVideoNumber = internalVideoNumber;
                usr.ExternalVideoNumber = externalVideoNumber;

                m_IuserDao.SaveOrUpdate(usr);


                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SetUserLobbyIcons: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2268 Request iPhone Starts
        #region SetPhoneNumber
        /// <summary>
        /// SetPhoneNumber
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetPhoneNumber(ref vrmDataObject obj)
        {
            try
            {
                int loginID = 0;
                vrmUser vuser = new vrmUser();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetPhoneNumber/login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out loginID);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((loginID <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetPhoneNumber/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                string phonenumber = "";
                node = xd.SelectSingleNode("//SetPhoneNumber/login/phonenumber");
                if (node != null)
                    phonenumber = node.InnerXml.Trim().ToString();
                else
                {
                    myvrmEx = new myVRMException(605);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (phonenumber.Trim() == "")
                {
                    myvrmEx = new myVRMException(605);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = new vrmUser();
                user = m_IuserDao.GetByUserId(loginID);

                user.companyId = organizationID;
                user.HelpReqPhone = phonenumber;
                m_IuserDao.Update(user);

                obj.outXml = "<success>1</success>";

            }
            catch (Exception e)
            {
                m_log.Error("HelpRequest", e);
                obj.outXml = ""; ;
                return false;
            }
            return true;
        }
        #endregion

        //Exchange

        #region SetDynamicHost
        /// <summary>
        /// SetDynamicHost
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDynamicHost(vrmUserTemplate template, string emailId, string firstName, string lastName, string loginName, int roleId, int userOrigin, bool createUserbyTemplate, ref int orgId, ref int userId, string UsrDomain) //ZD 101525//zD 101865//ZD 102052 //ZD 103784
        {
            try
            {
                string errMsg = "";
                int err = 0, endpointid = 0;
                string password = "pwd";
                int maxusrid = 0;
                vrmUser vuser = new vrmUser();
                //ZD 102052 Starts
                vrmUserRoles userrole = null;

                //ZD 103635
                if (string.IsNullOrEmpty(emailId))
                    return false;

                if (createUserbyTemplate)
                {
                    userrole = m_IUserRolesDao.GetById(template.roleId);

                    if (template.expirationDate <= DateTime.Today)
                    {
                        myvrmEx = new myVRMException(429);
                        return false;
                    }
                    else
                        vuser.accountexpiry = template.expirationDate;

                    if (template.timezone == 0)
                        vuser.TimeZone = sysSettings.TimeZone;
                    else
                        vuser.TimeZone = template.timezone;
                    if (template.groupId == 0)
                        template.groupId = -1;
                    vuser.PreferedGroup = template.groupId;
                    vuser.companyId = template.companyId;
                    vuser.PreferedRoom = template.location;
                    if (string.IsNullOrEmpty(vuser.PreferedRoom))
                        vuser.PreferedRoom = "";//ALLBUGS-81
                    TOTALTIMEFORNEWUSER = template.initialtime;
                    vuser.Language = template.languageId;
                    vuser.roleID = template.roleId;
                    vuser.emailmask = template.emailNotification;
                    vuser.enableParticipants = template.enableParticipants;
                    vuser.enableAV = template.enableAV;
                    vuser.enableAdditionalOption = template.enableAdditionalOption;
                    vuser.enableExchange = template.enableExchange;
                    vuser.enableDomino = template.enableDomino;
                    vuser.enableMobile = template.enableMobile;
                    vuser.EnableAVWO = template.enableAVWO;
                    vuser.EnableCateringWO = template.enableCateringWO;
                    vuser.EnableFacilityWO = template.enableFacilityWO;
                    vuser.DateFormat = template.DateFormat;//ZD 103673
                    vuser.TimeFormat = template.TimeFormat;

                    //Endpoint Details
                    vuser.DefLineRate = template.lineRateId;
                    vuser.connectionType = template.connectiontype;
                    vuser.DefVideoProtocol = template.videoProtocol;
                    vuser.IPISDNAddress = template.ipisdnaddress;
                    vuser.BridgeID = template.bridgeId;
                    vuser.DefaultEquipmentId = template.equipmentId;
                    vuser.outsidenetwork = template.outsideNetwork;

                    vuser.EndPoint.linerateid = template.lineRateId;
                    vuser.EndPoint.connectiontype = template.connectiontype;
                    vuser.EndPoint.protocol = template.videoProtocol;
                    vuser.EndPoint.address = template.ipisdnaddress;
                    vuser.EndPoint.bridgeid = template.bridgeId;
                    vuser.EndPoint.videoequipmentid = template.equipmentId;
                    vuser.EndPoint.profileId = 0;
                    vuser.EndPoint.isDefault = 0;

                }
                else
                {
                    userrole = m_IUserRolesDao.GetById(roleId);

                    vuser.PreferedGroup = -1;
                    vuser.companyId = orgId;
                    vuser.PreferedRoom = "";//ALLBUGS-81
                    vuser.Language = 1;
                    vuser.roleID = roleId;
                    vuser.emailmask = 1;
                    vuser.accountexpiry = sysSettings.ExpiryDate;
                    vuser.TimeZone = sysSettings.TimeZone;
                    vuser.DateFormat = "MM/dd/yyyy"; //ZD 103673 
                    vuser.TimeFormat = "1";

                    vuser.enableParticipants = 1;
                    vuser.enableAV = 1;
                    vuser.enableAdditionalOption = 1;//ZD 101093
                    vuser.enableExchange = vuser.enableDomino = vuser.enableMobile = 0;
                    vuser.EnableAVWO = 1;//ZD 101122
                    vuser.EnableCateringWO = 1;
                    vuser.EnableFacilityWO = 1;

                    //Endpoint Details
                    vuser.DefLineRate = -1;
                    vuser.connectionType = -1;
                    vuser.DefVideoProtocol = 0;
                    vuser.IPISDNAddress = "";
                    vuser.BridgeID = -1;
                    vuser.DefaultEquipmentId = -1;
                    vuser.outsidenetwork = 1;

                    vuser.EndPoint.linerateid = -1;
                    vuser.EndPoint.connectiontype = -1;
                    vuser.EndPoint.videoequipmentid = -1;
                    vuser.EndPoint.protocol = 0;
                    vuser.EndPoint.address = "";
                    vuser.EndPoint.bridgeid = -1;
                    vuser.EndPoint.profileId = 0;
                    vuser.EndPoint.isDefault = 0;

                }
                vuser.userid = 0;

                if (FetchMaxUserID(err, errMsg, null, ref maxusrid))
                    if (err < 0)
                        return false;
                userId = maxusrid + 1;
                vuser.userid = userId;
                vuser.FirstName = firstName;//"Host"; //ZD 101525
                if (lastName == "")
                    lastName = emailId.Split('@')[0];
                vuser.LastName = lastName;
                vuser.Email = emailId;
                vuser.UserOrgin = userOrigin;// zd 101865
                vuser.UserDomain = UsrDomain; //ZD 103784
                //ZD 102052 End
                string pass = string.Empty;
                if (password.Length > 0)
                {
                    crypto = new cryptography.Crypto();
                    pass = crypto.encrypt(password);
                }
                vuser.Password = pass;
                //ZD 102863 starts    
                //vuser.Login = loginName.Split('@')[0];
                if (loginName.IndexOf('@') >= 0)
                    vuser.Login = "";
                else
                    vuser.Login = loginName; //ZD 101525
                //ZD 102863 End
                //ZD 100942 - Start EWS DD Fix - One way
                vuser.PerCalStartTime = DateTime.Today;
                vuser.PerCalEndTime = DateTime.Today;
                vuser.RoomCalStartTime = DateTime.Today;
                vuser.RoomCalEndime = DateTime.Today;
                //ZD 100942 - End
                vuser.MenuMask = userrole.roleMenuMask;
                vuser.UserRolename = userrole.roleName; //ZD 103405
                vuser.Financial = 0;
                vuser.Admin = userrole.level;

                vuser.DoubleEmail = 0;
                vuser.AlternativeEmail = "";
                vuser.WorkPhone = vuser.CellPhone = vuser.ConferenceCode = vuser.LeaderPin = vuser.ParticipantCode = ""; //ZD 101446
                vuser.InternalVideoNumber = vuser.ExternalVideoNumber = vuser.HelpReqEmailID = vuser.HelpReqPhone = "";
                vuser.CCGroup = 0;
                vuser.RoomID = 0;
                vuser.PreferedOperator = 0;
                vuser.lockCntTrns = 0;

                vuser.Deleted = 0;
                vuser.DefAudio = 0;
                vuser.DefVideoSession = 0;
                vuser.EmailClient = -1;
                vuser.newUser = 1;
                vuser.PrefTZSID = 0;
                vuser.initialTime = 0;
                vuser.LockStatus = 0;
                vuser.LastLogin = DateTime.Now;
                vuser.addresstype = -1;
                vuser.approverCount = 0;
                vuser.LevelID = 0;
                vuser.preferredISDN = 0;
                vuser.portletId = 0;
                vuser.searchId = -1;                
                vuser.Timezonedisplay = "1";

                vuser.TickerStatus1 = vuser.TickerStatus = 1;
                vuser.TickerSpeed1 = vuser.TickerSpeed = 3;
                vuser.TickerPosition1 = vuser.TickerPosition = 0;
                vuser.TickerBackground1 = vuser.TickerBackground = "#660066";
                vuser.TickerDisplay1 = vuser.TickerDisplay = 0;
                vuser.RSSFeedLink1 = vuser.RSSFeedLink = "";

                vuser.Audioaddon = "0";
                vuser.DefaultTemplate = vuser.MailBlocked = 0;
                vuser.MailBlockedDate = DateTime.Now;
                vuser.EmailLangId = vuser.PluginConfirmations = vuser.SendSurveyEmail = 0;
                vuser.endpointId = 0;
                //ZD 100781 Starts
                vuser.PasswordTime = DateTime.UtcNow;
                vuser.IsPasswordReset = 0;
                //ZD 100781 Ends
                vuser.LastModifiedUser = 11;
                vuser.StaticID = ""; //GIS TrueDaybook Upgrade Problems
                //ZD 101865 starts
                //ZD 101970 starts
                vuser.PrivateVMR = "";
                vuser.ExternalUserId = "";
                vuser.LandingPageLink = "";
                vuser.LandingPageTitle = "";
                vuser.WebexPassword = "";
                vuser.WebexUserName = "";
                vuser.searchId = 0;
                vuser.BrokerRoomNum = "";
                vuser.RFIDValue = "";
                vuser.AudioDialInPrefix = "";
                vuser.PostLeaderPin = "";
                vuser.PreLeaderPin = "";
                vuser.PreConfCode = "";
                vuser.Type = "";
                vuser.VidyoURL = "";
                vuser.Extension = "";
                vuser.Pin = "";
                vuser.Title = "";
                vuser.securityKey = "";
                vuser.Telephone = "";
                vuser.Company = "";
                //ZD 101970 End

                //ZD 104116 Starts
                if (sysSettings.MaxBlueJeansUsers == -2) //ZD 104116
                {
                    vuser.BJNUserEmail = vuser.Email;
                    vuser.EnableBJNConf = 1;
                }
                //ZD 104116 Ends

                if (userOrigin == 2)
                {
                    vuser.enableExchange = 1;
                    string[] stremailDomain = emailId.Split('@');

                    if (!string.IsNullOrEmpty(stremailDomain[1]))
                    {
                        List<vrmEmailDomain> EmailDomains = m_IEmailDomainDAO.GetOrgIdByEmailDomain(stremailDomain[1]);
                        if (EmailDomains.Count > 0)
                        {
                            orgId = EmailDomains[0].orgId;//ZD 102052
                            vuser.companyId = EmailDomains[0].orgId;
                        }
                        else
                            vuser.companyId = orgId;//ZD 102052
                    }
                    else
                        vuser.companyId = orgId;//ZD 102052
                }
                // zd 101865 ends
                m_IuserDao.Save(vuser);

                if (createUserbyTemplate && template != null && !string.IsNullOrEmpty(template.deptId))
                {
                    vrmUserDepartment userDept = new vrmUserDepartment();
                    string[] deptId = template.deptId.Split(',');
                    for (int d = 0; d < deptId.Length; d++)
                    {
                        if (Convert.ToInt16(deptId[d]) == 0)
                            continue;
                        userDept.userId = vuser.userid;
                        userDept.departmentId = Convert.ToInt16(deptId[d]);
                        m_IuserDeptDAO.Save(userDept);
                    }
                }
                SetDynamicHostEndpoint(ref vuser, ref endpointid);
                //ZD 102052 End
                vrmAccount usracc = new vrmAccount();
                usracc.userId = vuser.userid;
                usracc.TotalTime = TOTALTIMEFORNEWUSER;
                usracc.TimeRemaining = TOTALTIMEFORNEWUSER;
                usracc.UsedMinutes = 0; //ZD 102043
                usracc.ScheduledMinutes = 0; //ZD 102043
                usracc.InitTime = DateTime.Now;
                usracc.ExpirationTime = DateTime.Today.AddYears(10);
                m_IuserAccountDAO.Save(usracc);


                //ZD 104116 Starts
                if (vuser.EnableBJNConf == 1 && !string.IsNullOrEmpty(vuser.BJNUserEmail))
                {
                    if (RTCobj == null)
                        RTCobj = new VRMRTC.VRMRTC();
                    NS_MESSENGER.ConfigParams configParams = new NS_MESSENGER.ConfigParams();
                    bool ret = RTCobj.LoadConfigParams(configPath, ref configParams);
                    if (ret)
                    {
                        BJNxml = "<GetUserDetails><UserID>" + vuser.userid.ToString() + "</UserID><organizationID>" + vuser.companyId.ToString() + "</organizationID><BJNUserEmail>" + vuser.BJNUserEmail + "</BJNUserEmail></GetUserDetails>";

                        NS_OPERATIONS.Operations ops = new NS_OPERATIONS.Operations(configParams);
                        ops.GetUserBJNDetails(BJNxml, ref outRTCXML, ref msg);
                    }
                }
                //ZD 104116 Ends
            }
            catch (Exception e)
            {
                m_log.Error("HelpRequest", e);
                return false;
            }
            return true;
        }
        #endregion

        #region SetDynamicHostEndpoint
        /// <summary>
        /// SetDynamicHost
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDynamicHostEndpoint(ref vrmUser user, ref int endpointid)//ZD 102052
        {
            try
            {
                int pId = 0, eId = 0;

                vrmEndPoint ve = new vrmEndPoint();
                if (endpointid == 0)
                {
                    if (pId == 0)
                    {
                        m_IeptDao.addProjection(Projections.Max("endpointid"));
                        IList maxId = m_IeptDao.GetObjectByCriteria(new List<ICriterion>());
                        if (maxId[0] != null)
                            eId = ((int)maxId[0]) + 1;
                        else
                            eId = 1;
                        m_IeptDao.clearProjection();
                    }
                    ve.endpointid = eId;
                    ve.profileId = pId;
                }
                ve.name = ve.profileName = user.FirstName + user.LastName;
                ve.password = "";
                ve.protocol = -1;
                //ZD 102052 Starts
                if (user.connectionType <= 0)
                    user.connectionType = -1;
                ve.connectiontype = user.connectionType;

                ve.addresstype = -1;
                if (string.IsNullOrEmpty(user.IPISDNAddress))
                    user.IPISDNAddress = "";
                ve.address = user.IPISDNAddress;
                ve.deleted = 0;
                ve.outsidenetwork = user.outsidenetwork;
                if (user.DefaultEquipmentId <= 0)
                    user.DefaultEquipmentId = -1;
                ve.videoequipmentid = user.DefaultEquipmentId;
                if (user.DefLineRate <= 0)
                    user.DefLineRate = 384;
                ve.linerateid = user.DefLineRate;
                if (user.BridgeID <= 0)
                    user.BridgeID = -1;
                ve.bridgeid = user.BridgeID;
                //ZD 102052 End
                ve.endptURL = "";
                ve.isDefault = 0;
                ve.encrypted = 1;
                ve.MCUAddress = "";
                ve.MCUAddressType = -1;
                ve.TelnetAPI = 0;
                ve.SSHSupport = 0;//ZD 101363
                ve.orgId = user.companyId;//ZD 102052
                ve.ExchangeID = "";
                ve.CalendarInvite = 0;
                ve.ApiPortNo = 23;
                ve.ConferenceCode = ve.LeaderPin = "";
                m_IeptDao.Save(ve);
                if (user != null)
                {
                    if (eId == 0)
                        user.endpointId = ve.endpointid;
                    else
                        user.endpointId = eId;

                    m_IuserDao.SaveOrUpdate(user);
                }
            }
            catch (Exception e)
            {
                m_log.Error("HelpRequest", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2693 Starts
        private bool InsertUserPCDetails(XmlNodeList userPCDetails, int userID, ref int AuditPCCount)//ZD 101026
        {
            try
            {
                List<vrmUserPC> UserExtVMRList = null;
                List<vrmUserPC> UserPCList = null;
                vrmUserPC ExtVMRUser = null;
                List<ICriterion> criterionList = null;

                int PCId = 0;
                string Description = "", SkypeURL = "", VCDialinIP = "", VCMeetingID = "";
                string VCDialinSIP = "", VCDialinH323 = "", VCPin = "", PCDialinNum = "", PCDialinFreeNum = "";
                string PCMeetingID = "", PCPin = "", Intructions = "";

                if (userPCDetails != null && userPCDetails.Count > 0)
                {
                    UserExtVMRList = new List<vrmUserPC>();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", userID));
                    UserPCList = m_IUserPCDAO.GetByCriteria(criterionList, true);
                    //ZD 101026 Starts
                    if (UserPCList.Count == 0 && userPCDetails.Count == 0)
                        AuditPCCount = 0;//No Need to check; //No Changes Made
                    else if (UserPCList.Count == userPCDetails.Count)
                        AuditPCCount = 1; //Need to check;
                    else
                        AuditPCCount = 2;//Changes Made;
                    //ZD 101026 End

                    for (int i = 0; i < UserPCList.Count; i++)
                    {
                        m_IUserPCDAO.Delete(UserPCList[i]);
                    }

                    for (int i = 0; i < userPCDetails.Count; i++)
                    {
                        Description = ""; SkypeURL = ""; VCDialinIP = ""; VCMeetingID = "";
                        VCDialinSIP = ""; VCDialinH323 = ""; VCPin = ""; PCDialinNum = ""; PCDialinFreeNum = "";
                        PCMeetingID = ""; PCPin = ""; Intructions = ""; PCId = 0;

                        ExtVMRUser = new vrmUserPC();

                        if (userPCDetails[i].SelectSingleNode("PCId") != null)
                            int.TryParse(userPCDetails[i].SelectSingleNode("PCId").InnerText.Trim(), out PCId);

                        if (userPCDetails[i].SelectSingleNode("Description") != null)
                            Description = userPCDetails[i].SelectSingleNode("Description").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("SkypeURL") != null)
                            SkypeURL = userPCDetails[i].SelectSingleNode("SkypeURL").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCDialinIP") != null)
                            VCDialinIP = userPCDetails[i].SelectSingleNode("VCDialinIP").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCMeetingID") != null)
                            VCMeetingID = userPCDetails[i].SelectSingleNode("VCMeetingID").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCDialinSIP") != null)
                            VCDialinSIP = userPCDetails[i].SelectSingleNode("VCDialinSIP").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCDialinH323") != null)
                            VCDialinH323 = userPCDetails[i].SelectSingleNode("VCDialinH323").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("VCPin") != null)
                            VCPin = userPCDetails[i].SelectSingleNode("VCPin").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCDialinNum") != null)
                            PCDialinNum = userPCDetails[i].SelectSingleNode("PCDialinNum").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCDialinFreeNum") != null)
                            PCDialinFreeNum = userPCDetails[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCMeetingID") != null)
                            PCMeetingID = userPCDetails[i].SelectSingleNode("PCMeetingID").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("PCPin") != null)
                            PCPin = userPCDetails[i].SelectSingleNode("PCPin").InnerText.Trim();

                        if (userPCDetails[i].SelectSingleNode("Intructions") != null)
                            Intructions = userPCDetails[i].SelectSingleNode("Intructions").InnerText.Trim();

                        ExtVMRUser.userid = userID;
                        ExtVMRUser.PCId = PCId;
                        ExtVMRUser.Description = Description;
                        ExtVMRUser.SkypeURL = SkypeURL;
                        ExtVMRUser.VCDialinIP = VCDialinIP;
                        ExtVMRUser.VCDialinSIP = VCDialinSIP;
                        ExtVMRUser.VCDialinH323 = VCDialinH323;
                        ExtVMRUser.VCMeetingID = VCMeetingID;
                        ExtVMRUser.VCPin = VCPin;
                        ExtVMRUser.PCDialinNum = PCDialinNum;
                        ExtVMRUser.PCDialinFreeNum = PCDialinFreeNum;
                        ExtVMRUser.PCMeetingID = PCMeetingID;
                        ExtVMRUser.PCPin = PCPin;
                        ExtVMRUser.Intructions = Intructions;

                        UserExtVMRList.Add(ExtVMRUser);
                    }
                    if (UserExtVMRList.Count > 0)
                        m_IUserPCDAO.SaveOrUpdateList(UserExtVMRList);
                }
                else
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", userID));
                    UserPCList = m_IUserPCDAO.GetByCriteria(criterionList, true);

                    for (int i = 0; i < UserPCList.Count; i++)
                    {
                        m_IUserPCDAO.Delete(UserPCList[i]);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("InsertUserPCDetails", e);
                return false;
            }
        }

        private void GetUserPCDetails(int userID, int PCType, ref StringBuilder UserPCOutxml)
        {
            try
            {
                vrmUserPC UserVMR = null;
                List<ICriterion> critList = new List<ICriterion>();
                UserPCOutxml = new StringBuilder();

                if (userID > 0)
                {
                    UserPCOutxml.Append("<PCDetails>");
                    critList.Add(Expression.Eq("userid", userID));
                    if (PCType > 0)
                        critList.Add(Expression.Eq("PCId", PCType));

                    List<vrmUserPC> usrExtVMRs = m_IUserPCDAO.GetByCriteria(critList);

                    for (int i = 0; i < usrExtVMRs.Count; i++)
                    {
                        UserVMR = usrExtVMRs[i];
                        UserPCOutxml.Append("<PCDetail>");
                        UserPCOutxml.Append("<PCId>" + UserVMR.PCId + "</PCId>");
                        UserPCOutxml.Append("<Description>" + UserVMR.Description + "</Description>");
                        UserPCOutxml.Append("<SkypeURL>" + UserVMR.SkypeURL + "</SkypeURL>");
                        UserPCOutxml.Append("<VCDialinIP>" + UserVMR.VCDialinIP + "</VCDialinIP>");
                        UserPCOutxml.Append("<VCMeetingID>" + UserVMR.VCMeetingID + "</VCMeetingID>");
                        UserPCOutxml.Append("<VCDialinSIP>" + UserVMR.VCDialinSIP + "</VCDialinSIP>");
                        UserPCOutxml.Append("<VCDialinH323>" + UserVMR.VCDialinH323 + "</VCDialinH323>");
                        UserPCOutxml.Append("<VCPin>" + UserVMR.VCPin + "</VCPin>");
                        UserPCOutxml.Append("<PCDialinNum>" + UserVMR.PCDialinNum + "</PCDialinNum>");
                        UserPCOutxml.Append("<PCDialinFreeNum>" + UserVMR.PCDialinFreeNum + "</PCDialinFreeNum>");
                        UserPCOutxml.Append("<PCMeetingID>" + UserVMR.PCMeetingID + "</PCMeetingID>");
                        UserPCOutxml.Append("<PCPin>" + UserVMR.PCPin + "</PCPin>");
                        UserPCOutxml.Append("<Intructions>" + UserVMR.Intructions + "</Intructions>");
                        UserPCOutxml.Append("</PCDetail>");
                    }
                    UserPCOutxml.Append("</PCDetails>");
                }
                else
                    UserPCOutxml.Append("<PCDetails></PCDetails>");
            }
            catch (Exception e)
            {
                m_log.Error("GetUserPCDetaisl", e);
            }
        }

        #region FetchPCDetails

        public bool FetchSelectedPCDetails(ref vrmDataObject obj)
        {
            try
            {
                int PCType = 0;
                int userID = 0;
                StringBuilder outXml = new StringBuilder();
                XmlDocument xd = new XmlDocument();
                myVRMException myVRMEx = null;
                xd.LoadXml(obj.inXml);
                XmlNode node;


                node = xd.SelectSingleNode("//FetchPCDetails/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                node = xd.SelectSingleNode("//FetchPCDetails/PCType");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out PCType);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                GetUserPCDetails(userID, PCType, ref outXml);

                obj.outXml = outXml.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2693 Ends

        //ZD 100157 Starts

        #region SetCalendarTimes
        /// <summary>
        /// Sets the Personal and Room Calendar Start/End Time
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetCalendarTimes(ref vrmDataObject obj)
        {
            XmlDocument xd = null;
            XmlNode node = null;
            int usrID = 0;
            vrmUser usr = null;
            string PerStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string PerEndTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string RoomStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string RoomEndTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            DateTime sysSttime = DateTime.Now;
            DateTime sysEndtime = DateTime.Now;
            int PerShowHrs = 0, RoomShwHrs = 0;

            try
            {

                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//SetCalendarTimes/userID");

                if (node != null)
                    int.TryParse(node.InnerText, out usrID);

                try
                {
                    usr = m_IuserDao.GetByUserId(usrID);
                }
                catch (Exception ex)
                {

                    obj.outXml = "<error><message>User does not exist</message></error>"; //205
                    return false;
                }

                node = xd.SelectSingleNode("//SetCalendarTimes/PerosnalCalendar/ShowPerHrs");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out PerShowHrs);
                    usr.PerCalShowHours = PerShowHrs;
                }
                node = xd.SelectSingleNode("//SetCalendarTimes/PerosnalCalendar/StartTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysSttime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysSttime);
                    usr.PerCalStartTime = sysSttime;
                }

                node = xd.SelectSingleNode("//SetCalendarTimes/PerosnalCalendar/EndTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysEndtime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysEndtime);
                    usr.PerCalEndTime = sysEndtime;
                }
                node = xd.SelectSingleNode("//SetCalendarTimes/RoomCalendar/ShowRoomHrs");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out RoomShwHrs);
                    usr.RoomCalShowHours = RoomShwHrs;
                }
                node = xd.SelectSingleNode("//SetCalendarTimes/RoomCalendar/StartTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysSttime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysSttime);
                    usr.RoomCalStartTime = sysSttime;
                }

                node = xd.SelectSingleNode("//SetCalendarTimes/RoomCalendar/EndTime");
                if (node != null)
                {
                    PerStTime = node.InnerText;
                    DateTime.TryParse(PerStTime, out sysEndtime);

                    timeZone.changeToGMTTime(usr.TimeZone, ref sysEndtime);
                    usr.RoomCalEndime = sysEndtime;
                }

                m_IuserDao.SaveOrUpdate(usr);


                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("SetCalendarTimes: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetCalendarTimes
        /// <summary>
        /// Sets the Personal and Room Calendar Start/End Time
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetCalendarTimes(ref vrmDataObject obj)
        {
            int usrID = 0;
            vrmUser usr = null;

            DateTime PerStTime = DateTime.Now, PerEndTime = DateTime.Now;
            DateTime RoomStrtTime = DateTime.Now, RoomEndTime = DateTime.Now;
            try
            {
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetCalendarTimes/userID");

                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out usrID);

                        if (usrID < 0)
                        {
                            obj.outXml = "<error><message>User does not exist</message></error>"; //205
                            return false;
                        }
                    }
                }

                m_IuserDao.clearFetch();
                usr = m_IuserDao.GetByUserId(usrID);

                OUTXML = new StringBuilder();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetCalendarTimes");
                    xWriter.WriteElementString("userID", usr.userid.ToString());
                    xWriter.WriteStartElement("PerosnalCalendar");
                    xWriter.WriteElementString("ShowPerHrs", usr.PerCalShowHours.ToString());

                    DateTime.TryParse(usr.PerCalStartTime.ToString(), out PerStTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref PerStTime);
                    xWriter.WriteElementString("StartDateTime", PerStTime.ToString());
                    xWriter.WriteElementString("StartTime", PerStTime.ToString("hh:mm tt"));

                    DateTime.TryParse(usr.PerCalEndTime.ToString(), out PerEndTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref PerEndTime);
                    xWriter.WriteElementString("EndDateTime", PerEndTime.ToString());
                    xWriter.WriteElementString("EndTime", PerEndTime.ToString("hh:mm tt"));

                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteStartElement("RoomCalendar");
                    xWriter.WriteElementString("ShowRoomHrs", usr.RoomCalShowHours.ToString());

                    DateTime.TryParse(usr.RoomCalStartTime.ToString(), out RoomStrtTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref RoomStrtTime);
                    xWriter.WriteElementString("StartDateTime", RoomStrtTime.ToString());
                    xWriter.WriteElementString("StartTime", RoomStrtTime.ToString("hh:mm tt"));

                    DateTime.TryParse(usr.RoomCalEndime.ToString(), out RoomEndTime);
                    timeZone.GMTToUserPreferedTime(usr.TimeZone, ref RoomEndTime);
                    xWriter.WriteElementString("EndDateTime", RoomEndTime.ToString());
                    xWriter.WriteElementString("EndTime", RoomEndTime.ToString("hh:mm tt"));
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = OUTXML.ToString();
                    return true;
                }
            }
            catch (Exception e)
            {
                m_log.Error("GetCalendarTimes: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //ZD 100157 Ends

        //ZD 100263 Starts
        #region CheckUserRights
        /// <summary>
        /// CheckUserRights
        /// </summary>
        /// <param name="LoginUser"></param>
        /// <param name="ModifiedUser"></param>
        /// <returns></returns>
        public bool CheckUserRights(vrmUser LoginUser, vrmUser ModifiedUser)
        {
            vrmUserRoles ModifiedUserRole = null;
            vrmUserRoles LoginUserRole = null;

            try
            {
                if (LoginUser.userid != ModifiedUser.userid && ModifiedUser.userid == 11) //ZD 100263//Only VRM Admin can change thier settings not other users  
                    return false;

                if (LoginUser.Admin == 2)
                {
                    ModifiedUserRole = m_IUserRolesDao.GetById(ModifiedUser.roleID);
                    LoginUserRole = m_IUserRolesDao.GetById(LoginUser.roleID);

                    if (ModifiedUserRole == null || LoginUserRole == null)
                        return false;

                    if ((LoginUserRole.crossaccess == 1))
                        return true;
                    if (ModifiedUserRole.crossaccess == 1 && LoginUserRole.crossaccess == 0)
                        return false;

                    if ((LoginUserRole.crossaccess == 0 && ModifiedUserRole.crossaccess == 0))
                    {
                        if ((LoginUser.companyId == ModifiedUser.companyId))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("CheckUserRights failed" + ex.Message);
                return false;
            }
        }
        #endregion
        //ZD 100263 End

        //ZD 100781 Starts
        #region PasswordChangeRequest
        /// <summary>
        /// PasswordChangeRequest
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool PasswordChangeRequest(ref vrmDataObject obj)
        {
            XmlDocument xd = null;
            XmlNode node = null;
            int usrID = 0;
            vrmUser usr = null;
            string userComment = "", type = "", pwd = "";
            List<ICriterion> criterionAdminList = new List<ICriterion>();
            emailFactory m_Email = null;
            List<ICriterion> icriterion = new List<ICriterion>();
            List<vrmUserRoles> userrole = new List<vrmUserRoles>();
            try
            {

                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//PasswordChangeRequest/sendRequestInfo/userID");

                if (node != null)
                    int.TryParse(node.InnerText, out usrID);

                try
                {
                    usr = m_IuserDao.GetByUserId(usrID);
                }
                catch (Exception ex)
                {
                    obj.outXml = "<error><message>User does not exist</message></error>";
                    return false;
                }

                node = xd.SelectSingleNode("//PasswordChangeRequest/sendRequestInfo/userComment");
                if (node != null)
                    userComment = node.InnerText.Trim();

                node = xd.SelectSingleNode("//PasswordChangeRequest/sendRequestInfo/type");
                if (node != null)
                    type = node.InnerText.Trim();

                node = xd.SelectSingleNode("//PasswordChangeRequest/sendRequestInfo/password");
                if (node != null)
                    pwd = node.InnerText.Trim();

                icriterion.Add(Expression.Eq("level", 2));
                icriterion.Add(Expression.Eq("crossaccess", 1));
                userrole = m_IUserRolesDao.GetByCriteria(icriterion, true);

                if (usr.roleID == userrole[0].roleID)
                    criterionAdminList.Add(Expression.Eq("roleID", usr.roleID));

                criterionAdminList.Add(Expression.Eq("companyId", usr.companyId));
                criterionAdminList.Add(Expression.Eq("Deleted", 0));
                criterionAdminList.Add(Expression.Eq("Admin", 2));
                List<vrmUser> results = m_IuserDao.GetByCriteria(criterionAdminList, true);



                if (type == "U")
                {
                    m_Email = new emailFactory(ref obj);
                    m_Email.SendRequestAdmin(usr, results, userComment);
                }
                else
                {
                    if (!string.IsNullOrEmpty(pwd))
                        usr.Password = pwd;
                    usr.LockStatus = 0;
                    usr.IsPasswordReset = 1;
                    usr.PasswordTime = DateTime.UtcNow;

                    m_IuserDao.SaveOrUpdate(usr);

                    m_Email = new emailFactory(ref obj);
                    m_Email.NotifyNewPassword(usr, results);
                }

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("PasswordChangeRequest: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion


        //ZD 100781 Ends
        //ZD 100152 Starts
        #region SetUserToken
        /// <summary>
        /// upates the Google User Token
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUserToken(ref vrmDataObject obj)
        {
            Tuple<int, string> usertp = null; //ALLDEV-856
            try
            {
                vrmUserToken _usertoken = null;
                XmlDocument _xdoc = new XmlDocument();
                _xdoc.LoadXml(obj.inXml);
                XmlNode _node;
                cryptography.Crypto _crypto = new cryptography.Crypto();


                int _userID = 0;
                _node = _xdoc.SelectSingleNode("UserToken/UserId");
                if (_node != null)
                    int.TryParse(_node.InnerXml.Trim(), out _userID);

                usertp = new Tuple<int, string>(_userID, ""); //ALLDEV-856

                _usertoken = m_IUserTokenDAO.GetByUserId(usertp); //ALLDEV-856
                if (_usertoken == null)
                    _usertoken = new vrmUserToken();

                string _UserEmail = _usertoken.UserEmail;
                _node = _xdoc.SelectSingleNode("UserToken/UserEmail");
                if (_node != null)
                    _UserEmail = _node.InnerXml.Trim();

                int _orgid = _usertoken.orgid;
                _node = _xdoc.SelectSingleNode("//UserToken/organizationID");
                if (_node != null)
                    int.TryParse(_node.InnerXml.Trim(), out _orgid);

                string _AccessToken = _usertoken.AccessToken;
                _node = _xdoc.SelectSingleNode("//UserToken/AccessToken");
                if (_node != null)
                    _AccessToken = _crypto.encrypt(_node.InnerXml.Trim());

                string _RefreshToken = _usertoken.RefreshToken;
                _node = _xdoc.SelectSingleNode("//UserToken/RefreshToken");
                if (_node != null)
                    _RefreshToken = _crypto.encrypt(_node.InnerXml.Trim());

                string _ChannelID = _usertoken.ChannelID;
                _node = _xdoc.SelectSingleNode("//UserToken/ChannelID");
                if (_node != null)
                    _ChannelID = _crypto.encrypt(_node.InnerXml.Trim());

                string _ChannelEtag = _usertoken.ChannelEtag;
                _node = _xdoc.SelectSingleNode("//UserToken/ChannelEtag");
                if (_node != null)
                    _ChannelEtag = _crypto.encrypt(_node.InnerXml.Trim());

                DateTime _ChannelExpiration = new DateTime();
                _ChannelExpiration = _usertoken.ChannelExpiration;
                _node = _xdoc.SelectSingleNode("//UserToken/ChannelExpiration");
                if (_node != null)
                    _ChannelExpiration = DateTime.Parse(_node.InnerXml.Trim());
                else
                    _ChannelExpiration = DateTime.UtcNow; //ALLDEV-856

                string _ChannelResourceId = _usertoken.ChannelResourceID;
                _node = _xdoc.SelectSingleNode("//UserToken/ChannelResourceId");
                if (_node != null)
                    _ChannelResourceId = _crypto.encrypt(_node.InnerXml.Trim());

                string _ChannelResourceUri = _usertoken.ChannelResourseURL;
                _node = _xdoc.SelectSingleNode("//UserToken/ChannelResourceUri");
                if (_node != null)
                    _ChannelResourceUri = _crypto.encrypt(_node.InnerXml.Trim());



                _usertoken.userid = _userID;
                _usertoken.UserEmail = _UserEmail;
                _usertoken.orgid = _orgid;
                _usertoken.AccessToken = _AccessToken;
                _usertoken.RefreshToken = _RefreshToken;
                _usertoken.ChannelID = _ChannelID;
                _usertoken.ChannelEtag = _ChannelEtag;
                _usertoken.ChannelExpiration = _ChannelExpiration;
                _usertoken.ChannelResourceID = _ChannelResourceId;
                _usertoken.ChannelResourseURL = _ChannelResourceUri;

                m_IUserTokenDAO.SaveOrUpdate(_usertoken);


                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetGoogleChannelExpiredList
        /// <summary>
        /// Get the List of Expired channel
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetGoogleChannelExpiredList(ref vrmDataObject obj)
        {


            try
            {
                _OUTXML = new StringBuilder();
                XmlDocument _xd = new XmlDocument();

                sysSettings _sys = new sysSettings(m_configPath);
                sysMailData _MailData = new sysMailData();
                _sys.getSysMail(ref _MailData);

                List<ICriterion> _UsrListCriterion = new List<ICriterion>();
                cryptography.Crypto _crypto = new cryptography.Crypto();


                if (sysSettings.PushtoGoogle == 1)
                {
                    _UsrListCriterion.Add(Expression.Le("ChannelExpiration", DateTime.UtcNow));
                    List<vrmUserToken> _UserList = m_IUserTokenDAO.GetByCriteria(_UsrListCriterion);

                    if (_UserList != null && _UserList.Count > 0)
                    {
                        using (_xWriter = XmlWriter.Create(_OUTXML, _xSettings))
                        {
                            _xSettings = new XmlWriterSettings();
                            _xSettings.OmitXmlDeclaration = true;

                            _xWriter.WriteStartElement("GetGoogleChannelExpiredList");
                            _xWriter.WriteElementString("GoogleClientID", _crypto.decrypt(sysSettings.GoogleClientID));
                            _xWriter.WriteElementString("GoogleSecretID", _crypto.decrypt(sysSettings.GoogleSecretID));
                            _xWriter.WriteElementString("ChannelAddress", _MailData.websiteURL);
                            _xWriter.WriteElementString("GoogleIntegration", sysSettings.GoogleIntegration.ToString()); //ALLDEV-856
                            _xWriter.WriteStartElement("UserList");
                            for (int i = 0; i < _UserList.Count; i++)
                            {
                                _xWriter.WriteStartElement("User");
                                _xWriter.WriteElementString("RefreshToken", _crypto.decrypt((_UserList[0].RefreshToken)));
                                _xWriter.WriteElementString("UserID", _UserList[0].userid.ToString());
                                _xWriter.WriteElementString("UserEmail", _UserList[0].UserEmail);
                                _xWriter.WriteFullEndElement(); //ZD 101086
                            }
                            _xWriter.WriteFullEndElement(); //ZD 101086
                            _xWriter.WriteFullEndElement(); //ZD 101086
                            _xWriter.Flush();
                        }
                    }
                }
                obj.outXml = _OUTXML.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetGoogleChannelDetails
        /// <summary>
        /// Get the channel details of the user
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetGoogleChannelDetails(ref vrmDataObject obj)
        {
            int UserID = 0;
            try
            {
                _OUTXML = new StringBuilder();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetGoogleChannelDetails/UserID");
                if (node != null)
                {
                    if (node.InnerText != "")
                        int.TryParse(node.InnerXml.Trim(), out UserID);
                }

                if (sysSettings.PushtoGoogle == 1)
                    FetchGoogleInfo(UserID, ref _OUTXML);
                else
                    _OUTXML.Append("<GoogleInfo><PushtoGoogle>0</PushtoGoogle><Google>" + sysSettings.PushtoGoogle.ToString() + "</Google></GoogleInfo>"); //ALLDEV-856
                obj.outXml = _OUTXML.ToString();

                return true;
            }
            catch (Exception ex)
            {

                m_log.Error("sytemException" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //ZD 100152 Ends

        //ZD 100664 Starts

        #region SetAuditUser
        /// <summary>
        /// SetAuditUser
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="LastModified"></param>
        /// <returns></returns>
        private bool SetAuditUser(int isNewUser, int userId, DateTime LastModifiedDate, int LastModifiedUser, ref int AuditPCCount)
        {
            StringBuilder stmt = new StringBuilder();
            int userCount = 0;
            object returnVal = null;
            string description = "No Changes Made", ModifiedDetails = "";
            try
            {
                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);

                m_rptLayer.OpenConnection();

                stmt = new StringBuilder();
                stmt.Append(" Insert into Audit_Usr_List_D ( ");
                stmt.Append(" [UserID],[FirstName],[LastName],[Email],[Financial],[Admin],[Login],[Password],[Company],");
                stmt.Append(" [TimeZone],[Language],[PreferedRoom],[AlternativeEmail],[DoubleEmail],[PreferedGroup],[CCGroup],");
                stmt.Append(" [Telephone],[RoomID],[PreferedOperator],[lockCntTrns],[DefLineRate],[DefVideoProtocol],[Deleted],");
                stmt.Append(" [DefAudio],[DefVideoSession],[MenuMask],[initialTime],[EmailClient],[newUser],[PrefTZSID],");
                stmt.Append(" [IPISDNAddress],[DefaultEquipmentId],[connectionType],[companyId],[securityKey],[LockStatus],");
                stmt.Append(" [LastLogin],[outsidenetwork],[emailmask],[roleID],[addresstype],[accountexpiry],[approverCount],");
                stmt.Append(" [BridgeID],[Title],[LevelID],[preferredISDN],[portletId],[searchId],[endpointId],[DateFormat],");
                stmt.Append(" [enableAV],[timezonedisplay],[timeformat],[enableParticipants],[enableAVWO],[enableCateringWO],[enableFacilityWO],[TickerStatus],[TickerSpeed],");//ZD 101122
                stmt.Append(" [TickerPosition],[TickerBackground],[TickerDisplay],[RSSFeedLink],[TickerStatus1],[TickerSpeed1],");
                stmt.Append(" [TickerPosition1],[TickerBackground1],[TickerDisplay1],[RSSFeedLink1],[enableExchange],[enableDomino],");
                stmt.Append(" [Audioaddon],[WorkPhone],[CellPhone],[ConferenceCode],[LeaderPin],[DefaultTemplate],[EmailLangId],");
                stmt.Append(" [MailBlocked],[MailBlockedDate],[enableMobile],[PluginConfirmations],[InternalVideoNumber], ");
                stmt.Append(" [ExternalVideoNumber],[HelpReqEmailID],[HelpReqPhone],[SendSurveyEmail],[PrivateVMR],");
                stmt.Append(" [EnableVNOCselection],[EntityID],[Extension],[Pin],[MemberID],[isLocked],[VidyoURL],[Type], ");
                stmt.Append(" [allowPersonalMeeting],[allowCallDirect],[ESUserID],[ParentReseller],[CorpUser],[Region], ");
                stmt.Append(" [Secured],[PreConfCode],[PreLeaderPin],[PostLeaderPin],[AudioDialInPrefix],[enablePCUser], ");
                stmt.Append(" [RFIDValue],[BrokerRoomNum],[PerCalStartTime],[PerCalEndTime],[RoomCalStartTime],[RoomCalEndime], ");
                stmt.Append(" [PerCalShowHours],[RoomCalShowHours],[RequestID],[StaticID],[IsStaticIDEnabled],[enableWebexUser],");
                stmt.Append(" [WebexUserName],[WebexPassword],[LandingPageTitle],[LandingPageLink],[VirtualRoomId],[UserDomain],");//ZD 104850
                stmt.Append(" [LastModifiedUser],[PasswordTime],[IsPasswordReset],[lastmodifieddate],[enableAdditionalOption],[ParticipantCode],[EnableBJNConf],[BJNUserName]) "); //ZD 101093 //ZD 101446 //ZD 104021
                stmt.Append(" (Select [UserID],[FirstName],[LastName],[Email],[Financial],[Admin],[Login],[Password],[Company],");
                stmt.Append(" [TimeZone],[Language],[PreferedRoom],[AlternativeEmail],[DoubleEmail],[PreferedGroup],[CCGroup],");
                stmt.Append(" [Telephone],[RoomID],[PreferedOperator],[lockCntTrns],[DefLineRate],[DefVideoProtocol],[Deleted],");
                stmt.Append(" [DefAudio],[DefVideoSession],[MenuMask],[initialTime],[EmailClient],[newUser],[PrefTZSID],");
                stmt.Append(" [IPISDNAddress],[DefaultEquipmentId],[connectionType],[companyId],[securityKey],[LockStatus],");
                stmt.Append(" [LastLogin],[outsidenetwork],[emailmask],[roleID],[addresstype],[accountexpiry],[approverCount],");
                stmt.Append(" [BridgeID],[Title],[LevelID],[preferredISDN],[portletId],[searchId],[endpointId],[DateFormat],");
                stmt.Append(" [enableAV],[timezonedisplay],[timeformat],[enableParticipants],[enableAVWO],[enableCateringWO],[enableFacilityWO],[TickerStatus],[TickerSpeed],");
                stmt.Append(" [TickerPosition],[TickerBackground],[TickerDisplay],[RSSFeedLink],[TickerStatus1],[TickerSpeed1],");
                stmt.Append(" [TickerPosition1],[TickerBackground1],[TickerDisplay1],[RSSFeedLink1],[enableExchange],[enableDomino],");
                stmt.Append(" [Audioaddon],[WorkPhone],[CellPhone],[ConferenceCode],[LeaderPin],[DefaultTemplate],[EmailLangId],");
                stmt.Append(" [MailBlocked],[MailBlockedDate],[enableMobile],[PluginConfirmations],[InternalVideoNumber], ");
                stmt.Append(" [ExternalVideoNumber],[HelpReqEmailID],[HelpReqPhone],[SendSurveyEmail],[PrivateVMR],");
                stmt.Append(" [EnableVNOCselection],[EntityID],[Extension],[Pin],[MemberID],[isLocked],[VidyoURL],[Type], ");
                stmt.Append(" [allowPersonalMeeting],[allowCallDirect],[ESUserID],[ParentReseller],[CorpUser],[Region], ");
                stmt.Append(" [Secured],[PreConfCode],[PreLeaderPin],[PostLeaderPin],[AudioDialInPrefix],[enablePCUser], ");
                stmt.Append(" [RFIDValue],[BrokerRoomNum],[PerCalStartTime],[PerCalEndTime],[RoomCalStartTime],[RoomCalEndime], ");
                stmt.Append(" [PerCalShowHours],[RoomCalShowHours],[RequestID],[StaticID],[IsStaticIDEnabled],[enableWebexUser],");
                stmt.Append(" [WebexUserName],[WebexPassword],[LandingPageTitle],[LandingPageLink],[VirtualRoomId],[UserDomain],");//ZD 104850
                stmt.Append(" [LastModifiedUser],[PasswordTime],[IsPasswordReset],[lastmodifieddate],[enableAdditionalOption],[ParticipantCode],[EnableBJNConf],[BJNUserName]"); //ZD 101446 //ZD 104021 
                stmt.Append(" from Usr_List_D where UserId = " + userId + ")");
                m_rptLayer.ExecuteNonQuery(stmt.ToString());

                if (isNewUser == MODIFYOLD)
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select count(*) from ( ");
                    stmt.Append(" select distinct  ");
                    stmt.Append(" [accountexpiry], [addresstype], [Admin], [allowCallDirect], [allowPersonalMeeting], [AlternativeEmail], ");
                    stmt.Append(" [Audioaddon], [AudioDialInPrefix], [BridgeID], [BrokerRoomNum], [CCGroup], [CellPhone], ");
                    stmt.Append(" [Company], [companyId], [ConferenceCode], [connectionType], [CorpUser], [DateFormat], [DefAudio], ");
                    stmt.Append(" [DefaultEquipmentId], [DefaultTemplate], [DefLineRate], [DefVideoProtocol], [DefVideoSession], [Deleted], ");
                    stmt.Append(" [DoubleEmail], [Email], [EmailClient], [EmailLangId], [emailmask], [enableAV], [enableDomino], [enableExchange], ");
                    stmt.Append(" [enableMobile], [enableParticipants], [enableAVWO],[enableCateringWO],[enableFacilityWO], [enablePCUser], [EnableVNOCselection], [enableWebexUser], [endpointId], ");//ZD 101122
                    stmt.Append(" [ESUserID], [Extension], [ExternalVideoNumber], [Financial], [FirstName], ");
                    stmt.Append(" [HelpReqEmailID], [HelpReqPhone],  [initialTime], [InternalVideoNumber], [IPISDNAddress], ");
                    stmt.Append(" [IsPasswordReset], ISNULL([IsStaticIDEnabled], '0') as IsStaticIDEnabled, [LandingPageLink], [LandingPageTitle], [Language], ");
                    stmt.Append(" [LastName], [LeaderPin], [LevelID], [Login], [MailBlocked], [MailBlockedDate], ");
                    stmt.Append(" [outsidenetwork], [ParentReseller], [Password], ");
                    stmt.Append(" [Pin], [PluginConfirmations], [portletId], ");
                    stmt.Append(" [PostLeaderPin], [PreConfCode], [PreferedGroup], [PreferedOperator], [PreferedRoom], [preferredISDN], ");
                    stmt.Append(" [PrefTZSID], [PreLeaderPin], [PrivateVMR], [Region], [RFIDValue], [roleID],  ");
                    stmt.Append(" [RoomID], [RSSFeedLink], [RSSFeedLink1], [searchId], [Secured], ");
                    stmt.Append(" [securityKey], [SendSurveyEmail], ISNULL([StaticID], '') as StaticID, [Telephone], [TickerBackground], [TickerBackground1], ");
                    stmt.Append(" [TickerDisplay], [TickerDisplay1], [TickerPosition], [TickerPosition1], [TickerSpeed], [TickerSpeed1], ");
                    stmt.Append(" [TickerStatus], [TickerStatus1], [timeformat], [TimeZone], [timezonedisplay], [Title], [Type], [UserID],[UserDomain],");//ZD 104850
                    stmt.Append(" [VidyoURL], [WebexPassword], [WebexUserName], [WorkPhone],[enableAdditionalOption],[ParticipantCode],[EnableBJNConf],[BJNUserName] "); //ZD 101093 //ZD 101446 //ZD 104021 
                    stmt.Append(" from (");
                    stmt.Append(" select top 2 * from Audit_Usr_List_D where userId = " + userId + "");
                    stmt.Append(" order by Lastmodifieddate desc");
                    stmt.Append(" ) as x ) as y");
                    returnVal = m_rptLayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out userCount);
                    if (userCount > 1)
                        description = "Changes Made";
                }


                if (isNewUser == MODIFYOLD && description == "No Changes Made")
                {
                    userCount = 0;
                    stmt = new StringBuilder();
                    stmt.Append(" select COUNT(*) from  ( ");
                    stmt.Append(" select DepartmentId from  Usr_Dept_D where userId = " + userId + "  ");
                    stmt.Append(" and DepartmentId not in (select DepartmentId from Audit_Usr_Dept_D where userId = " + userId + " and  ");
                    stmt.Append(" LastModifiedDate = (select MAX(Lastmodifieddate) from Audit_Usr_Dept_D ");
                    stmt.Append(" where userId = " + userId + ")) ");
                    stmt.Append(" union  ");
                    stmt.Append(" select DepartmentId from  Audit_Usr_Dept_D where userId = " + userId + "  and ");
                    stmt.Append(" LastModifiedDate = (select MAX(Lastmodifieddate) from Audit_Usr_Dept_D ");
                    stmt.Append(" where userId = " + userId + ") ");
                    stmt.Append(" and DepartmentId not in (select DepartmentId from Usr_Dept_D where userId = " + userId + ") ");
                    stmt.Append(" )  as x  ");
                    returnVal = m_rptLayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out userCount);
                    if (userCount > 0)
                        description = "Changes Made";
                }

                stmt = new StringBuilder();
                stmt.Append(" Insert into Audit_Usr_Dept_D ");
                stmt.Append(" ([userId], [DepartmentId], [LastModifiedDate])");
                stmt.Append(" select [UserId], [DepartmentId], '" + LastModifiedDate + "'");
                stmt.Append(" from Usr_Dept_D where userId=" + userId);
                m_rptLayer.ExecuteNonQuery(stmt.ToString());

                stmt = new StringBuilder();
                stmt.Append(" Insert into [Audit_Acc_Balance_D] ([ExpirationTime], [InitTime], [orgId], [TimeRemaining],  ");
                stmt.Append(" [TotalTime], [UserID], [UsedMinutes], [ScheduledMinutes], [LastModifiedDate]) "); //ZD 102043
                stmt.Append(" ( select [ExpirationTime], [InitTime], [orgId], [TimeRemaining], ");
                stmt.Append(" [TotalTime], [UserID], [UsedMinutes], [ScheduledMinutes], GETUTCDATE() from Acc_Balance_D) "); //ZD 102043
                m_rptLayer.ExecuteNonQuery(stmt.ToString());

                if (isNewUser == MODIFYOLD && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" Select count(*) from ( ");
                    stmt.Append(" Select distinct  ");
                    stmt.Append(" userID, [TotalTime], [orgId] ");
                    stmt.Append(" from (");
                    stmt.Append(" select top 2 * from [Audit_Acc_Balance_D] where userId = " + userId + "");
                    stmt.Append(" order by Lastmodifieddate desc");
                    stmt.Append(" ) as x ) as y");
                    returnVal = m_rptLayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out userCount);
                    if (userCount > 1)
                        description = "Changes Made";
                }

                stmt = new StringBuilder();
                stmt.Append(" Insert into Audit_Usr_PCDetails_D ");
                stmt.Append(" ([Description], [Intructions], [PCDialinFreeNum], [PCDialinNum], [PCId], ");
                stmt.Append(" [PCMeetingID], [PCPin], [SkypeURL], [userid], [VCDialinH323], [VCDialinIP], [VCDialinSIP], [VCMeetingID], [VCPin], [LastModifiedDate])");
                stmt.Append(" select [Description], [Intructions], [PCDialinFreeNum], [PCDialinNum], [PCId],");
                stmt.Append(" [PCMeetingID], [PCPin], [SkypeURL], [userid], [VCDialinH323], [VCDialinIP], [VCDialinSIP], [VCMeetingID], [VCPin], '" + LastModifiedDate + "'");
                stmt.Append(" from Usr_PCDetails_D where userId=" + userId);
                m_rptLayer.ExecuteNonQuery(stmt.ToString());


                if (AuditPCCount == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditPCCount == 2)
                    description = "Changes Made";

                if (isNewUser == MODIFYOLD && AuditPCCount == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select  ");
                    stmt.Append(" case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0 ");
                    stmt.Append(" end as cnt ");
                    stmt.Append(" from  ");
                    stmt.Append(" ( ");
                    stmt.Append(" select COUNT(*) as disCount,  ");
                    stmt.Append(" (select count (*) from [Audit_Usr_PCDetails_D] where userid = " + userId + " ");
                    stmt.Append(" and LastModifiedDate =  ");
                    stmt.Append(" (SELECT TOP 1 LastModifiedDate From ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_Usr_PCDetails_D] where userid = " + userId + " ORDER BY LastModifiedDate DESC) as x  ");
                    stmt.Append(" ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [Usr_PCDetails_D] where userid = " + userId + ") as actCnt ");
                    stmt.Append(" from ");
                    stmt.Append(" ( ");
                    stmt.Append(" select distinct [Description] , [Intructions], [PCDialinFreeNum],  ");
                    stmt.Append(" [PCDialinNum], [PCId], [PCMeetingID], [PCPin], [SkypeURL], [userid], [VCDialinH323], [VCDialinIP],  ");
                    stmt.Append(" [VCDialinSIP], [VCMeetingID], [VCPin] ");
                    stmt.Append(" from ");
                    stmt.Append(" (select * from [Audit_Usr_PCDetails_D] where userid = " + userId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_Usr_PCDetails_D] where userid = " + userId + "  ");
                    stmt.Append(" order by LastModifiedDate desc)) as x ");
                    stmt.Append(" ) as y ");
                    stmt.Append(" ) as z ");
                    returnVal = m_rptLayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out userCount);
                    if (userCount > 0)
                        description = "Changes Made";
                }

                if (isNewUser == MODIFYOLD && description == "No Changes Made")
                {
                    DataSet ds = new DataSet();
                    string txtEpt = "";
                    stmt = new StringBuilder();
                    stmt.Append(" select top 1 a.ValueChange from [Audit_Ept_List_D] a, [Usr_List_D] b  ");
                    stmt.Append(" where a.endpointId = b.endpointId and b.UserID = " + userId + " order by a.Lastmodifieddate desc ");
                    ds = m_rptLayer.ExecuteDataSet(stmt.ToString());
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["ValueChange"] != null)
                            txtEpt = ds.Tables[0].Rows[0]["ValueChange"].ToString();

                        if (txtEpt == "Changes Made")
                            description = "Changes Made";
                    }
                }

                if (isNewUser == CREATENEW)
                    description = "Created";

                stmt = new StringBuilder();
                stmt.Append(" Update Audit_Usr_List_D Set ValueChange = ' " + description + "' where UserId = " + userId + " and LastModifiedDate = '" + LastModifiedDate + "'");
                stmt.Append(" Insert into Audit_Summary_UsrList_D ");
                stmt.Append(" ([Description], [LastModifiedDate], [LastModifiedUser], [userId], [ModifiedDetails])");
                stmt.Append(" VALUES ('" + description + "', '" + LastModifiedDate + "', " + LastModifiedUser + ", " + userId + ", '" + ModifiedDetails + "')");

                m_rptLayer.ExecuteNonQuery(stmt.ToString());
                m_rptLayer.CloseConnection();
            }
            catch (Exception ex)
            {
                m_log.Error("SetAuditUser Failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetAuditUser
        /// <summary>
        /// GetAuditUser
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="LoginUserTimezone"></param>
        /// <param name="auditOutXML"></param>
        /// <returns></returns>
        private bool GetAuditUser(int userId, int LoginUserTimezone, ref StringBuilder auditOutXML)
        {
            string stmt = "";
            DataTable dt = null;
            DataSet ds = null;
            vrmUser LastUser = null;
            int LastModifiedUserId = 0;
            string LastModifiedDate = "", Description = "", ModifiedDetails = "";
            auditOutXML = new StringBuilder();
            try
            {
                if (m_rptLayer == null)
                    m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);

                stmt = " Select Top(4) *, dbo.changeTime(" + LoginUserTimezone + ", LastModifiedDate) AS LastModifiedDateTime from Audit_Summary_UsrList_D  where UserId = " + userId + " order by Lastmodifieddate desc ";

                ds = m_rptLayer.ExecuteDataSet(stmt.ToString());
                if (ds != null)
                    dt = ds.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[0]["LastModifiedUser"] != null)
                        int.TryParse(dt.Rows[i]["LastModifiedUser"].ToString(), out LastModifiedUserId);

                    if (dt.Rows[0]["LastModifiedDateTime"] != null)
                        LastModifiedDate = dt.Rows[i]["LastModifiedDateTime"].ToString();

                    if (dt.Rows[i]["ModifiedDetails"] != null)
                        ModifiedDetails = dt.Rows[i]["ModifiedDetails"].ToString();

                    if (dt.Rows[0]["Description"] != null)
                        Description = dt.Rows[i]["Description"].ToString();

                    LastUser = m_IuserDao.GetByUserId(LastModifiedUserId);
                    auditOutXML.Append("<LastModified>");
                    auditOutXML.Append("<ModifiedUserId>" + LastModifiedUserId + "</ModifiedUserId>");
                    auditOutXML.Append("<ModifiedUserName>" + LastUser.FirstName + " " + LastUser.LastName + "</ModifiedUserName>");
                    auditOutXML.Append("<ModifiedDateTime>" + LastModifiedDate + "</ModifiedDateTime>");
                    auditOutXML.Append("<ModifiedDetails>" + ModifiedDetails + "</ModifiedDetails>");
                    auditOutXML.Append("<Description>" + Description + "</Description>");
                    auditOutXML.Append("</LastModified>");
                }
            }
            catch (Exception ex)
            {
                m_log.Error("GetAuditUser Failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        //ZD 100664 End
        //ZD 100621 start
        #region SetUserRoomViewType
        /// <summary>
        /// SetUserRoomViewType
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUserRoomViewType(ref vrmDataObject obj)
        {
            XmlDocument xd = null;
            XmlNode node = null;
            int usrID = 0, OrgID = 0;
            vrmUser usr = null;
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                node = xd.SelectSingleNode("//SetUserRoomViewType/userID");
                if (node != null)
                    int.TryParse(node.InnerText, out usrID);

                try
                {
                    usr = m_IuserDao.GetByUserId(usrID);
                }
                catch (Exception ex)
                {

                    obj.outXml = "<error><message>User does not exist</message></error>"; //205
                    return false;
                }

                node = xd.SelectSingleNode("//SetUserRoomViewType/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText, out OrgID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(OrgID);

                int RoomViewType = 1;
                node = xd.SelectSingleNode("//SetUserRoomViewType/RoomViewType");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out RoomViewType);
                    usr.RoomViewType = RoomViewType;
                }
                //ZD 101175 Start
                int RoomRecordsView = 20;
                node = xd.SelectSingleNode("//SetUserRoomViewType/RoomRecordsView");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out RoomRecordsView);
                    usr.RoomRecordsView = RoomRecordsView;
                }
                //ZD 101175 End
                m_IuserDao.SaveOrUpdate(usr);


                obj.outXml = "<success>1</success>";
                return true;

            }
            catch (Exception e)
            {
                m_log.Error("RoomViewType: ", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion

        #region GetUserRoomViewType
        /// <summary>
        /// GetUserRoomViewType
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetUserRoomViewType(ref vrmDataObject obj)
        {
            int usrID = 0, OrgID = 0;
            vrmUser usr = null;
            try
            {
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetUserRoomViewType/userID");

                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out usrID);

                        if (usrID < 0)
                        {
                            obj.outXml = "<error><message>User does not exist</message></error>"; //205
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//GetUserRoomViewType/organizationID");

                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out OrgID);

                        if (OrgID < 0)
                        {
                            obj.outXml = "<error><message>Org does not exist</message></error>"; //205
                            return false;
                        }

                    }
                }
                m_IuserDao.clearFetch();
                usr = m_IuserDao.GetByUserId(usrID);
                OUTXML = new StringBuilder();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetUserRoomViewType");
                    xWriter.WriteElementString("userID", usr.userid.ToString());
                    xWriter.WriteElementString("organizationID", OrgID.ToString());
                    xWriter.WriteElementString("RoomViewType", usr.RoomViewType.ToString());
                    xWriter.WriteElementString("RoomRecordsView", usr.RoomRecordsView.ToString());//ZD 101175
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                    obj.outXml = OUTXML.ToString();
                    return true;
                }

            }
            catch (Exception e)
            {
                m_log.Error("GetRoomViewType: ", e);
                obj.outXml = "";
                return false;
            }

        }
        #endregion;
        //ZD 100621 End

        //ZD 100815 Starts
        #region GetUserType
        /// <summary>
        /// GetUserType
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetUserType(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int userType = 0; //ZD 100815
                int userID = 0;
                node = xd.SelectSingleNode("//GetUserType/UserID");
                int.TryParse(node.InnerXml.Trim(), out userID);

                vrmBaseUser user = new vrmBaseUser();

                user = (vrmBaseUser)m_IGuestUserDao.GetByUserId(userID);
                if (user != null)
                    userType = vrmUserConstant.TYPE_GUEST;
                else
                {
                    user = (vrmBaseUser)m_IinactiveUserDao.GetByUserId(userID);
                    if (user != null)
                        userType = vrmUserConstant.TYPE_GUEST;
                    else
                        userType = vrmUserConstant.TYPE_USER;
                    //user = (vrmBaseUser)m_IuserDao.GetByUserId(userID);
                }

                obj.outXml = "<GetUserType>";
                obj.outXml += "<UserType>" + userType.ToString() + "</UserType>";
                obj.outXml += "</GetUserType>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion
        //ZD 100815 Ends

        //ZD 101443 Starts
        #region SearchUserInfo
        /// <summary>
        /// SearchUserInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchUserInfo(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userId = 0, organizationID = 0, SearchUserID = 0, SelectedOption = 0;
            StringBuilder searchOutXml = new StringBuilder();
            List<vrmRoom> Roomlist = null;
            List<vrmMCU> MCUlist = null;
            List<vrmConference> ConfList = null;
            List<InventoryCategory> Invlist = null;
            List<WorkOrder> Wolist = null;
            List<vrmMCUVendor> vendorList = null;
            vrmUser Vuser = new vrmUser();
            vrmRoom Room = null;
            vrmMCU MCU = null;
            InventoryCategory inventory = null;
            vrmConference conf = null;
            vrmOrganization vrmOrg = null;
            WorkOrder workorder = null;
            long ttlRecords = 0;
            int PageNo = 0, ttlPages = 0;
            int SortBy = 0, SortingOrder = 0;//ZD 101525
            string role = "Administrator"; //ALLDEV-807
            try
            {
                #region Reading XML
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//UserSearch/UserID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out userId);
                        else
                        {
                            myvrmEx = new myVRMException(201);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        if ((userId <= 0))
                        {
                            myvrmEx = new myVRMException(422);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//UserSearch/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            myvrmEx = new myVRMException(423);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//UserSearch/SearchUserID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out SearchUserID);

                        xNode = xNavigator.SelectSingleNode("//UserSearch/SelectedOption");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out SelectedOption);

                        xNode = xNavigator.SelectSingleNode("//UserSearch/PageNo");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out PageNo);
                        //ZD 101525
                        xNode = xNavigator.SelectSingleNode("//UserSearch/SortBy");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out SortBy);

                        xNode = xNavigator.SelectSingleNode("//UserSearch/SortingOrder");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out SortingOrder);
                    }
                }
                #endregion

                #region Filter Type

                switch (SelectedOption)
                {
                    case (int)UserFilterType.Conferences:
                        Conference m_ConfFactory = new Conference(ref obj);
                        ConfList = new List<vrmConference>();
                        m_ConfFactory.FetchUserAssignedConferences(organizationID, SearchUserID, ref ConfList);
                        ttlRecords = ConfList.Count;
                        ConfList = ConfList.Skip(m_iMaxUsrRecords * (PageNo - 1)).Take(m_iMaxUsrRecords).ToList(); //ZD 101525
                        //ZD 101525
                        #region Sorting

                        switch (SortBy)
                        {
                            case vrmSortBy.UniqueID:
                                if (SortingOrder == 0)
                                    ConfList = ConfList.OrderBy(cnf => cnf.confnumname).ToList();
                                else
                                    ConfList = ConfList.OrderByDescending(cnf => cnf.confnumname).ToList();
                                break;
                            case vrmSortBy.ConfName:
                                if (SortingOrder == 0)
                                    ConfList = ConfList.OrderBy(cnf => cnf.externalname).ToList();
                                else
                                    ConfList = ConfList.OrderByDescending(cnf => cnf.externalname).ToList();
                                break;
                            case vrmSortBy.SiloName:
                                if (SortingOrder == 0)
                                    ConfList = ConfList.OrderBy(cnf => cnf.orgId).ToList();
                                else
                                    ConfList = ConfList.OrderByDescending(cnf => cnf.orgId).ToList();
                                break;
                            case vrmSortBy.ConfDate:
                                if (SortingOrder == 0)
                                    ConfList = ConfList.OrderBy(cnf => cnf.confdate).ToList();
                                else
                                    ConfList = ConfList.OrderByDescending(cnf => cnf.confdate).ToList();
                                break;
                        }
                        #endregion

                        break;
                    case (int)UserFilterType.Rooms:
                        {
                            RoomFactory m_RoomFactory = new RoomFactory(ref obj);
                            Roomlist = new List<vrmRoom>();
                            m_RoomFactory.FetchUserAssignedRooms(organizationID, SearchUserID, ref Roomlist);
                            ttlRecords = Roomlist.Count;
                            Roomlist = Roomlist.Skip(m_iMaxUsrRecords * (PageNo - 1)).Take(m_iMaxUsrRecords).ToList(); //ZD 101525
                        }
                        break;
                    case (int)UserFilterType.MCUs:
                        MCUlist = new List<vrmMCU>();
                        m_HardwareFactory.FetchUserAssignedMCUs(organizationID, SearchUserID, ref MCUlist);
                        ttlRecords = MCUlist.Count;
                        MCUlist = MCUlist.Skip(m_iMaxUsrRecords * (PageNo - 1)).Take(m_iMaxUsrRecords).ToList(); //ZD 101525
                        break;
                    case (int)UserFilterType.AV:
                    case (int)UserFilterType.Menu:
                    case (int)UserFilterType.Facilities:
                        Invlist = new List<InventoryCategory>();
                        m_WorkOrderFactory.FetchUserAssignedInventory(organizationID, SearchUserID, SelectedOption, ref Invlist);
                        ttlRecords = Invlist.Count;
                        Invlist = Invlist.Skip(m_iMaxUsrRecords * (PageNo - 1)).Take(m_iMaxUsrRecords).ToList(); //ZD 101525
                        break;
                    case (int)UserFilterType.AVWO:
                    case (int)UserFilterType.MenuWO:
                    case (int)UserFilterType.FacilitiesWO:
                        Wolist = new List<WorkOrder>();
                        m_WorkOrderFactory.FetchUserAssignedWorkorder(organizationID, SearchUserID, SelectedOption, ref Wolist);
                        ttlRecords = Wolist.Count;
                        Wolist = Wolist.Skip(m_iMaxUsrRecords * (PageNo - 1)).Take(m_iMaxUsrRecords).ToList(); //ZD 101525
                        break;
                    default:
                        break;
                }
                #endregion



                int utcEnabled = 0;
                DateTime setupTime = DateTime.UtcNow;
                DateTime tearDownTime = DateTime.UtcNow;
                DateTime confDateTime = DateTime.UtcNow;
                DateTime todayNow = DateTime.UtcNow;
                DateTime completeBy = DateTime.Now;
                int woTimeZone = 26;
                vrmUser user = null;
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(searchOutXml, xSettings))
                {
                    xWriter.WriteStartElement("UserSearch");

                    Vuser = m_IuserDao.GetByUserId(SearchUserID);
                    if (Vuser != null)
                        xWriter.WriteElementString("UserName", Vuser.FirstName + " " + Vuser.LastName);
                    else
                        xWriter.WriteElementString("UserName", "");

                    ttlPages = (int)(ttlRecords / m_iMaxUsrRecords);//ZD 101525

                    if (ttlRecords % m_iMaxUsrRecords > 0)
                        ttlPages++;

                    #region Conference Part
                    xWriter.WriteStartElement("AssignedConferences");
                    if (ConfList != null && ConfList.Count > 0)
                    {
                        vrmOrg = m_IOrgDAO.GetById(organizationID);
                        int confid = 0;
                        for (int c = 0; c < ConfList.Count; c++)
                        {
                            conf = ConfList[c];
                            confDateTime = conf.MCUPreStart;

                            if (utcEnabled == 0)
                                timeZone.userPreferedTime(Vuser.TimeZone, ref confDateTime);

                            xWriter.WriteStartElement("Conference");
                            xWriter.WriteElementString("organizationName", vrmOrg.orgname);
                            xWriter.WriteElementString("ConferenceID", conf.confid.ToString() + "," + conf.instanceid.ToString());
                            xWriter.WriteElementString("ConfereceUniqueId", conf.confnumname.ToString());
                            xWriter.WriteElementString("Id", conf.confid.ToString());
                            xWriter.WriteElementString("IsRecur", conf.recuring.ToString());
                            if (conf.recuring == 1)
                            {
                                if (confid == conf.confid)
                                    xWriter.WriteElementString("IsRecurInstance", "1");
                                else
                                    xWriter.WriteElementString("IsRecurInstance", "0");
                                confid = conf.confid;
                            }
                            else
                                xWriter.WriteElementString("IsRecurInstance", "0");
                            xWriter.WriteElementString("ConferenceName", conf.externalname);
                            xWriter.WriteElementString("ConferenceType", conf.conftype.ToString());
                            xWriter.WriteElementString("ConferenceDateTime", confDateTime.ToString("g"));

                            int totalDuration = conf.duration + conf.McuSetupTime - conf.MCUTeardonwnTime; //ZD 100085

                            xWriter.WriteElementString("ConferenceDuration", totalDuration.ToString());
                            xWriter.WriteElementString("ConferenceStatus", conf.status.ToString());
                            xWriter.WriteElementString("isOBTP", conf.isOBTP.ToString());
                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteFullEndElement();
                    #endregion

                    #region Room Part
                    xWriter.WriteStartElement("AssignedRooms");
                    if (Roomlist != null && Roomlist.Count > 0)
                    {
                        for (int r = 0; r < Roomlist.Count; r++)
                        {
                            Room = Roomlist[r];
                            xWriter.WriteStartElement("Room");
                            xWriter.WriteElementString("Id", Room.roomId.ToString());
                            xWriter.WriteElementString("Tier1", Room.tier2.TopTierName); //ZD 102481
                            xWriter.WriteElementString("Tier2", Room.tier2.Name);
                            xWriter.WriteElementString("RoomName", Room.Name);
                            //ALLDEV-807 Starts
                            role = "Approver";
                            for (int x = 0; x < Room.locationAssistant.Count; x++)
                            {
                                if (Room.locationAssistant[x].AssistantId == SearchUserID)
                                {
                                    role = "Administrator";
                                    break;
                                }
                            }
                            xWriter.WriteElementString("Role", role );//Room.assistant == SearchUserID ? "Administrator" : "Approver");
                            //ALLDEV-807 Ends
                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteFullEndElement();
                    #endregion

                    #region MCU Part
                    xWriter.WriteStartElement("AssignedMCUs");
                    if (MCUlist != null && MCUlist.Count > 0)
                    {
                        for (int m = 0; m < MCUlist.Count; m++)
                        {
                            MCU = MCUlist[m];
                            xWriter.WriteStartElement("MCU");
                            xWriter.WriteElementString("Id", MCU.BridgeID.ToString());
                            xWriter.WriteElementString("MCUName", MCU.BridgeName);

                            vendorList = vrmGen.getMCUVendor();
                            vendorList = vendorList.Where(v => v.id == MCU.MCUType.id).ToList();
                            xWriter.WriteElementString("InterfaceType", vendorList[0].name);

                            xWriter.WriteElementString("Status", MCU.Status == 1 ? "Active" : MCU.Status == 2 ? "Maintanance" : "Disabled");
                            xWriter.WriteElementString("Role", MCU.Admin == SearchUserID ? "Administrator" : "Approver");
                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteFullEndElement();
                    #endregion

                    #region Inventory Part
                    xWriter.WriteStartElement("AssignedInventories");
                    if (Invlist != null && Invlist.Count > 0)
                    {
                        for (int i = 0; i < Invlist.Count; i++)
                        {
                            inventory = Invlist[i];
                            xWriter.WriteStartElement("Inventory");
                            xWriter.WriteElementString("Id", inventory.ID.ToString());
                            xWriter.WriteElementString("IName", inventory.Name);
                            xWriter.WriteElementString("Role", "Admin-in-charge");

                            vrmUser usr = new vrmUser();
                            usr = m_IuserDao.GetByUserId(inventory.AdminID);
                            xWriter.WriteElementString("Incharge", usr.FirstName + " " + usr.LastName);
                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteFullEndElement();
                    #endregion

                    #region Work Order Part
                    xWriter.WriteStartElement("AssignedWorkOrder");
                    if (Wolist != null && Wolist.Count > 0)
                    {
                        for (int i = 0; i < Wolist.Count; i++)
                        {
                            workorder = Wolist[i];
                            xWriter.WriteStartElement("WorkOrder");
                            xWriter.WriteElementString("Id", workorder.ID.ToString());

                            woTimeZone = Vuser.TimeZone;
                            completeBy = workorder.CompletedBy;
                            timeZone.userPreferedTime(woTimeZone, ref completeBy);

                            xWriter.WriteElementString("EndDate", completeBy.ToString());
                            xWriter.WriteElementString("IName", workorder.Name);
                            xWriter.WriteElementString("Role", "Person-in-charge");

                            vrmUser usr = new vrmUser();
                            usr = m_IuserDao.GetByUserId(workorder.AdminID);
                            xWriter.WriteElementString("Incharge", usr.FirstName + " " + usr.LastName);
                            xWriter.WriteFullEndElement();
                        }
                    }
                    xWriter.WriteFullEndElement();
                    #endregion

                    //ZD 101525
                    xWriter.WriteElementString("PageNo", PageNo.ToString());
                    xWriter.WriteElementString("TotalPages", ttlPages.ToString());
                    xWriter.WriteElementString("TotalRecords", ttlRecords.ToString());
                    xWriter.WriteElementString("SortBy", SortBy.ToString());
                    xWriter.WriteElementString("SortingOrder", SortingOrder.ToString());

                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                    obj.outXml = searchOutXml.ToString();
                }

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region SetAssignedUserAdmin
        /// <summary>
        /// SetAssignedUserAdmin
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetAssignedUserAdmin(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userId = 0, organizationID = 0, NewUserID = 0, SelectedOption = 0, ExistingUserID = 0, SelectAll = 0, isDeleted = 0, erroNo = 740;
            StringBuilder searchOutXml = new StringBuilder();
            vrmUser Vuser = new vrmUser();
            XPathNavigator selectNode = null;
            try
            {
                #region Reading XML
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/UserID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out userId);
                        else
                        {
                            myvrmEx = new myVRMException(201);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        if ((userId <= 0))
                        {
                            myvrmEx = new myVRMException(422);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 0)
                        {
                            myvrmEx = new myVRMException(423);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/ExistingUserID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out ExistingUserID);

                        xNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/NewUserID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out NewUserID);

                        xNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/SelectedOption");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out SelectedOption);

                        xNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/IsDeleted");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out isDeleted);

                        selectNode = xNavigator.SelectSingleNode("//SetAssignedUserAdmin/SelectedID");
                    }
                }
                #endregion

                #region Filter Type

                switch (SelectedOption)
                {
                    case (int)UserFilterType.Conferences:
                        Conference m_ConfFactory = new Conference(ref obj);
                        if (!m_ConfFactory.UpdateUserAssignedConferences(isDeleted, organizationID, ExistingUserID, SelectAll, NewUserID, selectNode, obj.inXml))
                        {
                            myvrmEx = new myVRMException(740);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        break;
                    case (int)UserFilterType.Rooms:
                        {
                            RoomFactory m_RoomFactory = new RoomFactory(ref obj);
                            if (!m_RoomFactory.UpdateUserAssignedRooms(organizationID, ExistingUserID, SelectAll, NewUserID, selectNode, obj.inXml, ref erroNo))
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        break;
                    case (int)UserFilterType.MCUs:
                        {
                            if (!m_HardwareFactory.UpdateUserAssignedMCUs(organizationID, ExistingUserID, SelectAll, NewUserID, selectNode, obj.inXml, ref erroNo))
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        break;
                    case (int)UserFilterType.AV:
                        if (selectNode != null)
                        {
                            if (!m_WorkOrderFactory.UpdateUserAssignedInventory(selectNode, obj.inXml, NewUserID, woConstant.CATEGORY_TYPE_AV, organizationID, ref erroNo)) //ZD 103364
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        break;
                    case (int)UserFilterType.Menu:
                        if (selectNode != null)
                        {
                            if (!m_WorkOrderFactory.UpdateUserAssignedInventory(selectNode, obj.inXml, NewUserID, woConstant.CATEGORY_TYPE_CA, organizationID,  ref erroNo)) //ZD 103364
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        break;
                    case (int)UserFilterType.Facilities:
                        if (selectNode != null)
                        {
                            if (!m_WorkOrderFactory.UpdateUserAssignedInventory(selectNode, obj.inXml, NewUserID, woConstant.CATEGORY_TYPE_HK, organizationID, ref erroNo)) //ZD 103364 
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        break;
                    case (int)UserFilterType.AVWO:
                        if (selectNode != null)
                        {
                            if (!m_WorkOrderFactory.UpdateUserAssignedWorkorder(selectNode, obj.inXml, NewUserID, woConstant.CATEGORY_TYPE_AV, organizationID, ref erroNo)) //ZD 103364
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        break;
                    case (int)UserFilterType.MenuWO:
                        if (selectNode != null)
                        {
                            if (!m_WorkOrderFactory.UpdateUserAssignedWorkorder(selectNode, obj.inXml, NewUserID, woConstant.CATEGORY_TYPE_CA, organizationID, ref erroNo)) //ZD 103364
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        break;
                    case (int)UserFilterType.FacilitiesWO:
                        if (selectNode != null)
                        {
                            if (!m_WorkOrderFactory.UpdateUserAssignedWorkorder(selectNode, obj.inXml, NewUserID, woConstant.CATEGORY_TYPE_HK, organizationID, ref erroNo)) //ZD 103364
                            {
                                myvrmEx = new myVRMException(erroNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        break;
                    default:
                        break;
                }
                #endregion

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //ZD 101443 Ends

        //ZD 101525
        public string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\'", "&apos;;");
            return (input);
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        public string RevertBackTheInvalidChars(string input)
        {
            input = input.Replace("&lt;", "<");
            input = input.Replace("&gt;", ">");
            input = input.Replace("&amp;", "&");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&apos;", "\'");
            return (input);
        }

        #region CheckIsLDAPUser
        public bool CheckIsLDAPUser(ref List<string> groupArray, ref string lastName, ref string firstName, ref string email, ref string Login, ref string domain)//101812 //ZD 104089
        {
            string prntNode = "", userADDomain = "";//101812 //ZD 103244
            try
            {
                DirectoryEntry de;
                List<vrmLDAPUser> userList = new List<vrmLDAPUser>();
                List<ICriterion> criterionList = new List<ICriterion>();
                SearchResult srchrst = null;
                //Login = "Tamil";
                vrmLDAPConfig ldapSettings = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", 11));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapSettings = ldapSettingsList[0];

                if (ldapSettings != null && ldapSettings.EnableSSOMode == 0)
                    return false;
                //101812
                domain = domain + "\\";

                m_log.Error("Domain of the user " + domain + " : System Ldap domain " + ldapSettings.domainPrefix.ToLower().Trim());

                if (ldapSettings.domainPrefix.ToLower().Trim() != domain.ToLower().Trim() && ldapSettings.TenantOption == 2)//101812
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("domainPrefix", domain.Trim()));
                    ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    {
                        ldapSettings = ldapSettingsList[0];
                        organizationID = ldapSettings.OrgId;
                    }
                    else
                    {
                        m_log.Error("NO domain found, please check config " + domain);
                        return false;
                    }
                }

                de = new DirectoryEntry();
                de.Username = ldapSettings.domainPrefix + ldapSettings.login;//"myvrm\\jvideo";
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapSettings.serverPassword = crypto.decrypt(ldapSettings.password);
                de.Password = ldapSettings.serverPassword;
                de.Path = "LDAP://" + ldapSettings.serveraddress + ":" + ldapSettings.port;//ZD 103187 
                if (ldapSettings.TenantOption == 4 || ldapSettings.TenantOption == 5)//102732 //ZD 103187 
                {
                    de.Path = "GC://" + ldapSettings.serveraddress + ":" + ldapSettings.port;//ZD 103187 
                }
                m_log.Error("Path " + de.Path);
                m_log.Error("username " + de.Username);

                switch (ldapSettings.AuthType)
                {
                    case 0:
                        de.AuthenticationType = AuthenticationTypes.None;
                        break;
                    case 1:
                        de.AuthenticationType = AuthenticationTypes.Signing;
                        break;
                    case 2:
                        de.AuthenticationType = AuthenticationTypes.Sealing;
                        break;
                    case 3:
                        de.AuthenticationType = AuthenticationTypes.Secure;
                        break;
                    case 4:
                        de.AuthenticationType = AuthenticationTypes.Anonymous;
                        break;
                    case 5:
                        de.AuthenticationType = AuthenticationTypes.Delegation;
                        break;
                    case 6:
                        de.AuthenticationType = AuthenticationTypes.Encryption;
                        break;
                    case 7:
                        de.AuthenticationType = AuthenticationTypes.FastBind;
                        break;
                    case 8:
                        de.AuthenticationType = AuthenticationTypes.ReadonlyServer;
                        break;
                    case 9:
                        de.AuthenticationType = AuthenticationTypes.SecureSocketsLayer;
                        break;
                    case 10:
                        de.AuthenticationType = AuthenticationTypes.ServerBind;
                        break;
                    default:
                        de.AuthenticationType = AuthenticationTypes.None;
                        break;
                } //ZD 102999 //ZD 102999

                DirectorySearcher dsr = new DirectorySearcher(de);
                //dsr.Filter = ("(samaccountname=" + Login + ")");
                //ZD 104089 Start
                if (!string.IsNullOrEmpty(Login))
                    dsr.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + Login + "))";
                else if (!string.IsNullOrEmpty(email))
                    dsr.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(mail=" + email + "))";
                //ZD 104089 End
                dsr.SearchScope = System.DirectoryServices.SearchScope.Subtree;
                dsr.ServerTimeLimit = TimeSpan.FromSeconds(90);
                SearchResultCollection objSrchResults = dsr.FindAll();
                m_log.Error("Search User name of the user " + Login);
                prntNode = ldapSettings.domainPrefix.ToLower().Replace("\\", "");
                string UserDaominValue = domain.Replace(@"\", ""); //ZD 103244
                string[] DomainDelimeter = { ",DC=" }; //ZD 103244
                string[] Domain; //ZD 103244
                if (objSrchResults != null)
                {
                    if (objSrchResults.Count > 0)
                    {
                        //ZD 103244 Starts
                        for (int srchcnt = 0; srchcnt < objSrchResults.Count; srchcnt++)
                        {
                            m_log.Error("Searched User's count " + objSrchResults.Count);
                            srchrst = objSrchResults[srchcnt];

                            try
                            {
                                //CN=Oconnell\, Matthew G. -ND,OU=Users,OU=WDI,OU=FLOR,OU=DMTP,DC=wdw,DC=disney,DC=com
                                userADDomain = srchrst.GetDirectoryEntry().Properties["distinguishedname"].Value.ToString();
                                Domain = userADDomain.Split(DomainDelimeter, StringSplitOptions.None);
                                if (Domain != null && Domain.Length > 0)
                                {
                                    userADDomain = Domain[1];
                                }
                            }
                            catch (Exception ex)
                            {
                                m_log.Error("CheckIsLDAPUser: Exception in distinguishedname property.", ex);
                                continue;
                            }

                            m_log.Error("Searched User's distinguishedname -  " + srchrst.GetDirectoryEntry().Properties["distinguishedname"].Value.ToString());
                            m_log.Error("User' Domain -  " + userADDomain);

                            if (UserDaominValue.ToLower().Contains(userADDomain.ToLower()))//ZD 104848 //ALLOPS-37
                            {
                                m_log.Error("Searched User's distinguishedname -  " + userADDomain + " -  " + UserDaominValue);
                                try
                                {
                                    groupArray = srchrst.Properties["memberOf"].Cast<string>().ToArray().Select(grp => grp.Split(',')[0].Remove(0, 3)).ToList();
                                }
                                catch (Exception ex)
                                {
                                    m_log.Error("CheckIsLDAPUser: Exception in memberOf property.", ex);
                                }
                                try
                                {
                                    lastName = srchrst.Properties["SN"][0].ToString();
                                }
                                catch (Exception ex)
                                {
                                    m_log.Error("CheckIsLDAPUser: Exception in SN property.", ex);
                                }
                                try
                                {
                                    firstName = srchrst.Properties["Givenname"][0].ToString();
                                }
                                catch (Exception ex)
                                {
                                    m_log.Error("CheckIsLDAPUser: Exception in Givenname property.", ex);

                                }
                                try
                                {
                                    email = srchrst.Properties["mail"][0].ToString();
                                    //ZD 103635 Starts
                                    System.Net.Mail.MailAddress addresstoCheck = new System.Net.Mail.MailAddress(email);
                                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                    Match isMatch = regex.Match(email);
                                    if (!isMatch.Success)
                                        return false;
                                    //if (email.IndexOf("@") < 0)//Not valid Email Address
                                    //    return false;
                                    //ZD 103635 End
                                }
                                catch (Exception ex)
                                {
                                    m_log.Error("CheckIsLDAPUser: Exception in mail property.", ex);
                                    return false;//ZD 103635
                                }
                                //ZD 104089 Start
                                try
                                {
                                    Login = srchrst.Properties["samaccountname"][0].ToString();
                                }
                                catch (Exception ex)
                                {
                                    m_log.Error("CheckIsLDAPUser: Exception in samaccountname property.", ex);
                                }
                                //ZD 104089 End
                                break;
                            }
                            //ZD 103244 Ends
                        }
                        domain = UserDaominValue; //ZD 103784 //ALLOPS-37
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //ZD 101308 Start
        #region ChkADAuthentication
        public bool ChkADAuthentication(ref vrmDataObject obj)
        {
            string serveraddress = "", domain = "";
            int port = 0;
            try
            {
                m_ILDAPConfigDAO.clearFetch();
                List<ICriterion> cList = new List<ICriterion>();
                cList.Add(Expression.Eq("OrgId", 11));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(cList, true);

                if (ldapSettingsList != null && ldapSettingsList.Count > 0 && ldapSettingsList[0] != null)
                {
                    serveraddress = ldapSettingsList[0].serveraddress;
                    port = ldapSettingsList[0].port;
                    domain = ldapSettingsList[0].domainPrefix;
                }

                obj.outXml = "<ChkADAuthentication>";
                obj.outXml += "<LDAPServerAddress>" + serveraddress + "</LDAPServerAddress>";
                obj.outXml += "<LDAPPort>" + port + "</LDAPPort>";
                obj.outXml += "<LDAPDomain>" + domain + "</LDAPDomain>";
                obj.outXml += "</ChkADAuthentication>";

                return true;

            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion
        //ZD 101308 End
        //ZD 103878
        #region SetDeletedUserDetails
        public void SetDeletedUserDetails(ref vrmUser baseUser, ref vrmUserDeleteList usrDelList)
        {
            try
            {
                baseUser.id = usrDelList.id;
                baseUser.userid = usrDelList.userid;
                baseUser.FirstName = usrDelList.FirstName;
                baseUser.LastName = usrDelList.LastName;
                baseUser.Email = usrDelList.Email;
                baseUser.TimeZone = usrDelList.TimeZone;
                baseUser.roleID = usrDelList.roleID;
                baseUser.Language = usrDelList.Language;
                baseUser.companyId = usrDelList.companyId;
                baseUser.DateFormat = usrDelList.DateFormat;
                baseUser.TimeFormat = usrDelList.TimeFormat;
                baseUser.WorkPhone = usrDelList.WorkPhone;
                baseUser.CellPhone = usrDelList.CellPhone;
                baseUser.Login = usrDelList.Login;
                baseUser.EmailLangId = usrDelList.EmailLangId;
                baseUser.MailBlocked = usrDelList.MailBlocked;
                baseUser.enableWebexUser = usrDelList.enableWebexUser;
            }
            catch (Exception e)
            {
                m_log.Error("systemException", e);
            }
        }
        #endregion
    
    // Method added for ZD 103954 Start
        #region SetExpirePassword
        /// <summary>
        /// SetExpirePassword
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
    public bool SetExpirePassword(ref vrmDataObject obj)
    {
        bool bRet = true;
        vrmUser requestor = null;
        string encryptpassword = "", userEmail = "", oldpassword = "", newpassword = "",oldpassword1 = "", dbpassword = "";
        try
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(obj.inXml);
            XmlNode node;
            node = xd.SelectSingleNode("//SetExpirePassword/PasswordReset/email");
            if (node != null)
            {
                if (node.InnerXml.Trim() != "")
                userEmail = node.InnerXml.Trim();
            }
           cryptography.Crypto decryp = new cryptography.Crypto();
           requestor = m_IuserDao.GetByUserEmail(userEmail);
           if (requestor != null)
           {
            oldpassword1 = requestor.Password;
            dbpassword = decryp.decrypt(oldpassword1);
            node = xd.SelectSingleNode("//SetExpirePassword/PasswordReset/oldpassword");
            if (node != null)
            {
                if (node.InnerXml.Trim() != "")
                  oldpassword = node.InnerXml.Trim();
                if (oldpassword != dbpassword)
                {
                   myVRMException e = new myVRMException(758);
                   obj.outXml = e.FetchErrorMsg();
                   return false;
                }
            }
            node = xd.SelectSingleNode("//SetExpirePassword/PasswordReset/newpassword");
            if (node != null)
            {
                if (node.InnerXml.Trim() != "")
                    newpassword = node.InnerXml.Trim();
            }
            if (userEmail == "" || newpassword == "")
                return false;

                cryptography.Crypto crypto = new cryptography.Crypto();
                encryptpassword = crypto.encrypt(newpassword);
                requestor.Password = encryptpassword;
                requestor.PasswordTime = DateTime.UtcNow;
                requestor.LockStatus = vrmUserStatus.USER_ACTIVE;
                m_IuserDao.SaveOrUpdate(requestor);
            }
            else
            {
                obj.outXml = "<error></error>";
                return false;
            }
        }
        catch (Exception e)
        {
            m_log.Error("sytemException", e);
            bRet = false;
        }
        return bRet;
    }
    #endregion
    // ZD 103954 End

        //ZD 104862 start
        #region SetUsrBlockStatusFrmDataimport
        /// <summary>
        /// SetUsrBlockStatusFrmDataimport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetUsrBlockStatusFrmDataimport(ref vrmDataObject obj)
        {
            int userID = 0;
            string userlist = "", stmt ="";
            m_rptLayer = new ns_SqlHelper.SqlHelper(m_configPath);
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                node = xd.SelectSingleNode("//SetBlockUserFromDI/UserID");
                if(node != null)
                   int.TryParse(node.InnerXml.Trim(), out userID);
                node = xd.SelectSingleNode("//SetBlockUserFromDI/Userlist");
                if (node != null)
                   userlist = node.InnerXml.Trim();
                if (!string.IsNullOrEmpty(userlist))
                {
                    stmt = "Update Usr_List_D set lockCntTrns = 1 , lockstatus = '" + vrmUserStatus.DATAIMPORT_LOCKED + "', LastModifiedDate = '" + DateTime.Now + "',";
                    stmt += "LastModifiedUser = '" + userID + "' where UserID  not in  (" + userlist + ") and  LockStatus = 0 and Audioaddon = 0 and UserID <> 11 ";
                    m_rptLayer.OpenConnection();
                    m_rptLayer.ExecuteNonQuery(stmt);
                    m_rptLayer.CloseConnection();
                    obj.outXml = "<success> Operation succesful </success>";
                }

                return true;
            }
            catch(Exception ex)
            {
               if (m_rptLayer != null)
                    m_rptLayer.CloseConnection();
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                return false;
            }
            
        }
        #endregion

        //ZD 104862 End
        #region DeleteUserAssignedDetails
        public bool DeleteUserAssignedDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            String strSQL = "";
            Int32 strExec = -1;
            ns_SqlHelper.SqlHelper sqlCon = null;
            string delUserID = "";
            string selAdminID = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeleteUserAssignedDetails/DelUserID");
                delUserID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeleteUserAssignedDetails/SelAdminID");
                selAdminID = node.InnerXml.Trim();

                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                sqlCon.OpenConnection();
                sqlCon.OpenTransaction();
               
                strSQL = "update Mcu_List_D set Admin=" + selAdminID + " where Admin = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);
                
                strSQL = "update Loc_Assistant_D set AssistantId =" + selAdminID + " where AssistantId = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "UPDATE Loc_Assistant_D SET Loc_Assistant_D.EmailId = b.Email, Loc_Assistant_D.AssitantName = b.FirstName + ' ' + b.LastName";
                strSQL += " FROM Loc_Assistant_D a ,Usr_List_D b where a.AssistantId = b.UserID and a.AssistantId = " + selAdminID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete from Loc_Assistant_D where ID in(";
                strSQL += " select ID from (select Max(ID) as ID,roomid,AssistantId from Loc_Assistant_D group by roomid,AssistantId having count(*) >1) as X)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Grp_Participant_D where UserID = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Grp_Participant_D where GroupID in (select GroupID from Grp_Detail_D where Owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Grp_Detail_D where Owner = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Tmp_Room_D where TmpID in (select TmpID from Tmp_List_D where TmpOwner = " + delUserID + " and TmpPublic = 0)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Tmp_Group_D where TmpID in (select TmpID from Tmp_List_D where TmpOwner = " + delUserID + " and TmpPublic = 0)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Tmp_Participant_D where TmpID in (select TmpID from Tmp_List_D where TmpOwner = " + delUserID + " and TmpPublic = 0)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Tmp_AdvAVParams_D where TmpID in (select TmpID from Tmp_List_D where TmpOwner = " + delUserID + " and TmpPublic = 0)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete Tmp_List_D where TmpOwner = " + delUserID + " and TmpPublic = 0";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "update Tmp_List_D set TmpOwner = " + selAdminID + " where TmpOwner = " + delUserID + " and TmpPublic = 1";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete Tmp_Participant_D where UserID = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);
                //Room Approver with pending confernece, inserting MCU Approver 
                strSQL = "insert into Conf_Approval_D (confid , instanceid, entitytype, entityid, approverid, decision, responsetimestamp) ";
                strSQL += " select a.confid, a.instanceid, 2, 1, c.approverid,0,getdate() from conf_room_d a, Loc_Approver_D b, Mcu_Approver_D c  ";
                strSQL += " where a.RoomID = b.roomid and a.bridgeid = c.mcuid ";
                strSQL += " and confid in (select confid from conf_approval_d where approverid = " + delUserID + " and entitytype = 1 and decision = 0)";
                strSQL += " and ConfID not in (select confid from conf_approval_d where approverid not in (" + delUserID + ") and entitytype = 1 and decision = 0)";

                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = " delete from conf_approval_d where approverid = " + delUserID + " and entitytype = 1 and decision = 0 ";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete Loc_Approver_D where approverid = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                //MCU Approver with pending confernece
                strSQL = "insert into Conf_Approval_D (confid , instanceid, entitytype, entityid, approverid, decision, responsetimestamp) ";
                strSQL += " select a.confid, a.instanceid, 4, 1, s.approverid,0,getdate() from Conf_Conference_D a, Sys_Approver_D s  where  a.orgId = s.orgId and confid in ";
                strSQL += " (select confid from conf_approval_d where approverid = " + delUserID + " and entitytype = 2 and decision = 0)";
                strSQL += " and ConfID not in (select confid from conf_approval_d where approverid not in (" + delUserID + ") and entitytype = 2 and decision = 0)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = " delete from conf_approval_d where approverid = " + delUserID + " and entitytype = 2 and decision = 0";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from MCU_Approver_D where approverid = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete from conf_approval_d where approverid = " + delUserID + " and entitytype = 4 and decision = 0";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Sys_Approver_D where approverid = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Conf_Approval_D where decision = 0 and entitytype = 4 and approverid = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "update Conf_Conference_D set status = 0 where status = 1 and cast(confid as varchar(10)) + cast(instanceid as varchar(10)) not in (select cast(confid as varchar(10)) + cast(instanceid as varchar(10)) from Conf_Approval_D)";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                //Conference
                strSQL = "Delete from Conf_Room_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_User_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from ConfOverbook_Approval_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_Approval_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_AdvAVParams_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner =" + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_Bridge_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";

                strSQL = "Delete from Conf_CustomAttr_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_RoomSplit_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = " Update Conf_RecurInfo_D set dirty=1 WHERE confid in (select confid from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = " Delete from Conf_Attachments_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner =" + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_Message_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner =" + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "Delete from Conf_VNOCOperator_D where cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() and owner = " + delUserID + ")";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Conf_Conference_D where confdate >=getdate() and owner = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "update Conf_Conference_D set userid = owner where userid = " + delUserID + " and owner != " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "delete Conf_User_D where userid = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "update Inv_Category_D set AdminID = " + selAdminID + " where AdminID = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "update Inv_WorkOrder_D set AdminID = " + selAdminID + " where AdminID = " + delUserID + " and cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) ";
                strSQL += " in (select cast(confid as varchar(10))+'_'+ cast(instanceid as varchar(10)) from  Conf_Conference_D where confdate >=getdate() )";
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                strSQL = "update Inv_Category_D set AdminID = " + selAdminID + "  where AdminID = " + delUserID;
                strExec = sqlCon.ExecuteNonQuery(strSQL);

                sqlCon.CommitTransaction();

                sqlCon.CloseConnection();

            }
            catch (myVRMException e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion

        //ALLDEV-856 Start
        public bool FetchGoogleDetails(ref vrmDataObject obj)
        {
            Tuple<int, string> usertp = null;
            StringBuilder _outXml = new StringBuilder ();
            cryptography.Crypto _crypto = new cryptography.Crypto();
            try
            {
                sysSettings _sys = new sysSettings(m_configPath);
                sysMailData _MailData = new sysMailData();
                _sys.getSysMail(ref _MailData);

                usertp = new Tuple<int, string>(0, sysSettings.AdminEmailaddress);

                vrmUserToken usr = new vrmUserToken();
                usr = m_IUserTokenDAO.GetByUserId(usertp);

                vrmUser usrG = new vrmUser();
                usrG = m_IuserDao.GetByUserId(usr.userid);

                _xSettings = new XmlWriterSettings();
                _xSettings.OmitXmlDeclaration = true;
                using (_xWriter = XmlWriter.Create(_outXml, _xSettings))
                {
                    _xWriter.WriteStartElement("GoogleInfo");
                    _xWriter.WriteElementString("GoogleClientID", _crypto.decrypt(sysSettings.GoogleClientID));
                    _xWriter.WriteElementString("GoogleSecretID", _crypto.decrypt(sysSettings.GoogleSecretID));
                    _xWriter.WriteElementString("GoogleAPIKey", sysSettings.GoogleAPIKey);
                    _xWriter.WriteElementString("PushtoGoogle", sysSettings.PushtoGoogle.ToString());
                    _xWriter.WriteElementString("PollCount", sysSettings.PollCount.ToString()); //ALLDEV-856
                    _xWriter.WriteElementString("GoogleIntegration", sysSettings.GoogleIntegration.ToString()); //ALLDEV-856
                    _xWriter.WriteElementString("AdminEmailaddress", sysSettings.AdminEmailaddress); //ALLDEV-856
                    if (usr != null)
                    {
                        _xWriter.WriteElementString("RefreshToken", _crypto.decrypt((usr.RefreshToken)));
                        _xWriter.WriteElementString("AccessToken", usr.AccessToken);
                        _xWriter.WriteElementString("ChanelExpiration", usr.ChannelExpiration.ToString());

                    }
                    else
                    {
                        _xWriter.WriteElementString("RefreshToken", "");
                        _xWriter.WriteElementString("AccessToken", "");
                        _xWriter.WriteElementString("ChanelExpiration", DateTime.Now.AddMinutes(-2).ToString());
                    }
                    _xWriter.WriteElementString("UserId", usr.userid.ToString());
                    _xWriter.WriteElementString("OrgID", usrG.companyId.ToString());
                    _xWriter.WriteElementString("usertimezone", usrG.TimeZone.ToString());//ALLDEV-816
                    _xWriter.WriteElementString("ChannelAddress", _MailData.websiteURL);
                    _xWriter.WriteFullEndElement();

                }
                obj.outXml = _outXml.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("FetchGoogleInfo", ex);
                return false;
            }
        }
        //ALLDEV-856 End
}
    //// ZD 103954 End
    #endregion
}